# kmsdmp

EasyCrypt specs and proofs for KMS DMP as presented in

Machine-Checked Proof of Security for AWS Key Management Service
  J. Almeida, M. Barbosa, G. Barthe, M. Campagna, E. Cohen,
  B. Gregoire, V. Pereira, B. Portela, P-Y. Strub, S. Tasiran
  https://eprint.iacr.org

# Compiling

The EasyCrypt proofs can be checked using EasyCrypt *1.x-preview*
[easycrypt] without any additional dependencies.

[easycrypt]: https://github.com/EasyCrypt/easycrypt/tree/1.0-preview

From the root of this repository, you can now run the following
command to check all targets:

```bash
make check
```
