require import AllCore Distr SmtMap FSet List Real DBool Dexcepted.
require PKSMK.
require import Policy. 
require MainTheorem.

require import CRHash.

(**********************************************)
(*          DMP quorum-based policy           *)
(**********************************************)

theory GNPolicy.

type hsm_pkey.

clone import PKSMK as PKSMK_Pol.

type Pk.
type HId = hsm_pkey * Pk.
type OpId = PKSMK_Pol.pkey.
type OpSk = PKSMK_Pol.skey.
type Quorum = OpId fset * int.
type DomId.
type HstId.
type Fpr.
type Metadata = Quorum * Fpr option * HstId fset * DomId.
type Request = HId fset * OpId fset.
type Authorization = PKSMK_Pol.signature.
type Authorizations = (OpId,PKSMK_Pol.signature) fmap.
type Trust = (HId fset) * Metadata.  

op tmems(trust : Trust) = trust.`1.
op tquorum(trust : Trust) = trust.`2.`1.
op tops(trust: Trust) = trust.`2.`1.`1.
op tthr(trust: Trust) = trust.`2.`1.`2.
op tfpr(trust : Trust) = trust.`2.`2.
op thsts(trust : Trust) = trust.`2.`3.
op tr_initial(trust : Trust) = tfpr trust = None.
 
op valid_request(gen : HId fset, req : Request) = 
            req.`1 \subset gen.
  

clone import CRHash with
  type domain = Trust,
  type range = Fpr,
  type hkey  = unit,
  op hkgen <- dunit ().

op encode :Request -> PKSMK_Pol.message.

axiom encode_inj m1 m2 : m1 <> m2 => encode m1 <> encode m2.

op checkTrustProgress(old new : Trust) = hash () old = oget (tfpr new).

op checkTrustUpdate(old : Trust, new : Trust, 
                   auth : Authorizations) : bool = 
    (* Check threshold preserved *)
    (tthr old = tthr new)&&
    (* Signers are a quorum *)
    (fdom auth \subset tops old) &&
    (* Signers are a quorum of correct size *)
    ((tthr old) <= card (fdom auth)) &&
    (* Check hash consistency *)
    tfpr new = Some (hash () old) &&
    (* Check all new members signed *)  
    let newmems = tmems new `\` tmems old in
    let newops = tops new `\` tops old in
    (* Verify all signatures *)
    let msg = encode (newmems,newops) in
    all (fun o => PKSMK_Pol.verify (o,msg,oget auth.[o])) (elems (fdom auth)).

op max_ops : int.

op goodq(gops : OpId fset, quorum : Quorum) : bool = 
     (* max size on quorum list *)
     card quorum.`1 < max_ops /\
     (* the threshold defines how many sigs are needed *)
     1 < quorum.`2 /\
     (* this tolerates at most thr-1 bad ops *)
     card (quorum.`1 `\` gops) < quorum.`2 <= card quorum.`1.

op quorum_assumption(badOps : OpId fset, goodOps : OpId fset, t : Trust) = 
   tops t \subset (badOps `|` goodOps) => goodq goodOps (tquorum t).

clone import MainTheorem.MainTh with 
  type PKSMK_HSM.pkey <- hsm_pkey,
  type MRPKE.Pk <- Pk,
  type OpId <- OpId,
  type OpSk <- OpSk,
  type Trust <- Trust,
  op tr_mems <- tmems,
  op tr_initial <- tr_initial,
  type Request <- Request,
  type Authorization <- Authorization,
  op valid_request <- valid_request,
  op checkTrustProgress <- checkTrustProgress, 
  op checkTrustUpdate <- checkTrustUpdate,
  op KMSIND.Policy.quorum_assumption <- quorum_assumption,
  op KMSIND.q_ops <- PKSMK_Pol.q_gen,
  op KMSIND.q_auth <- PKSMK_Pol.q_sig.
import KMSIND.
import Policy.

axiom ge0_q_inst : 0 <= q_inst.


module OA (SSch:PKSMK_Pol.OrclInd) : OperatorActions = {
  var badOps : OpId fset

   proc init() = {
      badOps <- fset0;
   }

   proc newOp(badOp : bool) : OpId option * OpSk option = {
        var oopid,opid,sk;
        oopid <- (None,None);
        if (!badOp) {
           opid <@ SSch.keygen();
           oopid <- (Some opid, None);
        }
        else {
           (opid,sk) <$ keygen;
           badOps <- badOps `|` fset1 opid;
           oopid <- (Some opid, Some sk);
        }
        return oopid;
   }

   proc requestAuthorization(req : Request, 
                             opid : OpId) : Authorization option = {
        var msg,sig,gops;
        sig <- None;
        gops <@ SSch.pkeys();
        if ((opid \in gops)&&(req.`2 \subset (gops `|` badOps))) {
           msg <- encode req;
           sig <@ SSch.sign(opid,msg);
        }
       return sig;
   }

   proc isGoodInitialTrust(trust : Trust) : bool = {
        var gops;
        gops <@ SSch.pkeys();
        return (goodq gops (tquorum trust) && card (tops trust) <= max_size && (tops trust \subset (gops `|` badOps))); (* solid quorums *)
   }          
}.

module IOA(SSch : FullSigService) : OperatorActions = {
   include OA(SSch) [-init]
   proc init () = {
     OA.badOps <- fset0;
     SSch.init();
   }
}.

(*********************************************************)
(*                     SECURITY PROOF                    *)
(*********************************************************)

module HA0(H:CRHash.OrclInd) = {
  include HstPolSrv [-installUpdatedTrust]

   proc installUpdatedTrust(hstid : HstId, new : Trust) : bool = {
       var b,b1;
       b <- hstid \in HstPolSrv.hosts_tr && !tr_initial new;
       b1 <@ H.check(oget HstPolSrv.hosts_tr.[hstid],oget (tfpr new));
       if (b&&b1) {
          HstPolSrv.hosts_tr.[hstid] <- new;
       }
       return (b&&b1);
   }
}.

module HSMA0(SSch : PKSMK_Pol.OrclInd,H : CRHash.OrclInd)  = {

  proc checkTrustUpdate(old : Trust, new : Trust, 
                        auth : Authorizations) : bool= {
     var b,newmems,newops,l,msg,bv,o,h,pk;
     h <@ H.hash(old);
     b <- !tr_initial new && 
         (tthr old = tthr new)&&
         (fdom auth \subset tops old) &&
         ((tthr old) <= card (fdom auth)) &&
         tfpr new = h;
     if (b) {
           newmems <- tmems new `\` tmems old;
           newops <-tops new `\` tops old;
           b <- true;
           l <- elems (fdom auth);
           msg <- encode (newmems,newops);
           while (l <> []) {
             o <- head witness l;
             if (o \in auth) {
                pk <- oget auth.[o];
                bv <@ SSch.verify(o,msg,pk);
             }
             else {
                bv <- false;
             }
             b <- b && bv;
             l <- drop 1 l;
          }
     }
     return b;
  }    
}.

module RealTrustService0(SSch : PKSMK_Pol.OrclInd,H : CRHash.OrclInd) : TrustService = {
   module TS = RealTrustService(OpPolSrv(OA(SSch)))
   module OA = OpPolSrv(OA(SSch))
   module HA = HA0(H)
   module HSMA = HSMA0(SSch,H)

   include TS [isProtectedTrust,newOp,addHId,requestAuthorization,
               isGoodInitialTrust,isProtectedTrust]

   proc init() : unit = { 
        RealTrustService.count_ops <- 0;
        RealTrustService.count_auth <- 0;
        RealTrustService.count_hst <- 0;
        RealTrustService.count_updt <- 0;
        RealTrustService.count_chk <- 0;
        RealTrustService.protectedTrusts <- fset0;
        RealTrustService.parentTrust <- empty;
        OA.init(); 
        HA.init(); 
   }

   proc newHst() = {
      var hstid;
      hstid <- witness;
      if (RealTrustService.count_hst < q_hst) {
         hstid <@ HA.newHst();
         RealTrustService.count_hst <- RealTrustService.count_hst + 1;
      }
      return hstid;
   }

   proc installInitialTrust = HA.installInitialTrust

   proc installUpdatedTrust(hstid : HstId, new : Trust) = {
      var b;
      b <- false;
      if (RealTrustService.count_updt < q_updt) {
         b <@ HA.installUpdatedTrust(hstid, new);
         RealTrustService.count_updt <- RealTrustService.count_updt + 1;
      }
      return b;
   }

   proc isInstalledTrust = HA.isInstalledTrust

   proc checkTrustUpdate(old : Trust, new : Trust, 
                         auth : Authorizations) : bool = {
        var c, protected;
        c <- false;
        if (RealTrustService.count_chk < q_tkmng (* Check this : q_chk *) && 
            quorum_assumption OpPolSrv.badOps OpPolSrv.goodOps new) {
           c <@ HSMA.checkTrustUpdate(old,new,auth);
           protected <- old \in RealTrustService.protectedTrusts;
           if (c) {
              if (!new \in RealTrustService.parentTrust) {
                  RealTrustService.parentTrust.[new] <- old;
              }
              if (protected) {
                  RealTrustService.protectedTrusts <- RealTrustService.protectedTrusts `|` fset1 new;  
              }
           }
           RealTrustService.count_chk <- RealTrustService.count_chk + 1;
        }
        return c;
   }

   proc getInstalledTrust = HA.getInstalledTrust
   proc quorumAssumption = OA.quorumAssumption
   proc allGenuine = OA.allGenuine
}. 

module TrustOracles0(SSch : PKSMK_Pol.OrclInd,H : CRHash.OrclInd) : TrustOraclesT = {

   module TSrv = RealTrustService0(SSch,H)

   include TSrv [-init]

   proc init() : unit = { 
         TrustOracles.broken <- false; 
         TrustOracles.count_bprog <- 0;
         TrustOracles.count_bupdt <- 0;
         TSrv.init(); 
   }

   proc break_brokeTrustProgress(hstid :HstId, new : Trust) : unit = {
       var registered, check, check1;
       if (TrustOracles.count_bprog < q_bprog) {
          check1 <@ H.check(oget (HstPolSrv.hosts_tr.[hstid]),oget (tfpr new));
          registered <- new \in RealTrustService.parentTrust;
          check <- hstid \in HstPolSrv.hosts_tr && !tr_initial new && check1;
           TrustOracles.broken <- TrustOracles.broken || (registered && check            
                    && (!(HstPolSrv.hosts_tr.[hstid] = 
                          RealTrustService.parentTrust.[new])));
          TrustOracles.count_bprog <- TrustOracles.count_bprog + 1;
       }
   }

   proc break_brokeTrustUpdate(old : Trust, new : Trust, 
                               auth : Authorizations) : unit = {
        var c, protected, goodo, goodn, bad2,bad3,bad1;
        if (TrustOracles.count_bupdt < q_bupdt  && 
            quorum_assumption OpPolSrv.badOps OpPolSrv.goodOps new) {
           goodn <- all_genuine OpPolSrv.genuine (new);
           goodo <- all_genuine OpPolSrv.genuine (old);
           c <@ HSMA0(SSch,H).checkTrustUpdate(old,new,auth);
           protected <- old \in RealTrustService.protectedTrusts && goodo;
           bad1 <- old \in RealTrustService.protectedTrusts && !goodo;
           bad2 <- c && (new \in RealTrustService.parentTrust && 
                        Some old <>  RealTrustService.parentTrust.[new]);
           bad3 <- c && ((protected && !goodn)||(new \in RealTrustService.protectedTrusts && goodn && !protected));
           TrustOracles.broken <- TrustOracles.broken || bad1 || bad2 || bad3;
           TrustOracles.count_bupdt <- TrustOracles.count_bupdt + 1;
       }
   }

   proc break_corruptedProtectedTrust(trust : Trust) : unit = {
        var good,protected;
        good <- all_genuine OpPolSrv.genuine trust;
        protected <- trust \in RealTrustService.protectedTrusts;
        TrustOracles.broken <- TrustOracles.broken || (!good && protected);
   }

}.

module Bhop1(A : TrustAdv, Chain:CRHash.OrclInd) = {
   module SSch = RealSigServ
   module TO = TrustOracles0(SSch,Chain)
   module A = A(TO)

   proc main() : bool = {
     SSch.init();
     TO.init();
     A.break_trust();
     return TrustOracles.broken;
   }
}.

module Bhop2(A:TrustAdv, SSch:PKSMK_Pol.OrclInd) = {
  module Chain = IdealHash
  module TO = TrustOracles0(SSch,Chain)
  module A = A(TO)

   proc main() : bool = {
     Chain.init();
     TO.init();
     A.break_trust();
     return TrustOracles.broken;
   }
}.

phoare checkTrustUpdateCorrect o n a c :
 [RealTrustService0(RealSigServ, RealHash).HSMA.checkTrustUpdate  :
    c = RealHash.count_hash /\ ((old,new,auth) = (o,n,a))  ==> 
    if (c < CRHash.q_hash) then ((res = (!tr_initial n /\ checkTrustUpdate o n a)) /\ RealHash.count_hash = c + 1) 
    else RealHash.count_hash = c ] = 1%r.
proof.
conseq (_ : true ==> true)
       (_ : c = RealHash.count_hash /\ ((old,new,auth) = (o,n,a)) ==> 
            if (c < CRHash.q_hash) then ((res = (!tr_initial n /\ checkTrustUpdate o n a)) /\ RealHash.count_hash = c + 1) 
            else RealHash.count_hash = c) => //;last first.
(* lossless *)
+ proc; islossless.
  while true (size l{hr}).
  + move=> z; wp; conseq (_: true) => //; 1: smt (size_drop size_eq0 size_ge0).
    islossless. 
  skip;smt (size_eq0 size_ge0).
proc; inline *. 
case (c < CRHash.q_hash); last first. 
+ rcondf 3; 1: by auto. 
  sp;if => //=;sp. 
  while (c < CRHash.q_hash /\ RealHash.count_hash = c); first by auto.
  by skip => /#.
seq 5 : (c + 1 = RealHash.count_hash /\ c < CRHash.q_hash /\ ((old, new, auth).`1, (old, new, auth).`2, (old, new, auth).`3) = (o, n, a) /\
         p = old /\ h0 = hash () p /\ h = Some h0 /\
         b = (!tr_initial new &&
              (tthr old = tthr new) &&
              (fdom auth \subset tops old) && 
              (tthr old <= card (fdom auth)) &&
              tfpr new = h)); 1: by wp;skip => /#.
if =>//=; last by wp;skip => /#.
while (msg = encode (newmems, newops) /\ (forall o, o \in l => o \in auth) /\
       ((b /\ all (fun (o : OpId) => verify (o, msg, oget auth.[o])) l) = all (fun (o : OpId) => verify (o, msg, oget auth.[o])) (elems (fdom auth)))).
+ rcondt 2; 1: by auto; smt (mem_head_behead).
  auto => /> &1; case : (l{1}) => //= x lx h1 <-; rewrite drop0 /#.
auto => />; rewrite /checkTrustUpdate /=;smt (mem_fdom memE).
qed.

axiom hash_bound : q_tkmng + q_updt + q_bprog + q_bupdt < CRHash.q_hash.

axiom move_elsewhere : 
0 <= q_tkmng /\
0 <= q_updt /\
0 <= q_bprog /\
0 <= q_bupdt.

section.

declare module A : TrustAdv {OA,OpPolSrv,RealSigServ,RealHash,TrustOracles}.

lemma add_ha_hsma_module &m:
  Pr [ TrustSec(A,OpPolSrv(IOA(RealSigServ))).main() @ &m : res] =
  Pr [ IndHash(RealHash,  Bhop1(A)).main() @ &m : res].
proof.
byequiv (_: ={glob A} ==> _) => //=.
proc;inline *;wp.
call(_: ={glob OA, glob OpPolSrv, glob RealSigServ, glob RealTrustService, glob TrustOracles} /\ 
        (RealTrustService.count_chk + RealTrustService.count_updt + TrustOracles.count_bprog + TrustOracles.count_bupdt = RealHash.count_hash
         /\ RealTrustService.count_chk <= (* q_chk *)q_tkmng 
         /\ RealTrustService.count_updt <= q_updt 
         /\ TrustOracles.count_bprog <= q_bprog 
         /\ TrustOracles.count_bupdt <= q_bupdt){2}).
+ by sim />.
+ by sim />.
+ by proc; sim />.
+ by sim />.
+ proc; conseq />.
  inline OpPolSrv(IOA(RealSigServ)).quorumAssumption.
  sp;if => //=.
  seq 1 1: (  
    #[/3:-7,-6:]pre /\ 
    ={c} /\ 
    RealTrustService.count_chk{2} + RealTrustService.count_updt{2} + TrustOracles.count_bprog{2}  + TrustOracles.count_bupdt{2}  =
    RealHash.count_hash{2} - 1).
  + ecall{2} (checkTrustUpdateCorrect old{2} new{2} auth{2} RealHash.count_hash{2}). 
    inline *; auto; smt (hash_bound).
  wp 2 2; conseq (_ : ={c, glob RealTrustService}) => //; 1: smt().
  by sim.
+ by proc;sim />.
+ by proc;sim />.
+ rewrite /inv_1 /=;proc;inline *; conseq />.
  sp 1 1;if => //.
  rcondt{2} 7; 1: by auto; smt (hash_bound).
  swap{2} 8 4.
  wp 5 10 => /=.
  conseq (_: ={b, HstPolSrv.hosts_tr}) => //; 1: smt().
  by auto => /> *; have _: (RealHash.hk{2} = tt); smt(). 
+ by proc; auto.
+ by sim />.
+ by sim />.
+ by sim />.
+ by sim />.
+ rewrite /inv_1 /=;proc; conseq />;if => //.
  inline *.
  rcondt{2} 4; 1: by auto; smt (hash_bound).
  by auto => /> *; have _: (RealHash.hk{2} = tt); smt(). 
+ rewrite /inv_1 /=;proc;conseq />; if => //.
  sp.
  seq 1 1: (#[/:-7,-6:]pre /\ ={c} /\ 
    RealTrustService.count_chk{2} + RealTrustService.count_updt{2} + TrustOracles.count_bprog{2}  + TrustOracles.count_bupdt{2} =
    RealHash.count_hash{2} - 1).
  + ecall{2} (checkTrustUpdateCorrect old{2} new{2} auth{2} RealHash.count_hash{2}). 
    inline *; auto; smt (hash_bound).
  by auto => /#.  
+ by proc; auto.
auto => />; smt (move_elsewhere dunit_ll). 
qed.

lemma hop1 &m: 
  Pr[IndHash(IdealHash, Bhop1(A)).main() @ &m : res] =
  Pr[IndSig(RealSigServ, Bhop2(A)).main() @ &m : res].
proof.
byequiv (_: ={glob A} ==> _) => //=.
proc;inline *; sim;auto.
qed.

(*******************************************)
(******       Final step         ***********)
(*******************************************)

lemma in_fsetU1_id (x:'a) (s:'a fset):
    x \in s => s `|` fset1 x = s.
proof. 
  by move=> hx;rewrite fsetUC subset_fsetU_id // => z /in_fset1 ->.
qed.

op protected_spec (protectedTrusts:Trust fset) (parentTrust:(Trust, Trust) fmap) (qs : (pkey * message * signature) fset) 
                  (pks_sks : (pkey, skey) fmap) genuine badOps =
   (forall (trust : Trust), trust \in protectedTrusts =>
     (* All protected trusts are good *)
     all_genuine genuine trust /\
     (* All protected non-initial trusts have a parent who is also in protected trusts *)
     (! tr_initial trust =>
      (trust \in parentTrust /\
       oget parentTrust.[trust] \in protectedTrusts)) /\
     (* All protected trusts have at least one good op in every quorum *)
     (forall auth,
       auth \subset tops trust => tthr trust <= card auth =>
       exists pk, pk \in (auth `&` fdom pks_sks)) /\
     (* All protected trusts have all genuine members *) 
     tops trust \subset (fdom pks_sks `|` badOps)) /\
   (* All signed authorizations are for genuine  members *)
   (forall pk newmems newops s,
             ((pk,encode (newmems,newops),s) \in qs) =>
                  newmems \subset genuine /\
                  newops \subset (fdom pks_sks `|` badOps)).
  
lemma protected_spec_pks_sks protectedTrusts parentTrust qs pks_sks genuine badOps (pksk: pkey * skey): 
  protected_spec protectedTrusts parentTrust qs pks_sks genuine badOps => 
  pksk.`1 \notin pks_sks =>
  protected_spec protectedTrusts parentTrust qs pks_sks.[pksk.`1 <- pksk.`2] genuine badOps.
proof.
  move=> [h h'] hnin;split.
  + move => t h1; have [/> h2 h3 h4 h5]:= h t h1; split.
    + move=> auth w1 w2; have [pk ?]:= h4 auth w1 w2.
      exists pk; smt (mem_fdom_set  mem_fdom in_fsetI1 in_fsetI in_fset1).
    smt (fdom_set in_fsetU in_fset1).
  smt (fdom_set in_fsetU fcardI).
qed.

lemma trust_onegood (trust:Trust) (pks_sks : (pkey, skey) fmap): 
  card (trust.`2.`1.`1 `\` fdom pks_sks) < trust.`2.`1.`2 <= card trust.`2.`1.`1 =>
  forall (auth : OpId fset),
    auth \subset tops trust => tthr trust <= card auth => exists (pk : OpId), pk \in auth `&` fdom pks_sks.
proof.
  move=> h1 auth h2 h3. 
  have : (0 < card (auth `&` fdom pks_sks)).
  + have aux : ((auth `\` fdom pks_sks) \subset (tops trust `\` fdom pks_sks)).
    + smt (in_fsetD). search card.
    smt(@FSet). 
  rewrite cardE => sizeaux.
  have [pk] : (exists pk, pk \in (elems (auth `&` fdom pks_sks))).
  + by elim : (elems (auth `&` fdom pks_sks)) sizeaux => //= /#. 
  by rewrite -memE => ?;exists pk.
qed.

op parent_spec (parentTrust:(Trust, Trust) fmap) (pres : (range, domain) fmap) =
 forall t, t \in parentTrust =>
   (* All trusts that have a parent also have a hash preimage for fingerprint *)
   (oget (tfpr t) \in pres) /\
   (* All trusts that have a parent have a matching hash preimage. *)
   (!tr_initial t /\ pres.[oget (tfpr t)] = parentTrust.[t]).

hoare checkTrustUpdate (old0 new0 : Trust) (auth0 : Authorizations) qhash:
  RealTrustService0(IdealSigServ, IdealHash).HSMA.checkTrustUpdate :
   (old,new,auth) = (old0,new0,auth0) /\ 
   RealHash.count_hash < CRHash.q_hash /\
   RealHash.count_hash = qhash /\
   protected_spec RealTrustService.protectedTrusts RealTrustService.parentTrust RealSigServ.qs 
     RealSigServ.pks_sks OpPolSrv.genuine OA.badOps /\
   parent_spec RealTrustService.parentTrust RealHash.pres /\
   OA.badOps = OpPolSrv.badOps /\ 
   quorum_assumption OpPolSrv.badOps (fdom RealSigServ.pks_sks) new
   ==>
   RealHash.count_hash = qhash + 1 /\
   parent_spec RealTrustService.parentTrust RealHash.pres /\
   (res => 
     !tr_initial new0 /\ 
     tthr old0 = tthr new0 /\
     fdom auth0 \subset tops old0 /\
     tthr old0 <= card (fdom auth0) /\
     tfpr new0 = Some (hash () old0) /\
     Some old0 = RealHash.pres.[oget (tfpr new0)] /\
     (old0 \in RealTrustService.protectedTrusts => 
       (all_genuine OpPolSrv.genuine new0 /\
        tops new0 `\` tops old0 \subset fdom RealSigServ.pks_sks `|` OA.badOps)) /\
     (tops new0 \subset fdom RealSigServ.pks_sks `|`  OA.badOps => 
        goodq (fdom RealSigServ.pks_sks) (tquorum new0))).
proof.
  proc;inline * => /=.
  rcondt 3; 1: by move => *;wp;skip; smt (hash_bound).
  seq 8 : (#[/:-6,-4:]pre /\ 
     RealHash.count_hash = qhash + 1 /\
     (b => 
      !tr_initial new /\ 
      tthr old = tthr new /\ 
      fdom auth \subset tops old /\
      tthr old <= card (fdom auth) /\
      tfpr new = Some (hash () old) /\
      Some old = RealHash.pres.[oget (tfpr new)])).
  +   by auto => /> *; have _: (RealHash.hk{hr} = tt); smt (get_setE).
  if; last by done.
  swap 3 -2.
  seq 1 : (#pre); first by wp;skip=> /#.
  swap 4 -1; sp 3. 
  seq 1 : (#[/:-1]pre /\ 
      (b => forall o, (o \in (fdom auth0) /\ !(o \in l)) =>
       o \in RealSigServ.pks_sks => 
        (exists s, (o, encode (newmems,newops), s) \in RealSigServ.qs))).
  + by wp;skip; smt (memE).
  while (#pre).
  + wp;skip => |> &hr; case: (l{hr}) => //= *; smt (drop0).
  skip => |>;smt (in_fsetI  mem_fdom in_fsetD fsetUC).
qed.

lemma noadv &m :
  Pr [ IndSig(IdealSigServ, Bhop2(A)).main() @ &m : res ] = 0%r.
proof.
byphoare (_: true ==> res)=> //.
proc;hoare.
inline *;wp.
call (_: (* Never broken *)
         !TrustOracles.broken /\ 
         protected_spec  RealTrustService.protectedTrusts RealTrustService.parentTrust RealSigServ.qs RealSigServ.pks_sks OpPolSrv.genuine OA.badOps /\
         parent_spec RealTrustService.parentTrust RealHash.pres /\
         OA.badOps = OpPolSrv.badOps /\ 
         fdom RealSigServ.pks_sks = OpPolSrv.goodOps /\
         RealSigServ.count_pk <= RealTrustService.count_ops /\
         RealTrustService.count_chk + RealTrustService.count_updt + TrustOracles.count_bprog + TrustOracles.count_bupdt = RealHash.count_hash /\
         RealTrustService.count_chk <= (* q_chk *)q_tkmng /\
         RealTrustService.count_updt <= q_updt /\
         TrustOracles.count_bprog <= q_bprog /\
         TrustOracles.count_bupdt <= q_bupdt); 
last by auto => />;smt (in_fset0 mem_empty fdom0 move_elsewhere).
(* INVARIANT PRESERVATION *)
(* newOp *)
+ proc; conseq |>; inline*. 
  sp; if => //=; sp;if => //=.
  + rcondt 2; 1: by wp;skip => /#. 
    wp;rnd;wp;skip; smt (protected_spec_pks_sks fdom_set in_fsetU1_id mem_fdom).
  wp;rnd;wp;skip; smt (fdom_set in_fsetU in_fset1).

(* addHId *)
+ proc; conseq |>.
  if => //;wp;skip => *; smt (in_fsetU).
(* requestAuthorization *)
+ proc; conseq |>.
  sp; if => //; wp.
  inline OpPolSrv(OA(IdealSigServ)).requestAuthorization.
  sp;wp; if => //.
  inline OA(IdealSigServ).requestAuthorization.
  inline *; sp; wp.
  if => //; sp;wp.
  if => //; wp; if=> //.
  wp; rnd; skip => |> *.
  smt (in_fsetU1  encode_inj).
(* isGoodInitialTrust *)
+ proc;conseq |>.
  inline *; sp 5; if => //.
  wp;skip; smt (in_fsetU1 trust_onegood).
(* checkTrustUpdate *)
+ proc; conseq |>.
  sp; if => //.
  seq 1: (#[/2:-7,-6:]pre /\ 
    (c => 
       (!tr_initial new /\ 
        tthr old = tthr new /\
        fdom auth \subset tops old /\
        tthr old <= card (fdom auth) /\
        tfpr new = Some (hash () old) /\
        Some old = RealHash.pres.[oget (tfpr new)] /\
        (old \in RealTrustService.protectedTrusts => 
           (all_genuine OpPolSrv.genuine new /\
            tops new `\` tops old \subset fdom RealSigServ.pks_sks `|` OA.badOps)) /\
        (tops new \subset fdom RealSigServ.pks_sks `|`  OA.badOps => 
          goodq (fdom RealSigServ.pks_sks) (tquorum new)))) /\
     RealTrustService.count_chk + RealTrustService.count_updt + TrustOracles.count_bprog   
          + TrustOracles.count_bupdt  = RealHash.count_hash - 1).
  + ecall (checkTrustUpdate old{hr} new{hr} auth{hr} RealHash.count_hash{hr}).
    skip; smt (hash_bound).  
  wp; skip => |> *.
  have -> : RealTrustService.count_chk{hr} + 1 <= q_tkmng by smt ().
  have -> /= *: 
   RealTrustService.count_chk{hr} + 1 + RealTrustService.count_updt{hr} + TrustOracles.count_bprog{hr} +
     TrustOracles.count_bupdt{hr} = RealHash.count_hash{hr} by smt ().
  split => *.
  + split => *.
    + split.         
      + case: H0 => h h' /> t;rewrite in_fsetU1 => -[ht|->].
        + have /> := h t ht;smt (get_setE in_fsetU1).
        smt (mem_set get_setE in_fsetU1 trust_onegood in_fsetU in_fsetD).
      smt (mem_set get_setE). 
    smt (mem_set get_setE).  
  case: H0 => h h' /> t;rewrite in_fsetU1 => -[ht|->].
  + have /> := h t ht;smt (get_setE in_fsetU1).  
  smt (mem_set get_setE in_fsetU1 trust_onegood in_fsetU in_fsetD).
(* newHst *)
+ by proc; conseq |>.
(* installInitialTrust *)
+ by proc; conseq |>.
(* installUpdatedTrust *)
+ proc; conseq |>.
  sp; if => //;wp.
  inline *; wp; skip => />; smt (hash_bound).
(* isInstalledTrust *)
+ by proc; conseq |>.
(* getInstalledTrust *)
+ by proc; conseq |>.
(* quorumAssumption *)
+ by proc; conseq |>.
(* allGenuine *)
+ by proc; conseq |>.
(* isProtectedTrust *)
+ by proc; conseq |>.
(* break_brokeTrustProgress *)
+ proc; conseq |>.
  if => //;inline *.
  rcondt 4; 1: by auto; smt (hash_bound).
  by wp;skip => /> * /#.
(* .break_brokeTrustUpdate *)
+ proc; conseq |>.
  if => //; sp; wp.
  ecall (checkTrustUpdate old{hr} new{hr} auth{hr} RealHash.count_hash{hr}).
  skip => |> *.
  split; 1: smt (hash_bound).
  move=> *; split; 2: smt ().
  by case: result H11 => /> * /#.
(* break_corruptedProtectedTrust *)
proc; conseq />; wp; skip => /> /#.
qed.

lemma main_theorem_aux &m :
  Pr [TrustSec(A,OpPolSrv(IOA(RealSigServ))).main() @ &m : res ] =
  Pr[IndHash(RealHash, Bhop1(A)).main() @ &m : res] -
  Pr[IndHash(IdealHash, Bhop1(A)).main() @ &m : res] +
  Pr[IndSig(RealSigServ, Bhop2(A)).main() @ &m : res] -
  Pr[IndSig(IdealSigServ, Bhop2(A)).main() @ &m : res].
proof. rewrite (add_ha_hsma_module &m) (hop1 &m) (noadv &m); ring. qed.

end section.

clone import UF1_UF.

module OAR = IOA(RealSigServ).

lemma main_theorem_full &m 
     (A <: TrustAdvInd { OA, OrclCR, RealHash, RealSigServ, OrclUF, UF1, WAkg, MkAdvUF1, 
                         HstPolSrv, TrustOracles}):
  (forall (O <: TrustOraclesI), 
     islossless O.newOp => islossless O.addHId => islossless O.requestAuthorization =>
     islossless O.isGoodInitialTrust => islossless O.newHst => islossless O.installInitialTrust =>
     islossless O.installUpdatedTrust => islossless O.isInstalledTrust => islossless O.getInstalledTrust =>
     islossless O.checkTrustUpdate => islossless O.isProtectedTrust => islossless O.quorumAssumption => islossless O.allGenuine => 
     islossless A(O).main) =>
  `| Pr [ TrustSecInd(A,IdealTrustService(OpPolSrv(OAR))).main() @ &m : res ] 
     - Pr [ TrustSecInd(A,RealTrustService(OpPolSrv(OAR))).main() @ &m : res ] |
  <=  
     Pr[CR(MkAdvCR(Bhop1(B_idealtrust(A)))).main() @ &m : res] +
     PKSMK_Pol.q_gen%r * Pr[UF1(MkAdvUF1(MkAdvUF(Bhop2(B_idealtrust(A))))).main() @ &m : res].
proof.
move=> A_ll.
have Hrc := ind_cr (Bhop1(B_idealtrust(A))) _ &m.
+ move => O hash_ll check_ll; islossless.
  apply (A_ll (<:B_idealtrust_OO(TrustOracles0(RealSigServ, O)))); islossless.
  + while true (size l) => *.
    + wp; conseq (_: true). 
      + by move=> &hr; case: (l{hr}) => //= l; smt (drop0).
      by islossless.
    by skip; smt (size_ge0 size_eq0).     
  while true (size l) => *.
  + wp; conseq (_: true). 
    + by move=> &hr; case: (l{hr}) => //= l; smt (drop0).
    by islossless.
  by skip; smt (size_ge0 size_eq0).  
have Hsig := ind_uf1 (Bhop2(B_idealtrust(A))) &m _.
+ move=> O keygen_ll sign_ll pkeys_ll verify_ll; islossless; last by rewrite dunit_ll.
  apply (A_ll (<:B_idealtrust_OO(TrustOracles0(O, IdealHash)))); islossless.
  + while true (size l) => *.
    + wp; conseq (_: true). 
      + by move=> &hr; case: (l{hr}) => //= l; smt (drop0).
      by islossless.
    by skip; smt (size_ge0 size_eq0).     
  while true (size l) => *.
  + wp; conseq (_: true). 
    + by move=> &hr; case: (l{hr}) => //= l; smt (drop0).
    by islossless.
  by skip; smt (size_ge0 size_eq0).
have := RealIdealTrust OAR A A_ll _ _ _ _ &m.
+ by islossless. + by islossless. + by islossless. + by islossless.
rewrite (main_theorem_aux (B_idealtrust(A)) &m) => /#.
qed.

(* --------------------------------------------------------- *)
(* Intantiation of the master theorem                        *)

op isGoodInitialTrust (gOA: glob OAR) (t:Trust) =
  let (badOps,count_sig, count_pk, qs, pks_sks) = gOA in
   goodq (fdom pks_sks) (tquorum t) && card (tops t) <= max_size && tops t \subset (fdom pks_sks)  `|` badOps.

phoare op_oa (gOA : glob OAR) (t : Trust): 
  [OAR.isGoodInitialTrust : 
    t = trust /\ (glob OAR) = gOA ==> 
    res = isGoodInitialTrust gOA t /\ (glob OAR) = gOA ] = 1%r.
proof. by proc;inline *;wp;skip. qed.

abstract theory IOpAct.

  op rejected (trusts : (Trust , bool) fmap) = 
      FSet.fold (fun trust s => 
         if  card (tquorum trust).`1 <= max_size /\ card (tops trust) <= max_size then  
            (tquorum trust).`1 `|` tops trust `|` s
         else s) fset0 (fdom trusts).

  lemma rejected_sub trusts t b : trusts.[t] = Some b => 
        card (tquorum t).`1 <= max_size /\ card (tops t) <= max_size =>
        ((tquorum t).`1 `|` tops t) \subset rejected trusts.
  proof.
    rewrite /rejected foldE => ht.
    have : t \in (elems (fdom trusts)).
    + by rewrite -memE mem_fdom /dom ht.
    elim: (elems (fdom trusts)) => //= t1 ts hrec [-> | ]; smt (in_fsetU).
  qed.

  lemma nin_fsetD (x:'a) (s1 s2: 'a fset) :  !(x \in s1) => 
    s1 `\` (s2 `|` fset1 x) = s1 `\` s2.
  proof. rewrite fsetP => hx z; smt (in_fsetU in_fsetD in_fset1). qed.

  module BadIO : OperatorActions = {
    var bad : bool
    include IOperatorActions(OAR) [-init, newOp]

    proc init() : unit = {
      IOperatorActions(OAR).init();
      bad <- false;
    }

    proc newOp(badOp : bool) : OpId option * OpSk option = {
      var oopid <- (Some witness, None);
      if (badOp || RealSigServ.count_pk < q_gen) {
      oopid <@ IOperatorActions(OAR).newOp(badOp);
      bad <- bad || oget oopid.`1 \in rejected IOperatorActions.trusts;
      }
      return oopid;
    }
  }.

  module type AdvIOpAct (O: OperatorActions) = {
    proc main() : bool {O.newOp O.requestAuthorization O.isGoodInitialTrust}
  }.

  module Main (A:AdvIOpAct) (O:OperatorActions) = {
    proc main () = {
      var r;
      O.init();
      r <@ A(O).main();
      return r;
    }
  }.

  section.
    declare module A: AdvIOpAct {BadIO, IOperatorActions}.

    axiom A_ll (O <: OperatorActions{A}):
      islossless O.newOp => islossless O.requestAuthorization => islossless O.isGoodInitialTrust => islossless A(O).main.

    local equiv upto_bad : Main(A, OAR).main ~ Main(A, BadIO).main : ={glob A} ==> !BadIO.bad{2} => ={res}.
    proof.
      proc; call (_: BadIO.bad, ={glob OAR} /\ forall t b, IOperatorActions.trusts{2}.[t] = Some b => b = isGoodInitialTrust (glob OAR){2} t).
      + by apply A_ll.
      (* newOp *)
      + proc; inline *; sp.
        if{2}.
        + sp; if =>//.
          + wp;sp;if => //; auto => />;rewrite /isGoodInitialTrust /=.
            move=> &2 Hb hw ??? pksk ?;split => ? hr; last by apply hw.
            move=> t b ^ /rejected_sub hs /hw ->.
            smt (nin_fsetD fdom_set in_fsetU in_fset1 in_fsetD).
          auto => />; rewrite /isGoodInitialTrust /=.
          move=> &2 hb hw ? opid ?? hr t b ^ /rejected_sub hs /hw ->.
          smt (fsetP in_fsetU in_fset1).
        rcondt{1} 1; 1: by auto => /#.
        by rcondf{1} 2; auto => /#.
      + by move=> *;islossless.
      + by move=> *;proc;sp;if => //;wp;conseq (_: true) => />;islossless.
      + by rewrite /isGoodInitialTrust /=; sim />.
      + by move=> *;islossless.
      + by move=> *;conseq />;islossless.
      + rewrite /isGoodInitialTrust; proc; inline *; auto => />;smt (get_setE).
      + by move=> *;islossless.
      + by move=> *;conseq />;islossless.
      inline *;auto => />; smt (emptyE).
    qed.

    lemma iopact_concl_aux &m : 
      Pr[Main(A, OAR).main() @ &m : res] <= 
        Pr[Main(A, IOperatorActions(OAR)).main() @ &m : res] + 
        Pr[Main(A, BadIO).main() @ &m : BadIO.bad].
    proof. 
      have -> : Pr[Main(A, IOperatorActions(OAR)).main() @ &m : res] = 
                Pr[Main(A, BadIO).main() @ &m : res].
      + byequiv => //; proc; call (_: ={glob IOperatorActions(OAR)}).
        + proc; inline *; sp.
          if{2}; 1: by sim.
          rcondt{1} 1; 1: by auto => /#.
          by rcondf{1} 2; auto => /#.
        + by sim. + by sim.
        by inline *;auto.
      by byequiv upto_bad => // /#.        
    qed.
  end section.

end IOpAct.

clone PKSMK_HSM.UF1_UF as UF1_UF_HSM.

section.

declare module A : 
  KMSAdv{MRPKE.MRPKE_lor, RealSigServ, PKSMK_HSM.RealSigServ, OpPolSrv, HstPolSrv, RealTrustService,
          TrustOracles, IOperatorActions, KMSProcedures, HSMs, HstS, HSMsI, KMSProcedures3, HSMs_MRPKEOr,
          KMSProcedures3pre, HstSP, OA, OrclUF, UF1, OrclCR, RealHash, WAkg, MkAdvUF1,
          UF1_UF_HSM.WAkg, UF1_UF_HSM.MkAdvUF1, PKSMK_HSM.OrclUF, PKSMK_HSM.UF1}.

axiom A_ll (O <: KMSOracles):
  islossless O.newOp => islossless O.requestAuthorization =>
  islossless O.newHst => islossless O.installInitialTrust =>
  islossless O.installUpdatedTrust => islossless O.newHSM =>
  islossless O.newToken => islossless O.updateTokenTrust =>
  islossless O.addTokenKey => islossless O.unwrap => 
  islossless O.corrupt => islossless O.test => 
  islossless A(O).guess.

local module AdvIO (OA:OperatorActions) = {
  module OA = { 
    include OA [-init]
    proc init() = {}
  }

  proc main = KMSRoR3pre(A, OA).main
}.
   
local clone import IOpAct as IOpAct0.

local lemma ror3pre_io &m : 
  Pr[KMSRoR3pre(A, OAR).main() @ &m : res] - 
    Pr[KMSRoR3pre(A, IOperatorActions(OAR)).main() @ &m : res] <=
  Pr[Main(AdvIO, BadIO).main () @ &m : BadIO.bad].
proof.
  have -> : Pr[KMSRoR3pre(A, OAR).main() @ &m : res] = 
            Pr[Main(AdvIO, OAR).main() @ &m : res].
  + by byequiv => //; proc;inline *;wp;sim;auto.
  have -> : Pr[KMSRoR3pre(A, IOperatorActions(OAR)).main() @ &m : res] = 
            Pr[Main(AdvIO,IOperatorActions(OAR)).main() @ &m : res].  
  + by byequiv =>    //;proc;inline *;wp;sim;auto.
  have /# := iopact_concl_aux AdvIO _ &m.   
  move=> O newOp_ll request_ll is_Good_ll; islossless.
  apply (A_ll (<:KMSProcedures3pre(AdvIO(O).OA)) _ _ _ _ _ _ _ _ _ _ _ _); 1..5,7..12: by islossless.
  proc.
  sp; if => //; if => //.
  seq 1 : (size (nosigpks_elems' PKSMK_HSM.RealSigServ.qs OpPolSrv.genuine RealTrustService.parentTrust) <=
              (q_hid + q_tkmng + q_tkmng) * max_size) => //. 
  + by conseq />;islossless.
  + if; 1: by islossless.
    seq 1: true => //; 2: by islossless.
    rnd; skip => &hr [hs ?] /=; rewrite -nosigpks_nosigpks_elems'.
    by apply keygen_restr_ll.
  by hoare; conseq />. 
qed.

local module BadIO' : OperatorActions = {
    include IOperatorActions(OAR) [-init, newOp]

    proc init() : unit = {
      IOperatorActions(OAR).init();
      BadIO.bad <- false;
    }

    proc newOp(badOp : bool) : OpId option * OpSk option = {
      var oopid <- (Some witness, None);
      if (badOp || RealSigServ.count_pk < q_gen) {
      oopid <@ IOperatorActions(OAR).newOp(badOp);
      BadIO.bad <- BadIO.bad || (card (fdom IOperatorActions.trusts) <= q_tkmng + q_inst /\ 
             oget oopid.`1 \in rejected IOperatorActions.trusts);
      }
      return oopid;
    }
}.

local module Main' = {
  proc main () = {
    var b, r;
    BadIO'.init();   
    KMSProcedures3pre(AdvIO(BadIO').OA).init();
    b <@ KMSRoR3pre(A, AdvIO(BadIO').OA).A.guess();
    r <- b = KMSProcedures.b;
    return r;
  }
}.

local lemma aux1 &m : 
  Pr[Main(AdvIO, BadIO).main () @ &m : BadIO.bad] = 
  Pr[Main'.main () @ &m : BadIO.bad /\ KMSProcedures.count_ops <= PKSMK_Pol.q_gen].
proof.
  byequiv => //.
  proc;inline *;wp.
  call (_: ={glob OA, glob RealSigServ, glob IOperatorActions, glob BadIO, glob PKSMK_HSM.RealSigServ, 
              glob HSMs, glob RealTrustService , glob OpPolSrv, glob HstPolSrv, glob KMSProcedures} /\
              (KMSProcedures.count_ops <= PKSMK_Pol.q_gen /\
               KMSProcedures.count_inst <= q_inst /\
               KMSProcedures.count_tkmng <= q_tkmng /\
               card (fdom IOperatorActions.trusts) <= KMSProcedures.count_tkmng + KMSProcedures.count_inst){1}).
  + proc; conseq />.
    sp;if => //; wp=> /=. 
    seq 1 1 : (#{~KMSProcedures.count_ops{1} + 1 <= q_gen}post /\ KMSProcedures.count_ops{1} < q_gen).
    + conseq />; inline *.
      sp;wp;if => //.
      sp;wp;if => //; sim.
      wp 4 4. 
      conseq (_: ={oopid, badOp1, BadIO.bad, IOperatorActions.trusts,
                    OA.badOps, RealTrustService.count_ops, OpPolSrv.badOps,
                     OpPolSrv.goodOps, RealSigServ.count_pk, RealSigServ.pks_sks}); 1:smt().
      by sim.
    by auto => /#.
  + proc;sim />.   
  + proc;sim />.   
  + proc;conseq />.
    sp; if => //.
    inline *; wp 15 15 => /=.
    sp 14 14; if => //;last by auto => /> /#.
    seq 4 4: (#post /\ ={trust3, trust2, trust0, trust, hstid0, OpPolSrv.genuine}); last by sim />.
    auto => /> *; split => *; 2: smt().
    split; 1: smt ().
    by rewrite fdom_set fcardU fcard1; smt (fcard_ge0).
  + proc;sim />. 
  + proc;sim />.   
  + proc;conseq />.
    sp; if => //; wp; if;[1: done | 3: by skip => /> /#].
    conseq />.
    seq 4 4 : (#{/~(rtd{1} = _)}{~(rtd{2} = _)}pre /\ ={td, tk, rtd}); 1: by sim />.
    inline *; conseq />.
    seq 4 4:  (#post /\ ={trust1, trust0, trust, OpPolSrv.genuine}); 2: by sim />.
    auto => /> *; split => *; 2: smt().
    split; 1: smt ().
    by rewrite fdom_set fcardU fcard1; smt (fcard_ge0).
  + proc;conseq />.
    sp; if => //; wp => /=.
    conseq (_: ={rtd, PKSMK_HSM.RealSigServ.count_sig, PKSMK_HSM.RealSigServ.qs,
                 RealTrustService.parentTrust, RealTrustService.protectedTrusts, RealTrustService.count_chk, KMSProcedures.tklist}).
    + move=> /> /#.
    by sim.
  + proc; conseq />.
    seq 2 2 : (#pre /\ ={rtd, key}); 1: by auto.
    if => //; wp => /=.
    conseq (_: ={rtd, PKSMK_HSM.RealSigServ.count_sig, PKSMK_HSM.RealSigServ.qs, KMSProcedures.handles, KMSProcedures.tklist}).
    + smt ().
    by sim.
  + proc;sim />. 
  + proc;sim />. 
  + proc;sim />. 
  auto => /> *.
  smt (lt0_q_gen fdom0 fcards0 ge0_q_inst  more_bounds).
qed.

op n_keygen :int.

axiom lt0_n_keygen : 0 < n_keygen.

axiom mu1_keygen x : mu1 (dmap keygen fst) x <= inv (n_keygen%r).

lemma bound_pos : 0%r <= ((q_tkmng + q_inst) * 2 * max_size)%r / n_keygen%r.
proof.
  apply StdOrder.RealOrder.divr_ge0; apply le_fromint;smt (more_bounds ge0_max_size ge0_q_inst lt0_n_keygen).
qed.

require import Mu_mem.

lemma mu_keygen_in (trusts : (Trust, bool) fmap) : 
   mu keygen (fun (x:keys) => card (fdom trusts) <= q_tkmng + q_inst /\ x.`1 \in rejected trusts) <= 
   ((q_tkmng + q_inst) * 2 * max_size)%r / GNPolicy.n_keygen%r.
proof.
  case: (card (fdom trusts) <= q_tkmng + q_inst) => /= *;last by rewrite mu0 bound_pos.
  have -> : mu keygen (fun (x : keys) => x.`1 \in rejected trusts) = 
            mu (dmap keygen fst) (mem (rejected trusts)).
  + by rewrite dmapE.
  apply: (StdOrder.RealOrder.ler_trans ( (card (rejected trusts))%r / GNPolicy.n_keygen%r)).
  + by apply mu_mem_le => *;apply mu1_keygen.
  apply StdOrder.RealOrder.ler_pmul2r.
  + apply StdOrder.RealOrder.invr_gt0;apply lt_fromint; apply lt0_n_keygen.
  rewrite /rejected foldE; apply le_fromint.
  pose f := (fun (trust : Trust) (s : OpId fset) =>
        if card (tquorum trust).`1 <= max_size /\ card (tops trust) <= max_size then
          (tquorum trust).`1 `|` tops trust `|` s
        else s).
  have : forall l, card (foldr f fset0 l) <= size l * (2 * max_size).
  + elim => /=; 1: by rewrite fcards0.
    move=> t l hrec. rewrite {1}/f.
    case: (card (tquorum t).`1 <= max_size /\ card (tops t) <= max_size).
    + rewrite !fcardU;smt (fcard_ge0).
    smt (ge0_max_size).
  smt (ge0_max_size cardE).  
qed.

local lemma aux2 &m : 
  Pr[Main'.main () @ &m : BadIO.bad /\ KMSProcedures.count_ops <= PKSMK_Pol.q_gen] <=
  PKSMK_Pol.q_gen%r * (((q_tkmng + q_inst) * 2 * max_size)%r / n_keygen%r).
proof.
  fel 2 KMSProcedures.count_ops
    (fun x => ((q_tkmng + q_inst) * 2 * max_size)%r / n_keygen%r)
    q_gen
    BadIO.bad
    [KMSProcedures3pre(AdvIO(BadIO').OA).newOp: (KMSProcedures.count_ops < q_gen)]
    (true) => //.
  + by rewrite StdBigop.Bigreal.sumr_const count_predT size_range; smt (lt0_q_gen).
  + by inline *; auto.
  + proc.
    rcondt 2; 1: by auto.
    inline *.
    sp; wp; if; 2: by hoare => //;smt (bound_pos).
    sp;wp; if.
    + wp;sp; if => //;wp => /=.
      + rcondt 2; 1: by auto;smt().
        wp;conseq (_ : card (fdom IOperatorActions.trusts) <= q_tkmng + q_inst /\
                      pk \in rejected IOperatorActions.trusts).
        + by move=> &hr />.
        rnd (fun (p:keys) => card (fdom IOperatorActions.trusts) <= q_tkmng + q_inst /\
                    p.`1 \in rejected IOperatorActions.trusts); auto=> /> *.
        by apply mu_keygen_in.
      conseq (_ : card (fdom IOperatorActions.trusts) <= q_tkmng + q_inst /\
                   opid2 \in rejected IOperatorActions.trusts).
      + by move=> &hr />.
      rnd (fun (p:keys) => card (fdom IOperatorActions.trusts) <= q_tkmng + q_inst /\
             p.`1 \in rejected IOperatorActions.trusts); skip=> /> *.
      by apply mu_keygen_in.
    hoare => //; smt (bound_pos).    
  + move=> c;proc.
    rcondt 2; 1: by auto.
    wp;conseq (_:true) => //= /#.
  by move=> b c; proc; rcondf 2; auto.
qed.



lemma Mastertheorem &m :
  Pr[KMSRoR(A, HSMs(PKSMK_HSM.RealSigServ), HstS(OpPolSrv(OAR)), OpPolSrv(OAR)).main() @ &m : res] <=

     1%r / 2%r + 

     Pr[MRPKE.MRPKE_Sec(Bhop3(A, IOperatorActions(IOA(RealSigServ)))).game(false) @ &m : res] -
       Pr[MRPKE.MRPKE_Sec(Bhop3(A, IOperatorActions(IOA(RealSigServ)))).game(true) @ &m : res] +

     q_hid%r * ((q_hid + q_tkmng + q_tkmng) * max_size)%r / MainTh.n_keygen%r +

     q_gen%r * (((q_tkmng + q_inst) * 2 * max_size)%r / n_keygen%r) +

     Pr[CR(MkAdvCR(Bhop1(B_idealtrust(MainTh.Bhop2(A))))).main() @ &m : res] +
  
     q_gen%r * Pr[UF1(MkAdvUF1(MkAdvUF(Bhop2(B_idealtrust(MainTh.Bhop2(A)))))).main() @ &m : res] + 

     PKSMK_HSM.q_gen%r * 
      Pr[PKSMK_HSM.UF1(UF1_UF_HSM.MkAdvUF1(PKSMK_HSM.MkAdvUF(MainTh.Bhop1(A,IOA(RealSigServ))))).main()@ &m : res].
proof.
  have := mastertheorem OAR A A_ll _ _ _ _ _ &m.
  + islossless. + islossless. + islossless. + islossless.
  + by exists isGoodInitialTrust; apply op_oa.
  have := main_theorem_full &m (<:MainTh.Bhop2(A)) _.
  + move=> O *; islossless.
    + by apply (A_ll (KMSProceduresHop2(O))); islossless.
  have /= := ror3pre_io &m.  
  have := aux1 &m.
  have := aux2 &m.
  have := UF1_UF_HSM.ind_uf1 (MainTh.Bhop1(A, IOA(RealSigServ))) &m _.
  + move=> O keygen_ll sign_ll pkeys_ll verify_ll.
    + islossless.
      by apply (A_ll (KMSProcedures(HSMsI(O), HstSP(O, OpPolSrv(IOA(RealSigServ))), OpPolSrv(IOA(RealSigServ)))));
      islossless; apply DBool.dbool_ll.
  smt ().
qed.

end section.

end GNPolicy.
