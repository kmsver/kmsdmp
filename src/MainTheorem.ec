require import AllCore Distr Dexcepted Real RealExtra List FSet SmtMap.
require import Finite.
require (*--*) FinType.
require (*--*) RndExcept.
require (*--*) KMSProto_final.

abstract theory MainTh.

clone include KMSProto_final.Hop_final.
import KMSProto.
import KMSIND.
import PKSMK_HSM.
import Policy.
import MRPKE.

section Proof.

declare module OA : OperatorActions {IOperatorActions, HSMs_MRPKEOr, MRPKE_lor,KMSProcedures3pre, KMSProcedures3,TrustOracles, RealTrustService, HstSP, RealSigServ, KMSProcedures,
                          HSMs, HstPolSrv,OpPolSrv,HstS, HSMsI}.

declare module A : KMSAdv {IOperatorActions, HSMs_MRPKEOr, MRPKE_lor, KMSProcedures3pre, KMSProcedures3, TrustOracles, RealTrustService, HSMsI,OA, KMSProcedures, HstSP, RealSigServ, IdealSigServ,HSMs, HstPolSrv,OpPolSrv,HstS}.

axiom A_ll (O<:KMSOracles) :
  islossless O.newOp =>
  islossless O.requestAuthorization =>
  islossless O.newHst =>
  islossless O.installInitialTrust =>
  islossless O.installUpdatedTrust =>
  islossless O.newHSM =>
  islossless O.newToken =>
  islossless O.updateTokenTrust =>
  islossless O.addTokenKey =>
  islossless O.unwrap =>
  islossless O.corrupt =>
  islossless O.test =>
  islossless A(O).guess.

axiom init_ll : islossless OA.init.
axiom newOp_ll : islossless OA.newOp.
axiom request_ll : islossless OA.requestAuthorization.
axiom isGood_ll : islossless OA.isGoodInitialTrust.

(* This axiom is proved for concrete policy GNPolicy, instantiation is in GNPolicy.ec *)
axiom op_oa : exists (isGoodInitialTrust : (glob OA) -> Trust -> bool),
              forall (gOA : glob OA) (t : Trust),
                phoare [ OA.isGoodInitialTrust : t = trust /\ (glob OA) = gOA ==> res = isGoodInitialTrust gOA t /\ (glob OA) = gOA ] = 1%r.

lemma mastertheorem &m:
 Pr [  KMSRoR(A,HSMs(RealSigServ),HstS(OpPolSrv(OA)),OpPolSrv(OA)).main() @ &m : res ] <=
 1%r/2%r + 
 (* mrpke advantage *)
 Pr [ MRPKE_Sec(Bhop3(A,IOperatorActions(OA))).game(false) @ &m : res] -  
 Pr [ MRPKE_Sec(Bhop3(A,IOperatorActions(OA))).game(true) @ &m : res] +
 (* statistical distance caused by key collisions *)
 KMSIND.q_hid%r * ((KMSIND.q_hid + KMSIND.q_tkmng + KMSIND.q_tkmng) * KMSIND.max_size)%r / n_keygen%r + 
 (* This is replaced with a concrete term once GNPolicy is instantiated *)
 Pr[KMSRoR3pre(A, OA).main() @ &m : res] - 
 Pr[KMSRoR3pre(A, IOperatorActions(OA)).main() @ &m : res] +
 (* Policy advantage *)
 Pr [ TrustSecInd(Bhop2(A),RealTrustService(OpPolSrv(OA))).main() @ &m : res] -
 Pr [ TrustSecInd(Bhop2(A),IdealTrustService(OpPolSrv(OA))).main() @ &m : res] +
 (* Signature advantage *)
 Pr [ PKSMK_HSM.IndSig(RealSigServ,Bhop1(A,OA)).main() @ &m : res] -
 Pr [ PKSMK_HSM.IndSig(IdealSigServ,Bhop1(A,OA)).main() @ &m : res].
proof.
  have H0 := concl3 OA A A_ll init_ll newOp_ll request_ll isGood_ll op_oa &m.
  have := hop4 A (IOperatorActions(OA)).
  have := hop3 OA A &m .
  have := noadv A (IOperatorActions(OA)) &m.
  smt ().
qed.

end section Proof.

end MainTh.
