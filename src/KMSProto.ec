require import AllCore List FSet SmtMap Distr DBool Real RealExtra.
require PKSMK.
require MRPKE.
require KMSIND.

abstract theory KMSProto.

clone import MRPKE.

clone import PKSMK as PKSMK_HSM.

type HId = pkey * Pk.

type Authorized = HId fset.
type OpId.
type OpSk.
type HstId.
type Trust.
op tr_data(t : Trust) = t.   
op tr_mems : Trust -> Authorized.    
op tr_initial :Trust -> bool.
type Genuine = HId fset.
type HonestOps = OpId fset.
type Request.
type Authorization.
type Authorizations = (OpId,Authorization) fmap.
op valid_request : Genuine -> Request -> bool.
op newHst : HstId distr.
axiom newHst_ll : is_lossless newHst.
op checkTrustProgress(old : Trust, new : Trust) : bool.
op checkTrustUpdate (old new: Trust) (auth : Authorizations): bool.

type Key.
type Handle.

op genKey : Key distr.
axiom genKey_ll : is_lossless genKey.
hint solve 2 random : newHst_ll genKey_ll.

type EKeys = MCTxt.

type TkWrapped = {
  tkw_ekeys : EKeys;
  tkw_signer : HId;
  tkw_sig : signature;
}.

op proj_pks (hids: HId fset) : MPk = image snd hids.
op encode_tag(t : Trust) : Tag.
op decode_tag(tag : Tag) : Trust.
axiom taginj : forall t,
     t = decode_tag (encode_tag t).
lemma tagdiff : forall t t',
    t <> t' <=> encode_tag t <> encode_tag t'
by smt (taginj).

type Installable = bool.

op encode_msg : Trust * EKeys * HId * Installable -> message.
axiom encode_msg_inj t1 t2 k1 k2 h1 h2 i1 i2 :
   encode_msg (t1,k1,h1,i1) = encode_msg (t2,k2,h2,i2) => 
       (t1 = t2 /\ k1 = k2 /\ h1 = h2 /\ i1 = i2).

op checkToken(inst : Installable, old new : Trust, tkw : TkWrapped) =
        let sid = tkw.`tkw_signer in 
        let cphs = tkw.`tkw_ekeys in
        let msg = encode_msg  (new,cphs,sid,inst)  in
        let sig = tkw.`tkw_sig in
        let check = verify(sid.`1, msg, sig) in
            (check && (proj_pks (tr_mems old)) = fdom cphs  && (sid \in tr_mems old)).

clone import KMSIND.KMSIND with 
   type HId <- HId,
   type Policy.OpId <- OpId,
   type Policy.OpSk <- OpSk,
   type Policy.HstId <- HstId,
   type Policy.Trust <- Trust,
   type Policy.TrustData <- Trust,
   op Policy.tr_data <- tr_data,   
   op Policy.tr_mems <- tr_mems,
   op Policy.tr_initial <- tr_initial,
   type Policy.Request <- Request,
   type Policy.Authorization <- Authorization,
   op Policy.newHst <- newHst,
   op Policy.checkTrustProgress <- checkTrustProgress,
   op Policy.checkTrustUpdate <- checkTrustUpdate,
   op Policy.valid_request<- valid_request,
   type Key <- Key,
   type Handle <- Handle,
   op genKey <- genKey,
   type TkWrapped <- TkWrapped,
   op checkToken <- checkToken
   proof genKey_ll by apply genKey_ll.
import KMSIND.Policy.

op recover_token : message -> signature -> Token option.

axiom recover_encode m sig: 
  recover_token m sig <> None => 
     let tk = oget (recover_token m sig) in
     m = (encode_msg (tk.`tk_trust, tk.`tk_wdata.`tkw_ekeys, tk.`tk_wdata.`tkw_signer, tk.`tk_updt)) /\
     sig = tk.`tk_wdata.`tkw_sig.

axiom encode_recover tk: 
   recover_token (encode_msg (tk.`tk_trust, tk.`tk_wdata.`tkw_ekeys, tk.`tk_wdata.`tkw_signer, tk.`tk_updt)) tk.`tk_wdata.`tkw_sig = Some tk.
 
op encode_ptxt : Keys -> PTxt.
op decode_ptxt : PTxt -> Keys.
axiom encdec_ptxt  ks : decode_ptxt (encode_ptxt ks) = ks.

op hid_in (hid: HId) (ekeys: (pkey,(Pk*Sk)) fmap) : bool = 
          hid.`1 \in ekeys /\ hid.`2 = (oget ekeys.[hid.`1]).`1.

op hid_in_nopk (hid: HId) (ekeys: (pkey,Pk) fmap) : bool = 
          hid.`1 \in ekeys /\ hid.`2 = (oget ekeys.[hid.`1]).

module HSMs(SigSch : PKSMK_HSM.FullSigService) = {
   (* [who] maps encryption keys to signature verification keys *)
   var who : (Pk, pkey) fmap
   (* [ekeys] maps verification keys to encryption key-pairs *)
   var benc_keys : (pkey,(Pk*Sk)) fmap

   proc init() : unit = {
       SigSch.init();
       who <- empty;
       benc_keys <- empty;
   }

   proc gen() : HId = {
        var verk,pk, sk;
        (sk,pk) <$ gen;
        if (pk \in who) {
          verk <- oget who.[pk];
        } else {
          verk <@ SigSch.keygen();
          if (verk \in benc_keys) {
            (pk, sk) <- oget benc_keys.[verk];
          } else {
            who.[pk] <- verk;
            benc_keys.[verk] <- (pk,sk);
          }
        }
        return (verk,pk);
   }

   proc checkTrustUpdate(old : TkData, new : Trust, 
                         auth : Authorizations) : TkData option = {
        var c;
        c <@ HSMPolSrv.checkTrustUpdate(old.`td_trust,new,auth);
        return if (c) 
               then Some (set_trust new old)
               else None;
   }

   proc wrap(hid : HId,td : TkData) : Token = {
        var mem,tk,tkw,keys,ekeys,sig,msg,tag,ptxt,inst;
        inst <- td.`td_updt;
        mem <- tr_mems td.`td_trust;
        keys <- td.`td_skeys;
        tag <- encode_tag td.`td_trust;
        ptxt <- encode_ptxt keys;
        ekeys <$ mencrypt (proj_pks mem) tag ptxt;
        msg<-encode_msg (td.`td_trust,ekeys,hid,inst);
        sig <@ SigSch.sign(hid.`1,msg);
        tkw <- {| tkw_ekeys = ekeys; 
                  tkw_signer = hid; 
                  tkw_sig = oget sig |};
        tk <- {| tk_updt = td.`td_updt;
                 tk_trust = td.`td_trust;
                 tk_wdata = tkw; |};
        return tk;
   }

   proc unwrap(hid : HId, tk : Token) : TkData option = {
        var check,optxt,rtd,cph,cphs,tag,dkey,trust,inst;
        rtd <- None;
        trust <- tk.`tk_trust;
        inst <- tk.`tk_updt;
        check <- checkToken inst trust trust tk.`tk_wdata;
        if (check && hid \in tr_mems trust && hid_in hid benc_keys) {
           dkey <- (oget benc_keys.[hid.`1]).`2;
           tag <- encode_tag tk.`tk_trust;
           cphs <-tk.`tk_wdata.`tkw_ekeys;
           cph <- oget (cphs.[(oget benc_keys.[hid.`1]).`1]);
           optxt <- decrypt dkey tag cph;
           if (optxt<>None) {
              rtd <- Some {| td_updt = inst;
                             td_trust = trust;
                             td_skeys = decode_ptxt (oget optxt) |};
           }
        }
        return rtd;
   }
}.

end KMSProto.
