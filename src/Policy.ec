require import AllCore Distr FSet SmtMap List.

(**************************************)
(* Abstract DMP policy                *)
(**************************************)
theory Policy.

type HId.
type Authorized = HId fset.


type OpId.
type OpSk.
type HstId.

type Trust.
type TrustData.

op tr_data : Trust -> TrustData.

op tr_mems : TrustData -> Authorized.

op tr_initial : TrustData -> bool.

type Genuine = HId fset.

type Request.
type Authorization.
type Authorizations = (OpId,Authorization) fmap.

module type OperatorActions = {
  proc init() : unit
  proc newOp(badOp : bool) : OpId option * OpSk option
  proc requestAuthorization(request : Request, 
                            opid : OpId) : Authorization option
  proc isGoodInitialTrust(trust : TrustData) : bool
}.

op valid_request : Genuine -> Request -> bool.

op all_genuine (genuine : Genuine) (trust : TrustData) =
   forall m, m \in tr_mems trust =>  m \in genuine.

module type OpPolSrvT = {
   proc init() : unit
   proc newOp(badOp : bool) : OpId option * OpSk option
   proc addHId(hid : HId) : unit
   proc requestAuthorization(request : Request, 
                             opid : OpId) : Authorization option
   proc isGoodInitialTrust(trust : TrustData) : bool

   proc quorumAssumption(trust:Trust) : bool
   proc allGenuine(trust:TrustData) : bool
}.

op quorum_assumption (badOps : OpId fset) (goodOps : OpId fset) (t : Trust) : bool.

module OpPolSrv(OA : OperatorActions) : OpPolSrvT = {
   var genuine : Genuine
   var goodOps : OpId fset
   var badOps : OpId fset

   proc init() = {
     OA.init();
     genuine <- fset0;
     goodOps <- fset0;
     badOps <- fset0;
   } 

   proc newOp(badOp : bool) : OpId option * OpSk option = { 
     var opid;
     opid <@ OA.newOp(badOp);
     goodOps <- if !badOp then (goodOps `|` fset1 (oget opid.`1)) else goodOps;
     badOps <- if badOp then (badOps `|` fset1 (oget opid.`1)) else badOps;
     return opid;
   }

   proc addHId(hid : HId) : unit = {
     if (!(hid \in genuine)) {
       genuine <- genuine `|` fset1 hid;
     }
   }

   proc requestAuthorization(request : Request, 
                             opid : OpId) : Authorization option = {
     var auth;
     auth <- None;
     if (valid_request genuine request) {
       auth <@ OA.requestAuthorization(request,opid);
     }
     return auth;
   }

   proc isGoodInitialTrust(trust : TrustData) : bool = {
     var b;
     b <@ OA.isGoodInitialTrust(trust);
     return b && tr_initial trust && all_genuine genuine trust;
   }

   proc quorumAssumption (trust: Trust) = {
     return quorum_assumption badOps goodOps trust;
   }

   proc allGenuine(trust:TrustData) = {
     return all_genuine genuine trust;
   }
}.

op newHst : HstId distr.
axiom newHst_ll : is_lossless newHst.
hint solve 2 random : newHst_ll.

op checkTrustProgress(old : Trust, new : Trust) : bool.

module HstPolSrv = {
   var hosts_tr: (HstId,Trust) fmap 

   proc init() : unit = {
       hosts_tr <- empty;
   }
   proc newHst() : HstId = {
       var hstid;
       hstid <$ newHst;
       return hstid;
   }
   proc installInitialTrust(hstid : HstId, trust : Trust) : bool = {
       var b;
       b <- false;
       if (tr_initial (tr_data trust)) { 
          hosts_tr.[hstid] <- trust;
          b <- true;
       }
       return b;
   }
   proc installUpdatedTrust(hstid : HstId, new : Trust) : bool = {
       var b;
       b <- hstid \in hosts_tr && !tr_initial (tr_data new) &&
                checkTrustProgress (oget (hosts_tr.[hstid])) new;
       if (b) {
          hosts_tr.[hstid] <- new;
       }
       return b;
   }
   proc isInstalledTrust(hstid : HstId, trust : Trust) : bool = {
       return Some trust = hosts_tr.[hstid];
   }
   proc getInstalledTrust(hstid : HstId) : Trust option = {
       return hosts_tr.[hstid];
   }
   
}.

op checkTrustUpdate (old : Trust) (new: TrustData) (auth : Authorizations): bool.

module HSMPolSrv = {
  proc checkTrustUpdate(old : Trust, new : Trust, auth : Authorizations) : bool = {
      return !tr_initial (tr_data new) && checkTrustUpdate old (tr_data new) auth;
  }
}.

module type TrustService = {
   proc init() : unit

   proc newOp(badOp : bool) : OpId option * OpSk option
   proc addHId(hid : HId) : unit
   proc requestAuthorization(request : Request, 
                             opid : OpId) : Authorization option
   proc isGoodInitialTrust(trust : Trust) : bool

   proc newHst() : HstId
   proc installInitialTrust(hstid : HstId, trust : Trust) : bool
   proc installUpdatedTrust(hstid : HstId, new : Trust) : bool
   proc isInstalledTrust(hstid : HstId, trust : Trust) : bool
   proc getInstalledTrust(hstid : HstId) : Trust option

   proc checkTrustUpdate(old : Trust, new : Trust, 
                                 auth : Authorizations) : bool

   proc isProtectedTrust(trust : Trust) : bool
   proc quorumAssumption(trust:Trust) : bool
   proc allGenuine(trust:TrustData) : bool  
}.

op q_ops : int.
op q_auth : int.
op q_hst : int.
op q_updt : int.
op q_chk : int.

module RealTrustService(OA : OpPolSrvT) : TrustService = {
   module HA = HstPolSrv
   module HSMA = HSMPolSrv
   var count_ops : int
   var count_auth : int
   var count_hst : int
   var count_updt : int
   var count_chk : int

   var protectedTrusts : Trust fset
   var parentTrust : (Trust,Trust) fmap

   proc init() : unit = { 
           count_ops <- 0;
           count_auth <- 0;
           count_hst <- 0;
           count_updt <- 0;
           count_chk <- 0;
           protectedTrusts <- fset0; 
           parentTrust <- empty;
           OA.init(); 
           HA.init();
   }

   proc newOp(badOp : bool) = {
      var opid;
      opid <- witness;
      if (count_ops < q_ops) {
         opid <@ OA.newOp(badOp);
         count_ops <- count_ops + 1;
      }
      return opid;
   }

   proc addHId = OA.addHId
   proc requestAuthorization(request : Request, 
                             opid : OpId)  = {
      var auth;
      auth <- witness;
      if (count_auth < q_auth) {
         auth <@ OA.requestAuthorization(request,opid);
         count_auth <- count_auth + 1;
      }
      return auth;
   }

   proc isGoodInitialTrust(trust : Trust) = {
       var b;
       b <@ OA.isGoodInitialTrust(tr_data trust);
       if (b) {
          protectedTrusts <- protectedTrusts `|` fset1 trust;
       }
       return b;
   }

   proc newHst() = {
      var hstid;
      hstid <- witness;
      if (count_hst < q_hst) {
         hstid <@ HA.newHst();
         count_hst <- count_hst + 1;
      }
      return hstid;
   }

   proc installInitialTrust = HA.installInitialTrust

   proc installUpdatedTrust(hstid : HstId, new : Trust) = {
      var b;
      b <- false;
      if (count_updt < q_updt) {
         b <@ HA.installUpdatedTrust(hstid, new);
         count_updt <- count_updt + 1;
      }
      return b;
   }

   proc isInstalledTrust = HA.isInstalledTrust

   proc getInstalledTrust = HA.getInstalledTrust

   proc checkTrustUpdate(old : Trust, new : Trust, 
                         auth : Authorizations) : bool = {
        var c, protected, qa;
        c <- false;
        qa <@ OA.quorumAssumption(new);
        if (count_chk < q_chk && qa) {
           c <@ HSMA.checkTrustUpdate(old,new,auth);
           protected <- old \in protectedTrusts;
           if (c) {
              if (!new \in parentTrust) {
                  parentTrust.[new] <- old; (* might be non-injective *)
              }
              if (protected) {
                  protectedTrusts <- protectedTrusts `|` fset1 new;  
              }
           }
           count_chk <- count_chk + 1;
        }
        return c;
   }

   proc isProtectedTrust(trust : Trust) : bool = {
        return trust \in protectedTrusts;
   }
   proc quorumAssumption = OA.quorumAssumption
   proc allGenuine = OA.allGenuine
}.

module IdealTrustService(OA: OpPolSrvT) : TrustService = {
   module RT = RealTrustService(OA)
   module HA = HstPolSrv
   module HSMA = HSMPolSrv

   
   proc init = RT.init

   proc newOp = RT.newOp
   proc addHId = RT.addHId
   proc isGoodInitialTrust = RT.isGoodInitialTrust
   proc requestAuthorization = RT.requestAuthorization 
   proc quorumAssumption = RT.quorumAssumption
   proc allGenuine = RT.allGenuine
   proc newHst = RT.newHst
   proc installInitialTrust = RT.installInitialTrust

   proc installUpdatedTrust(hstid : HstId, new : Trust) : bool = {
       var b, registered;
       b <- false;
       if (RT.count_updt < q_updt) {
          registered <- new \in RealTrustService.parentTrust;
          b <- hstid \in HA.hosts_tr && !tr_initial (tr_data new) &&
               if !registered 
               then checkTrustProgress (oget (HA.hosts_tr.[hstid])) new
               else checkTrustProgress (oget (HA.hosts_tr.[hstid])) new 
                    && HA.hosts_tr.[hstid] = 
                       RealTrustService.parentTrust.[new];
          if (b) {
             HA.hosts_tr.[hstid] <- new;
          }
          RT.count_updt <- RT.count_updt + 1;
       }
       return b;
   }
   proc isInstalledTrust = RT.isInstalledTrust
   proc getInstalledTrust = RT.getInstalledTrust

   proc checkTrustUpdate(old : Trust, new : Trust, 
                         auth : Authorizations) : bool = {
        var c, protected, goodo, goodn, qa;
        c <- false;
        qa <@ OA.quorumAssumption(new);
        if (RT.count_chk < q_chk && qa) {
           c <@ HSMA.checkTrustUpdate(old,new,auth);
           goodn <- all_genuine OpPolSrv.genuine (tr_data new);
           goodo <- all_genuine OpPolSrv.genuine (tr_data old);
           c <- c && !(new \in RT.parentTrust && Some old <>  RT.parentTrust.[new]);
           protected <- old \in RT.protectedTrusts && goodo;
           c <- c && !((protected && !goodn)||(new \in RT.protectedTrusts && goodn && !protected));
           if (c) {
              RT.parentTrust.[new] <- old;
              if (protected && goodn) {
                 RT.protectedTrusts <- RT.protectedTrusts `|` fset1 new; 
              } 
           }
           RT.count_chk <- RT.count_chk + 1;
        }
        return c;
   }
   

   proc isProtectedTrust(trust : Trust) : bool = {
        var good,protected;
        good <- all_genuine OpPolSrv.genuine (tr_data trust);
        protected <- trust \in RT.protectedTrusts;
        return good && protected;
   }
}.

module type TrustOraclesT = {
   proc newOp(badOp : bool) : OpId option * OpSk option
   proc addHId(hid : HId) : unit
   proc requestAuthorization(request : Request, 
                             opid : OpId) : Authorization option
   proc isGoodInitialTrust(trust : Trust) : bool
   proc checkTrustUpdate(old : Trust, new : Trust, 
                         auth : Authorizations) : bool

   proc newHst() : HstId
   proc installInitialTrust(hstid : HstId, trust : Trust) : bool
   proc installUpdatedTrust(hstid : HstId, new : Trust) : bool
   proc isInstalledTrust(hstid : HstId, trust : Trust) : bool
   proc getInstalledTrust(hstid : HstId) : Trust option
   proc quorumAssumption(trust:Trust) : bool
   proc allGenuine(trust:TrustData) : bool

   proc isProtectedTrust(trust : Trust) : bool

   proc break_brokeTrustProgress(hstid :HstId, new : Trust) : unit
   proc break_brokeTrustUpdate(old new : Trust, 
                                auth : Authorizations) : unit
   proc break_corruptedProtectedTrust(trust : Trust) : unit
}.

op q_bprog : int.
op q_bupdt : int.

module TrustOracles(OA : OpPolSrvT) : TrustOraclesT = {
   var count_bprog : int
   var count_bupdt : int

   module TSrv = RealTrustService(OA)

   var broken : bool

   proc init() : unit = { 
          broken <- false; 
          count_bprog <- 0;
          count_bupdt <- 0;
          TSrv.init(); 
   }

   proc newOp = TSrv.newOp
   proc addHId = TSrv.addHId
   proc requestAuthorization = TSrv.requestAuthorization 
   proc isGoodInitialTrust = TSrv.isGoodInitialTrust

   proc newHst = TSrv.newHst
   proc installInitialTrust = TSrv.installInitialTrust
   proc installUpdatedTrust = TSrv.installUpdatedTrust
   proc isInstalledTrust = TSrv.isInstalledTrust
   proc getInstalledTrust = TSrv.getInstalledTrust
   proc quorumAssumption = TSrv.quorumAssumption
   proc allGenuine = TSrv.allGenuine

   proc checkTrustUpdate = TSrv.checkTrustUpdate

   proc isProtectedTrust = TSrv.isProtectedTrust

   proc break_brokeTrustProgress(hstid :HstId, new : Trust) : unit = {
       var registered, check;
       if (count_bprog < q_bprog) {
          registered <- new \in RealTrustService.parentTrust;
          check <- hstid \in HstPolSrv.hosts_tr && !tr_initial (tr_data new) &&
                   checkTrustProgress (oget (HstPolSrv.hosts_tr.[hstid])) new;
          broken <- broken || (registered && check            
                    && (!(HstPolSrv.hosts_tr.[hstid] = 
                          RealTrustService.parentTrust.[new])));
          count_bprog <- count_bprog + 1;
       }
   }

   proc break_brokeTrustUpdate(old : Trust, new : Trust, 
                               auth : Authorizations) : unit = {
       var c, protected, goodo, goodn, bad2,bad3,bad1;
       if (count_bupdt < q_bupdt && quorum_assumption  OpPolSrv.badOps OpPolSrv.goodOps new) {
          goodn <- all_genuine OpPolSrv.genuine (tr_data new);
          goodo <- all_genuine OpPolSrv.genuine (tr_data old);
          c <@ HSMPolSrv.checkTrustUpdate(old,new,auth); 
          protected <- old \in RealTrustService.protectedTrusts && goodo;
          bad1 <- old \in RealTrustService.protectedTrusts && !goodo;
          bad2 <- c && (new \in RealTrustService.parentTrust && 
                       Some old <>  RealTrustService.parentTrust.[new]);
          bad3 <- c && ((protected && !goodn)||(new \in RealTrustService.protectedTrusts && goodn && !protected));
          broken <- broken || bad1 || bad2 || bad3;
          count_bupdt <- count_bupdt + 1;
       }
   }

   proc break_corruptedProtectedTrust(trust : Trust) : unit = {
        var good,protected;
        good <- all_genuine OpPolSrv.genuine (tr_data trust);
        protected <- trust \in RealTrustService.protectedTrusts;
        broken <- broken || (!good && protected);
   }

}.

module type TrustAdv(TO : TrustOraclesT) = {
   proc break_trust() : unit
}.

module TrustSec(A : TrustAdv, OA : OpPolSrvT) = {
   module TO = TrustOracles(OA)
   module A = A(TO)

   proc main() : bool = {
       TO.init();
       A.break_trust();
       return TO.broken;
   }
}.

module type TrustOraclesI = {
   proc newOp(badOp : bool) : OpId option * OpSk option
   proc addHId(hid : HId) : unit
   proc requestAuthorization(request : Request, 
                             opid : OpId) : Authorization option
   proc isGoodInitialTrust(trust : Trust) : bool
   proc newHst() : HstId
   proc installInitialTrust(hstid : HstId, trust : Trust) : bool
   proc installUpdatedTrust(hstid : HstId, new : Trust) : bool
   proc isInstalledTrust(hstid : HstId, trust : Trust) : bool
   proc getInstalledTrust(hstid : HstId) : Trust option
   proc checkTrustUpdate(old : Trust, new : Trust, 
                         auth : Authorizations) : bool
   proc isProtectedTrust(trust : Trust) : bool
   proc quorumAssumption(trust:Trust) : bool
   proc allGenuine(trust:TrustData) : bool  
}.

module type TrustAdvInd(O:TrustOraclesI) = {
  proc main(): bool
}.


module TrustSecInd(A :TrustAdvInd, TSrv : TrustService) = {
   module A = A(TSrv)
   proc main() : bool = {
       var r;
       TSrv.init();
       r <@ A.main();
       return r;
   }
}.

module B_idealtrust_OO (TO: TrustOraclesT) : TrustOraclesI = {
   proc newOp = TO.newOp
   proc addHId = TO.addHId
   proc requestAuthorization = TO.requestAuthorization
   proc isGoodInitialTrust = TO.isGoodInitialTrust
   proc newHst = TO.newHst
   proc installInitialTrust = TO.installInitialTrust
   proc quorumAssumption = TO.quorumAssumption
   proc allGenuine = TO.allGenuine

   proc installUpdatedTrust(hstid : HstId, new : Trust) : bool = {
        var b;
        TO.break_brokeTrustProgress(hstid,new);
        b <@ TO.installUpdatedTrust(hstid,new);
        return b;
   }
   proc isInstalledTrust= TO.isInstalledTrust
   proc getInstalledTrust= TO.getInstalledTrust

   proc checkTrustUpdate(old : Trust, new : Trust, 
                         auth : Authorizations) : bool = {
        var b;
        TO.break_brokeTrustUpdate(old,new,auth);
        b <@ TO.checkTrustUpdate(old,new,auth);
        return b;
   }

   proc isProtectedTrust(trust : Trust) : bool = {
        var b;
        TO.break_corruptedProtectedTrust(trust);
        b <@ TO.isProtectedTrust(trust);
        return b;
   }

}.
  
module B_idealtrust(A : TrustAdvInd, TO : TrustOraclesT) = {
  proc break_trust () : unit = {
      A(B_idealtrust_OO(TO)).main();
  }    
}.


module Wrap(OA : OpPolSrvT, O : TrustService)  = {
   proc init = TrustOracles(OA).init
   proc newOp = O.newOp
   proc addHId = O.addHId
   proc requestAuthorization = O.requestAuthorization
   proc isGoodInitialTrust = O.isGoodInitialTrust
   proc quorumAssumption = OA.quorumAssumption
   proc allGenuine = OA.allGenuine

   proc newHst = O.newHst
   proc installInitialTrust = O.installInitialTrust
   proc installUpdatedTrust(hstid : HstId, new : Trust) : bool = {
        var b;
        TrustOracles(OA).break_brokeTrustProgress(hstid,new);
        b <@ O.installUpdatedTrust(hstid,new);
        return b;
   }
   proc isInstalledTrust= O.isInstalledTrust
   proc getInstalledTrust= O.getInstalledTrust

   proc checkTrustUpdate(old : Trust, new : Trust, 
                         auth : Authorizations) : bool = {
        var b;
        TrustOracles(OA).break_brokeTrustUpdate(old,new,auth);
        b <@ O.checkTrustUpdate(old,new,auth);
        return b;
   }

   proc isProtectedTrust(trust : Trust) : bool = {
        var b;
        TrustOracles(OA).break_corruptedProtectedTrust(trust);
        b <@ O.isProtectedTrust(trust);
        return b;
  }

}.

(*****************************************)
(*                  PROOF                *)
(*****************************************)

axiom inst_bound: q_updt = q_bprog.
axiom upd_bound: q_chk = q_bupdt.

section PROOFS.

  declare module OA : OperatorActions {OpPolSrv, HSMPolSrv, HstPolSrv, TrustOracles }.
  declare module A  : TrustAdvInd {OA, OpPolSrv, HSMPolSrv, HstPolSrv, TrustOracles }.

  axiom A_ll (O <: TrustOraclesI) :
    islossless O.newOp =>
    islossless O.addHId =>
    islossless O.requestAuthorization =>
    islossless O.isGoodInitialTrust =>
    islossless O.newHst =>
    islossless O.installInitialTrust =>
    islossless O.installUpdatedTrust =>
    islossless O.isInstalledTrust =>
    islossless O.getInstalledTrust =>
    islossless O.checkTrustUpdate => islossless O.isProtectedTrust => 
    islossless O.quorumAssumption => islossless O.allGenuine =>
    islossless A(O).main.

  axiom init_ll  : islossless OA.init.
  axiom newOp_ll : islossless OA.newOp.
  axiom requestAuthorization_ll : islossless OA.requestAuthorization.
  axiom isGoodInitialTrust_ll : islossless OA.isGoodInitialTrust.

  hint solve 2 lossless : A_ll init_ll newOp_ll requestAuthorization_ll isGoodInitialTrust_ll.

lemma RealIdealTrust  &m :
     `| Pr [ TrustSecInd(A,IdealTrustService(OpPolSrv(OA))).main() @ &m : res ]
      - Pr [ TrustSecInd(A,RealTrustService(OpPolSrv(OA))).main() @ &m : res ] |
      <=  Pr [ TrustSec(B_idealtrust(A),OpPolSrv(OA)).main() @ &m : res ].
proof.
(*************************************************)
cut -> : Pr[TrustSecInd(A, IdealTrustService(OpPolSrv(OA))).main() @ &m : res] =
         Pr[TrustSecInd(A, Wrap(OpPolSrv(OA),IdealTrustService(OpPolSrv(OA)))).main() @ &m : res].
+ byequiv => //.
  proc; call(_ : ={glob OA, glob RealTrustService, glob OpPolSrv, glob HstPolSrv}); 
   1..10,12..13: by proc;inline *;sim.
  + by proc;inline *;wp;skip. 
  (* init *)
  by inline *; wp;call(_:true);wp;skip.
(************************************************)
cut -> : Pr[TrustSecInd(A,RealTrustService(OpPolSrv(OA))).main() @ &m : res] =
         Pr[TrustSecInd(A, Wrap(OpPolSrv(OA),RealTrustService(OpPolSrv(OA)))).main() @ &m : res].
+ byequiv => //.
  proc; call(_ : ={glob OA, glob RealTrustService, glob OpPolSrv, glob HSMPolSrv, glob HstPolSrv}); 1..10,12..13: by proc;inline *;sim.
  + by proc;inline *;wp;skip. 
  (* init *)
  by inline *;wp;call(_:true);wp;skip.
(************************************************)
cut -> : Pr [ TrustSec(B_idealtrust(A), OpPolSrv(OA)).main() @ &m : res ] =
         Pr [ TrustSecInd(A,Wrap(OpPolSrv(OA),RealTrustService(OpPolSrv(OA)))).main() @ &m : TrustOracles.broken].
+ byequiv => //.
  proc;inline *.
  call(_ : ={glob OA, glob RealTrustService, glob OpPolSrv, glob HSMPolSrv, glob TrustOracles, glob HstPolSrv}); 1..13: by  proc;inline *;sim.
  by wp;call(_:true);wp;skip.
(************************************************)
byequiv :  (TrustOracles.broken); [| by auto | by move=> /> /# ].
proc;inline *.
seq 15 15 : (
 (={glob A, glob OA, glob RealTrustService, glob OpPolSrv, glob HSMPolSrv,glob TrustOracles, glob HstPolSrv} /\ 
    !TrustOracles.broken{1} /\ 
    TrustOracles.count_bprog{2} = RealTrustService.count_updt{2} /\ 
    TrustOracles.count_bupdt{2} = RealTrustService.count_chk{2})).
+ by wp;call(_:true);wp;skip;progress.
(************************************************)
call (_: TrustOracles.broken, ={glob OA, glob RealTrustService, glob OpPolSrv, 
                                glob HSMPolSrv, glob TrustOracles, glob HstPolSrv}/\ 
         TrustOracles.count_bprog{2} = RealTrustService.count_updt{2}  /\ TrustOracles.count_bupdt{2} = RealTrustService.count_chk{2},={TrustOracles.broken}).
+ by move=> O; apply (A_ll O).
(************************************************)
(* newOp *)
+ by proc*;inline *; sp;if;auto => />; wp;call(_:true);wp;skip;progress => /#.  
+ by move => *; conseq />; islossless; apply newOp_ll.
+ by move=> *; conseq />; islossless; apply newOp_ll.
(* addHId *)
+ by move => *;proc *;inline*;wp;skip;progress.
+ by move => *;conseq />; islossless.
+ by move => *;conseq />; islossless.
(* requestAuthorization *)
+ move => *;proc *;inline*.
  sp;if; auto => />. 
  sp;if => //=; first by progress => /#.
  by wp;call(_:true);skip;progress.
+ by move=> *;conseq />; islossless; apply requestAuthorization_ll.
+ by move=> *;conseq />; islossless; apply requestAuthorization_ll.
(* isGoodInitialTrust *)
+ by move => *;proc *;inline*;wp;call(_:true);wp;skip;progress.
+ by move=> *;conseq />; islossless; apply isGoodInitialTrust_ll.
+ by move=> *;conseq />; islossless; apply isGoodInitialTrust_ll.
(* newHst *)
+ by move => *;proc *;inline*;sp;if;auto => />;wp;rnd;skip.
+ by move=> *;conseq />; islossless; apply newHst_ll.
+ by move=> *;conseq />; islossless; apply newHst_ll.
(* installInitialTrust *)
by move => *;proc;inline*;wp;skip.
by move=> *; conseq />; islossless.
by move=> *; conseq />; islossless.
(* installUpdatedTrust *)
by move => *;proc *;inline*;wp;skip=> />;  smt(inst_bound upd_bound). 
by move => *;proc *;inline*;wp;skip => /> /#.
by move => *;proc *;inline*;wp;skip => /> /#.
(* isInstalledTrust *)
by move => *;proc *;inline*;wp;skip=> /> /#.
by move => *; conseq />; islossless.
by move => *; conseq />; islossless.
(* getInstalledTrust *)
by move => *;proc *;inline*;wp;skip=> /> /#.
by move => *; conseq />; islossless.
by move => *; conseq />; islossless.
(* checkTrustUpdate *)
by move => *;proc *;inline*;wp;skip => />;smt(inst_bound upd_bound @SmtMap).
by move => *;proc *;inline*;wp;skip => /> /#.
by move => *;proc *;inline*;wp;skip => /> /#.
(* is protected trust *)
by proc;inline *;wp;skip => /#.
by move => *;proc;inline *;wp;skip => /#.
by move => *;proc;inline *;wp;skip => /#.
(* quorumAssumption *)
by move => *;proc;inline*;wp;skip.
by move=> *; conseq />; islossless.
by move=> *; conseq />; islossless.
(* allGenuine *)
by move => *;proc;inline*;wp;skip.
by move=> *; conseq />; islossless.
by move=> *; conseq />; islossless.

by skip => /#.
qed.

(*****************************************************)

module IOperatorActions(OA : OperatorActions) = {
   var trusts : (TrustData , bool) fmap

   include OA [-init,isGoodInitialTrust]

   proc init() : unit = {
       OA.init();
       trusts <- empty;
   }

   proc isGoodInitialTrust(trust : TrustData) : bool = {
      var b;

      if (!(trust \in trusts)) {
         b <@ OA.isGoodInitialTrust(trust);
         trusts.[trust] <- b;
      }
      return oget trusts.[trust];
   } 
}.

end section PROOFS.

end Policy.
 