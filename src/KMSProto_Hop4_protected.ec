require import AllCore List FSet SmtMap Distr DBool Dexcepted Real RealExtra.
require KMSProto_Hop3_KeyCols.

abstract theory Hop4.

clone include KMSProto_Hop3_KeyCols.Hop3.
import KMSProto.
import KMSIND.
import PKSMK_HSM.
import Policy.
import MRPKE.

(******************************************************)
(*  Slice protected trusts                            *)  
(******************************************************)

module KMSProcedures3(OA : OperatorActions) : KMSOracles = {
  module HSMsPTS = HSMsP_TS(IdealTrustService(OpPolSrv(OA)))
  module HstSPTS = HstSP_TS(IdealTrustService(OpPolSrv(OA))) 
  module KP = KMSProcedures3pre(OA)
  include KP [-newToken,updateTokenTrust,addTokenKey,test,init,corrupt]

  var wrapped_keys : (Token, Keys) fmap

  proc init() = {
     HSMsPTS.init();
     HstSPTS.init();
     IdealTrustService(OpPolSrv(OA)).init();
     KMSProcedures.hids <- fset0;
     KMSProcedures.tklist   <- fset0;
     KMSProcedures.corrupted <- fset0;
     KMSProcedures.tested <- empty;
     KMSProcedures.handles <- fset0;
     KMSProcedures.b         <$ {0,1};
     KMSProcedures.count_ops <- 0;
     KMSProcedures.count_auth <- 0;
     KMSProcedures.count_hst <- 0;
     KMSProcedures.count_updt <- 0;
     KMSProcedures.count_hid <- 0;
     KMSProcedures.count_tkmng <- 0;
     KMSProcedures.count_unw <- 0;
     KMSProcedures.count_corr <- 0;
     KMSProcedures.count_test <- 0;
     KMSProcedures.count_inst <- 0;
     wrapped_keys <- empty;
  }

  proc newToken(hid : HId, new : Trust) : Token option = {
      var rtd,tk,td;
      rtd <- None;
      if (KMSProcedures.count_tkmng < q_tkmng /\ 
          size (elems (tr_mems (tr_data new))) < max_size) {
         if (hid \in KMSProcedures.hids && tr_initial new) {
            td <- {| td_updt = true;
                     td_trust = new;
                     td_skeys = empty; |};
            tk <@ HSMsPTS.wrap(hid,td);
            rtd <- Some tk;
            KMSProcedures.tklist <- KMSProcedures.tklist `|` fset1 tk;
            wrapped_keys.[tk] <- empty;
            IdealTrustService(OpPolSrv(OA)).isGoodInitialTrust(new);
         }
         KMSProcedures.count_tkmng <- KMSProcedures.count_tkmng + 1;
      }
      return rtd; 
  } 

  proc updateTokenTrust(hid : HId, new_trust : Trust,
                       auth : Authorizations, tk : Token) : Token option = {
      var tdo,tdoo,tkn,rtd,protected,check,optxt,trust,tkw,sid,cphs,msg,sig,inst;
      rtd <- None;
      if (KMSProcedures.count_tkmng < q_tkmng /\ 
          size (elems (tr_mems (tr_data new_trust))) < max_size /\
          size (elems (tr_mems (tr_data tk.`tk_trust))) < max_size) {
         if (hid \in KMSProcedures.hids) {
            tdo <- None;
            protected <@ IdealTrustService(OpPolSrv(OA)).isProtectedTrust(tk.`tk_trust);
            if (protected) {
               inst <- tk.`tk_updt;
               trust <- tk.`tk_trust;
               tkw <- tk.`tk_wdata;
               sid <- tkw.`tkw_signer;
               cphs <- tkw.`tkw_ekeys;
               msg <- encode_msg  (trust,cphs,sid,inst);
               sig <- tkw.`tkw_sig;
               check <@ IdealSigServ.verify(sid.`1, msg, sig);
               check <- check && (proj_pks (tr_mems trust)) = fdom cphs &&  (sid \in tr_mems trust);
               if (check && hid \in tr_mems trust &&  hid_in hid HSMs.benc_keys) {
                  optxt <- Some (encode_ptxt (oget KMSProcedures3.wrapped_keys.[tk]));
                  tdo <- Some {| td_updt = inst;
                                 td_trust = trust;
                                 td_skeys = decode_ptxt (oget optxt) |};
 
               }
            }
            else{ 
               tdo <@ HSMsPTS.unwrap(hid, tk); 
            }
            if (tdo <> None /\ quorum_assumption OpPolSrv.badOps OpPolSrv.goodOps new_trust) {
               tdoo <@ HSMsPTS.checkTrustUpdate(oget tdo,new_trust,auth);
               if (tdoo <> None) {
                 (* We now split the treatment of protected tokens *)
                 protected <@ IdealTrustService(OpPolSrv(OA)).isProtectedTrust((oget tdoo).`td_trust);
                 if (protected) {
                    tkn <@ HSMsPTS.wrap(hid,(oget tdoo));
                    wrapped_keys.[tkn] <- (oget tdoo).`td_skeys;
                 }
                 else {
                    tkn <@ HSMsPTS.wrap(hid,(oget tdoo));
                 }
                 KMSProcedures.tklist <- KMSProcedures.tklist `|` fset1 tkn;
                 rtd <- Some tkn;
              } 
            }  
         } 
         KMSProcedures.count_tkmng <- KMSProcedures.count_tkmng + 1;
      }
      return rtd;
  }

  proc addTokenKey(hid : HId, hdl : Handle, 
                tk : Token) : Token option = {
      var tdo,tdoo, td,rtd,key, tkr, protected,check,optxt,trust,tkw,sid,cphs,msg,sig,inst;
      rtd <- None;
      key <$ genKey;
      if (KMSProcedures.count_tkmng < q_tkmng /\ size (elems (tr_mems (tr_data tk.`tk_trust))) < max_size ) {
         if ((hid \in KMSProcedures.hids)&&(!(hdl \in KMSProcedures.handles))) {
            tdo <- None;
            protected <@ IdealTrustService(OpPolSrv(OA)).isProtectedTrust(tk.`tk_trust);
            if (protected) {
               inst <- tk.`tk_updt;
               trust <- tk.`tk_trust;
               tkw <- tk.`tk_wdata;
               sid <- tkw.`tkw_signer;
               cphs <- tkw.`tkw_ekeys;
               msg <- encode_msg  (trust,cphs,sid,inst);
               sig <- tkw.`tkw_sig;
               check <@ IdealSigServ.verify(sid.`1, msg, sig);
               check <- check && (proj_pks (tr_mems trust)) = fdom cphs &&  (sid \in tr_mems trust);
               if (check && hid \in tr_mems trust &&  hid_in hid HSMs.benc_keys) {
                  optxt <- Some (encode_ptxt (oget KMSProcedures3.wrapped_keys.[tk]));
                  tdo <- Some {| td_updt = inst;
                                 td_trust = trust;
                                 td_skeys = decode_ptxt (oget optxt) |};
               }
            }
            else{ 
               tdo <@ HSMsPTS.unwrap(hid, tk); 
            }
            if (tdo <> None) {
               td <- oget tdo;
               tdoo <- add_key hdl key td;
               if (tdoo <> None) {
                 (* We now split the treatment of protected tokens *)
                 protected <@ IdealTrustService(OpPolSrv(OA)).isProtectedTrust((oget tdoo).`td_trust);
                 if (protected) {
                   tkr <@ HSMsPTS.wrap(hid,(oget tdoo));
                    wrapped_keys.[tkr] <- (oget tdoo).`td_skeys;
                 }
                 else {
                    tkr <@ HSMsPTS.wrap(hid,(oget tdoo));
                 }
                 KMSProcedures.tklist <- KMSProcedures.tklist `|` fset1 tkr;
                 KMSProcedures.handles <- KMSProcedures.handles `|` fset1 hdl;
                 rtd <- Some tkr;
              } 
            }    
         }
         KMSProcedures.count_tkmng <- KMSProcedures.count_tkmng + 1;
      }
      return rtd;
  }

  proc corrupt(hid : HId, tk : Token, hdl : Handle) : Key option = {
      var rsk,tdo,trust,protected,check,optxt,tkw,sid,cphs,msg,sig,inst;
      rsk <- None;
      if (KMSProcedures.count_corr < q_corr) {
         if ((hid \in KMSProcedures.hids)&&(!(hdl \in KMSProcedures.tested))) {
            tdo <- None;
            protected <@ IdealTrustService(OpPolSrv(OA)).isProtectedTrust(tk.`tk_trust);
            if (protected) {
               inst <- tk.`tk_updt;
               trust <- tk.`tk_trust;
               tkw <- tk.`tk_wdata;
               sid <- tkw.`tkw_signer;
               cphs <- tkw.`tkw_ekeys;
               msg <- encode_msg  (trust,cphs,sid,inst);
               sig <- tkw.`tkw_sig;
               check <@ IdealSigServ.verify(sid.`1, msg, sig);
               check <- check && (proj_pks (tr_mems trust)) = fdom cphs &&  (sid \in tr_mems trust);
               if (check && hid \in tr_mems trust &&  hid_in hid HSMs.benc_keys) {
                  optxt <- Some (encode_ptxt (oget KMSProcedures3.wrapped_keys.[tk]));
                  tdo <- Some {| td_updt = inst;
                                 td_trust = trust;
                                 td_skeys = decode_ptxt (oget optxt) |};
 
               }
            }
            else{ 
               tdo <@ HSMsPTS.unwrap(hid, tk); 
            }
            if (tdo <> None && hdl \in (oget tdo).`td_skeys) {
                rsk <- (oget tdo).`td_skeys.[hdl];
                KMSProcedures.corrupted <- KMSProcedures.corrupted `|` fset1 hdl;
            }
         }
         KMSProcedures.count_corr <- KMSProcedures.count_corr + 1;
      }
      return rsk;    
  }  

  proc test(hid : HId, hstid : HstId, tk : Token, hdl : Handle) : Key option = {
      var rkey,installed,rsk,tdo,trust,protected,check,optxt,tkw,sid,cphs,msg,sig,inst;
      rsk <- None;
      rkey <$ genKey;
      if (KMSProcedures.count_test < q_test) {
         installed <@ HstSPTS.isInstalledTrust(hstid,tk);
         if ((hid \in KMSProcedures.hids)&&
             (!(hdl \in KMSProcedures.corrupted))&&
             installed) {
            tdo <- None;
            protected <@ IdealTrustService(OpPolSrv(OA)).isProtectedTrust(tk.`tk_trust);
            if (protected) {
               inst <- tk.`tk_updt;
               trust <- tk.`tk_trust;
               tkw <- tk.`tk_wdata;
               sid <- tkw.`tkw_signer;
               cphs <- tkw.`tkw_ekeys;
               msg <- encode_msg  (trust,cphs,sid,inst);
               sig <- tkw.`tkw_sig;
               check <@ IdealSigServ.verify(sid.`1, msg, sig);
               check <- check && (proj_pks (tr_mems trust)) = fdom cphs &&  (sid \in tr_mems trust);
               if (check && hid \in tr_mems trust &&  hid_in hid HSMs.benc_keys) {
                  optxt <- Some (encode_ptxt (oget KMSProcedures3.wrapped_keys.[tk]));
                  tdo <- Some {| td_updt = inst;
                                 td_trust = trust;
                                 td_skeys = decode_ptxt (oget optxt) |};
               }
            }
            if (tdo <> None && hdl \in (oget tdo).`td_skeys) {
                 rsk <- (oget tdo).`td_skeys.[hdl];
                 if (hdl \in KMSProcedures.tested) {
                       rkey <- oget KMSProcedures.tested.[hdl]; 
                 }
                 else {
                      KMSProcedures.tested.[hdl] <- rkey;
                 }
                 rsk <- if (KMSProcedures.b) then Some rkey else rsk; 
            }
         }
         KMSProcedures.count_test <- KMSProcedures.count_test + 1;
      }
      return rsk; 
  }

}.

module KMSRoR3(A : KMSAdv, OA : OperatorActions) = {
   module KMSProc = KMSProcedures3(OA)
   module A = A(KMSProc)

   proc main() : bool = {
        var b;
        KMSProc.init();
        b <@ A.guess();
        return (b = KMSProcedures.b) ;     
   }
}.

axiom corr_keys : forall pk sk1 sk2 c  t, 
   (sk1,pk) \in gen => (sk2,pk) \in gen => 
           decrypt sk1  t c = decrypt sk2  t c.

axiom bpke_keys : (forall ks pks ptxt tag , ks \in mencrypt pks tag ptxt =>
                 pks = fdom ks).

axiom corr_decr : forall ks pks ptxt tag pk sk, (sk,pk) \in gen => ks \in mencrypt pks tag ptxt => pk \in ks =>
                                       decrypt sk tag (oget ks.[pk]) = Some ptxt.

axiom encode_ptxtK : cancel encode_ptxt decode_ptxt.
axiom decode_ptxtK : cancel decode_ptxt encode_ptxt.

lemma injective_encode_msg : injective encode_msg.
proof. by move=> [????] [????] /encode_msg_inj. qed.

lemma injective_encode_tag: injective encode_tag.
proof. by apply (can_inj encode_tag decode_tag)=> t; rewrite -taginj. qed.

section.
declare module OA : OperatorActions {IOperatorActions, RealTrustService, RealSigServ, KMSProcedures, HSMs, HstPolSrv, OpPolSrv, HstS, HSMsI,  KMSProcedures3}.
declare module A : KMSAdv      {OA, IOperatorActions, RealTrustService, RealSigServ, KMSProcedures, HSMs, HstPolSrv, OpPolSrv, HstS, HSMsI,  KMSProcedures3}.

lemma hop3 &m :
   Pr [  KMSRoR3pre(A,IOperatorActions(OA)).main() @ &m : res ] =  
   Pr [  KMSRoR3(A,IOperatorActions(OA)).main() @ &m : res ].
proof.
  byequiv (_: ={glob A, glob OA} ==> _) => //=.
  proc; inline*.

  call (_: ={glob OA, glob IOperatorActions, glob RealSigServ, glob HSMs,
             glob HstPolSrv, glob RealTrustService, glob OpPolSrv, 
            glob KMSProcedures} /\
           (forall hid, hid \in OpPolSrv.genuine <=> hid \in KMSProcedures.hids){2} /\
           (forall hid, hid \in OpPolSrv.genuine => hid.`1 \in RealSigServ.pks_sks){2} /\
           (forall hid, hid \in OpPolSrv.genuine => hid.`1 \in HSMs.benc_keys){2} /\
           (forall hid, hid \in OpPolSrv.genuine => hid.`2 \in HSMs.who){2} /\
           (forall pk, pk \in HSMs.who => (oget HSMs.who.[pk],pk) \in OpPolSrv.genuine){2} /\
           (forall verk, verk \in HSMs.benc_keys => (oget HSMs.benc_keys.[verk]).`1 \in HSMs.who){2} /\
           (forall verk, verk \in HSMs.benc_keys => verk \in RealSigServ.pks_sks){2} /\
           (forall verk, verk \in HSMs.benc_keys => (verk,(oget HSMs.benc_keys.[verk]).`1) \in KMSProcedures.hids){2} /\
           (forall hid1 hid2, hid1 \in KMSProcedures.hids =>
                              hid2 \in KMSProcedures.hids =>
                              hid1 <> hid2 => 
                              hid1.`1 <> hid2.`1 /\ hid1.`2 <> hid2.`2){2} /\
           (forall vk, vk \in HSMs.benc_keys =>
                       ((oget HSMs.benc_keys.[vk]).`2, (oget HSMs.benc_keys.[vk]).`1) \in gen){2} /\
           (forall hid, hid \in OpPolSrv.genuine =>
                        (oget HSMs.benc_keys.[hid.`1]).`1 = hid.`2){2} /\
           (forall hstid, hstid \in HstPolSrv.hosts_tr =>
                oget HstPolSrv.hosts_tr.[hstid] \in 
                       RealTrustService.protectedTrusts){2} /\
           (forall t, t \in RealTrustService.protectedTrusts =>
                      all_genuine OpPolSrv.genuine t){2} /\

           (forall t, t \in RealTrustService.parentTrust => 
                       !tr_initial t){2} /\
           (forall (tk : Token), !tr_initial tk.`tk_trust =>
                tk.`tk_updt =>
                signedTk tk RealSigServ.qs =>
                    tk.`tk_trust \in RealTrustService.parentTrust){2} /\
           (forall tk,
                signedTk tk RealSigServ.qs =>
                     all_genuine OpPolSrv.genuine tk.`tk_trust =>
                         signedTr tk.`tk_trust RealSigServ.qs){2} /\
           (forall t, t \in RealTrustService.parentTrust => 
               all_genuine OpPolSrv.genuine 
                  (oget  RealTrustService.parentTrust.[t]) =>
                       signedTr (oget  RealTrustService.parentTrust.[t])
                             RealSigServ.qs){2} /\
           (forall t, t \in RealTrustService.protectedTrusts =>
                signedTr t RealSigServ.qs){2} /\
           (forall t, tr_initial t =>
                  signedTr t RealSigServ.qs =>
                   all_genuine OpPolSrv.genuine t =>
                   t \in IOperatorActions.trusts =>
                   oget IOperatorActions.trusts.[t] =>
                   t \in RealTrustService.protectedTrusts){2} /\
           (forall (tk : Token), tr_initial tk.`tk_trust =>
                  signedTk tk RealSigServ.qs =>
                   all_genuine OpPolSrv.genuine tk.`tk_trust =>
                   tk.`tk_trust \in IOperatorActions.trusts =>
                   oget IOperatorActions.trusts.[tk.`tk_trust] =>
                   signedTr tk.`tk_trust RealSigServ.qs){2} /\
           (forall t, !tr_initial t => 
               t \in RealTrustService.protectedTrusts => 
                    (t \in RealTrustService.parentTrust /\
                      oget  RealTrustService.parentTrust.[t] \in 
                           RealTrustService.protectedTrusts)){2} /\
           (forall t, t \in RealTrustService.parentTrust => 
               oget  RealTrustService.parentTrust.[t] \in 
                  RealTrustService.protectedTrusts =>
                    t \in RealTrustService.protectedTrusts){2} /\
           (forall (tk : Token), 
                signedTk tk RealSigServ.qs{2} =>
                 tk.`tk_trust \in RealTrustService.protectedTrusts{2} =>
            tk \in KMSProcedures3.wrapped_keys{2} /\   
            tk.`tk_wdata.`tkw_ekeys \in
                mencrypt (proj_pks (tr_mems tk.`tk_trust)) (encode_tag tk.`tk_trust)
                         (encode_ptxt (oget KMSProcedures3.wrapped_keys{2}.[tk]))) /\
           (forall (tk : Token), tr_initial tk.`tk_trust =>
                   all_genuine OpPolSrv.genuine tk.`tk_trust =>
                  signedTk tk RealSigServ.qs =>
                      tk.`tk_trust \in IOperatorActions.trusts) {2} /\
     (RealSigServ.count_sig <= KMSProcedures.count_tkmng){2}
); last first.

(* initialization *)
+ wp;rnd;wp;call (_:true);wp;skip; smt(in_fset0 emptyE). 
(* New op *)
+ by sim />.

(* request authorization *)
+ by sim />.

(* New hst *)
+ by sim />.

(* Install initial trust *)
+ proc; conseq />; inline *.
  sp; if; [done | | skip => //].
  sp 14 14.
  if; [smt() | | wp;skip => /# ].
  sp.
  seq 2 2 : (#pre /\ ={b2} /\ trust{2} \in IOperatorActions.trusts{2} /\ b2{2} = oget IOperatorActions.trusts{2}.[trust{2}]).
  + if;[done | | by auto].
    by wp;call(_:true);wp;skip; smt (get_setE). 
  conseq />. 
  seq 3 3 : (#post /\ KMSProcedures.count_inst{1} < q_inst /\ ={hstid0,b, hstid, trust} /\ (b => trust \in RealTrustService.protectedTrusts){1}).
  + auto => />; smt(in_fsetU1 mem_set get_setE oget_some).
  conseq />; if => //;auto => /> *; smt (get_setE).

(* Install updated trust *)
+ proc; conseq />; inline *.
  sp 1 1;if => //=.
  sp 3 3.
  if; [smt() | | wp;skip => /#].
  sp 13 13.
  if; [smt() | | wp;skip => /#].
  by wp; skip; smt(in_fsetU1 mem_set get_setE oget_some injective_encode_msg).

(* NEW HSM *)
+ proc; inline*. 
  sp;if => //=. if => //=.
  seq 1 1 : (#pre /\ ={sk,pk} /\ ((sk,pk) \in  gen){2}).
  + by rnd; skip => /#.
  if; [smt() | wp; skip => /# | ].
  seq 1 1 : (#pre /\ ={pksig,sksig} /\ ((pksig,sksig) \in keygen /\
                              (pksig,sksig) \in keygen \ ((fun (pksk : pkey*skey) => nosigpks RealSigServ.qs OpPolSrv.genuine RealTrustService.parentTrust pksk.`1))){2}).
  + by rnd; skip => />; smt(supp_dexcepted).
  if; [smt() | | ].
  + sp 2 2.
    if; [smt() | exfalso => /# | ].
    sp 3 3.           
    if; [smt() | | exfalso => /# ].
    sp 2 2.
    if; [smt() | | exfalso => /# ].
    wp; skip; progress; first 14 by smt(in_fsetU1 mem_set get_setE).
    + have /supp_dexcepted ? := H29.
      have :(!nosigpks RealSigServ.qs OpPolSrv.genuine RealTrustService.parentTrust pksig){2}.
      + smt(in_fsetU1 mem_set get_setE oget_some injective_encode_msg nosigpks_prop).
      have ? : !(pksig{2}, pk{2}) \in tr_mems tk.`tk_trust `\` OpPolSrv.genuine{2}.
      + smt (in_fsetU in_fset1 in_fsetD nosigpks_prop). 
      have ? : !(pksig{2}, pk{2}) \in tr_mems tk.`tk_trust `\` OpPolSrv.genuine{2}.
      + by move : H29 H33 H35 H36;rewrite /all_genuine; smt (in_fsetU in_fsetD in_fset1).
      have ? /#: all_genuine (OpPolSrv.genuine{2}) tk.`tk_trust. 
      by move : H34 H35; rewrite /all_genuine /signedTk; smt (in_fsetU in_fsetD in_fset1).
    + have /supp_dexcepted /= []?? := H29.
      have ? : !(pksig{2}, pk{2}) \in tr_mems (oget RealTrustService.parentTrust{2}.[t]) `\` OpPolSrv.genuine{2}.
      + rewrite nosigpks_prop in H37.  
        have cons : (
             (!((oget RealTrustService.parentTrust{2}.[t]) \in  
                   frng RealTrustService.parentTrust{2})) \/
             (!(pksig{2}, pk{2}) \in 
                 tr_mems (oget RealTrustService.parentTrust{2}.[t]) `\` 
               OpPolSrv.genuine{2})); smt(@SmtMap).
      smt (in_fsetU in_fsetD in_fset1).
    + have /supp_dexcepted /= []?? := H29.
      have ? : !(pksig{2}, pk{2}) \in tr_mems t `\` OpPolSrv.genuine{2}.
      + smt (in_fsetU1 mem_set get_setE oget_some injective_encode_msg nosigpks_prop).
      smt (in_fsetU in_fsetD in_fset1).
    + have /supp_dexcepted /= []?? := H29.
      have ? : !(pksig{2}, pk{2}) \in tr_mems tk.`tk_trust `\` OpPolSrv.genuine{2}.
      + smt (in_fsetU1 mem_set get_setE oget_some injective_encode_msg nosigpks_prop).
      smt (in_fsetU in_fsetD in_fset1).
    case (all_genuine OpPolSrv.genuine{2} tk.`tk_trust).
    + smt(in_fsetU1 mem_set get_setE).
    move => *; have /supp_dexcepted /= []?? := H29.
    move : H39; rewrite nosigpks_prop.
    smt(in_fsetU1 mem_set get_setE in_fsetD).
  sp 1 1.
  if; [smt() | wp; skip => /# | ].
  sp 3 3.
  if; [smt() | | exfalso => /#].
  sp 2 2.
  if; [smt() | | exfalso => /#].
  wp; skip; progress;first 14 by smt(in_fsetU1 mem_set get_setE).
  + have  /supp_dexcepted /= []?? := H29.
    have ? : !(pksig{2}, pk{2}) \in tr_mems tk.`tk_trust `\` OpPolSrv.genuine{2}.
    + smt (in_fsetU in_fset1 in_fsetD nosigpks_prop).
    smt (in_fsetU in_fset1 in_fsetD).
  + have  /supp_dexcepted /= []?? := H29.
    have ? : !(pksig{2}, pk{2}) \in tr_mems (oget RealTrustService.parentTrust{2}.[t]) `\` OpPolSrv.genuine{2}.
    + rewrite nosigpks_prop in H37. 
        have cons : (
             (!((oget RealTrustService.parentTrust{2}.[t]) \in  
                   frng RealTrustService.parentTrust{2})) \/
             (!(pksig{2}, pk{2}) \in 
                 tr_mems (oget RealTrustService.parentTrust{2}.[t]) `\` 
               OpPolSrv.genuine{2})); smt(@SmtMap).
    smt (in_fsetU in_fsetD in_fset1).
  + have  /supp_dexcepted /= []?? := H29.
    have ? : !(pksig{2}, pk{2}) \in tr_mems t `\` OpPolSrv.genuine{2}.
    + smt (in_fsetU1 mem_set get_setE oget_some injective_encode_msg nosigpks_prop).
    smt (in_fsetU in_fsetD in_fset1).
  + have /supp_dexcepted /= []?? := H29.
    have ? : !(pksig{2}, pk{2}) \in tr_mems tk.`tk_trust `\` OpPolSrv.genuine{2}.
    + smt (in_fsetU1 mem_set get_setE oget_some injective_encode_msg nosigpks_prop).
    smt (in_fsetU in_fsetD in_fset1).
  case (all_genuine OpPolSrv.genuine{2} tk.`tk_trust).
  + smt(in_fsetU1 mem_set get_setE).
  move => *; have /supp_dexcepted /= []?? := H29.
  move : H39; rewrite nosigpks_prop.
  smt(in_fsetU1 mem_set get_setE in_fsetD).

(* New Token *)
+ proc;inline *.
  sp;if => //=.
  sp; if; [smt() |  | wp;skip => /#]. 
  rcondt {1} 14; 1: by move => *;wp;rnd;wp;skip; smt (sig_bound).
  rcondt {2} 14; first by move => *;wp;rnd;wp;skip; smt (sig_bound).
  rcondt {1} 14; first by move => *;wp;rnd;wp;skip=> /#.
  rcondt {2} 14; first by move => *;wp;rnd;wp;skip=> /#.
  case (new{1} \notin IOperatorActions.trusts{1}).
  + rcondt{1} 27; first by auto => />.
    rcondt{2} 28; first by auto => />.
    wp;call(_:true);auto => />; smt(in_fsetU1 mem_set get_setE oget_some injective_encode_msg).
  rcondf{1} 27;  first by auto => />.
  rcondf{2} 28; first by auto => />. 
  wp;rnd;wp;rnd;wp;skip => /> *;split.
  + smt(in_fsetU1 mem_set get_setE oget_some injective_encode_msg).
    progress; [
      1..6 : by smt(in_fsetU1 mem_set get_setE oget_some injective_encode_msg)
    | 7 : by smt(in_fsetU1 mem_set get_setE injective_encode_msg)
    | 8.. : by smt(in_fsetU1 mem_set get_setE oget_some injective_encode_msg)].

(* Update Token Trust *)        
+ proc; sp; if => //=.
  if; [ smt() | | wp;skip => /# ].
  seq 1 3 : (#pre /\ ={tdo} /\ (tdo{2} <> None => 
             ((oget tdo{2}).`td_trust = tk{2}.`tk_trust) /\
             (tk{2}.`tk_wdata.`tkw_signer \in tr_mems tk{2}.`tk_trust) /\
             (tk{2}.`tk_wdata.`tkw_signer \in KMSProcedures.hids{2} => signedTk tk{2} RealSigServ.qs{2}))).
  + inline *.
    sp 0 5. 
    case (protected{2}); last first.
    + rcondf{2} 1; first by done. 
      by wp;skip => /#.
    rcondt{2} 1; first by done.
    wp;skip => /> *.
    + have ? : 
       (tk{2} \in KMSProcedures3.wrapped_keys{2}) /\
        tk{2}.`tk_wdata.`tkw_ekeys \in
           mencrypt (proj_pks (tr_mems tk{2}.`tk_trust)) (encode_tag tk{2}.`tk_trust)
             (encode_ptxt (oget KMSProcedures3.wrapped_keys{2}.[tk{2}])).
      + by apply (H21 tk{2}); smt(in_fsetU1 mem_set get_setE oget_some injective_encode_msg  corr_decr).
    smt(in_fsetU1 mem_set get_setE oget_some injective_encode_msg corr_decr decode_ptxtK encode_ptxtK 
             mem_image mem_fdom).
  inline{1} OpPolSrv(IOperatorActions(OA)).quorumAssumption.
  sp 2 0.
  if;[done | | wp; skip => /#].
  seq 1 1 : ( #{/~(forall (t : Trust), t \in RealTrustService.protectedTrusts{2} => signedTr t RealSigServ.qs{2})}pre /\
                  ={tdoo} /\ 
                 (tdoo{2}<>None => 
                       (oget tdoo{1} = set_trust new_trust{2} (oget tdo{2})) /\ 
                       new_trust{2} \in RealTrustService.parentTrust{2} /\ 
                       (oget tdo{2}).`td_trust = oget RealTrustService.parentTrust{2}.[new_trust{2}] /\ 
                       (all_genuine OpPolSrv.genuine{2} new_trust{2} /\ 
                          new_trust{2} \in RealTrustService.protectedTrusts{2} <=> 
                       all_genuine OpPolSrv.genuine{2} (oget tdo{2}).`td_trust  /\ 
                          (oget tdo{2}).`td_trust \in  RealTrustService.protectedTrusts{2}) /\ 
                   (forall t, (t \in RealTrustService.protectedTrusts{2} /\ t <> new_trust{2}) => 
                               signedTr t RealSigServ.qs{2})) /\
                   (tdoo{2} = None => forall t, (t \in RealTrustService.protectedTrusts{2}) =>
                                  signedTr t RealSigServ.qs{2})).
  + inline *; wp;skip=> /> *. 
      smt(in_fsetU1 mem_set get_setE oget_some injective_encode_msg  corr_decr decode_ptxtK encode_ptxtK).
  if => //=.
  + inline IdealTrustService(OpPolSrv(IOperatorActions(OA))).isProtectedTrust.
    sp;if{2}.
    + inline *. 
      rcondt {1} 13;first by move => *;wp;rnd;wp;skip;smt (sig_bound).
      rcondt {2} 13;first by move => *;wp;rnd;wp;skip;smt (sig_bound).
      rcondt {1} 13;first by move => *;wp;rnd;wp;skip => /#.
      rcondt {2} 13;first by move => *;wp;rnd;wp;skip => /#.
      wp;rnd;wp;rnd;wp;skip => /> *;split;progress;
      [
       1..3: by smt(in_fsetU1 mem_set get_setE injective_encode_msg decode_ptxtK encode_ptxtK)        | 5..6:  by smt(in_fsetU1 mem_set get_setE injective_encode_msg decode_ptxtK encode_ptxtK)
       | 7: by smt(in_fsetU1 mem_set get_setE injective_encode_msg decode_ptxtK encode_ptxtK)
       | 8..: by smt(in_fsetU1 mem_set get_setE injective_encode_msg decode_ptxtK encode_ptxtK)]. 

       rewrite /signedTr.
       move : (H30 H32) => [/ #] *. case (t = new_trust{2}); last by smt(in_fsetU1 mem_set get_setE injective_encode_msg decode_ptxtK encode_ptxtK).
       move => * //. rewrite H38.
       exists {| tk_trust = t; tk_updt = true; 
                 tk_wdata = {| tkw_ekeys = ekeysL; tkw_signer = hid{2}; tkw_sig = sL |} |}. 
          by smt(in_fsetU1 mem_set get_setE injective_encode_msg decode_ptxtK encode_ptxtK).
    inline *.
    rcondt {1} 13;first by move => *;wp;rnd;wp;skip;smt (sig_bound).
    rcondt {2} 13;first by move => *;wp;rnd;wp;skip;smt (sig_bound).
    case (hid.`1{2} \in RealSigServ.pks_sks{2}).
    + rcondt {1} 13;first by move => *;wp;rnd;wp;skip => /#.
      rcondt {2} 13;first by move => *;wp;rnd;wp;skip => /#.
      wp;rnd;wp;rnd;wp;skip;
        smt(in_fsetU1 mem_set get_setE injective_encode_msg decode_ptxtK encode_ptxtK).
    rcondf {1} 13;first by move => *;wp;rnd;wp;skip;progress => /#.
    rcondf {2} 13;first by move => *;wp;rnd;wp;skip;progress => /#.
    wp;rnd;wp;skip;smt(in_fsetU1 mem_set get_setE).
  wp;skip; smt(in_fsetU1 mem_set get_setE injective_encode_msg decode_ptxtK encode_ptxtK).
(* Add token key *)
+ proc; conseq />.
  sp;seq 1 1 : (#pre /\ ={key});first by rnd;skip;smt().
  if; [ smt() | | wp;skip => /# ].
  if; [ smt() | | wp;skip => /# ].
  seq 1 3 : (#pre /\ ={tdo} /\ (tdo{2} <> None => 
             ((oget tdo{2}).`td_trust = tk{2}.`tk_trust) /\
             (tk{2}.`tk_wdata.`tkw_signer \in tr_mems tk{2}.`tk_trust) /\
             (tk{2}.`tk_wdata.`tkw_signer \in KMSProcedures.hids{2} => signedTk tk{2} RealSigServ.qs{2}))).
  + inline *; conseq />.
    sp 0 5. 
    if{2}; wp;skip;last by smt().
    move=> /> *.
    have ? : (
       (tk{2} \in KMSProcedures3.wrapped_keys{2}) /\
       tk{2}.`tk_wdata.`tkw_ekeys \in
         mencrypt (proj_pks (tr_mems tk{2}.`tk_trust)) (encode_tag tk{2}.`tk_trust)
          (encode_ptxt (oget KMSProcedures3.wrapped_keys{2}.[tk{2}]))).
    + by apply (H21 tk{2});smt(in_fsetU1 mem_set get_setE oget_some injective_encode_msg  corr_decr).
    smt(in_fsetU1 mem_set get_setE oget_some injective_encode_msg corr_decr decode_ptxtK encode_ptxtK 
             mem_image mem_fdom).
  if => //=.
  + inline IdealTrustService(OpPolSrv(IOperatorActions(OA))).isProtectedTrust.
    sp;if => //=.
    + inline *.
      rcondt {1} 13;first by move => *;wp;rnd;wp;skip;smt (sig_bound).
      rcondt {1} 13;first by move => *;wp;rnd;wp;skip => /#.
      sp;if{2}.
      + rcondt {2} 13;first by move => *;wp;rnd;wp;skip;smt (sig_bound).
        rcondt {2} 13;first by move => *;wp;rnd;wp;skip => /#.
        auto => />;progress;
         smt(in_fsetU1 mem_set get_setE injective_encode_msg decode_ptxtK encode_ptxtK).
      rcondt {2} 13;first by move => *;wp;rnd;wp;skip;smt (sig_bound).
      rcondt {2} 13;first by move => *;wp;rnd;wp;skip => /#.
        auto => />;progress; try rewrite oget_some;
         smt(in_fsetU1 mem_set get_setE injective_encode_msg decode_ptxtK encode_ptxtK).
    by auto =>/>;smt(in_fsetU1 mem_set get_setE injective_encode_msg decode_ptxtK encode_ptxtK).
  by auto => />;smt(in_fsetU1 mem_set get_setE injective_encode_msg decode_ptxtK encode_ptxtK).

(* Unwrap *)
+ proc; inline*; auto => /#.

(* Corrupt *)
+ proc; inline*; conseq />. 
  sp 1 1.
  if; [done | | skip => //].
  if; [done | | wp;skip => //].
  seq 0 5 : (#pre /\ (tdo = None){2} /\ (trust0 = tk.`tk_trust){2} /\
              (good = all_genuine OpPolSrv.genuine (tr_data trust0)){2} /\
              (protected0 = trust0 \in IdealTrustService.RT.protectedTrusts){2} /\
              (protected = (good && protected0)){2}); 1: by auto.
  if{2}; last by auto. 
  sp 15 12.
  if; [done | | sp 1 0; if; [done | exfalso => /# | by auto ]].
  sp 5 2.    
  rcondt{1} 1. 
  + move => &m0; skip => /> *.
    have ? : (
       (tk{m0} \in KMSProcedures3.wrapped_keys{m0}) /\
        tk{m0}.`tk_wdata.`tkw_ekeys \in
           mencrypt (proj_pks (tr_mems tk{m0}.`tk_trust)) (encode_tag tk{m0}.`tk_trust)
             (encode_ptxt (oget KMSProcedures3.wrapped_keys{m0}.[tk{m0}]))).
    + smt(in_fsetU1 mem_set get_setE oget_some injective_encode_msg  corr_decr).
    smt(in_fsetU1 mem_set get_setE oget_some injective_encode_msg corr_decr decode_ptxtK encode_ptxtK 
             mem_image mem_fdom).
  sp 2 0.
  if. 
  + move=> *; have ? : (
       (tk{2} \in KMSProcedures3.wrapped_keys{2}) /\
      tk{2}.`tk_wdata.`tkw_ekeys \in
       mencrypt (proj_pks (tr_mems tk{2}.`tk_trust)) (encode_tag tk{2}.`tk_trust)
         (encode_ptxt (oget KMSProcedures3.wrapped_keys{2}.[tk{2}]))).
    + smt(in_fsetU1 mem_set get_setE oget_some injective_encode_msg  corr_decr).
    smt(in_fsetU1 mem_set get_setE oget_some injective_encode_msg corr_decr decode_ptxtK encode_ptxtK 
             mem_image mem_fdom).
  + wp; skip => /> *.
    have ? : (
       (tk{2} \in KMSProcedures3.wrapped_keys{2}) /\
      tk{2}.`tk_wdata.`tkw_ekeys \in
       mencrypt (proj_pks (tr_mems tk{2}.`tk_trust)) (encode_tag tk{2}.`tk_trust)
         (encode_ptxt (oget KMSProcedures3.wrapped_keys{2}.[tk{2}]))).
    + smt(in_fsetU1 mem_set get_setE oget_some injective_encode_msg  corr_decr).
    smt(in_fsetU1 mem_set get_setE oget_some injective_encode_msg corr_decr decode_ptxtK encode_ptxtK 
             mem_image mem_fdom).
  wp;skip;smt(in_fsetU1 mem_set get_setE oget_some injective_encode_msg  corr_decr decode_ptxtK encode_ptxtK).

(* Test *)
+ proc; conseq />; inline *.
  sp;seq 1 1 : (#pre /\ ={rkey}); first by rnd;skip.
  if; [done | | skip => //].
  sp 6 6.
  if; [done | | wp;skip => //].
  sp 0 5.
  if{2}; last first.
  + sp 15 0.
    if{1}; [ | sp 1 0; if; [smt() | exfalso => /# | wp;skip => /#] ].
    rcondf{1} 6.
    + move => &m0; wp; skip => /> *.
      have ? /#: tk{m0}.`tk_trust \in RealTrustService.protectedTrusts{m0}. 
      have ? : tk{m0}.`tk_trust = oget HstPolSrv.hosts_tr{m0}.[hstid{m0}]; smt().
    + by sp 6 0; if; [smt() | exfalso => /# | wp;skip => /#].
    sp 15 12.
    if; [smt() | | sp 1 0; if; [smt() | exfalso => /# | wp;skip => /#]].
    rcondt{1} 6.    
    + move => &m0.
      wp; skip; progress.
      have ? : (
       (tk{m0} \in KMSProcedures3.wrapped_keys{m0}) /\
         tk{m0}.`tk_wdata.`tkw_ekeys \in
           mencrypt (proj_pks (tr_mems tk{m0}.`tk_trust)) (encode_tag tk{m0}.`tk_trust)
           (encode_ptxt (oget KMSProcedures3.wrapped_keys{m0}.[tk{m0}]))).
      + smt(in_fsetU1 mem_set get_setE oget_some injective_encode_msg  corr_decr).
      smt(in_fsetU1 mem_set get_setE oget_some injective_encode_msg corr_decr decode_ptxtK encode_ptxtK 
             mem_image mem_fdom).
    sp 7 2.
    if; [ | | wp;skip => /#].
    + move=> /> *; have ? : (
       (tk{2} \in KMSProcedures3.wrapped_keys{2}) /\
        tk{2}.`tk_wdata.`tkw_ekeys \in
         mencrypt (proj_pks (tr_mems tk{2}.`tk_trust)) (encode_tag tk{2}.`tk_trust)
          (encode_ptxt (oget KMSProcedures3.wrapped_keys{2}.[tk{2}]))).
      + smt(in_fsetU1 mem_set get_setE oget_some injective_encode_msg  corr_decr).
      smt(in_fsetU1 mem_set get_setE oget_some injective_encode_msg corr_decr decode_ptxtK encode_ptxtK 
             mem_image mem_fdom).
    wp;skip => /> *; case (KMSProcedures.b{2}) => // *.
    have ? : (
       (tk{2} \in KMSProcedures3.wrapped_keys{2}) /\
      tk{2}.`tk_wdata.`tkw_ekeys \in
       mencrypt (proj_pks (tr_mems tk{2}.`tk_trust)) (encode_tag tk{2}.`tk_trust)
         (encode_ptxt (oget KMSProcedures3.wrapped_keys{2}.[tk{2}]))).
    + smt(in_fsetU1 mem_set get_setE oget_some injective_encode_msg  corr_decr).
    smt(in_fsetU1 mem_set get_setE oget_some injective_encode_msg corr_decr decode_ptxtK encode_ptxtK 
             mem_image mem_fdom).
qed.

end section.

end Hop4.
