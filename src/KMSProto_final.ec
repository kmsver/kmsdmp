require import AllCore List FSet SmtMap Distr DBool Dexcepted Real RealExtra.
require import PROM. 

require KMSProto_Hop5_mrenc.

abstract theory Hop_final.

clone include KMSProto_Hop5_mrenc.Hop5.
import KMSProto.
import KMSIND.
import PKSMK_HSM.
import Policy.
import MRPKE.

hint solve 2 random : newHst_ll genKey_ll.

axiom witness_map ['a 'b] : witness = empty<:'a,'b>.

section PROOFS.

  declare module A : KMSAdv {TrustOracles, RealSigServ, HSMs, KMSProcedures,OpPolSrv,HstPolSrv, KMSProcedures3}.

  declare module OA : OperatorActions {A, TrustOracles, RealSigServ, KMSProcedures, HSMs,OpPolSrv,HstPolSrv, KMSProcedures3}.

  local clone import GenEager as Eager
    with type from <- Handle,
         type to   <- Key,
         op sampleto <- fun _ => genKey,
          type input <- unit,
         type output <- bool
         proof sampleto_ll by (move=> ?;apply genKey_ll).

  local module KMSProcedures5(RO:FRO) : KMSOracles = {
    module HSMsPTS = HSMsP_TS(IdealTrustService(OpPolSrv(OA)))
    module HstSPTS = HstSP_TS(IdealTrustService(OpPolSrv(OA))) 
    include KMSProcedures4(OA) [-newToken,addTokenKey,test,corrupt]

    proc newToken(hid : HId, new : Trust) : Token option = {
      var rtd,tk,td;
      rtd <- None;
      if (KMSProcedures.count_tkmng < q_tkmng /\ 
          size (elems (tr_mems (tr_data new))) < max_size) {
         if (hid \in KMSProcedures.hids && tr_initial new) {
            td <- {| td_updt = true;
                     td_trust = new;
                     td_skeys = empty; |};
            tk <@ HSMsPTS.wrap(hid,td);
            rtd <- Some tk;
            KMSProcedures.tklist <- KMSProcedures.tklist `|` fset1 tk;
            KMSProcedures3.wrapped_keys.[tk] <- empty;
            IdealTrustService(OpPolSrv(OA)).isGoodInitialTrust(new);
         }
         KMSProcedures.count_tkmng <- KMSProcedures.count_tkmng + 1;
      }
      return rtd; 
    }

    proc corrupt(hid : HId, tk : Token, hdl : Handle) : Key option = {
      var (*cond,*) rsk1, rsk,tdo,trust,protected,check,tkw,sid,cphs,msg,sig, inst;
      rsk <- None;
      if (KMSProcedures.count_corr < q_corr) {
         if ((hid \in KMSProcedures.hids)&&(!(hdl \in KMSProcedures.tested))) {
            tdo <- None;
            protected <@ IdealTrustService(OpPolSrv(OA)).isProtectedTrust(tk.`tk_trust);
            if (protected) {
              inst <- tk.`tk_updt;
              trust <- tk.`tk_trust;
              tkw <- tk.`tk_wdata;
              sid <- tkw.`tkw_signer;
              cphs <- tkw.`tkw_ekeys;
              msg <- encode_msg  (trust,cphs,sid, inst);
              sig <- tkw.`tkw_sig;
              check <@ IdealSigServ.verify(sid.`1, msg, sig);
              check <- check && (proj_pks (tr_mems trust)) = fdom cphs &&  (sid \in tr_mems trust);
              if (check && hid \in tr_mems trust && hid_in hid HSMs.benc_keys) {
                 if (hdl \in (oget KMSProcedures3.wrapped_keys.[tk])) {
                  (* cond <@ RO.in_dom (hdl, Unknown);
                   if (cond) {
                     rsk1 <@ RO.get(hdl);
                     rsk <- Some rsk1;
                   }*)
                   rsk1 <@ RO.get(hdl);
                   rsk <- Some rsk1;
                   KMSProcedures.corrupted <- KMSProcedures.corrupted `|` fset1 hdl;
                 }
               }
            }
            else{ 
               tdo <@ HSMsPTS.unwrap(hid, tk); 
               if (tdo <> None && hdl \in (oget tdo).`td_skeys) {
                 rsk <- (oget tdo).`td_skeys.[hdl];
                 KMSProcedures.corrupted <- KMSProcedures.corrupted `|` fset1 hdl;
               }
            }
         }
         KMSProcedures.count_corr <- KMSProcedures.count_corr + 1;
      }
      return rsk;    
    }  

    proc test(hid : HId, hstid : HstId, tk : Token, hdl : Handle) : Key option = {
      var isin, rkey, rsk, rsk', installed, protected, inst, trust, tkw, sid, cphs, msg, sig, check;
      rsk <- None;
      rkey <$ genKey;
      if (KMSProcedures.count_test < q_test) {
         installed <@ HstSPTS.isInstalledTrust(hstid,tk);
         if ((hid \in KMSProcedures.hids)&&
             (!(hdl \in KMSProcedures.corrupted))&&
             installed) {
           (* We now reject unprotected tokens *)
           protected <@ IdealTrustService(OpPolSrv(OA)).isProtectedTrust(tk.`tk_trust);
           if (protected) {
             inst <- tk.`tk_updt;
             trust <- tk.`tk_trust;
             tkw <- tk.`tk_wdata;
             sid <- tkw.`tkw_signer;
             cphs <- tkw.`tkw_ekeys;
             msg <- encode_msg  (trust,cphs,sid,inst);
             sig <- tkw.`tkw_sig;
             check <@ IdealSigServ.verify(sid.`1, msg, sig);
             check <- check && (proj_pks (tr_mems trust)) = fdom cphs &&  (sid \in tr_mems trust);

             if (check && hid \in tr_mems trust &&  hid_in hid HSMs.benc_keys) {
               if (hdl \in (oget KMSProcedures3.wrapped_keys.[tk])) { 
               
                 if (hdl \in KMSProcedures.tested) {
                   rsk <- KMSProcedures.tested.[hdl]; 
                 } else {
                   isin <@  RO.in_dom(hdl, Unknown);
                   if (isin) {
                      rsk' <@ RO.get(hdl);
                      rsk <- if (KMSProcedures.b) then Some rkey else Some rsk'; 
                      KMSProcedures.tested.[hdl] <- oget rsk;
                   }
                 }
               }
             }
           }
         }
         KMSProcedures.count_test <- KMSProcedures.count_test + 1;
      }
      return rsk; 
    }

    proc addTokenKey(hid : HId, hdl : Handle, 
                   tk : Token) : Token option = {
      var keys1, tdo,tdoo, td,rtd,key, tkr, protected,check,trust,tkw,
          sid,cphs,msg,sig,mem,tag,fake,ekeys,sigo,inst;
      rtd <- None;
      key <$ genKey; 
      if (KMSProcedures.count_tkmng < q_tkmng /\ 
          size (elems (tr_mems (tr_data tk.`tk_trust))) < max_size ) {
         if ((hid \in KMSProcedures.hids)&&(!(hdl \in KMSProcedures.handles))) {
           tdo <- None;
           protected <@ IdealTrustService(OpPolSrv(OA)).isProtectedTrust(tk.`tk_trust);
           if (protected) {
             inst <- tk.`tk_updt;
             trust <- tk.`tk_trust;
             tkw <- tk.`tk_wdata;
             sid <- tkw.`tkw_signer;
             cphs <- tkw.`tkw_ekeys;
             msg <- encode_msg  (trust,cphs,sid,inst);
             sig <- tkw.`tkw_sig;
             check <@ IdealSigServ.verify(sid.`1, msg, sig);
             check <- check && (proj_pks (tr_mems trust)) = fdom cphs &&  (sid \in tr_mems trust);
             if (check && hid \in tr_mems trust &&  hid_in hid HSMs.benc_keys) {
               keys1 <- oget (KMSProcedures3.wrapped_keys.[tk]);
               if (hdl \notin keys1) {
                 keys1.[hdl] <- witness;
                 RO.sample(hdl);
                 mem <- tr_mems trust;
                 tag <- encode_tag trust;
                 fake <- encode_ptxt (fake_keys keys1);
                 ekeys <$ mencrypt (proj_pks mem) tag fake;
                 msg<- encode_msg (trust,ekeys,hid,false);
                 sigo <@ IdealSigServ.sign(hid.`1,msg);
                 tkw <- {| tkw_ekeys = ekeys; 
                           tkw_signer = hid; 
                           tkw_sig = oget sigo |};
                 tkr <- {| tk_updt = false;
                           tk_trust = tk.`tk_trust;
                           tk_wdata = tkw; |};
                 KMSProcedures3.wrapped_keys.[tkr] <- keys1;
                 KMSProcedures.tklist <- KMSProcedures.tklist `|` fset1 tkr;
                 KMSProcedures.handles <- KMSProcedures.handles `|` fset1 hdl;
                 rtd <- Some tkr;
               }
             } 
           } else{ 
             tdo <@ HSMsPTS.unwrap(hid, tk); 
             if (tdo <> None) {
               td <- oget tdo;
               tdoo <- add_key hdl key td;
               if (tdoo <> None) {
                  tkr <@ HSMsPTS.wrap(hid,(oget tdoo));
                  KMSProcedures.tklist <- KMSProcedures.tklist `|` fset1 tkr;
                  KMSProcedures.handles <- KMSProcedures.handles `|` fset1 hdl;
                  rtd <- Some tkr;
               } 
             }
           }
         }
         KMSProcedures.count_tkmng <- KMSProcedures.count_tkmng + 1;
      }
      return rtd;
    }
  }.

  local module KMSRoR5(RO:FRO) = {
    module KMSProc = KMSProcedures5(RO)
  
    module A = A(KMSProc)
  
    proc distinguish() : bool = {
      var b : bool;
    
      KMSProc.init();
      b <@ A.guess();
    
      return b = KMSProcedures.b;
    }
  }.

  local module KMSProcedures6 : KMSOracles = {
    module HstSPTS = HstSP_TS(IdealTrustService(OpPolSrv(OA))) 
    include KMSProcedures5(RRO) [-test]

    proc test(hid : HId, hstid : HstId, tk : Token, hdl : Handle) : Key option = {
      var isin, rsk, rsk', installed, protected, inst, trust, tkw, sid, cphs, msg, sig, check;
      rsk <- None;
      if (KMSProcedures.count_test < q_test) {
         installed <@ HstSPTS.isInstalledTrust(hstid,tk);
         if ((hid \in KMSProcedures.hids)&&
             (!(hdl \in KMSProcedures.corrupted))&&
             installed) {
           (* We now reject unprotected tokens *)
           protected <@ IdealTrustService(OpPolSrv(OA)).isProtectedTrust(tk.`tk_trust);
           if (protected) {
             inst <- tk.`tk_updt;
             trust <- tk.`tk_trust;
             tkw <- tk.`tk_wdata;
             sid <- tkw.`tkw_signer;
             cphs <- tkw.`tkw_ekeys;
             msg <- encode_msg  (trust,cphs,sid,inst);
             sig <- tkw.`tkw_sig;
             check <@ IdealSigServ.verify(sid.`1, msg, sig);
             check <- check && (proj_pks (tr_mems trust)) = fdom cphs &&  (sid \in tr_mems trust);

             if (check && hid \in tr_mems trust &&  hid_in hid HSMs.benc_keys) {
               if (hdl \in (oget KMSProcedures3.wrapped_keys.[tk])) {          
                 if (hdl \in KMSProcedures.tested) {
                   rsk <- KMSProcedures.tested.[hdl]; 
                 } else {
                   isin <@  RRO.in_dom(hdl, Unknown);
                   if (isin) {
                      rsk' <@ RRO.get(hdl);
                      rsk <- Some rsk'; 
                      KMSProcedures.tested.[hdl] <- oget rsk;
                   }
                 }
              }
            }
          }
        }
        KMSProcedures.count_test <- KMSProcedures.count_test + 1;
      }
      return rsk; 
     }
   }.

  local module KMSRoR6 = {
    module KMSProc = KMSProcedures6
  
    module A = A(KMSProc)
  
    proc main() : bool = {
      var b : bool;
      FRO.init (); 
      KMSProc.init();
      b <@ A.guess();
      KMSProcedures.b <$ {0,1};
      return b = KMSProcedures.b;
    }
  }.

lemma fake_keys_set  ['a] m x (a:'a): fake_keys m.[x <- a] = (fake_keys m).[x <- witness].
proof. by rewrite /fake_keys map_set. qed.

lemma dom_fake_keys ['a] m1 m2 : dom m1 = dom m2 => fake_keys<:'a> m1 = fake_keys<:'a> m2.
proof.
  rewrite /fake_keys => hd; apply fmap_eqP => hdl.
  rewrite !mapE /=; have /#: hdl \in m1 = hdl \in m2 by rewrite hd.
qed.

op inv_wrap (w1 w2:(Token, Keys) fmap) = 
  dom w1 = dom w2 /\ forall tk, dom (oget w1.[tk]) = dom (oget w2.[tk]).

lemma inv_wrap_set w1 w2 tk m1 m2 : 
  dom m1 = dom m2 => inv_wrap w1 w2 => inv_wrap w1.[tk <- m1] w2.[tk <- m2].
proof. 
  rewrite /inv_wrap => hd [hdw hw]; split.
  + by apply fun_ext => tk1; rewrite !mem_set hdw.
  move=> tk1; apply fun_ext => hdl.
  rewrite !get_setE; case (tk1 = tk);last by rewrite hw.
  by rewrite !oget_some hd.
qed.

local equiv isProctectedTrustP trust0 : IdealTrustService(OpPolSrv(OA)).isProtectedTrust ~ IdealTrustService(OpPolSrv(OA)).isProtectedTrust : 
     ={trust, IdealTrustService.RT.protectedTrusts, OpPolSrv.genuine, RealTrustService.protectedTrusts} /\ trust{1} = trust0 ==> 
     ={res, IdealTrustService.RT.protectedTrusts, OpPolSrv.genuine, RealTrustService.protectedTrusts} /\ 
      (res = (all_genuine OpPolSrv.genuine (trust0) &&  
                    trust0 \in IdealTrustService.RT.protectedTrusts)){1}.
proof. proc; auto => />. qed.

local equiv unwrapP trust : KMSProcedures4(OA).HSMsPTS.unwrap ~ KMSProcedures4(OA).HSMsPTS.unwrap :
    ={hid,tk, glob HSMs, glob RealSigServ} /\ tk{1}.`tk_trust = trust ==>
    ={res} /\ (res <> None => (oget res).`td_trust = trust){1}.
proof. by proc; inline *; auto => />; rewrite oget_some. qed.

local equiv checkTrustUpdateP trust protected : KMSProcedures4(OA).HSMsPTS.checkTrustUpdate ~ KMSProcedures4(OA).HSMsPTS.checkTrustUpdate :
  ={old, new, auth, glob RealTrustService, OpPolSrv.badOps, OpPolSrv.goodOps, OpPolSrv.genuine} /\ old{1}.`td_trust = trust /\
  protected = (all_genuine OpPolSrv.genuine trust && trust \in IdealTrustService.RT.protectedTrusts){1}
  ==>
  ={res, glob RealTrustService} /\ (res <> None => 
            (all_genuine OpPolSrv.genuine (oget res).`td_trust && (oget res).`td_trust \in IdealTrustService.RT.protectedTrusts) =>
            protected){1}.
proof. proc; inline *; auto => |> *; smt (in_fsetU in_fset1). qed.

local lemma KMS_5 &minit: 
  Pr[ KMSRoR4(A,OA).main() @ &minit : res] = Pr[ Eager(KMSRoR5).main1() @ &minit : res].
proof.
  byequiv=> //.
  proc;inline *;wp.
  call (_: ={glob RealSigServ, glob HSMs, glob HstPolSrv, glob RealTrustService, glob OA, glob OpPolSrv, 
             KMSProcedures.hids, KMSProcedures.tklist, KMSProcedures.corrupted, KMSProcedures.handles, KMSProcedures.b, 
             KMSProcedures.count_ops, KMSProcedures.count_auth, KMSProcedures.count_hst, KMSProcedures.count_updt, KMSProcedures.count_hid, KMSProcedures.count_tkmng, KMSProcedures.count_unw,KMSProcedures.count_corr,KMSProcedures.count_test,
             KMSProcedures.count_inst} /\
             inv_wrap KMSProcedures3.wrapped_keys{1} KMSProcedures3.wrapped_keys{2} /\ 
             (forall hdl,  hdl \in KMSProcedures.tested{1} => hdl \in FRO.m{2}) /\
             (forall hdl, hdl \in FRO.m{2} =>  hdl \in  KMSProcedures.handles{1}) /\
             dom KMSProcedures.tested{1} = dom KMSProcedures.tested{2} /\
             (forall hdl tk key, !hdl \in  KMSProcedures.tested{1} => (oget KMSProcedures3.wrapped_keys{1}.[tk]).[hdl] = Some key =>
               exists f, FRO.m{2}.[hdl] = Some (key,f) /\ (!hdl \in KMSProcedures.corrupted{1} => f = Unknown)) /\
             (KMSProcedures.b{1} => ={KMSProcedures.tested}) /\
             (forall hdl tk, !KMSProcedures.b{1} => hdl \in KMSProcedures.tested{1} => 
               hdl{2} \in oget KMSProcedures3.wrapped_keys{1}.[tk{2}] =>
               oget KMSProcedures.tested{2}.[hdl] = oget (oget KMSProcedures3.wrapped_keys{1}.[tk{2}]).[hdl])); try by sim />. 
  (* newToken *)
  + conseq |>; proc; sp 1 1; if => //; if => //.
    swap 5 1. 
    seq 5 5 : (#[/3:]pre /\ ={td,tk,rtd}); 1: by sim />.
    auto => |>. 
    move=> &1 &2 *.
    split.
     smt (get_setE mem_empty inv_wrap_set).
    split.
     progress.
     move : (H3 hdl tk0 key H10 ). 
     case (tk0 =tk{2}); first by smt(@SmtMap). 
     smt (get_setE mem_empty inv_wrap_set).
    auto => |>. 
     smt(@SmtMap).
     wp;skip;progress;smt(@SmtMap).

  (* updateTokenTrust *)
  + proc => /=; sp 1 1; if => //; if => //. sp 1 1.
    seq 1 1 : (#[/:-4,-3:]pre /\ ={protected} /\
               (protected = (all_genuine OpPolSrv.genuine tk.`tk_trust &&  
                    tk.`tk_trust \in IdealTrustService.RT.protectedTrusts)){1}).
    + ecall (isProctectedTrustP tk{1}.`tk_trust) => //.
    if => //; last first.
    + seq 1 1 : (#[/3:]pre /\ ={tdo} /\ (tdo <> None => (oget tdo).`td_trust = tk.`tk_trust){1}).
      + ecall (unwrapP tk.`tk_trust{1}) => //.
      if => //.
      seq 1 1: (#[/:-6]pre /\ ={tdoo} /\ 
        (tdoo <> None => !(all_genuine OpPolSrv.genuine (oget tdoo).`td_trust && (oget tdoo).`td_trust \in IdealTrustService.RT.protectedTrusts)){1}).
      + ecall (checkTrustUpdateP tk{1}.`tk_trust protected{1}); auto => /#.
      if => //.
      seq 1 1 : (#pre /\ ={protected} /\ !protected{1}).
      + by ecall (isProctectedTrustP (oget tdoo{1}).`td_trust) => />.
      by rcondf{1} 1 => //; rcondf{2} 1 => //; sim />.
      by  auto => />.
      by  auto => />.
    seq 10 10 : (#[/3:]pre /\ ((tdo{1} <> None) = (tdo{2} <> None)) /\
       (tdo{1} <> None => (oget tdo{1}).`td_updt = (oget tdo{2}).`td_updt /\ 
                          (oget tdo{1}).`td_trust = (oget tdo{2}).`td_trust /\ ((oget tdo).`td_trust = tk.`tk_trust){1} /\
                          ((oget tdo).`td_skeys = oget KMSProcedures3.wrapped_keys.[tk]){1} /\
                          dom (oget tdo{1}).`td_skeys =  dom (oget tdo{2}).`td_skeys)).
    + by inline *; auto => /> &m1 &m2 ? hw *; rewrite !encdec_ptxt /= hw.
    if => [/# | | //].
    seq 1 1 : (#[/:-6]pre /\ ((tdoo{1} <> None) = (tdoo{2} <> None)) /\
       (tdoo{1} <> None => ((oget tdoo{1}).`td_updt = (oget tdoo{2}).`td_updt /\ 
                          (oget tdoo{1}).`td_trust = (oget tdoo{2}).`td_trust /\ 
                          ((oget tdoo).`td_skeys = oget KMSProcedures3.wrapped_keys.[tk]){1} /\
                          dom (oget tdoo{1}).`td_skeys =  dom (oget tdoo{2}).`td_skeys) /\
                          (all_genuine OpPolSrv.genuine{1} ((oget tdoo){1}.`td_trust) && ((oget tdoo){1}.`td_trust \in IdealTrustService.RT.protectedTrusts{1})))).
    + inline *; auto;smt(in_fsetU in_fset1).
    if => [/# | | //].
    seq 1 1: (#pre /\ ={protected} /\ protected{1}).
    + ecall (isProctectedTrustP (oget tdoo{1}).`td_trust); auto => /#.
    rcondt{1} 1 => //; rcondt{2} 1; 1: by auto.
    auto; call (_: ={glob IdealSigServ}); 1: by sim.
    auto => |> &m1 &m2 hw h1 ? h2 h3 h4 h5 ?? ?? h /h{h} |> -> -> -> hd ???.
    rewrite (dom_fake_keys _ _ hd) => |> ???; split; 1: by apply inv_wrap_set.
    split; 1: by move=> ??? /h3; rewrite get_setE; case: (tk0 = _);rewrite /= ?oget_some => hh /hh.
    by move=> ?? hb ht; rewrite !get_setE; case: (tk0 = _) => /=; rewrite ?oget_some; apply h5.
      by  auto => />.
      by  auto => />.
      by  auto => />.

  (* addTokenKey *)
  + proc => /=.
    sp 1 1.
    case (KMSProcedures.count_tkmng{1} < q_tkmng /\
           size (elems (tr_mems (tr_data tk{1}.`tk_trust))) < max_size);last first.
    + by rcondf{1} 2; 1:(by auto); rcondf{2} 2; auto.  
    rcondt{1} 2; 1:(by auto); rcondt{2} 2; 1: by auto.  
    case: (((hid \in KMSProcedures.hids) && ! (hdl \in KMSProcedures.handles)){1}); last first.
    + by rcondf{1} 2; 1:(by auto); rcondf{2} 2; auto.  
    rcondt{1} 2; 1:(by auto); rcondt{2} 2; 1: by auto.  
    swap 1 2.
    sp 1 1;  seq 1 1 : (#pre /\ ={protected} /\
               (protected = 
                 (all_genuine OpPolSrv.genuine (tr_data tk.`tk_trust) &&  
                    tk.`tk_trust \in IdealTrustService.RT.protectedTrusts)){1}).
    + by ecall (isProctectedTrustP tk{1}.`tk_trust) => />.
    case: (protected{1}); last first.
    + rcondf{1} 2; 1:(by auto); rcondf{2} 2;1:by auto.
      seq 2 2 : (#[/3:]pre /\ ={key, tdo} /\ (tdo <> None => (oget tdo).`td_trust = tk.`tk_trust){1}).
      + by ecall (unwrapP tk.`tk_trust{1});auto => />.
      if => //; last first.
      by auto => />.
      sp 2 2; if => //.
      seq 1 0 : (#[/:-7]pre /\ ={tdoo, tdo, key} /\ !protected{1}).
      + inline *; auto => &1 &2 />. 
        by rewrite /add_key; case: (hdl{2} \in (oget tdo{2}).`td_skeys) => // /#.
      rcondf{1} 1; 1: by auto.
      conseq |>; wp; call (_: ={glob RealSigServ}); 1: by sim.
      by auto => />; smt (in_fsetU).
      by auto => />.
    rcondt{1} 2; 1:(by auto); rcondt{2} 2;1:by auto.
    swap 1 10.
    inline IdealSigServ.verify; sp 12 12.
    if => //; last by rcondf{1} 2; auto => />.
    rcondt{1} 4; 1: by auto.
    swap{1} 3 1. sp 3 1.
    if{2}; last first.
    + rcondf{1} 3; auto => &m1 |>;rewrite /add_key /= encdec_ptxt.
      by move=> [hd hw]; rewrite hw => />.
    rcondt{1} 3.
    + auto => &m1 |>;rewrite /add_key /= encdec_ptxt.
      by move=> [hd hw]; rewrite hw => />.
    inline{2} FRO.sample.
    rcondt{2} 4; 1: by auto => /#.
    rcondt{1} 4.
    + by move=> &m;inline *;auto=> &m1 />; rewrite /add_key /tr_data encdec_ptxt /= => /> /#.
    auto; call (_: ={glob RealSigServ}); 1: by sim.
    inline *; wp;rnd;wp;auto => |>.
    move=> &m1 &m2; rewrite /add_key /= encdec_ptxt /= => -[hd hw].
    rewrite hw => |> ???????????????????.
    rewrite !fake_keys_set !(dom_fake_keys _ _ (hw tk{m2})) /= => |> *; split.
    + apply inv_wrap_set => //.
      by apply fun_ext => tk1; rewrite !mem_set hw.
    split; 1: smt (mem_set in_fsetU in_fset1).
    split; 1: smt (mem_set in_fsetU in_fset1).
    smt (get_setE domNE).
  (* corrupt *)
  + proc.
    sp 1 1; if => //.
    if => //; 1: by move=> /> ?????? h;rewrite h.
    sp 1 1; seq 1 1 : (#pre /\ ={protected} /\
               (protected = 
                 (all_genuine OpPolSrv.genuine (tr_data tk.`tk_trust) &&  
                    tk.`tk_trust \in IdealTrustService.RT.protectedTrusts)){1}).
    + by ecall (isProctectedTrustP tk{1}.`tk_trust) => />.
    if => //; last first.
    + seq 1 1 : (#[/3:-3]pre /\ ={tdo}); 1: by sim />.
      conseq |>; auto; smt (in_fsetU).
    inline IdealSigServ.verify; sp 12 12.
    if => //; last first. by rcondf{1} 1 => />;auto => />.
    sp 2 0.
    if => //. 
    + by move=> /> ??? hd *; rewrite encdec_ptxt hd.
    inline *; auto => /> &m1 &m2.
    rewrite encdec_ptxt => /> ????? htro ???? ht ????????. 
    rewrite /dom;case _: (oget KMSProcedures3.wrapped_keys{m1}.[tk{m2}]).[hdl{m2}] => [ | key1] heq //= ??.
    have [f [hin ?]] := htro _ _ _ ht heq; rewrite hin oget_some /=.
    smt (get_setE in_fsetU in_fset1).
    by auto => />.
    by auto => />.

  (* test *)
  + proc.
    case  (KMSProcedures.count_test{1} < q_test);last first.
    rcondf {1} 3;first by auto => />.
    rcondf {2} 3;first by auto => />.
    by auto => />.

    rcondt {1} 3;first by auto => />.
    rcondt {2} 3;first by auto => />.

    sp 1 1; seq 2 2 : (#pre /\ ={rkey, installed}); 1: by sim />.
    if => //=.
    sp 1 0; seq 1 1 : (#pre /\ ={protected} /\
               (protected = 
                 (all_genuine OpPolSrv.genuine (tr_data tk.`tk_trust) &&  
                    tk.`tk_trust \in IdealTrustService.RT.protectedTrusts)){1}).
    + ecall (isProctectedTrustP tk{1}.`tk_trust) => //.
    if => //; last by rcondf{1} 1;auto => />.
    inline IdealSigServ.verify.
    sp 12 12; if => //; last by rcondf{1} 1; auto => />.
    sp 2 0; if => //; 1: by move=> /> ??? hw *; rewrite encdec_ptxt hw.
    sp 1 0;if.
    + by move=> /> ?????? ->.
    + by auto => |>; rewrite encdec_ptxt => * /#.
    inline FRO.in_dom FRO.get; sp 0 3.
    seq 0 0 : (#pre /\ isin{2} /\ FRO.m{2}.[hdl{2}] = Some(oget rsk{1}, Unknown) /\ (oget KMSProcedures3.wrapped_keys{1}.[tk{2}]).[hdl{2}] = Some (oget rsk{1})). 
    + auto => /> &m1 &m2 ; rewrite encdec_ptxt=> ???? het htro ??? hc ????????? hw ht.
      move: hw;rewrite /dom. case _: (oget KMSProcedures3.wrapped_keys{m1}.[tk{m2}]).[hdl{m2}] => // key1 heq _. 
      by smt(). by smt().
    rcondt{2} 1; 1: by auto.
    rcondt{2} 3; 1: by auto => />.
    auto => /> &m1 &m2.
    rewrite /=encdec_ptxt => /> *.
    rewrite H23 H24 /=; split; 1: smt (mem_set).
    split; 1: smt (mem_set).
    split; 1: by apply fun_ext => ?; rewrite !mem_set H3.
    split; 1: by move=> ???; rewrite mem_set get_setE /#.
    split; 1: by move=> /H5 ->.
    smt(mem_set get_setE).
    auto => />.
    auto => />.

  (* initialisation of the game *)
  auto; call(_ : true); auto => /> *;split; 1: by move=> ?;rewrite mem_empty.
  split; 1: by move=> ?;rewrite mem_empty.
  split; 1: by move=> ???;rewrite emptyE oget_none witness_map emptyE.
  by move=> ??;rewrite mem_empty. 
qed.

local lemma KMS_FR &minit:
  Pr[ Eager(KMSRoR5).main1() @ &minit : res] = Pr[ Eager(KMSRoR5).main2() @ &minit : res].
proof. byequiv (Eager_1_2 KMSRoR5) => //. qed.

local lemma KMS_6 &minit: 
  Pr[ Eager(KMSRoR5).main2() @ &minit : res] = Pr[ KMSRoR6.main() @ &minit : res].
proof.
  byequiv=> //.
  proc.
  call{1} (RRO_resample_ll).
  inline *.
  swap{2} -2;wp.
  call (_ : ={glob KMSProcedures5} /\ 
              (forall c, c \in FRO.m{1} = c \in FRO.m{2}) /\
              (forall c, !c \in KMSProcedures.tested{1} => FRO.m{1}.[c] = FRO.m{2}.[c]));try by sim />.
  (* addTokenKey *)
  + proc=> /=; conseq />.
    case (KMSProcedures.count_tkmng{1} < q_tkmng /\
          size (elems (tr_mems (tr_data tk{1}.`tk_trust))) < max_size); last first.
    rcondf {1} 3; first by auto => />.
    rcondf {2} 3; first by auto => />.
    by auto => />.
    rcondt {1} 3; first by auto => />.
    rcondt {2} 3; first by auto => />.
    seq 2 2: (={rtd, key} /\ #pre); 1:by sim />.

    if => //.
    seq 2 2 : (={tdo, protected} /\ #pre); 1: by sim />.  
    if => //; last by sim />.
    seq 9 9 : (={trust, tkw, sid, cphs, msg, sig, check} /\ #pre); 1:by sim />.
    if => //.
    sp 1 1;if => //.
    seq 2 2 : (={keys1} /\ #[/3:-2]pre); 2: by sim />.
    conseq />.
    inline *;auto=> />;smt (mem_set get_setE oget_some).
    by auto => />.
    by auto => />.
    by auto => />.

  (* corrupt *)
  + proc => /=.  conseq />.
    sp 1 1; if => //.
    if => //; last by auto => />.
    seq 2 2 : (={rsk, tdo, protected} /\ #[/3:]pre);1: by sim />.
    if => //;last by sim />.    
    seq 9 9 : (={trust, tkw, sid, cphs, msg, sig, check} /\ #pre); 1:by sim />.  
    if => //;last by auto => />. 
    if => //;last by auto => />.
    inline *; auto => />;  smt (mem_set get_setE oget_some).

  + proc => /=; conseq />. 
    case (KMSProcedures.count_test{1} < q_test); last first.
    rcondf {1} 3; first by auto => />.
    rcondf {2} 2; first by auto => />.
    rnd {1}. auto => />.
    rcondt {1} 3; first by auto => />.
    rcondt {2} 2; first by auto => />.
    swap{1} 2 1.
    seq 2 2 : (={rsk, installed} /\ #pre);1: by sim />.
    if{2}; last by rcondf{1} 2; auto; smt (genKey_ll).
    rcondt{1} 2; 1: by auto.
    swap{1} 1 1.
    seq 1 1 : (#pre /\ ={protected});1: by sim />.
    if{2};last  by rcondf{1} 2; auto; smt (genKey_ll).
    rcondt{1} 2; 1: by auto. 
    swap{1} 1 9. seq 9 9 : (#pre /\ = {inst,trust, tkw, sid, cphs, msg, sig, check}); 1: by sim />.
    if{2}; last by rcondf{1} 2; auto; smt (genKey_ll).
    rcondt{1} 2; 1: by auto. 
    if{2}; last by rcondf{1} 2; auto; smt (genKey_ll).
    rcondt{1} 2; 1: by auto.
    if{2}; first by rcondt{1} 2; auto; smt (genKey_ll).
    rcondf{1} 2; 1: by auto. 
    inline *.
    case: ((in_dom_with FRO.m hdl Unknown){2}); last first.
    + rcondf{1} 5; 1: by auto => /> /#.
      rcondf{2} 4; 1: by auto => /> /#.
      by auto => /> *;rewrite genKey_ll.
    rcondt{1} 5; 1: by auto => /> /#.
    rcondt{2} 4; 1: by auto => /> /#.
    rcondt{1} 7; 1: by auto => /> /#.
    rcondt{2} 6; 1: by auto => /> /#.
    swap{2} 5 - 4; swap{1} 6 -4; wp => /=.
    case: (KMSProcedures.b{1}).
    + by rnd{1};rnd;auto => />;smt( genKey_ll mem_set get_setE).
    rnd;rnd{1};auto => />;smt( genKey_ll mem_set get_setE).
  auto => /=;call (_: true); auto => />; apply dbool_ll.
qed.

local lemma KMS_pr &minit: Pr[ KMSRoR6.main() @ &minit : res] <= 1%r/2%r.
proof.
  byphoare=> //.
  proc. 
  rnd (pred1 b);conseq (_:true) => //=.
  by move=> b; rewrite dbool1E /pred1 => />.
qed.

lemma noadv &minit: 
  Pr[ KMSRoR4(A,OA).main() @ &minit : res] <= 1%r/2%r.
proof.
  by rewrite (KMS_5 &minit) (KMS_FR &minit) (KMS_6 &minit) (KMS_pr &minit).
qed.

end section PROOFS.

end Hop_final.
