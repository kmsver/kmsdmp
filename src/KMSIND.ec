require import FSet SmtMap Distr DBool AllCore List.
require import Real RealExtra.
require import Policy.

abstract theory KMSIND.

type HId.

(* Query bounds *)
op q_ops : int.
op q_auth : int.
op q_hst : int.
op q_updt : int.
op q_hid : int.
op q_tkmng : int.
op q_unw : int.
op q_corr : int.
op q_test : int.
op q_inst : int.
op max_size : int.

clone import Policy with
        type HId <- HId,
        op q_ops <- q_ops,
        op q_auth <- q_auth,
        op q_hst <- q_hst,
        op q_updt <- q_updt,
        op q_chk <- q_tkmng.

type Key.
type Handle.  

op genKey : Key distr.
axiom genKey_ll : is_lossless genKey.
hint solve 2 random : genKey_ll.

type Keys = (Handle,Key) fmap.

type Installable = bool.

type TkData = {
  td_updt : Installable;
  td_trust : Trust;
  td_skeys : Keys;
}.

type TkWrapped.

type Token = {
  tk_updt : Installable;
  tk_trust : Trust;
  tk_wdata : TkWrapped;
}.

op set_trust td_trust_new td_old =
       {| td_updt = true;
          td_trust = td_trust_new;
          td_skeys = td_old.`td_skeys; |}.

op add_key hdl key td_old =
  if (hdl \in td_old.`td_skeys)
  then None
  else Some {| td_updt = false;
               td_trust = td_old.`td_trust;
               td_skeys = td_old.`td_skeys.[hdl <- key]; |}. 

op get_key hdl td = if hdl \in td.`td_skeys
                    then Some (oget td.`td_skeys.[hdl])
                    else None. 

module type HSMService = {
  proc init() : unit
  proc gen() : HId
  proc checkTrustUpdate(old : TkData, new : Trust, auth : Authorizations) : TkData option
  proc wrap(hid : HId, td : TkData) : Token
  proc unwrap(hid : HId, tk : Token) : TkData option
}.

op checkToken(inst : Installable, old new : Trust, tkw : TkWrapped) : bool.

module type HstService = {
  proc init() : unit
  proc newHst() : HstId
  proc installInitialTrust(hstid : HstId, tk : Token) : bool
  proc installUpdatedTrust(hstid : HstId, tk : Token) : bool
  proc isInstalledTrust(hstid : HstId, tk : Token) : bool
}.

module HstS(OA : OpPolSrvT) : HstService = {

  proc init = HstPolSrv.init
  proc newHst = HstPolSrv.newHst

  proc installInitialTrust(hstid : HstId, tk : Token) : bool = {
      var b;
      (* signer must be in trust and trust must be installable *)
      b <- tk.`tk_updt /\ checkToken tk.`tk_updt tk.`tk_trust tk.`tk_trust tk.`tk_wdata;
      if (b) {
         b <@ OA.isGoodInitialTrust(tr_data tk.`tk_trust);
         if (b) {
            HstPolSrv.installInitialTrust(hstid,tk.`tk_trust);
         }
      }
      return b;
  }

  proc installUpdatedTrust(hstid : HstId, tk : Token) : bool = {
      var b,installed,old;
      installed <- false;
      if (hstid \in HstPolSrv.hosts_tr) {
         old <- oget HstPolSrv.hosts_tr.[hstid];
         (* signer must be in old trust and trust must be installable *)
         b <- tk.`tk_updt /\ checkToken tk.`tk_updt old tk.`tk_trust tk.`tk_wdata;
         if (b) {
            installed <@ HstPolSrv.installUpdatedTrust(hstid,tk.`tk_trust);
         }
      }
      return installed;
  }
  proc isInstalledTrust(hstid : HstId, tk : Token) = {
      var b;
      b <@ HstPolSrv.isInstalledTrust(hstid,tk.`tk_trust);
      return b;
  }
}.

(********************************)
(* IND Security                 *)
(********************************)


module KMSProcedures(HSMs : HSMService,HstS : HstService, OA : OpPolSrvT) = {

  var hids : HId fset
  (* The following list avoids trivial CCA breaks *)
  var tklist : Token fset
  (* We keep two blacklists for corruption/test interaction *)
  var corrupted : Handle fset
  var tested : (Handle,Key) fmap
  (* We impose unique handles for corruption/testing *)
  var handles : Handle fset
  var b : bool

  var count_ops : int
  var count_auth : int
  var count_hst : int
  var count_updt : int
  var count_hid : int
  var count_tkmng : int
  var count_unw : int
  var count_corr : int
  var count_test : int
  var count_inst : int

  (* Initialize game state *)
  proc init() = {
     HSMs.init();
     OA.init();
     HstS.init();
     hids <- fset0;
     tklist   <- fset0;
     corrupted <- fset0;
     tested <- empty;
     handles <- fset0;
     b <$ {0,1};
     count_ops <- 0;
     count_auth <- 0;
     count_hst <- 0;
     count_updt <- 0;
     count_hid <- 0;
     count_tkmng <- 0;
     count_unw <- 0;
     count_corr <- 0;
     count_test <- 0;
     count_inst <- 0;
  }

   proc newOp (badOp : bool) = {
        var opid;
        opid <- witness;
        if (count_ops < q_ops) {
           opid <@ OA.newOp(badOp);
           count_ops <- count_ops + 1;
        }
        return opid;
   }

   proc requestAuthorization(request : Request, opid : OpId) : Authorization option = {
       var auth;
       auth <- witness;
       if (count_auth < q_auth) {        
           auth <@ OA.requestAuthorization(request,opid);
           count_auth <- count_auth + 1;
       }
       return auth;
  }

   proc newHst() = {
      var hstid;
      hstid <- witness;
      if (count_hst < q_hst) {
         hstid <@ HstS.newHst();
         count_hst <- count_hst + 1;
      }
      return hstid;
   }

   proc installInitialTrust(hstid: HstId, tk: Token) = {
     var r <- false; 
     if (count_inst < q_inst) { 
      r <@ HstS.installInitialTrust(hstid, tk);
      count_inst <- count_inst + 1;
     }
     return r;
   }

   proc installUpdatedTrust(hstid : HstId, new : Token) = {
      var b;
      b <- false;
      if (count_updt < q_updt) {
         b <@ HstS.installUpdatedTrust(hstid, new);
         count_updt <- count_updt + 1;
      }
      return b;
   }

  proc newHSM() : HId option = {
    var hid, rhid;
    rhid <- None;
    if (count_hid < q_hid) {
       hid <@ HSMs.gen(); 
       if (!(hid \in hids)) {
           hids <- hids `|` fset1 hid;
       }
       OA.addHId(hid);
       count_hid <- count_hid + 1;
       rhid <- Some hid;
    }
    return rhid;
  }

  proc newToken(hid : HId, new : Trust) : Token option = {
      var rtd,tk,td;
      rtd <- None;
      if (count_tkmng < q_tkmng /\
          size (elems (tr_mems (tr_data new))) < max_size) {
         if (hid \in hids /\ tr_initial (tr_data new)) {
            td <- {| td_updt = true;
                     td_trust = new;
                     td_skeys = empty; |};
            tk <@ HSMs.wrap(hid,td);
            tklist <- tklist `|` fset1 tk;
            rtd <- Some tk;
         }
         count_tkmng <- count_tkmng + 1;
      }
      return rtd; 
  } 

  proc updateTokenTrust(hid : HId, new_trust : Trust,
                       auth : Authorizations, tk : Token) : Token option = {
      var tdo,tdoo,tkn,rtd, qa;
      rtd <- None;
      if (count_tkmng < q_tkmng /\
          size (elems (tr_mems (tr_data new_trust))) < max_size /\
          size (elems (tr_mems (tr_data tk.`tk_trust))) < max_size) {
         if (hid \in hids) {
            tdo <@ HSMs.unwrap(hid,tk);
            qa <@ OA.quorumAssumption(new_trust);
            if (tdo <> None /\ qa) {
               tdoo <@ HSMs.checkTrustUpdate(oget tdo,new_trust,auth);
               if (tdoo <> None) {
                 tkn <@ HSMs.wrap(hid,(oget tdoo));
                 tklist <- tklist `|` fset1 tkn;
                 rtd <- Some tkn;
               }
           } 
         }  
         count_tkmng <- count_tkmng + 1;
      } 
      return rtd;
  }

  proc addTokenKey(hid : HId, hdl : Handle, 
                tk : Token) : Token option = {
      var tdo,tdoo, td,rtd,key, tkr;
      rtd <- None;
      key <$ genKey;
      if (count_tkmng < q_tkmng /\ size (elems (tr_mems (tr_data tk.`tk_trust))) < max_size ) {
         if ((hid \in hids)&&(!(hdl \in handles))) {
            tdo <@ HSMs.unwrap(hid,tk);
            if (tdo <> None) {
               td <- oget tdo;
               tdoo <- add_key hdl key td;
               if (tdoo <> None) {
                 tkr <@ HSMs.wrap(hid,(oget tdoo));
                 tklist <- tklist `|` fset1 tkr;
                 handles <- handles `|` fset1 hdl;
                 rtd <- Some tkr;
              }
            } 
         }    
         count_tkmng <- count_tkmng + 1;
      }
      return rtd;
  }

  proc unwrap(hid : HId, tk : Token) : TkData option = {
      var rtd, ag;
      rtd <- None;
      if (count_unw < q_unw) {
         if (hid \in hids) {
            ag <- OA.allGenuine(tr_data (tk_trust tk));
            if (!(tk \in tklist) || !ag) {
             rtd <@ HSMs.unwrap(hid,tk);
             }
         }
         count_unw <- count_unw + 1;
      }
      return rtd;    
  }  

  proc corrupt(hid : HId, tk : Token, hdl : Handle) : Key option = {
      var rsk,tdo;
      rsk <- None;
      if (count_corr < q_corr) {
         if ((hid \in hids)&&(!(hdl \in tested))) {
            tdo <@ HSMs.unwrap(hid,tk);
            if (tdo <> None && hdl \in (oget tdo).`td_skeys) {
                rsk <- (oget tdo).`td_skeys.[hdl];
                corrupted <- corrupted `|` fset1 hdl;
            }
         }
         count_corr <- count_corr + 1;
      }
      return rsk;    
  }  

  proc test(hid : HId, hstid : HstId, tk : Token, hdl : Handle) : Key option = {
      var rkey, rsk,tdo,installed;
      rsk <- None;
      rkey <$ genKey;
      if (count_test < q_test) {
         installed <@ HstS.isInstalledTrust(hstid,tk);
         if ((hid \in hids)&&
             (!(hdl \in corrupted))&&
             installed) {
              tdo <@ HSMs.unwrap(hid,tk);
              if (tdo <> None && hdl \in (oget tdo).`td_skeys) {
                 rsk <- (oget tdo).`td_skeys.[hdl];
                 if (hdl \in tested) {
                    rkey <- oget tested.[hdl]; 
                 }
                 else {
                    tested.[hdl] <- rkey;
                 }
                 rsk <- if (b) then Some rkey else rsk; 
              }
         }
         count_test <- count_test + 1;
      }
      return rsk; 
  }
}.

module type KMSOracles = {
  proc newOp(badOp : bool) : OpId option * OpSk option
  proc requestAuthorization(request : Request, opid : OpId) : Authorization option
  proc newHst() : HstId
  proc installInitialTrust(hstid : HstId, tk : Token) : bool
  proc installUpdatedTrust(hstid : HstId, tk : Token) : bool
  proc newHSM() : HId option
  proc newToken(hid : HId, new : Trust) : Token option
  proc updateTokenTrust(hid : HId, new_trust : Trust, auth : Authorizations,
                   tk : Token) : Token option
  proc addTokenKey(hid : HId, hdl : Handle, tk : Token) : Token option
  proc unwrap(hid : HId, tk : Token) : TkData option
  proc corrupt(hid : HId, tk : Token, hdl : Handle) : Key option 
  proc test(hid : HId, hstid : HstId, tk : Token, hdl : Handle) : Key option
}.

module type KMSAdv(O:KMSOracles) = {
       proc guess(): bool
}.

module KMSRoR(A : KMSAdv, HSMs : HSMService, HstS : HstService, OA : OpPolSrvT) = {
   module KMSProc = KMSProcedures(HSMs,HstS,OA)
   module A = A(KMSProc)

   proc main() : bool = {
        var b;
        KMSProc.init();
        b <@ A.guess();
        return (b = KMSProc.b) ;     
   }
}.

section CondProb.

module KMSProceduresCondProb(HSMs : HSMService,HstS : HstService, OA : OpPolSrvT) = {
  include KMSProcedures(HSMs, HstS, OA) [-init]

  (* Initialize game state *)
  proc init(bval : bool) = {
     HSMs.init();
     HstS.init();
     OA.init();
     KMSProcedures.hids <- fset0;
     KMSProcedures.tklist   <- fset0;
     KMSProcedures.corrupted <- fset0;
     KMSProcedures.tested <- empty;
     KMSProcedures.handles <- fset0;
     KMSProcedures.b <- bval;
     KMSProcedures.count_ops <- 0;
     KMSProcedures.count_auth <- 0;
     KMSProcedures.count_hst <- 0;
     KMSProcedures.count_updt <- 0;
     KMSProcedures.count_updt <- 0;
     KMSProcedures.count_hid <- 0;
     KMSProcedures.count_tkmng <- 0;
     KMSProcedures.count_unw <- 0;
     KMSProcedures.count_corr <- 0;
     KMSProcedures.count_test <- 0;
     KMSProcedures.count_inst <- 0;
  }

}.

module KMSRoRCondProb(A : KMSAdv, HSMs : HSMService, HstS : HstService, OA : OpPolSrvT) = {
   module KMSProc = KMSProceduresCondProb(HSMs,HstS,OA)
   module A = A(KMSProc)

  proc game(b : bool) : bool = {
    var b';

    KMSProc.init(b);
    b' <@ A.guess();
    
    return (b = b');
  }

   proc main() : bool = {
     var b, adv;
      
     b <$ {0,1};
     adv <@ game(b);
     
     (* Note here negation of output, in order
        to give a switched advantage relation below,
        and enable false = true + epsilon rearrangement. *)
     return (!adv) ;     
   }
}.

declare module A : KMSAdv {KMSProcedures, OpPolSrv}.
declare module HSMs : HSMService {A,KMSProcedures, OpPolSrv}.
declare module HstS : HstService {A,HSMs,KMSProcedures, OpPolSrv}.
declare module OA : OperatorActions {A,HSMs,HstS,KMSProcedures, OpPolSrv}.

equiv condprob_equiv : 
  KMSRoR(A,HSMs,HstS,OpPolSrv(OA)).main ~ KMSRoRCondProb(A,HSMs,HstS,OpPolSrv(OA)).main : 
  ={glob A, glob HSMs, glob HstS, glob OA} ==> res{1} = !res{2}.
proof.
  proc;inline *.
  wp; call (_: ={glob HSMs, glob HstS, glob OA, glob KMSProcedures, glob OpPolSrv});
    try by sim.
  swap{2} [1..3] 6;swap{1} 6 -3; swap{2} 2 1;wp;rnd;wp. 
  conseq (_: ={glob A, glob HSMs, glob HstS, glob OA}) => />;[smt () | sim].
qed.

equiv KMSRoRCondProb_E : KMSRoRCondProb(A, HSMs, HstS, OpPolSrv(OA)).game ~ KMSRoRCondProb(A, HSMs, HstS, OpPolSrv(OA)).game :
  ={b,glob OA, glob HstS, glob HSMs, glob A} ==> ={res}.
proof. by sim. qed.

axiom A_ll (O <: KMSOracles{A}) :
  islossless O.newOp =>
  islossless O.requestAuthorization =>
  islossless O.newHst => 
  islossless O.installInitialTrust => 
  islossless O.installUpdatedTrust => 
  islossless O.newHSM => 
  islossless O.newToken => 
  islossless O.updateTokenTrust => 
  islossless O.addTokenKey => 
  islossless O.unwrap => 
  islossless O.corrupt => 
  islossless O.test => 
  islossless A(O).guess.

axiom init_ll  : islossless OA.init.
axiom newOp_ll : islossless OA.newOp.
axiom requestAuthorization_ll : islossless OA.requestAuthorization.
axiom isGoodInitialTrust_ll : islossless OA.isGoodInitialTrust.
axiom newHst_ll : islossless HstS.newHst.
axiom installInitialTrust_ll : islossless HstS.installInitialTrust.
axiom installUpdatedTrust_ll : islossless HstS.installUpdatedTrust.
axiom gen_ll : islossless HSMs.gen.
axiom wrap_ll : islossless HSMs.wrap.
axiom unwrap_ll : islossless HSMs.unwrap.
axiom checkTrustUpdate_ll : islossless HSMs.checkTrustUpdate.
axiom isInstalledTrust_ll : islossless HstS.isInstalledTrust.
axiom HstSinit_ll : islossless HstS.init.
axiom HSMsinit_ll : islossless HSMs.init.

lemma game_ll : islossless KMSRoRCondProb(A, HSMs, HstS, OpPolSrv(OA)).game.
proof.
  islossless.
  apply (A_ll (<:KMSProceduresCondProb(HSMs,HstS,OpPolSrv(OA))) _ _ _ _ _ _ _ _ _ _ _ _) => /=.
  + by islossless; apply newOp_ll.
  + by islossless; apply requestAuthorization_ll.
  + by islossless; apply newHst_ll.
  + by islossless; apply installInitialTrust_ll.
  + by islossless; apply installUpdatedTrust_ll.
  + by islossless; apply gen_ll.
  + by islossless; apply wrap_ll.
  + by islossless;[apply wrap_ll | apply checkTrustUpdate_ll | apply unwrap_ll].
  + by islossless;[apply wrap_ll | apply unwrap_ll ].
  + by islossless; apply unwrap_ll.
  + by islossless; apply unwrap_ll.
  + by islossless;[apply unwrap_ll| apply isInstalledTrust_ll].
  + by apply init_ll.
  + by apply HstSinit_ll.
  by apply HSMsinit_ll. 
qed.

require import Ring StdRing.
import RField.

lemma AdvantageRelation &m :
  2%r * Pr[KMSRoR(A,HSMs,HstS,OpPolSrv(OA)).main()  @ &m: res] - 1%r 
  = Pr[KMSRoRCondProb(A,HSMs,HstS,OpPolSrv(OA)).game(false)  @ &m: res]
    - Pr[KMSRoRCondProb(A,HSMs,HstS,OpPolSrv(OA)).game(true)  @ &m: !res].
proof.
  have -> : Pr[KMSRoR(A, HSMs, HstS, OpPolSrv(OA)).main() @ &m : res] = 
            Pr[KMSRoRCondProb(A,HSMs,HstS,OpPolSrv(OA)).main() @ &m : !res].
  + by byequiv condprob_equiv.
  have -> : Pr[KMSRoRCondProb(A, HSMs, HstS, OpPolSrv(OA)).main() @ &m : !res] = 
            1%r/2%r * Pr[KMSRoRCondProb(A, HSMs, HstS, OpPolSrv(OA)).game(false) @ &m : res] + 
            1%r/2%r * Pr[KMSRoRCondProb(A, HSMs, HstS, OpPolSrv(OA)).game(true) @ &m : res].
  + byphoare (_ : (glob OA, glob HstS, glob HSMs, glob A){hr} = 
                  (glob OA, glob HstS, glob HSMs, glob A){m} ==> !res) => //.
    proc.
    pose rt := Pr[KMSRoRCondProb(A, HSMs, HstS, OpPolSrv(OA)).game(true) @ &m : res].
    pose rf := Pr[KMSRoRCondProb(A, HSMs, HstS, OpPolSrv(OA)).game(false) @ &m : res].
    seq 1 : b (1%r/2%r) rt (1%r/2%r) rf ((glob OA,glob HstS, glob HSMs, glob A) = 
                                         (glob OA,glob HstS, glob HSMs, glob A){m}); 1: by auto.
    + by rnd (fun x => x);skip => /= *; rewrite dboolE. 
    + rewrite /= /rt.
      have hc : phoare [ KMSRoRCondProb(A, HSMs, HstS, OpPolSrv(OA)).game : 
                      b /\ (glob OA,glob HstS, glob HSMs, glob A) = (glob OA,glob HstS, glob HSMs, glob A){m} ==> res ] = (Pr[KMSRoRCondProb(A, HSMs, HstS, OpPolSrv(OA)).game(true) @ &m : res]).
      + by bypr=> /> *; byequiv  KMSRoRCondProb_E => />.      
      by call hc.
    + by rnd (fun x => !x);skip => /= *; rewrite dboolE.   
    + rewrite /= /rf.
      have hc : phoare [ KMSRoRCondProb(A, HSMs, HstS, OpPolSrv(OA)).game : 
                      !b /\ (glob OA,glob HstS, glob HSMs, glob A) = (glob OA,glob HstS, glob HSMs, glob A){m} ==>
                      res ] = (Pr[KMSRoRCondProb(A, HSMs, HstS, OpPolSrv(OA)).game(false) @ &m : res]).
      + by bypr=> /> *; byequiv  KMSRoRCondProb_E => />.      
      by call hc.
    + smt().
have -> : ( 1%r / 2%r * Pr[KMSRoRCondProb(A, HSMs, HstS, OpPolSrv(OA)).game(true) @ &m : res] = 1%r/2%r * (1%r -  Pr[KMSRoRCondProb(A, HSMs, HstS, OpPolSrv(OA)).game(true) @ &m : !res])).
have -> :(Pr[KMSRoRCondProb(A, HSMs, HstS, OpPolSrv(OA)).game(true) @ &m : res] = 1%r -  Pr[KMSRoRCondProb(A, HSMs, HstS, OpPolSrv(OA)).game(true) @ &m : !res]).
rewrite Pr[mu_not] -subr_eq opprK addrC subrK. byphoare. apply game_ll. done.
by smt().
by smt().
have -> : ((1%r / 2%r *
 (1%r - Pr[KMSRoRCondProb(A, HSMs, HstS, OpPolSrv(OA)).game(true) @ &m : !res]) = 1%r / 2%r -  1%r / 2%r * Pr[KMSRoRCondProb(A, HSMs, HstS, OpPolSrv(OA)).game(true) @ &m : !res])) by smt().
have -> // : (2%r *
(1%r / 2%r * Pr[KMSRoRCondProb(A, HSMs, HstS, OpPolSrv(OA)).game(false) @ &m : res] +
 (1%r / 2%r -
  1%r / 2%r * Pr[KMSRoRCondProb(A, HSMs, HstS, OpPolSrv(OA)).game(true) @ &m : !res])) -
1%r = Pr[KMSRoRCondProb(A, HSMs, HstS, OpPolSrv(OA)).game(false) @ &m : res] - 
Pr[KMSRoRCondProb(A, HSMs, HstS, OpPolSrv(OA)).game(true) @ &m : !res]) by smt().
qed.

end section CondProb.

end KMSIND.
