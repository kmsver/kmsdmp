require import AllCore List FSet SmtMap Distr DBool Dexcepted Real RealExtra.

require KMSProto.

abstract theory Hop1.

clone include KMSProto.KMSProto.
import KMSIND.
import PKSMK_HSM.
import Policy.
import MRPKE.

axiom sig_bound : q_tkmng < q_sig.

(********************************************************)
(*      Idealized Signature                             *)
(********************************************************)

module HSMsP(SSch : PKSMK_HSM.FullSigService) = {
   include HSMs(SSch) [-unwrap]

   proc unwrap(hid : HId, tk : Token) : TkData option = {
        var check,optxt,rtd,cph,cphs,tag,dkey,trust,tkw,sid,msg,sig,inst;
        rtd <- None;
        trust <- tk.`tk_trust;
        inst <- tk.`tk_updt;
        (* Replace check with procedural version *)
        tkw <- tk.`tk_wdata;
        sid <- tkw.`tkw_signer;
        cphs <- tkw.`tkw_ekeys;
        msg <- encode_msg  (trust,cphs,sid,inst);
        sig <- tkw.`tkw_sig;
        check <@ SSch.verify(sid.`1, msg, sig);
        check <- check && (proj_pks (tr_mems trust)) = fdom cphs &&  (sid \in tr_mems trust);
        (* check <- checkToken inst trust trust tk.`tk_initial tk.`tk_wdata; *)
        if (check && hid \in tr_mems trust &&  hid_in hid HSMs.benc_keys) {
           dkey <- (oget HSMs.benc_keys.[hid.`1]).`2;
           tag <- encode_tag tk.`tk_trust;
           cphs <-tk.`tk_wdata.`tkw_ekeys;
           cph <- oget (cphs.[(oget HSMs.benc_keys.[hid.`1]).`1]);
           optxt <- decrypt dkey tag cph;
           if (optxt<>None) {
              rtd <- Some {| td_updt = inst;
                             td_trust = trust;
                             td_skeys = decode_ptxt (oget optxt) |};
           }
        }
        return rtd;
   }
    
}.

module HstSP(SSch : SigServiceV, OA : OpPolSrvT) : HstService = {
  include HstS(OA) [init,newHst]
  proc installInitialTrust(hstid : HstId, tk : Token) : bool = {
      var b,sid,cphs,msg,sig,check,tkw,trust,inst;
      b <- false;
      inst <- tk.`tk_updt;
      trust <- tk.`tk_trust;
      tkw <- tk.`tk_wdata;
      sid <- tkw.`tkw_signer;
      cphs <- tkw.`tkw_ekeys;
      msg <- encode_msg  (trust,cphs,sid,inst);
      sig <- tkw.`tkw_sig;
      check <@ SSch.verify(sid.`1, msg, sig);
      if (inst && check && (proj_pks (tr_mems trust)) = fdom cphs && (sid \in tr_mems trust)) {
         b <@ OA.isGoodInitialTrust(trust);
         if (b) {
            HstPolSrv.installInitialTrust(hstid,trust);
         }
      }
      return b;
  }

  proc installUpdatedTrust(hstid : HstId, tk : Token) : bool = {
      var b,sid,cphs,msg,sig,check,tkw,trust,old,inst;
      b <- false;
      if (hstid \in HstPolSrv.hosts_tr) {
         inst <- tk.`tk_updt;
         trust <- tk.`tk_trust;
      (* Replace check with procedural version *)
         tkw <- tk.`tk_wdata;
         sid <- tkw.`tkw_signer;
         cphs <- tkw.`tkw_ekeys;
         msg <- encode_msg  (trust,cphs,sid,inst);
         sig <- tkw.`tkw_sig;
         check <@ SSch.verify(sid.`1, msg, sig);
         old <- (oget HstPolSrv.hosts_tr.[hstid]);
         (* signer must be in old trust *)
         if (inst && check && (proj_pks (tr_mems old)) = fdom cphs && (sid \in tr_mems old)) {
            b <@ HstPolSrv.installUpdatedTrust(hstid,trust);
         }
      }
      return b;

  }
  proc isInstalledTrust(hstid : HstId, tk : Token) = {
      var b;
      b <@ HstPolSrv.isInstalledTrust(hstid,tk.`tk_trust);
      return b;
  }
}.


module KMSRoR_(A : KMSAdv, 
                OA : OperatorActions) = 
                KMSRoR(A,HSMs(RealSigServ),HstS(OpPolSrv(OA)),OpPolSrv(OA)).

module KMSRoR0(A : KMSAdv, 
                OA : OperatorActions) = 
                KMSRoR(A,HSMsP(RealSigServ),HstSP(RealSigServ,OpPolSrv(OA)),OpPolSrv(OA)).

section PROOF.

declare module OA : OperatorActions {RealSigServ, KMSProcedures, HSMs, HstPolSrv, OpPolSrv, HstS}.
declare module A  : KMSAdv     {OA, RealSigServ, KMSProcedures, HSMs, HstPolSrv, OpPolSrv, HstS}.
                                 
local equiv sign_E n : RealSigServ.sign ~ RealSigServ.sign : 
   ={pk, m, glob RealSigServ} /\ RealSigServ.count_sig{1} = n ==> ={res, glob RealSigServ} /\ RealSigServ.count_sig{1} <= n + 1.
proof. 
  proc;sp; if; [done | | skip => /#].
  by wp;if => //;auto.
qed.

local equiv wrap_E n : HSMs(RealSigServ).wrap ~ HSMs(RealSigServ).wrap : 
   ={hid,td, glob RealSigServ} /\ RealSigServ.count_sig{1} = n ==> ={res,glob RealSigServ} /\ RealSigServ.count_sig{1} <= n + 1.
proof.
  proc; wp; ecall (sign_E RealSigServ.count_sig{1}); auto.
qed.

local equiv unwrap_E : HSMs(RealSigServ).unwrap ~ HSMsP(RealSigServ).unwrap : 
   ={hid, tk, HSMs.benc_keys} ==> ={res, HSMs.benc_keys}.
proof.
  proc; inline *; wp; skip => /> /#.
qed.

lemma makesigproc &m :
  Pr [ KMSRoR_(A,OA).main() @ &m : res ] =
  Pr [ KMSRoR0(A,OA).main() @ &m : res ].
proof.
byequiv; last 2 by [].
proc; inline*.
call (_: (={glob HSMPolSrv,glob HstS, glob RealSigServ,glob OA, glob HstPolSrv, glob KMSProcedures, glob OpPolSrv, glob HSMs}) /\RealSigServ.count_sig{1} <= KMSProcedures.count_tkmng{1}); last by wp;rnd;wp;call(_:true);wp;skip => />.
+ by proc;inline *;sp;if => //=;wp;call(_:true);wp;skip. 
+ by proc => /=; sim />.
+ by proc => /=; sim />.
+ proc => /=; conseq />.
  inline *; sp; if; [smt()| |skip;smt()].
  wp;sp; if; [smt()| |skip;smt()].
  wp; call(_:true);wp;skip => /#.
+ by proc; inline *; wp; skip => /#.
+ by proc; sim />.
+ proc; sp; if => //=.
  wp;if;[done | wp | skip => /#].
  ecall (wrap_E RealSigServ.count_sig{1}); wp; skip => /#.
+ proc; sp; if => //; wp; conseq />.
  seq 0 0 : (#pre /\ RealSigServ.count_sig{1} <= KMSProcedures.count_tkmng{2} + 1); 1: by skip => /#.
  if => //.
  seq 1 1 : (#pre /\ ={tdo}); 1: by call unwrap_E.
  inline OpPolSrv(OA).quorumAssumption; sp 2 2.
  if => //.
  seq 1 1 : (#pre /\ ={tdoo}); 1: sim />.
  if => //.
  by wp; ecall (wrap_E RealSigServ.count_sig{1}); wp; skip => /#.
+ proc; sp. 
  seq 1 1: (#pre /\ ={key}); 1: by auto.
  if => //; wp.
  seq 0 0 : (#pre /\ RealSigServ.count_sig{1} <= KMSProcedures.count_tkmng{2} + 1); 1: by skip => /#. 
  if => //.
  seq 1 1 : (#pre /\ ={tdo}); 1: by call unwrap_E.
  if => //.
  sp; if => //; wp; ecall (wrap_E RealSigServ.count_sig{1}); skip => /#.
+ by proc; sim /> (_: ={HSMs.benc_keys}); conseq unwrap_E.
+ by proc; sim /> (_: ={HSMs.benc_keys}); conseq unwrap_E.
by proc; sim /> (_: ={HSMs.benc_keys}); conseq unwrap_E.
qed.

end section PROOF.

module KMSRoR1(A : KMSAdv, 
               OA : OperatorActions) = 
               KMSRoR(A,HSMsP(IdealSigServ),HstSP(IdealSigServ,OpPolSrv(OA)),OpPolSrv(OA)).

module HSMsI(SigSch : PKSMK_HSM.OrclInd) = {
   var who : (Pk, pkey) fmap
   var benc_keys : (pkey,(Pk*Sk)) fmap

   proc init() : unit = {
       who <- empty;
       benc_keys <- empty;
   }

   proc gen() : HId = {
        var verk,pk, sk;
        (sk,pk) <$ gen;
        if (pk \in who) {
          verk <- oget who.[pk];
        } else {
          verk <@ SigSch.keygen();
          if (verk \in benc_keys) {
            (pk, sk) <- oget benc_keys.[verk];
          } else {
            who.[pk] <- verk;
            benc_keys.[verk] <- (pk,sk);
          }
        }
        return (verk,pk);
   }

   proc checkTrustUpdate(old : TkData, new : Trust, 
                       auth : Authorizations) : TkData option = {
        var c;
        c <@ HSMPolSrv.checkTrustUpdate(old.`td_trust,new,auth);
        return if (c)
               then Some (set_trust new old)
               else None;
   }

   proc wrap(hid : HId,td : TkData) : Token = {
        var mem,tk,tkw,keys,ekeys,sig,msg,tag,ptxt,inst;
        inst <- td.`td_updt;
        mem <- tr_mems td.`td_trust;
        keys <- td.`td_skeys;
        tag <- encode_tag td.`td_trust;
        ptxt <- encode_ptxt keys;
        ekeys <$ mencrypt (proj_pks mem) tag ptxt;
        msg<-encode_msg (td.`td_trust,ekeys,hid,inst);
        sig <@ SigSch.sign(hid.`1,msg);
        tkw <- {| tkw_ekeys = ekeys; 
                  tkw_signer = hid; 
                  tkw_sig = oget sig |};
        tk <- {| tk_updt = td.`td_updt;
                 tk_trust = td.`td_trust;
                 tk_wdata = tkw; |};
        return tk;
   }

   proc unwrap(hid : HId, tk : Token) : TkData option = {
        var check,optxt,rtd,cph,cphs,tag,dkey,trust,tkw,sid,msg,sig,inst;
        rtd <- None;
        trust <- tk.`tk_trust;
        inst <- tk.`tk_updt;
        (* Replace check with procedural version *)
        tkw <- tk.`tk_wdata;
        sid <- tkw.`tkw_signer;
        cphs <- tkw.`tkw_ekeys;
        msg <- encode_msg  (trust,cphs,sid,inst);
        sig <- tkw.`tkw_sig;
        check <@ SigSch.verify(sid.`1, msg, sig);
        check <- check && (proj_pks (tr_mems trust)) = fdom cphs &&  (sid \in tr_mems trust);
        (* check <- checkToken trust trust tk.`tk_wdata; *)
        if (check && hid \in tr_mems trust &&  hid_in hid benc_keys) {
           dkey <- (oget benc_keys.[hid.`1]).`2;
           tag <- encode_tag tk.`tk_trust;
           cphs <-tk.`tk_wdata.`tkw_ekeys;
           cph <- oget (cphs.[(oget benc_keys.[hid.`1]).`1]);
           optxt <- decrypt dkey tag cph;
           if (optxt<>None) {
              rtd <- Some {| td_updt = inst;
                             td_trust = trust;
                             td_skeys = decode_ptxt (oget optxt) |};
           }
        }
        return rtd;
   }
}.

(* The adversary *)

module Bhop1(A : KMSAdv, 
             OA : OperatorActions, 
             HopScheme : PKSMK_HSM.OrclInd) = 
             KMSRoR(A,HSMsI(HopScheme),HstSP(HopScheme,OpPolSrv(OA)),OpPolSrv(OA)).

section.

declare module OA : OperatorActions {RealSigServ, KMSProcedures, HSMs, HstPolSrv, OpPolSrv, HstS, HSMsI}.
declare module A  : KMSAdv     {OA, RealSigServ, KMSProcedures, HSMs, HstPolSrv, OpPolSrv, HstS, HSMsI}.

lemma hop1 &m:
  Pr [ KMSRoR0(A,OA).main() @ &m : res ] -
  Pr [ KMSRoR1(A,OA).main() @ &m : res ] = 
  Pr [ IndSig(RealSigServ,Bhop1(A,OA)).main() @ &m : res ] - Pr [ IndSig(IdealSigServ,Bhop1(A,OA)).main() @ &m : res ].
proof.
cut ->: Pr [ KMSRoR0(A,OA).main() @ &m : res ] =
        Pr [ IndSig(RealSigServ,Bhop1(A,OA)).main() @ &m : res ].
+ byequiv; last 2 by [].
  proc; inline*. sp; wp.
  call (_: (={glob HSMPolSrv,glob RealSigServ,glob Scheme, glob OA, glob HstPolSrv, glob KMSProcedures, glob OpPolSrv,glob HstS}
          /\ (glob HSMs){1} = (glob HSMsI){2})); first 12 by sim.
  by wp; rnd; wp; call (_:true);wp;skip.
cut -> //: Pr [  KMSRoR1(A,OA).main() @ &m : res ] =
           Pr [ IndSig(IdealSigServ,Bhop1(A,OA)).main() @ &m : res].
byequiv; last 2 by []. 
proc; inline*; sp; wp.
call (_: (={glob HSMPolSrv,glob RealSigServ,glob Scheme, glob OA, glob HstPolSrv, glob KMSProcedures, glob OpPolSrv,glob HstS}
           /\ (glob HSMs){1} = (glob HSMsI){2})); first 12 by sim.
by wp;rnd; wp; call (_:true);wp; skip.
qed.

lemma concl1 &m: 
  Pr[ KMSRoR(A,HSMs(RealSigServ),HstS(OpPolSrv(OA)),OpPolSrv(OA)).main() @ &m : res ] = 
  Pr[ KMSRoR1(A, OA).main() @ &m : res ] + 
  Pr[ IndSig(RealSigServ, Bhop1(A, OA)).main() @ &m : res ] - Pr[ IndSig(IdealSigServ, Bhop1(A, OA)).main() @ &m : res ].
proof.
rewrite (makesigproc OA A &m).
rewrite (_: Pr [ KMSRoR0(A,OA).main() @ &m : res ] =  
            Pr [ KMSRoR1(A,OA).main() @ &m : res ] +
            Pr [ IndSig(RealSigServ,Bhop1(A,OA)).main() @ &m : res] -
            Pr [ IndSig(IdealSigServ,Bhop1(A,OA)).main() @ &m : res]) //. 
by have /# := hop1 &m.
qed.

end section.

end Hop1.


