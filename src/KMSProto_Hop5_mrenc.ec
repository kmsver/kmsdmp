require import AllCore List FSet SmtMap Distr DBool Dexcepted Real RealExtra.

require KMSProto_Hop4_protected.

abstract theory Hop5.

clone include KMSProto_Hop4_protected.Hop4.
import KMSProto.
import KMSIND.
import PKSMK_HSM.
import Policy.
import MRPKE.

(******************************************************)
(*             Replace protected keys with witness    *)  
(******************************************************)

op fake_keys (l : (Handle,'a) fmap) : (Handle,Key) fmap =
  map (fun k v => witness) l.

module KMSProcedures4(OA : OperatorActions) : KMSOracles = {
  module HSMsPTS = HSMsP_TS(IdealTrustService(OpPolSrv(OA)))
  include KMSProcedures3(OA) [-updateTokenTrust,addTokenKey]

  proc updateTokenTrust(hid : HId, new_trust : Trust,
                       auth : Authorizations, tk : Token) : Token option = {
      var tdo,tdoo,tkn,rtd,protected,check,optxt,trust,tkw,sid,cphs,msg,sig,td,mem,keys,tag,ptxt,fake,ekeys,sigo,inst;
      rtd <- None;
      if (KMSProcedures.count_tkmng < q_tkmng /\ 
          size (elems (tr_mems (tr_data new_trust))) < max_size /\
          size (elems (tr_mems (tr_data tk.`tk_trust))) < max_size) {
         if (hid \in KMSProcedures.hids) {
            tdo <- None;
            protected <@ IdealTrustService(OpPolSrv(OA)).isProtectedTrust(tk.`tk_trust);
            if (protected) {
               inst <- tk.`tk_updt;
               trust <- tk.`tk_trust;
               tkw <- tk.`tk_wdata;
               sid <- tkw.`tkw_signer;
               cphs <- tkw.`tkw_ekeys;
               msg <- encode_msg  (trust,cphs,sid,inst);
               sig <- tkw.`tkw_sig;
               check <@ IdealSigServ.verify(sid.`1, msg, sig);
               check <- check && (proj_pks (tr_mems trust)) = fdom cphs &&  (sid \in tr_mems trust);
               if (check && hid \in tr_mems trust &&  hid_in hid HSMs.benc_keys) {
                  optxt <- Some (encode_ptxt (oget KMSProcedures3.wrapped_keys.[tk]));
                  tdo <- Some {| td_updt = inst;
                                 td_trust = trust;
                                 td_skeys = decode_ptxt (oget optxt) |};
               }
            }
            else{ 
               tdo <@ HSMsPTS.unwrap(hid, tk); 
            }
            if (tdo <> None /\ quorum_assumption OpPolSrv.badOps OpPolSrv.goodOps new_trust) {
               tdoo <@ HSMsPTS.checkTrustUpdate(oget tdo,new_trust,auth);
              if (tdoo <> None) {
                 (* This should be in sync with above *)
                 protected <@ IdealTrustService(OpPolSrv(OA)).isProtectedTrust((oget tdoo).`td_trust);
                 if (protected) {
                     td <- oget tdoo;
                     inst <- td.`td_updt;
                     mem <- tr_mems td.`td_trust;
                     keys <- td.`td_skeys;
                     tag <- encode_tag td.`td_trust;
                     ptxt <- encode_ptxt keys;
                     fake <- encode_ptxt (fake_keys keys);
                     ekeys <$ mencrypt (proj_pks mem) tag  fake;
                     msg<- encode_msg (td.`td_trust,ekeys,hid,inst);
                     sigo <@ IdealSigServ.sign(hid.`1,msg);
                     tkw <- {| tkw_ekeys = ekeys; 
                            tkw_signer = hid; 
                            tkw_sig = oget sigo |};
                     tkn <- {| tk_updt = inst;
                            tk_trust = td.`td_trust;
                            tk_wdata = tkw; |};
                     KMSProcedures3.wrapped_keys.[tkn] <- (oget tdoo).`td_skeys;
                 }
                 else {
                    tkn <@ HSMsPTS.wrap(hid,(oget tdoo));
                 }
                 KMSProcedures.tklist <- KMSProcedures.tklist `|` fset1 tkn;
                 rtd <- Some tkn;
              } 
            }
         }
         KMSProcedures.count_tkmng <- KMSProcedures.count_tkmng  + 1;
      } 
      return rtd;
  }

  proc addTokenKey(hid : HId, hdl : Handle, 
                   tk : Token) : Token option = {
      var tdo,tdoo, td,rtd,key, tkr, protected,check,optxt,trust,tkw,sid,cphs,msg,sig,mem,keys,tag,ptxt,fake,ekeys,sigo,inst;
      rtd <- None;
      key <$ genKey;
      if (KMSProcedures.count_tkmng < q_tkmng /\
          size (elems (tr_mems (tr_data tk.`tk_trust))) < max_size) {
         if ((hid \in KMSProcedures.hids)&&(!(hdl \in KMSProcedures.handles))) {
            tdo <- None;
            protected <@ IdealTrustService(OpPolSrv(OA)).isProtectedTrust(tk.`tk_trust);
            if (protected) {
               inst <- tk.`tk_updt;
               trust <- tk.`tk_trust;
               tkw <- tk.`tk_wdata;
               sid <- tkw.`tkw_signer;
               cphs <- tkw.`tkw_ekeys;
               msg <- encode_msg  (trust,cphs,sid,inst);
               sig <- tkw.`tkw_sig;
               check <@ IdealSigServ.verify(sid.`1, msg, sig);
               check <- check && (proj_pks (tr_mems trust)) = fdom cphs &&  (sid \in tr_mems trust);
               if (check && hid \in tr_mems trust &&  hid_in hid HSMs.benc_keys) {
                  optxt <- Some (encode_ptxt (oget KMSProcedures3.wrapped_keys.[tk]));
                  tdo <- Some {| td_updt = inst;
                                 td_trust = trust;
                                 td_skeys = decode_ptxt (oget optxt) |}; 
               }
            }
            else{ 
               tdo <@ HSMsPTS.unwrap(hid, tk); 
            }
            if (tdo <> None) {
               td <- oget tdo;
               tdoo <- add_key hdl key td;
               if (tdoo <> None) {
                 protected <@ IdealTrustService(OpPolSrv(OA)).isProtectedTrust((oget tdoo).`td_trust);
                 if (protected) {
                     td <- oget tdoo;
                     inst <- td.`td_updt;
                     mem <- tr_mems td.`td_trust;
                     keys <- td.`td_skeys;
                     tag <- encode_tag td.`td_trust;
                     ptxt <- encode_ptxt keys;
                     fake <- encode_ptxt (fake_keys keys);
                     ekeys <$ mencrypt (proj_pks mem) tag  fake;
                     msg<- encode_msg (td.`td_trust,ekeys,hid,inst);
                     sigo <@ IdealSigServ.sign(hid.`1,msg);
                     tkw <- {| tkw_ekeys = ekeys; 
                            tkw_signer = hid; 
                            tkw_sig = oget sigo |};
                     tkr <- {| tk_updt = inst;
                               tk_trust = td.`td_trust;
                               tk_wdata = tkw; |};
                     KMSProcedures3.wrapped_keys.[tkr] <- (oget tdoo).`td_skeys;
                     }
                 else {
                    tkr <@ HSMsPTS.wrap(hid,(oget tdoo));
                 }
                 KMSProcedures.tklist <- KMSProcedures.tklist `|` fset1 tkr;
                 KMSProcedures.handles <- KMSProcedures.handles `|` fset1 hdl;
                 rtd <- Some tkr;
              } 
            }    
         }
         KMSProcedures.count_tkmng <- KMSProcedures.count_tkmng  + 1;
      }
      return rtd;
  }

}.

module HSMs_MRPKEOr(OA : OperatorActions, MRPKEOr : MRPKE.MRPKE_OrclT) = {
   include HSMsP_TS(IdealTrustService(OpPolSrv(OA))) [-gen,unwrap,init]
   (* [ekeys] maps verification keys to encryption key-pairs *)
   var benc_keys_stripped : (pkey,Pk) fmap

   proc init() = {
        benc_keys_stripped <- empty;
        HSMsP_TS(IdealTrustService(OpPolSrv(OA))).init();
   }
   
   proc gen() : HId = {
        var verk,pk;
        pk <@ MRPKEOr.gen();
        if (pk \in HSMs.who) {
          verk <- oget HSMs.who.[pk];
        } else {
          verk <@ IdealSigServ.keygen();
          if (verk \in benc_keys_stripped) {
            pk <- oget benc_keys_stripped.[verk];
          } else {
            HSMs.who.[pk] <- verk;
            benc_keys_stripped.[verk] <- pk;
          }
        }
        return (verk,pk);
   }

   proc unwrap(hid : HId, tk : Token) : TkData option = {
        var check,optxt,rtd,cph,cphs,tag,pubkey,trust,tkw,sid,msg,sig,inst;
        rtd <- None;
        inst <- tk.`tk_updt;
        trust <- tk.`tk_trust;
        tkw <- tk.`tk_wdata;
        sid <- tkw.`tkw_signer;
        cphs <- tkw.`tkw_ekeys;
        msg <- encode_msg  (trust,cphs,sid,inst);
        sig <- tkw.`tkw_sig;
        check <@ IdealSigServ.verify(sid.`1, msg, sig);
        check <- check && (proj_pks (tr_mems trust)) = fdom cphs &&  (sid \in tr_mems trust);
        (* check <- checkToken trust trust tk.`tk_initial tk.`tk_wdata; *)
        if (check && hid \in tr_mems trust &&  hid_in_nopk hid benc_keys_stripped) {
           pubkey <- (oget benc_keys_stripped.[hid.`1]);
           tag <- encode_tag tk.`tk_trust;
           cphs <-tk.`tk_wdata.`tkw_ekeys;
           cph <- oget (cphs.[(oget benc_keys_stripped.[hid.`1])]);
           optxt <- MRPKEOr.dec (pubkey, tag, cph);
           if (optxt<>None) {
              rtd <- Some {| td_updt = inst;
                             td_trust = trust;
                             td_skeys = decode_ptxt (oget optxt) |};
           }
        }
        return rtd;
   }

   proc wrap_lor(hid : HId,td : TkData) : Token = {
        var mem,tk,tkw,keys,ekeys,sig,msg,tag,ptxt,fake,inst;
        inst <- td.`td_updt;
        mem <- tr_mems td.`td_trust;
        keys <- td.`td_skeys;
        tag <- encode_tag td.`td_trust;
        ptxt <- encode_ptxt keys;
        fake <- encode_ptxt (fake_keys keys);
        ekeys <@ MRPKEOr.lor((proj_pks mem), tag, ptxt, fake);
        msg<- encode_msg (td.`td_trust,oget ekeys,hid,inst);
        sig <@ IdealSigServ.sign(hid.`1,msg);
        tkw <- {| tkw_ekeys = oget ekeys; 
                  tkw_signer = hid; 
                  tkw_sig = oget sig |};
        tk <- {| tk_updt = td.`td_updt;
                 tk_trust = td.`td_trust;
                 tk_wdata = tkw; |};
        return tk;
   }
}.

module KMSRoR4(A : KMSAdv, OA : OperatorActions) = {
   module KMSProc = KMSProcedures4(OA)
   module A = A(KMSProc)

   proc main() : bool = {
        var b;
        KMSProc.init();
        b <@ A.guess();
        return (b = KMSProcedures.b) ;     
   }
}.

module KMSProceduresHop3(OA : OperatorActions, SigSch : PKSMK_HSM.FullSigService, MRPKEOr : MRPKE.MRPKE_OrclT) : KMSOracles = {
  module HSMsO = HSMs_MRPKEOr(OA,MRPKEOr)
  module KP = KMSProcedures3(OA)
  include KP [-updateTokenTrust,addTokenKey,newHSM, init,unwrap,corrupt,test]

  proc init() = {
     HSMsO.init();
     HstSP_TS(IdealTrustService(OpPolSrv(OA))) .init();
     IdealTrustService(OpPolSrv(OA)).init();
     KMSProcedures.hids <- fset0;
     KMSProcedures.tklist   <- fset0;
     KMSProcedures.corrupted <- fset0;
     KMSProcedures.tested <- empty;
     KMSProcedures.handles <- fset0;
     KMSProcedures.b         <$ {0,1};
     KMSProcedures.count_ops <- 0;
     KMSProcedures.count_auth <- 0;
     KMSProcedures.count_hst <- 0;
     KMSProcedures.count_updt <- 0;
     KMSProcedures.count_hid <- 0;
     KMSProcedures.count_tkmng <- 0;
     KMSProcedures.count_unw <- 0;
     KMSProcedures.count_corr <- 0;
     KMSProcedures.count_test <- 0;
     KMSProcedures.count_inst <- 0;
     KMSProcedures3.wrapped_keys <- empty;
  }

   proc newHSM() : HId option = {
     var hid,verk,pk,pksig,sksig, rhid;
     rhid <- None;
     if (KMSProcedures.count_hid < q_hid) {
       if (size (nosigpks_elems' PKSMK_HSM.RealSigServ.qs OpPolSrv.genuine RealTrustService.parentTrust) <=
            (q_hid + q_tkmng + q_tkmng) * max_size) {
         pk <@ MRPKEOr.gen();
         if (pk \in HSMs.who) {
           verk <- oget HSMs.who.[pk];
         } else {
           (pksig,sksig) <$ keygen \ (fun (pksk : pkey*skey) => 
                              nosigpks RealSigServ.qs OpPolSrv.genuine RealTrustService.parentTrust pksk.`1);
           if (pksig \notin RealSigServ.pks_sks) { RealSigServ.pks_sks.[pksig] <- sksig; }
           verk  <-pksig;
           if (verk \in HSMsO.benc_keys_stripped) {
             pk <- oget HSMsO.benc_keys_stripped.[verk];
           } else {
             HSMs.who.[pk] <- verk;
             HSMsO.benc_keys_stripped.[verk] <- pk;
           }
         }
         hid <- (verk,pk);
         if (! (hid \in KMSProcedures.hids)) {      
           KMSProcedures.hids <- KMSProcedures.hids `|` fset1 hid;
         }
         OpPolSrv(OA).addHId(hid);
         KMSProcedures.count_hid <- KMSProcedures.count_hid + 1;
         rhid <- Some hid;
       }
     } 
     return rhid;
  }

  proc updateTokenTrust(hid : HId, new_trust : Trust,
                       auth : Authorizations, tk : Token) : Token option = {
      var tdo,tdoo,tkn,rtd,protected,trust,check,optxt,tkw,sid,cphs,msg,sig,inst;
      rtd <- None;
      if (KMSProcedures.count_tkmng < q_tkmng /\ 
          size (elems (tr_mems (tr_data new_trust))) < max_size /\
          size (elems (tr_mems (tr_data tk.`tk_trust))) < max_size) {
         if (hid \in KMSProcedures.hids) {
            tdo <- None;
            protected <@ IdealTrustService(OpPolSrv(OA)).isProtectedTrust(tk.`tk_trust);
            if (protected) {
               inst <- tk.`tk_updt;
               trust <- tk.`tk_trust;
               tkw <- tk.`tk_wdata;
               sid <- tkw.`tkw_signer;
               cphs <- tkw.`tkw_ekeys;
               msg <- encode_msg  (trust,cphs,sid,inst);
               sig <- tkw.`tkw_sig;
               check <@ IdealSigServ.verify(sid.`1, msg, sig);
               check <- check && (proj_pks (tr_mems trust)) = fdom cphs &&  (sid \in tr_mems trust);
               if (check && hid \in tr_mems trust && hid_in_nopk hid HSMsO.benc_keys_stripped) {
                  optxt <- Some (encode_ptxt (oget KMSProcedures3.wrapped_keys.[tk]));
                  tdo <- Some {| td_updt = inst;
                                 td_trust = trust;
                                 td_skeys = decode_ptxt (oget optxt) |};
               }
            }
            else{ 
               tdo <@ HSMsO.unwrap(hid, tk); 
            }
            if (tdo <> None /\ quorum_assumption OpPolSrv.badOps OpPolSrv.goodOps new_trust) {
               tdoo <@ HSMsO.checkTrustUpdate(oget tdo,new_trust,auth);
               if (tdoo <> None) {
                 protected <@  IdealTrustService(OpPolSrv(OA)).isProtectedTrust((oget tdoo).`td_trust);
                 (* If our trust is still protected, we must continue calling the lor oracle *)
                 if (protected) {
                    tkn <@ HSMsO.wrap_lor(hid, (oget tdoo));
                    KMSProcedures3.wrapped_keys.[tkn] <- (oget tdoo).`td_skeys;
                 }
                 else { tkn <@ HSMsO.wrap(hid,(oget tdoo));}
                 KMSProcedures.tklist <- KMSProcedures.tklist `|` fset1 tkn;
                 rtd <- Some tkn;
              } 
            }  
         }
         KMSProcedures.count_tkmng <- KMSProcedures.count_tkmng  + 1;
      } 
      return rtd;
  }

  proc addTokenKey(hid : HId, hdl : Handle, 
                tk : Token) : Token option = {
      var tdo,tdoo, td,rtd,key, tkr, protected, trust,optxt,check,tkw,sid,cphs,msg,sig,inst;
      rtd <- None;
      key <$ genKey;
      if (KMSProcedures.count_tkmng < q_tkmng /\ 
          size (elems (tr_mems (tr_data tk.`tk_trust))) < max_size) {
         if ((hid \in KMSProcedures.hids)&&(!(hdl \in KMSProcedures.handles))) {
            tdo <- None;
            protected <@ IdealTrustService(OpPolSrv(OA)).isProtectedTrust(tk.`tk_trust);
            if (protected) {
               inst <- tk.`tk_updt;
               trust <- tk.`tk_trust;
               tkw <- tk.`tk_wdata;
               sid <- tkw.`tkw_signer;
               cphs <- tkw.`tkw_ekeys;
               msg <- encode_msg  (trust,cphs,sid,inst);
               sig <- tkw.`tkw_sig;
               check <@ IdealSigServ.verify(sid.`1, msg, sig);
               check <- check && (proj_pks (tr_mems trust)) = fdom cphs &&  (sid \in tr_mems trust);
               if (check && hid \in tr_mems trust && hid_in_nopk hid HSMsO.benc_keys_stripped) {
                  optxt <- Some (encode_ptxt (oget KMSProcedures3.wrapped_keys.[tk]));
                  tdo <- Some {| td_updt = tk.`tk_updt;
                                 td_trust = tk.`tk_trust;
                                 td_skeys = decode_ptxt (oget optxt) |};
               }
            }
            else{ 
               tdo <@ HSMsO.unwrap(hid, tk); 
            }
            if (tdo <> None) {
               td <- oget tdo;
               tdoo <- add_key hdl key td;
               if (tdoo <> None) {
                 (* We now split the treatment of protected tokens *)
                 protected <@  IdealTrustService(OpPolSrv(OA)).isProtectedTrust((oget tdoo).`td_trust);
                 if (protected) {
                    (* If our trust is still protected, we must continue calling the lor oracle *)
                    tkr <@ HSMsO.wrap_lor(hid, (oget tdoo));
                    KMSProcedures3.wrapped_keys.[tkr] <- (oget tdoo).`td_skeys;
                 }
                 else {
                    (* Otherwise, we produce a standard MR encryption *)
                    tkr <@ HSMsO.wrap(hid,(oget tdoo));
                 }
                 KMSProcedures.tklist <- KMSProcedures.tklist `|` fset1 tkr;
                 KMSProcedures.handles <- KMSProcedures.handles `|` fset1 hdl;
                 rtd <- Some tkr;
              } 
            }    
         }
         KMSProcedures.count_tkmng <- KMSProcedures.count_tkmng  + 1;
      } 
      return rtd;
  }

  proc unwrap(hid : HId, tk : Token) : TkData option = {
      var rtd;
      rtd <- None;
      if (KMSProcedures.count_unw < q_unw) {
         if (hid \in KMSProcedures.hids) {
            if (!(tk \in KMSProcedures.tklist) || !(all_genuine OpPolSrv.genuine (tr_data (tk_trust tk)))) {
                rtd <@ HSMsO.unwrap(hid,tk);
             }
         }
         KMSProcedures.count_unw <- KMSProcedures.count_unw  + 1;
      } 
      return rtd;    
  }   

  proc corrupt(hid : HId, tk : Token, hdl : Handle) : Key option = {
      var rsk,tdo,trust,protected,check,optxt,tkw,sid,cphs,msg,sig,inst;
      rsk <- None;
      if (KMSProcedures.count_corr < q_corr) {
         if ((hid \in KMSProcedures.hids)&&(!(hdl \in KMSProcedures.tested))) {
            tdo <- None;
            protected <@ IdealTrustService(OpPolSrv(OA)).isProtectedTrust(tk.`tk_trust);
            if (protected) {
               inst <- tk.`tk_updt;
               trust <- tk.`tk_trust;
               tkw <- tk.`tk_wdata;
               sid <- tkw.`tkw_signer;
               cphs <- tkw.`tkw_ekeys;
               msg <- encode_msg  (trust,cphs,sid,inst);
               sig <- tkw.`tkw_sig;
               check <@ IdealSigServ.verify(sid.`1, msg, sig);
               check <- check && (proj_pks (tr_mems trust)) = fdom cphs && (sid \in tr_mems trust);
               if (check && hid \in tr_mems trust && hid_in_nopk hid HSMsO.benc_keys_stripped) {
                  optxt <- Some (encode_ptxt (oget KMSProcedures3.wrapped_keys.[tk]));
                  tdo <- Some {| td_updt = inst;
                                 td_trust = trust;
                                 td_skeys = decode_ptxt (oget optxt) |};
               }
            }
            else{ 
               tdo <@ HSMsO.unwrap(hid, tk); 
            }
            if (tdo <> None && hdl \in (oget tdo).`td_skeys) {
                rsk <- (oget tdo).`td_skeys.[hdl];
                KMSProcedures.corrupted <- KMSProcedures.corrupted `|` fset1 hdl;
            }
         }
         KMSProcedures.count_corr <- KMSProcedures.count_corr  + 1;
      }
      return rsk;    
  }  

  proc test(hid : HId, hstid : HstId, tk : Token, hdl : Handle) : Key option = {
      var rkey,installed,rsk,tdo,trust,protected,check,optxt,tkw,sid,cphs,msg,sig,inst;
      rsk <- None;
      rkey <$ genKey;
      if (KMSProcedures.count_test < q_test) {
         installed <@ HstSP_TS(IdealTrustService(OpPolSrv(OA))).isInstalledTrust(hstid,tk);
         if ((hid \in KMSProcedures.hids)&&
             (!(hdl \in KMSProcedures.corrupted))&&
             installed) {
            tdo <- None;
            (* We now reject unprotected tokens *)
            protected <@ IdealTrustService(OpPolSrv(OA)).isProtectedTrust(tk.`tk_trust);
            if (protected) {
               inst <- tk.`tk_updt;
               trust <- tk.`tk_trust;
               tkw <- tk.`tk_wdata;
               sid <- tkw.`tkw_signer;
               cphs <- tkw.`tkw_ekeys;
               msg <- encode_msg  (trust,cphs,sid,inst);
               sig <- tkw.`tkw_sig;
               check <@ IdealSigServ.verify(sid.`1, msg, sig);
               check <- check && (proj_pks (tr_mems trust)) = fdom cphs &&  (sid \in tr_mems trust);
               if (check && hid \in tr_mems trust && hid_in_nopk hid HSMsO.benc_keys_stripped) {
                  optxt <- Some (encode_ptxt (oget KMSProcedures3.wrapped_keys.[tk]));
                  tdo <- Some {| td_updt = inst;
                                 td_trust = trust;
                                 td_skeys = decode_ptxt (oget optxt) |};
               }
            }
            if (tdo <> None && hdl \in (oget tdo).`td_skeys) {
                 rsk <- (oget tdo).`td_skeys.[hdl];
                 if (hdl \in KMSProcedures.tested) {
                       rkey <- oget KMSProcedures.tested.[hdl]; 
                 }
                 else {
                      KMSProcedures.tested.[hdl] <- rkey;
                 }
                 rsk <- if (KMSProcedures.b) then Some rkey else rsk; 
            }
         }
         KMSProcedures.count_test <- KMSProcedures.count_test  + 1;
      }
      return rsk; 
  }

}.

lemma tk_eq1 (tk1 tk2 : Token) :
  tk1 = tk2 <=>
  ((tk1.`tk_wdata.`tkw_signer = tk2.`tk_wdata.`tkw_signer) /\
  (tk1.`tk_updt = tk2.`tk_updt) /\
  (tk1.`tk_trust = tk2.`tk_trust) /\
  (tk1.`tk_wdata.`tkw_ekeys = tk2.`tk_wdata.`tkw_ekeys) /\
  (tk1.`tk_wdata.`tkw_sig = tk2.`tk_wdata.`tkw_sig)) by smt().

lemma tk_eq2 tk t s ks hid i:
  tk = {| tk_updt = i; tk_trust = t; tk_wdata =
           {| tkw_ekeys = ks; tkw_signer =hid ; tkw_sig = s |}; |} <=>
  ((tk.`tk_wdata.`tkw_signer =hid) /\
  (tk.`tk_trust =t) /\
  (tk.`tk_updt = i) /\
  (tk.`tk_wdata.`tkw_ekeys = ks) /\
  (tk.`tk_wdata.`tkw_sig = s)) by smt().

module Bhop3(A : KMSAdv, OA : OperatorActions,  
             O : MRPKE.MRPKE_OrclT)
  = {
  module A = A(KMSProceduresHop3(OA,IdealSigServ,O))
  
    proc guess() : bool = {
       var b;
       KMSProceduresHop3(OA,IdealSigServ,O).init();
       b <@ A.guess();
       return (b = KMSProcedures.b);
  }
}.

axiom mrenc_bounds : q_hid <= MRPKE.q_gen /\
                      q_tkmng + q_unw + q_corr <= MRPKE.q_dec /\
                      q_tkmng <= MRPKE.q_lor /\
                      q_maxpks = max_size.

op inv_benc_keys (benc_keys : (pkey, Pk * Sk) fmap) (benc_keys_stripped : (pkey, Pk) fmap) 
                 (pklist : (Pk, Sk) fmap) (pks_sks : (pkey, skey) fmap) (hids : HId fset)  (who : (Pk, pkey) fmap)= 
  forall vk, 
     (* one to one between benc_key indices *)
     (vk \in benc_keys = vk \in benc_keys_stripped) /\
     (vk \in benc_keys = vk \in pks_sks) /\
     (* and they point to the same bpke pk *)
     (vk \in benc_keys => 
       ( (oget benc_keys.[vk]).`1 = oget benc_keys_stripped.[vk] /\
         (* and such pks exist in the bpke game pk list *)
         (oget benc_keys.[vk]).`1 \in pklist /\
         (* and bpke keys are in the support of gen on left side *)
         ((oget benc_keys.[vk]).`2, (oget benc_keys.[vk]).`1) \in gen /\
         (vk, (oget benc_keys.[vk]).`1) \in hids /\
         (oget benc_keys.[vk]).`1 \in who)).

op inv_genuine (genuine : Genuine) (pks_sks : (pkey, skey) fmap) (benc_keys : (pkey, Pk * Sk) fmap) (hids : HId fset) (who : (Pk, pkey) fmap) =
   (forall hid, 
     (hid \in genuine <=> hid \in hids) /\
     (hid \in genuine => 
       (* genuine hids will always use ideal signature *)
       (hid.`1 \in pks_sks /\
       (* genuine hids are consistent with benckey storage *)
       hid.`1 \in benc_keys /\
       (oget benc_keys.[hid.`1]).`1 = hid.`2) /\
       hid.`2 \in who)) /\
   (forall pk, pk \in who => (oget who.[pk],pk) \in genuine).

op inv_encode (tklist : Token fset) (qs:(pkey * message * signature) fset) =
  forall (tk : Token), 
       ((tk.`tk_wdata.`tkw_signer.`1,
           encode_msg (tk.`tk_trust, tk.`tk_wdata.`tkw_ekeys,tk.`tk_wdata.`tkw_signer,tk.`tk_updt), 
                    tk.`tk_wdata.`tkw_sig) \in qs) =>
        (* all signed tokens are in tklist *)
        (tk \in tklist /\
        (* all signed tokens have consistent members with bpke recipients *)
         (forall hid, hid \in tr_mems  (tk.`tk_trust) => hid.`2 \in tk.`tk_wdata.`tkw_ekeys) /\
         (* all signed tokens have ciphertexts that were correctly encrypted *)
         exists ptxt,
           tk.`tk_wdata.`tkw_ekeys \in mencrypt (proj_pks (tr_mems (tk.`tk_trust))) (encode_tag (tk.`tk_trust)) ptxt).

op inv_bounds (count_gen count_hid count_sig count_tkmng count_dec count_unw count_corr count_lor : int)= 
   count_gen <= count_hid /\
   count_sig <= count_tkmng /\
   count_dec <= count_tkmng + count_unw + count_corr /\
   count_lor <= count_tkmng /\
   count_unw <= q_unw /\
   count_corr <= q_corr /\
   count_tkmng <= q_tkmng.

section.

declare module A : KMSAdv { HSMs_MRPKEOr,KMSProcedures3, MRPKE_lor,TrustOracles, RealSigServ, HSMs,  KMSProcedures,OpPolSrv,HstPolSrv}.
declare module OA : OperatorActions {A, HSMs_MRPKEOr,KMSProceduresHop3, MRPKE_lor,KMSProcedures3, TrustOracles, 
                                     RealSigServ, KMSProcedures, HSMs,OpPolSrv,HstPolSrv}.

equiv newOp_E : KMSProcedures3(OA).newOp ~ KMSProceduresHop3(OA, IdealSigServ, MRPKE_lor).newOp :
  ={badOp} /\
  ={glob OA, glob RealSigServ, glob HstPolSrv, glob RealTrustService, glob OpPolSrv, glob KMSProcedures,
      KMSProcedures3.wrapped_keys, HSMs.who} /\
  inv_benc_keys HSMs.benc_keys{1} HSMs_MRPKEOr.benc_keys_stripped{2} MRPKE_lor.pklist{2} RealSigServ.pks_sks{1}
    KMSProcedures.hids{1} HSMs.who{1} /\
  (forall (pk : Pk), pk \in MRPKE_lor.pklist{2} => (oget MRPKE_lor.pklist{2}.[pk], pk) \in gen) /\
  inv_genuine OpPolSrv.genuine{1} RealSigServ.pks_sks{1} HSMs.benc_keys{1} KMSProcedures.hids{1} HSMs.who{1} /\
  inv_encode KMSProcedures.tklist{1} RealSigServ.qs{2} /\
  (forall (tk : Token) (pk : Pk) (cph : CTxt),
     ! (tk.`tk_trust \in RealTrustService.protectedTrusts{2}) =>
     tk.`tk_wdata.`tkw_ekeys.[pk] = Some cph => ! ((pk, encode_tag tk.`tk_trust, cph) \in MRPKE_lor.lorlist{2})) /\
  (forall (t : Trust), t \in RealTrustService.protectedTrusts{1} => all_genuine OpPolSrv.genuine{1} t) /\
  (forall (hid1 hid2 : HId),
     (hid1 \in KMSProcedures.hids{1}) /\ (hid2 \in KMSProcedures.hids{1}) /\ hid1 <> hid2 =>
     hid1.`1 <> hid2.`1 /\ hid1.`2 <> hid2.`2) /\
  inv_bounds MRPKE_lor.count_gen{2} KMSProcedures.count_hid{2} RealSigServ.count_sig{2} KMSProcedures.count_tkmng{2}
    MRPKE_lor.count_dec{2} KMSProcedures.count_unw{2} KMSProcedures.count_corr{2} MRPKE_lor.count_lor{2} 
  ==>
  ={res} /\
  ={glob OA, glob RealSigServ, glob HstPolSrv, glob RealTrustService, glob OpPolSrv, glob KMSProcedures,
      KMSProcedures3.wrapped_keys, HSMs.who} /\
  inv_benc_keys HSMs.benc_keys{1} HSMs_MRPKEOr.benc_keys_stripped{2} MRPKE_lor.pklist{2} RealSigServ.pks_sks{1}
    KMSProcedures.hids{1} HSMs.who{1} /\
  (forall (pk : Pk), pk \in MRPKE_lor.pklist{2} => (oget MRPKE_lor.pklist{2}.[pk], pk) \in gen) /\
  inv_genuine OpPolSrv.genuine{1} RealSigServ.pks_sks{1} HSMs.benc_keys{1} KMSProcedures.hids{1} HSMs.who{1} /\
  inv_encode KMSProcedures.tklist{1} RealSigServ.qs{2} /\
  (forall (tk : Token) (pk : Pk) (cph : CTxt),
     ! (tk.`tk_trust \in RealTrustService.protectedTrusts{2}) =>
     tk.`tk_wdata.`tkw_ekeys.[pk] = Some cph => ! ((pk, encode_tag tk.`tk_trust, cph) \in MRPKE_lor.lorlist{2})) /\
  (forall (t : Trust), t \in RealTrustService.protectedTrusts{1} => all_genuine OpPolSrv.genuine{1} t) /\
  (forall (hid1 hid2 : HId),
     (hid1 \in KMSProcedures.hids{1}) /\ (hid2 \in KMSProcedures.hids{1}) /\ hid1 <> hid2 =>
     hid1.`1 <> hid2.`1 /\ hid1.`2 <> hid2.`2) /\
  inv_bounds MRPKE_lor.count_gen{2} KMSProcedures.count_hid{2} RealSigServ.count_sig{2} KMSProcedures.count_tkmng{2}
    MRPKE_lor.count_dec{2} KMSProcedures.count_unw{2} KMSProcedures.count_corr{2} MRPKE_lor.count_lor{2}.
proof. time by proc; sim |>. qed.

equiv requestAuthorization_E : 
    KMSProcedures3(OA).requestAuthorization ~ KMSProceduresHop3(OA, IdealSigServ, MRPKE_lor).requestAuthorization : 
  ={request, opid} /\
  ={glob OA, glob RealSigServ, glob HstPolSrv, glob RealTrustService, glob OpPolSrv, glob KMSProcedures,
      KMSProcedures3.wrapped_keys, HSMs.who} /\
  inv_benc_keys HSMs.benc_keys{1} HSMs_MRPKEOr.benc_keys_stripped{2} MRPKE_lor.pklist{2} RealSigServ.pks_sks{1}
    KMSProcedures.hids{1} HSMs.who{1} /\
  (forall (pk : Pk), pk \in MRPKE_lor.pklist{2} => (oget MRPKE_lor.pklist{2}.[pk], pk) \in gen) /\
  inv_genuine OpPolSrv.genuine{1} RealSigServ.pks_sks{1} HSMs.benc_keys{1} KMSProcedures.hids{1} HSMs.who{1} /\
  inv_encode KMSProcedures.tklist{1} RealSigServ.qs{2} /\
  (forall (tk : Token) (pk : Pk) (cph : CTxt),
     ! (tk.`tk_trust \in RealTrustService.protectedTrusts{2}) =>
     tk.`tk_wdata.`tkw_ekeys.[pk] = Some cph => ! ((pk, encode_tag tk.`tk_trust, cph) \in MRPKE_lor.lorlist{2})) /\
  (forall (t : Trust), t \in RealTrustService.protectedTrusts{1} => all_genuine OpPolSrv.genuine{1} t) /\
  (forall (hid1 hid2 : HId),
     (hid1 \in KMSProcedures.hids{1}) /\ (hid2 \in KMSProcedures.hids{1}) /\ hid1 <> hid2 =>
     hid1.`1 <> hid2.`1 /\ hid1.`2 <> hid2.`2) /\
  inv_bounds MRPKE_lor.count_gen{2} KMSProcedures.count_hid{2} RealSigServ.count_sig{2} KMSProcedures.count_tkmng{2}
    MRPKE_lor.count_dec{2} KMSProcedures.count_unw{2} KMSProcedures.count_corr{2} MRPKE_lor.count_lor{2}
  ==>
  ={res} /\
  ={glob OA, glob RealSigServ, glob HstPolSrv, glob RealTrustService, glob OpPolSrv, glob KMSProcedures,
      KMSProcedures3.wrapped_keys, HSMs.who} /\
  inv_benc_keys HSMs.benc_keys{1} HSMs_MRPKEOr.benc_keys_stripped{2} MRPKE_lor.pklist{2} RealSigServ.pks_sks{1}
    KMSProcedures.hids{1} HSMs.who{1} /\
  (forall (pk : Pk), pk \in MRPKE_lor.pklist{2} => (oget MRPKE_lor.pklist{2}.[pk], pk) \in gen) /\
  inv_genuine OpPolSrv.genuine{1} RealSigServ.pks_sks{1} HSMs.benc_keys{1} KMSProcedures.hids{1} HSMs.who{1} /\
  inv_encode KMSProcedures.tklist{1} RealSigServ.qs{2} /\
  (forall (tk : Token) (pk : Pk) (cph : CTxt),
     ! (tk.`tk_trust \in RealTrustService.protectedTrusts{2}) =>
     tk.`tk_wdata.`tkw_ekeys.[pk] = Some cph => ! ((pk, encode_tag tk.`tk_trust, cph) \in MRPKE_lor.lorlist{2})) /\
  (forall (t : Trust), t \in RealTrustService.protectedTrusts{1} => all_genuine OpPolSrv.genuine{1} t) /\
  (forall (hid1 hid2 : HId),
     (hid1 \in KMSProcedures.hids{1}) /\ (hid2 \in KMSProcedures.hids{1}) /\ hid1 <> hid2 =>
     hid1.`1 <> hid2.`1 /\ hid1.`2 <> hid2.`2) /\
  inv_bounds MRPKE_lor.count_gen{2} KMSProcedures.count_hid{2} RealSigServ.count_sig{2} KMSProcedures.count_tkmng{2}
    MRPKE_lor.count_dec{2} KMSProcedures.count_unw{2} KMSProcedures.count_corr{2} MRPKE_lor.count_lor{2}.
proof. by proc; sim |>. qed.

equiv newHst_E : KMSProcedures3(OA).newHst ~ KMSProceduresHop3(OA, IdealSigServ, MRPKE_lor).newHst :
  true /\
  ={glob OA, glob RealSigServ, glob HstPolSrv, glob RealTrustService, glob OpPolSrv, glob KMSProcedures,
      KMSProcedures3.wrapped_keys, HSMs.who} /\
  inv_benc_keys HSMs.benc_keys{1} HSMs_MRPKEOr.benc_keys_stripped{2} MRPKE_lor.pklist{2} RealSigServ.pks_sks{1}
    KMSProcedures.hids{1} HSMs.who{1} /\
  (forall (pk : Pk), pk \in MRPKE_lor.pklist{2} => (oget MRPKE_lor.pklist{2}.[pk], pk) \in gen) /\
  inv_genuine OpPolSrv.genuine{1} RealSigServ.pks_sks{1} HSMs.benc_keys{1} KMSProcedures.hids{1} HSMs.who{1} /\
  inv_encode KMSProcedures.tklist{1} RealSigServ.qs{2} /\
  (forall (tk : Token) (pk : Pk) (cph : CTxt),
     ! (tk.`tk_trust \in RealTrustService.protectedTrusts{2}) =>
     tk.`tk_wdata.`tkw_ekeys.[pk] = Some cph => ! ((pk, encode_tag tk.`tk_trust, cph) \in MRPKE_lor.lorlist{2})) /\
  (forall (t : Trust), t \in RealTrustService.protectedTrusts{1} => all_genuine OpPolSrv.genuine{1} t) /\
  (forall (hid1 hid2 : HId),
     (hid1 \in KMSProcedures.hids{1}) /\ (hid2 \in KMSProcedures.hids{1}) /\ hid1 <> hid2 =>
     hid1.`1 <> hid2.`1 /\ hid1.`2 <> hid2.`2) /\
  inv_bounds MRPKE_lor.count_gen{2} KMSProcedures.count_hid{2} RealSigServ.count_sig{2} KMSProcedures.count_tkmng{2}
    MRPKE_lor.count_dec{2} KMSProcedures.count_unw{2} KMSProcedures.count_corr{2} MRPKE_lor.count_lor{2}
  ==>
  ={res} /\
  ={glob OA, glob RealSigServ, glob HstPolSrv, glob RealTrustService, glob OpPolSrv, glob KMSProcedures,
      KMSProcedures3.wrapped_keys, HSMs.who} /\
  inv_benc_keys HSMs.benc_keys{1} HSMs_MRPKEOr.benc_keys_stripped{2} MRPKE_lor.pklist{2} RealSigServ.pks_sks{1}
    KMSProcedures.hids{1} HSMs.who{1} /\
  (forall (pk : Pk), pk \in MRPKE_lor.pklist{2} => (oget MRPKE_lor.pklist{2}.[pk], pk) \in gen) /\
  inv_genuine OpPolSrv.genuine{1} RealSigServ.pks_sks{1} HSMs.benc_keys{1} KMSProcedures.hids{1} HSMs.who{1} /\
  inv_encode KMSProcedures.tklist{1} RealSigServ.qs{2} /\
  (forall (tk : Token) (pk : Pk) (cph : CTxt),
     ! (tk.`tk_trust \in RealTrustService.protectedTrusts{2}) =>
     tk.`tk_wdata.`tkw_ekeys.[pk] = Some cph => ! ((pk, encode_tag tk.`tk_trust, cph) \in MRPKE_lor.lorlist{2})) /\
  (forall (t : Trust), t \in RealTrustService.protectedTrusts{1} => all_genuine OpPolSrv.genuine{1} t) /\
  (forall (hid1 hid2 : HId),
     (hid1 \in KMSProcedures.hids{1}) /\ (hid2 \in KMSProcedures.hids{1}) /\ hid1 <> hid2 =>
     hid1.`1 <> hid2.`1 /\ hid1.`2 <> hid2.`2) /\
  inv_bounds MRPKE_lor.count_gen{2} KMSProcedures.count_hid{2} RealSigServ.count_sig{2} KMSProcedures.count_tkmng{2}
    MRPKE_lor.count_dec{2} KMSProcedures.count_unw{2} KMSProcedures.count_corr{2} MRPKE_lor.count_lor{2}.
proof. by proc; sim |>. qed.

equiv installInitialTrust_E : KMSProcedures3(OA).installInitialTrust ~ KMSProceduresHop3(OA, IdealSigServ, MRPKE_lor).installInitialTrust :
  ={hstid, tk} /\
  ={glob OA, glob RealSigServ, glob HstPolSrv, glob RealTrustService, glob OpPolSrv, glob KMSProcedures,
      KMSProcedures3.wrapped_keys, HSMs.who} /\
  inv_benc_keys HSMs.benc_keys{1} HSMs_MRPKEOr.benc_keys_stripped{2} MRPKE_lor.pklist{2} RealSigServ.pks_sks{1}
    KMSProcedures.hids{1} HSMs.who{1} /\
  (forall (pk : Pk), pk \in MRPKE_lor.pklist{2} => (oget MRPKE_lor.pklist{2}.[pk], pk) \in gen) /\
  inv_genuine OpPolSrv.genuine{1} RealSigServ.pks_sks{1} HSMs.benc_keys{1} KMSProcedures.hids{1} HSMs.who{1} /\
  inv_encode KMSProcedures.tklist{1} RealSigServ.qs{2} /\
  (forall (tk : Token) (pk : Pk) (cph : CTxt),
     ! (tk.`tk_trust \in RealTrustService.protectedTrusts{2}) =>
     tk.`tk_wdata.`tkw_ekeys.[pk] = Some cph => ! ((pk, encode_tag tk.`tk_trust, cph) \in MRPKE_lor.lorlist{2})) /\
  (forall (t : Trust), t \in RealTrustService.protectedTrusts{1} => all_genuine OpPolSrv.genuine{1} t) /\
  (forall (hid1 hid2 : HId),
     (hid1 \in KMSProcedures.hids{1}) /\ (hid2 \in KMSProcedures.hids{1}) /\ hid1 <> hid2 =>
     hid1.`1 <> hid2.`1 /\ hid1.`2 <> hid2.`2) /\
  inv_bounds MRPKE_lor.count_gen{2} KMSProcedures.count_hid{2} RealSigServ.count_sig{2} KMSProcedures.count_tkmng{2}
    MRPKE_lor.count_dec{2} KMSProcedures.count_unw{2} KMSProcedures.count_corr{2} MRPKE_lor.count_lor{2}
  ==>
  ={res} /\
  ={glob OA, glob RealSigServ, glob HstPolSrv, glob RealTrustService, glob OpPolSrv, glob KMSProcedures,
      KMSProcedures3.wrapped_keys, HSMs.who} /\
  inv_benc_keys HSMs.benc_keys{1} HSMs_MRPKEOr.benc_keys_stripped{2} MRPKE_lor.pklist{2} RealSigServ.pks_sks{1}
    KMSProcedures.hids{1} HSMs.who{1} /\
  (forall (pk : Pk), pk \in MRPKE_lor.pklist{2} => (oget MRPKE_lor.pklist{2}.[pk], pk) \in gen) /\
  inv_genuine OpPolSrv.genuine{1} RealSigServ.pks_sks{1} HSMs.benc_keys{1} KMSProcedures.hids{1} HSMs.who{1} /\
  inv_encode KMSProcedures.tklist{1} RealSigServ.qs{2} /\
  (forall (tk : Token) (pk : Pk) (cph : CTxt),
     ! (tk.`tk_trust \in RealTrustService.protectedTrusts{2}) =>
     tk.`tk_wdata.`tkw_ekeys.[pk] = Some cph => ! ((pk, encode_tag tk.`tk_trust, cph) \in MRPKE_lor.lorlist{2})) /\
  (forall (t : Trust), t \in RealTrustService.protectedTrusts{1} => all_genuine OpPolSrv.genuine{1} t) /\
  (forall (hid1 hid2 : HId),
     (hid1 \in KMSProcedures.hids{1}) /\ (hid2 \in KMSProcedures.hids{1}) /\ hid1 <> hid2 =>
     hid1.`1 <> hid2.`1 /\ hid1.`2 <> hid2.`2) /\
  inv_bounds MRPKE_lor.count_gen{2} KMSProcedures.count_hid{2} RealSigServ.count_sig{2} KMSProcedures.count_tkmng{2}
    MRPKE_lor.count_dec{2} KMSProcedures.count_unw{2} KMSProcedures.count_corr{2} MRPKE_lor.count_lor{2}.
proof. 
  proc; conseq |>.
  inline *;sp;if; [done | | by skip].
  sp;wp;if; [done | | by skip].
  auto;call(_:true);wp;skip; smt(in_fsetU1 get_setE get_set_sameE).
qed.

equiv installUpdatedTrust_E : KMSProcedures3(OA).installUpdatedTrust ~ KMSProceduresHop3(OA, IdealSigServ, MRPKE_lor).installUpdatedTrust :
  ={hstid, new} /\
  ={glob OA, glob RealSigServ, glob HstPolSrv, glob RealTrustService, glob OpPolSrv, glob KMSProcedures,
      KMSProcedures3.wrapped_keys, HSMs.who} /\
  inv_benc_keys HSMs.benc_keys{1} HSMs_MRPKEOr.benc_keys_stripped{2} MRPKE_lor.pklist{2} RealSigServ.pks_sks{1}
    KMSProcedures.hids{1} HSMs.who{1} /\
  (forall (pk : Pk), pk \in MRPKE_lor.pklist{2} => (oget MRPKE_lor.pklist{2}.[pk], pk) \in gen) /\
  inv_genuine OpPolSrv.genuine{1} RealSigServ.pks_sks{1} HSMs.benc_keys{1} KMSProcedures.hids{1} HSMs.who{1} /\
  inv_encode KMSProcedures.tklist{1} RealSigServ.qs{2} /\
  (forall (tk : Token) (pk : Pk) (cph : CTxt),
     ! (tk.`tk_trust \in RealTrustService.protectedTrusts{2}) =>
     tk.`tk_wdata.`tkw_ekeys.[pk] = Some cph => ! ((pk, encode_tag tk.`tk_trust, cph) \in MRPKE_lor.lorlist{2})) /\
  (forall (t : Trust), t \in RealTrustService.protectedTrusts{1} => all_genuine OpPolSrv.genuine{1} t) /\
  (forall (hid1 hid2 : HId),
     (hid1 \in KMSProcedures.hids{1}) /\ (hid2 \in KMSProcedures.hids{1}) /\ hid1 <> hid2 =>
     hid1.`1 <> hid2.`1 /\ hid1.`2 <> hid2.`2) /\
  inv_bounds MRPKE_lor.count_gen{2} KMSProcedures.count_hid{2} RealSigServ.count_sig{2} KMSProcedures.count_tkmng{2}
    MRPKE_lor.count_dec{2} KMSProcedures.count_unw{2} KMSProcedures.count_corr{2} MRPKE_lor.count_lor{2}
  ==>
  ={res} /\
  ={glob OA, glob RealSigServ, glob HstPolSrv, glob RealTrustService, glob OpPolSrv, glob KMSProcedures,
      KMSProcedures3.wrapped_keys, HSMs.who} /\
  inv_benc_keys HSMs.benc_keys{1} HSMs_MRPKEOr.benc_keys_stripped{2} MRPKE_lor.pklist{2} RealSigServ.pks_sks{1}
    KMSProcedures.hids{1} HSMs.who{1} /\
  (forall (pk : Pk), pk \in MRPKE_lor.pklist{2} => (oget MRPKE_lor.pklist{2}.[pk], pk) \in gen) /\
  inv_genuine OpPolSrv.genuine{1} RealSigServ.pks_sks{1} HSMs.benc_keys{1} KMSProcedures.hids{1} HSMs.who{1} /\
  inv_encode KMSProcedures.tklist{1} RealSigServ.qs{2} /\
  (forall (tk : Token) (pk : Pk) (cph : CTxt),
     ! (tk.`tk_trust \in RealTrustService.protectedTrusts{2}) =>
     tk.`tk_wdata.`tkw_ekeys.[pk] = Some cph => ! ((pk, encode_tag tk.`tk_trust, cph) \in MRPKE_lor.lorlist{2})) /\
  (forall (t : Trust), t \in RealTrustService.protectedTrusts{1} => all_genuine OpPolSrv.genuine{1} t) /\
  (forall (hid1 hid2 : HId),
     (hid1 \in KMSProcedures.hids{1}) /\ (hid2 \in KMSProcedures.hids{1}) /\ hid1 <> hid2 =>
     hid1.`1 <> hid2.`1 /\ hid1.`2 <> hid2.`2) /\
  inv_bounds MRPKE_lor.count_gen{2} KMSProcedures.count_hid{2} RealSigServ.count_sig{2} KMSProcedures.count_tkmng{2}
    MRPKE_lor.count_dec{2} KMSProcedures.count_unw{2} KMSProcedures.count_corr{2} MRPKE_lor.count_lor{2}.
proof. by proc; sim />. qed.  

equiv newHSM_E : KMSProcedures3(OA).newHSM ~ KMSProceduresHop3(OA, IdealSigServ, MRPKE_lor).newHSM : 
  true /\
  ={glob OA, glob RealSigServ, glob HstPolSrv, glob RealTrustService, glob OpPolSrv, glob KMSProcedures,
      KMSProcedures3.wrapped_keys, HSMs.who} /\
  inv_benc_keys HSMs.benc_keys{1} HSMs_MRPKEOr.benc_keys_stripped{2} MRPKE_lor.pklist{2} RealSigServ.pks_sks{1}
    KMSProcedures.hids{1} HSMs.who{1} /\
  (forall (pk : Pk), pk \in MRPKE_lor.pklist{2} => (oget MRPKE_lor.pklist{2}.[pk], pk) \in gen) /\
  inv_genuine OpPolSrv.genuine{1} RealSigServ.pks_sks{1} HSMs.benc_keys{1} KMSProcedures.hids{1} HSMs.who{1} /\
  inv_encode KMSProcedures.tklist{1} RealSigServ.qs{2} /\
  (forall (tk : Token) (pk : Pk) (cph : CTxt),
     ! (tk.`tk_trust \in RealTrustService.protectedTrusts{2}) =>
     tk.`tk_wdata.`tkw_ekeys.[pk] = Some cph => ! ((pk, encode_tag tk.`tk_trust, cph) \in MRPKE_lor.lorlist{2})) /\
  (forall (t : Trust), t \in RealTrustService.protectedTrusts{1} => all_genuine OpPolSrv.genuine{1} t) /\
  (forall (hid1 hid2 : HId),
     (hid1 \in KMSProcedures.hids{1}) /\ (hid2 \in KMSProcedures.hids{1}) /\ hid1 <> hid2 =>
     hid1.`1 <> hid2.`1 /\ hid1.`2 <> hid2.`2) /\
  inv_bounds MRPKE_lor.count_gen{2} KMSProcedures.count_hid{2} RealSigServ.count_sig{2} KMSProcedures.count_tkmng{2}
    MRPKE_lor.count_dec{2} KMSProcedures.count_unw{2} KMSProcedures.count_corr{2} MRPKE_lor.count_lor{2}
  ==>
  ={res} /\
  ={glob OA, glob RealSigServ, glob HstPolSrv, glob RealTrustService, glob OpPolSrv, glob KMSProcedures,
      KMSProcedures3.wrapped_keys, HSMs.who} /\
  inv_benc_keys HSMs.benc_keys{1} HSMs_MRPKEOr.benc_keys_stripped{2} MRPKE_lor.pklist{2} RealSigServ.pks_sks{1}
    KMSProcedures.hids{1} HSMs.who{1} /\
  (forall (pk : Pk), pk \in MRPKE_lor.pklist{2} => (oget MRPKE_lor.pklist{2}.[pk], pk) \in gen) /\
  inv_genuine OpPolSrv.genuine{1} RealSigServ.pks_sks{1} HSMs.benc_keys{1} KMSProcedures.hids{1} HSMs.who{1} /\
  inv_encode KMSProcedures.tklist{1} RealSigServ.qs{2} /\
  (forall (tk : Token) (pk : Pk) (cph : CTxt),
     ! (tk.`tk_trust \in RealTrustService.protectedTrusts{2}) =>
     tk.`tk_wdata.`tkw_ekeys.[pk] = Some cph => ! ((pk, encode_tag tk.`tk_trust, cph) \in MRPKE_lor.lorlist{2})) /\
  (forall (t : Trust), t \in RealTrustService.protectedTrusts{1} => all_genuine OpPolSrv.genuine{1} t) /\
  (forall (hid1 hid2 : HId),
     (hid1 \in KMSProcedures.hids{1}) /\ (hid2 \in KMSProcedures.hids{1}) /\ hid1 <> hid2 =>
     hid1.`1 <> hid2.`1 /\ hid1.`2 <> hid2.`2) /\
  inv_bounds MRPKE_lor.count_gen{2} KMSProcedures.count_hid{2} RealSigServ.count_sig{2} KMSProcedures.count_tkmng{2}
    MRPKE_lor.count_dec{2} KMSProcedures.count_unw{2} KMSProcedures.count_corr{2} MRPKE_lor.count_lor{2}.
proof.
proc;inline *.
sp 1 1; if;[done | | by auto].
if; [done | | by skip].
rcondt {2} 2; 1: by move => *; wp;skip; smt (mrenc_bounds).
seq 1 7 : (#{/~inv_bounds _ _ _ _ _ _ _ _}pre /\
          inv_bounds (MRPKE_lor.count_gen{2} - 1) KMSProcedures.count_hid{2} RealSigServ.count_sig{2} KMSProcedures.count_tkmng{2}
             MRPKE_lor.count_dec{2} KMSProcedures.count_unw{2} KMSProcedures.count_corr{2} MRPKE_lor.count_lor{2} /\
             (sk,pk){1} = k{2} /\ k{2} \in gen /\ pk{2} = k{2}.`2 /\ pk{2} \in MRPKE_lor.pklist{2}).
+ by wp;rnd;wp;skip;smt(in_fsetU1 get_setE get_set_sameE).
sp;if => //=. 
+ rcondf{1} 3; 1: by move => *;wp;skip;smt(in_fsetU1 get_setE get_set_sameE).
  rcondf{2} 3; 1: by move => *;wp;skip;smt(in_fsetU1 get_setE get_set_sameE).
  by wp;skip;by smt(in_fsetU1 get_setE get_set_sameE).
seq 1 1 : (#pre /\ ={pksig,sksig} /\ (pksig,sksig){1} \in keygen).
+ by wp;rnd;skip => />;smt (supp_dexcepted).
if => //=.
+ rcondf{1} 3; 1: by move => *;wp;skip => /#.
  rcondf{2} 3; 1: by move => *;wp;skip => /#. 
  rcondt{1} 6; 1: by move => *;wp;skip => /#. 
  rcondt{2} 6; 1: by move => *;wp;skip => /#. 
  rcondt{1} 8; 1: by move => *;wp;skip => /#. 
  rcondt{2} 8;  1: by move => *;wp;skip => /#. 
  auto => />;rewrite /inv_benc_keys; progress; smt(in_fsetU1 get_setE get_set_sameE mem_fdom).

rcondt{1} 2; 1: by move => *;wp;skip => /#.
rcondt{2} 2; 1: by move => *;wp;skip => /#. 
rcondf{1} 4; 1: by move => *;wp;skip => /#. 
rcondf{2} 4; 1: by move => *;wp;skip => /#. 
rcondf{1} 5; 1: by move => *;wp;skip => /#. 
rcondf{2} 5; 1: by move => *;wp;skip => /#. 
by wp;skip => |> * /#.
qed.

equiv newToken_E : KMSProcedures3(OA).newToken ~ KMSProceduresHop3(OA, IdealSigServ, MRPKE_lor).newToken :
  ={hid, new} /\
  ={glob OA, glob RealSigServ, glob HstPolSrv, glob RealTrustService, glob OpPolSrv, glob KMSProcedures,
      KMSProcedures3.wrapped_keys, HSMs.who} /\
  inv_benc_keys HSMs.benc_keys{1} HSMs_MRPKEOr.benc_keys_stripped{2} MRPKE_lor.pklist{2} RealSigServ.pks_sks{1}
    KMSProcedures.hids{1} HSMs.who{1} /\
  (forall (pk : Pk), pk \in MRPKE_lor.pklist{2} => (oget MRPKE_lor.pklist{2}.[pk], pk) \in gen) /\
  inv_genuine OpPolSrv.genuine{1} RealSigServ.pks_sks{1} HSMs.benc_keys{1} KMSProcedures.hids{1} HSMs.who{1} /\
  inv_encode KMSProcedures.tklist{1} RealSigServ.qs{2} /\
  (forall (tk : Token) (pk : Pk) (cph : CTxt),
     ! (tk.`tk_trust \in RealTrustService.protectedTrusts{2}) =>
     tk.`tk_wdata.`tkw_ekeys.[pk] = Some cph => ! ((pk, encode_tag tk.`tk_trust, cph) \in MRPKE_lor.lorlist{2})) /\
  (forall (t : Trust), t \in RealTrustService.protectedTrusts{1} => all_genuine OpPolSrv.genuine{1} t) /\
  (forall (hid1 hid2 : HId),
     (hid1 \in KMSProcedures.hids{1}) /\ (hid2 \in KMSProcedures.hids{1}) /\ hid1 <> hid2 =>
     hid1.`1 <> hid2.`1 /\ hid1.`2 <> hid2.`2) /\
  inv_bounds MRPKE_lor.count_gen{2} KMSProcedures.count_hid{2} RealSigServ.count_sig{2} KMSProcedures.count_tkmng{2}
    MRPKE_lor.count_dec{2} KMSProcedures.count_unw{2} KMSProcedures.count_corr{2} MRPKE_lor.count_lor{2}
  ==>
  ={res} /\
  ={glob OA, glob RealSigServ, glob HstPolSrv, glob RealTrustService, glob OpPolSrv, glob KMSProcedures,
      KMSProcedures3.wrapped_keys, HSMs.who} /\
  inv_benc_keys HSMs.benc_keys{1} HSMs_MRPKEOr.benc_keys_stripped{2} MRPKE_lor.pklist{2} RealSigServ.pks_sks{1}
    KMSProcedures.hids{1} HSMs.who{1} /\
  (forall (pk : Pk), pk \in MRPKE_lor.pklist{2} => (oget MRPKE_lor.pklist{2}.[pk], pk) \in gen) /\
  inv_genuine OpPolSrv.genuine{1} RealSigServ.pks_sks{1} HSMs.benc_keys{1} KMSProcedures.hids{1} HSMs.who{1} /\
  inv_encode KMSProcedures.tklist{1} RealSigServ.qs{2} /\
  (forall (tk : Token) (pk : Pk) (cph : CTxt),
     ! (tk.`tk_trust \in RealTrustService.protectedTrusts{2}) =>
     tk.`tk_wdata.`tkw_ekeys.[pk] = Some cph => ! ((pk, encode_tag tk.`tk_trust, cph) \in MRPKE_lor.lorlist{2})) /\
  (forall (t : Trust), t \in RealTrustService.protectedTrusts{1} => all_genuine OpPolSrv.genuine{1} t) /\
  (forall (hid1 hid2 : HId),
     (hid1 \in KMSProcedures.hids{1}) /\ (hid2 \in KMSProcedures.hids{1}) /\ hid1 <> hid2 =>
     hid1.`1 <> hid2.`1 /\ hid1.`2 <> hid2.`2) /\
  inv_bounds MRPKE_lor.count_gen{2} KMSProcedures.count_hid{2} RealSigServ.count_sig{2} KMSProcedures.count_tkmng{2}
    MRPKE_lor.count_dec{2} KMSProcedures.count_unw{2} KMSProcedures.count_corr{2} MRPKE_lor.count_lor{2}.
proof.
proc; conseq |>.
sp; if => //=.
wp; if;[done | | wp;skip;smt ()].
inline *.
sp; seq 1 1 : (#pre /\ ={ekeys} /\ 
              (ekeys \in  mencrypt (proj_pks mem) tag ptxt){1}); 
      first  by rnd;skip;progress => /#.
rcondt {1} 5; 1: by move => *;wp;skip;smt (sig_bound).
rcondt {1} 5; 1: by move => *;wp;skip;smt ().
rcondt {2} 5; 1: by move => *;wp;skip;smt (sig_bound).
rcondt {2} 5; 1: by move => *;wp;skip;smt ().
wp; call (_: true);wp;rnd;wp;skip => |> *.
have -> /= : 
   inv_bounds MRPKE_lor.count_gen{2} KMSProcedures.count_hid{2} (RealSigServ.count_sig{2} + 1)
   (KMSProcedures.count_tkmng{2} + 1) MRPKE_lor.count_dec{2} KMSProcedures.count_unw{2} KMSProcedures.count_corr{2}
   MRPKE_lor.count_lor{2} by smt ().
have -> /= * : 
  inv_encode
   (KMSProcedures.tklist{2} `|`
    fset1
      {| tk_updt = true; tk_trust = new{2}; tk_wdata =
          {| tkw_ekeys = ekeys{2}; tkw_signer = hid{2}; tkw_sig = oget (Some sL); |}; |})
   (RealSigServ.qs{2} `|` fset1 (hid{2}.`1, encode_msg (new{2}, ekeys{2}, hid{2}, true), sL)).
+ rewrite /inv_encode => tk.
  rewrite in_fsetU1 /= => -[]; 1: smt(in_fsetU).
  by move=> [#? /encode_msg_inj [] ] /> *;smt (bpke_keys in_fsetU1 mem_fdom imageE imageP).
smt (in_fsetU1).
qed.

equiv unwrap_E : KMSProcedures3(OA).unwrap ~ KMSProceduresHop3(OA, IdealSigServ, MRPKE_lor).unwrap :
  ={hid, tk} /\
  ={glob OA, glob RealSigServ, glob HstPolSrv, glob RealTrustService, glob OpPolSrv, glob KMSProcedures,
      KMSProcedures3.wrapped_keys, HSMs.who} /\
  inv_benc_keys HSMs.benc_keys{1} HSMs_MRPKEOr.benc_keys_stripped{2} MRPKE_lor.pklist{2} RealSigServ.pks_sks{1}
    KMSProcedures.hids{1} HSMs.who{1} /\
  (forall (pk : Pk), pk \in MRPKE_lor.pklist{2} => (oget MRPKE_lor.pklist{2}.[pk], pk) \in gen) /\
  inv_genuine OpPolSrv.genuine{1} RealSigServ.pks_sks{1} HSMs.benc_keys{1} KMSProcedures.hids{1} HSMs.who{1} /\
  inv_encode KMSProcedures.tklist{1} RealSigServ.qs{2} /\
  (forall (tk : Token) (pk : Pk) (cph : CTxt),
     ! (tk.`tk_trust \in RealTrustService.protectedTrusts{2}) =>
     tk.`tk_wdata.`tkw_ekeys.[pk] = Some cph => ! ((pk, encode_tag tk.`tk_trust, cph) \in MRPKE_lor.lorlist{2})) /\
  (forall (t : Trust), t \in RealTrustService.protectedTrusts{1} => all_genuine OpPolSrv.genuine{1} t) /\
  (forall (hid1 hid2 : HId),
     (hid1 \in KMSProcedures.hids{1}) /\ (hid2 \in KMSProcedures.hids{1}) /\ hid1 <> hid2 =>
     hid1.`1 <> hid2.`1 /\ hid1.`2 <> hid2.`2) /\
  inv_bounds MRPKE_lor.count_gen{2} KMSProcedures.count_hid{2} RealSigServ.count_sig{2} KMSProcedures.count_tkmng{2}
  MRPKE_lor.count_dec{2} KMSProcedures.count_unw{2} KMSProcedures.count_corr{2} MRPKE_lor.count_lor{2}
  ==> 
  ={res} /\
  ={glob OA, glob RealSigServ, glob HstPolSrv, glob RealTrustService, glob OpPolSrv, glob KMSProcedures,
      KMSProcedures3.wrapped_keys, HSMs.who} /\
  inv_benc_keys HSMs.benc_keys{1} HSMs_MRPKEOr.benc_keys_stripped{2} MRPKE_lor.pklist{2} RealSigServ.pks_sks{1}
    KMSProcedures.hids{1} HSMs.who{1} /\
  (forall (pk : Pk), pk \in MRPKE_lor.pklist{2} => (oget MRPKE_lor.pklist{2}.[pk], pk) \in gen) /\
  inv_genuine OpPolSrv.genuine{1} RealSigServ.pks_sks{1} HSMs.benc_keys{1} KMSProcedures.hids{1} HSMs.who{1} /\
  inv_encode KMSProcedures.tklist{1} RealSigServ.qs{2} /\
  (forall (tk : Token) (pk : Pk) (cph : CTxt),
     ! (tk.`tk_trust \in RealTrustService.protectedTrusts{2}) =>
     tk.`tk_wdata.`tkw_ekeys.[pk] = Some cph => ! ((pk, encode_tag tk.`tk_trust, cph) \in MRPKE_lor.lorlist{2})) /\
  (forall (t : Trust), t \in RealTrustService.protectedTrusts{1} => all_genuine OpPolSrv.genuine{1} t) /\
  (forall (hid1 hid2 : HId),
     (hid1 \in KMSProcedures.hids{1}) /\ (hid2 \in KMSProcedures.hids{1}) /\ hid1 <> hid2 =>
     hid1.`1 <> hid2.`1 /\ hid1.`2 <> hid2.`2) /\
  inv_bounds MRPKE_lor.count_gen{2} KMSProcedures.count_hid{2} RealSigServ.count_sig{2} KMSProcedures.count_tkmng{2}
  MRPKE_lor.count_dec{2} KMSProcedures.count_unw{2} KMSProcedures.count_corr{2} MRPKE_lor.count_lor{2}.
proof.
proc;inline *.
sp 1 1; if;[done | | by skip => /#].
if; 1: done; last first.
+ by wp;skip; smt().
sp 2 0; if; 1: done; last first.
+ by wp;skip; smt().
sp 15 15;if;[smt() | | by wp;skip => /#].
rcondt{2} 9; 1: by auto; smt (mrenc_bounds).
auto => /> *; split => *.
+ smt(corr_keys taginj).
split; 2: smt().
move /negP : H23 => /(_ _) //.
have : (oget HSMs_MRPKEOr.benc_keys_stripped{2}.[hid{2}.`1] \in tk{2}.`tk_wdata.`tkw_ekeys); first by smt(mem_fdom imageE imageP). 
have [#?? /(_ H21) [#] ?]:= H hid{2}.`1.
rewrite mem_fdom => ? /= *.
smt(corr_keys taginj).
qed.

equiv corrupt_E : KMSProcedures3(OA).corrupt ~ KMSProceduresHop3(OA, IdealSigServ, MRPKE_lor).corrupt :
  ={hid, tk, hdl} /\
  ={glob OA, glob RealSigServ, glob HstPolSrv, glob RealTrustService, glob OpPolSrv, glob KMSProcedures,
      KMSProcedures3.wrapped_keys, HSMs.who} /\
  inv_benc_keys HSMs.benc_keys{1} HSMs_MRPKEOr.benc_keys_stripped{2} MRPKE_lor.pklist{2} RealSigServ.pks_sks{1}
    KMSProcedures.hids{1} HSMs.who{1} /\
  (forall (pk : Pk), pk \in MRPKE_lor.pklist{2} => (oget MRPKE_lor.pklist{2}.[pk], pk) \in gen) /\
  inv_genuine OpPolSrv.genuine{1} RealSigServ.pks_sks{1} HSMs.benc_keys{1} KMSProcedures.hids{1} HSMs.who{1} /\
  inv_encode KMSProcedures.tklist{1} RealSigServ.qs{2} /\
  (forall (tk : Token) (pk : Pk) (cph : CTxt),
     ! (tk.`tk_trust \in RealTrustService.protectedTrusts{2}) =>
     tk.`tk_wdata.`tkw_ekeys.[pk] = Some cph => ! ((pk, encode_tag tk.`tk_trust, cph) \in MRPKE_lor.lorlist{2})) /\
  (forall (t : Trust), t \in RealTrustService.protectedTrusts{1} => all_genuine OpPolSrv.genuine{1} t) /\
  (forall (hid1 hid2 : HId),
     (hid1 \in KMSProcedures.hids{1}) /\ (hid2 \in KMSProcedures.hids{1}) /\ hid1 <> hid2 =>
     hid1.`1 <> hid2.`1 /\ hid1.`2 <> hid2.`2) /\
  inv_bounds MRPKE_lor.count_gen{2} KMSProcedures.count_hid{2} RealSigServ.count_sig{2} KMSProcedures.count_tkmng{2}
    MRPKE_lor.count_dec{2} KMSProcedures.count_unw{2} KMSProcedures.count_corr{2} MRPKE_lor.count_lor{2}
  ==> 
  ={res} /\
  ={glob OA, glob RealSigServ, glob HstPolSrv, glob RealTrustService, glob OpPolSrv, glob KMSProcedures,
      KMSProcedures3.wrapped_keys, HSMs.who} /\
  inv_benc_keys HSMs.benc_keys{1} HSMs_MRPKEOr.benc_keys_stripped{2} MRPKE_lor.pklist{2} RealSigServ.pks_sks{1}
    KMSProcedures.hids{1} HSMs.who{1} /\
  (forall (pk : Pk), pk \in MRPKE_lor.pklist{2} => (oget MRPKE_lor.pklist{2}.[pk], pk) \in gen) /\
  inv_genuine OpPolSrv.genuine{1} RealSigServ.pks_sks{1} HSMs.benc_keys{1} KMSProcedures.hids{1} HSMs.who{1} /\
  inv_encode KMSProcedures.tklist{1} RealSigServ.qs{2} /\
  (forall (tk : Token) (pk : Pk) (cph : CTxt),
     ! (tk.`tk_trust \in RealTrustService.protectedTrusts{2}) =>
     tk.`tk_wdata.`tkw_ekeys.[pk] = Some cph => ! ((pk, encode_tag tk.`tk_trust, cph) \in MRPKE_lor.lorlist{2})) /\
  (forall (t : Trust), t \in RealTrustService.protectedTrusts{1} => all_genuine OpPolSrv.genuine{1} t) /\
  (forall (hid1 hid2 : HId),
     (hid1 \in KMSProcedures.hids{1}) /\ (hid2 \in KMSProcedures.hids{1}) /\ hid1 <> hid2 =>
     hid1.`1 <> hid2.`1 /\ hid1.`2 <> hid2.`2) /\
  inv_bounds MRPKE_lor.count_gen{2} KMSProcedures.count_hid{2} RealSigServ.count_sig{2} KMSProcedures.count_tkmng{2}
  MRPKE_lor.count_dec{2} KMSProcedures.count_unw{2} KMSProcedures.count_corr{2} MRPKE_lor.count_lor{2}.
proof.
  conseq />;proc.
  sp; if; 1: done; last by auto => /#.
  wp => /=.
  if; 1: done; last by auto => /#.
  seq 3 3 : ( ={hdl, rsk, tdo, KMSProcedures.count_corr, KMSProcedures.corrupted} /\ KMSProcedures.count_corr{1} < q_corr /\
               inv_bounds MRPKE_lor.count_gen{2} KMSProcedures.count_hid{2} RealSigServ.count_sig{2} KMSProcedures.count_tkmng{2}
                 MRPKE_lor.count_dec{2} KMSProcedures.count_unw{2} (KMSProcedures.count_corr{2} + 1) MRPKE_lor.count_lor{2});
    last by if => //;wp;skip => /#.
  inline IdealTrustService(OpPolSrv(OA)).isProtectedTrust.
  sp; if => //.
  + by conseq />; inline *;wp;skip => /> /#.
  move=> /=.  
  inline *.
  wp 16 16. sp 15 15.
  if;[1: smt()| 3: by skip => /> /#].
  rcondt{2} 9; 1: by auto; smt (mrenc_bounds).
  auto => /> *; split => *.
  + smt(corr_keys taginj).
  split; 2: smt().
  move /negP : H24 => /(_ _) //.
  have : (oget HSMs_MRPKEOr.benc_keys_stripped{2}.[hid{2}.`1] \in tk{2}.`tk_wdata.`tkw_ekeys); first by smt(mem_fdom imageE imageP). 
  have [#?? /(_ H22) [#] ?]:= H hid{2}.`1.
  rewrite mem_fdom => ? /= *.
  smt(corr_keys taginj).
qed.

equiv test_E : KMSProcedures4(OA).test ~ KMSProceduresHop3(OA, IdealSigServ, MRPKE_lor).test : 
  ={hid, hstid, tk, hdl} /\
  ={glob OA, glob RealSigServ, glob HstPolSrv, glob RealTrustService, glob OpPolSrv, glob KMSProcedures,
      KMSProcedures3.wrapped_keys, HSMs.who} /\
  inv_benc_keys HSMs.benc_keys{1} HSMs_MRPKEOr.benc_keys_stripped{2} MRPKE_lor.pklist{2} RealSigServ.pks_sks{1}
    KMSProcedures.hids{1} HSMs.who{1} /\
  (forall (pk : Pk), pk \in MRPKE_lor.pklist{2} => (oget MRPKE_lor.pklist{2}.[pk], pk) \in gen) /\
  inv_genuine OpPolSrv.genuine{1} RealSigServ.pks_sks{1} HSMs.benc_keys{1} KMSProcedures.hids{1} HSMs.who{1} /\
  inv_encode KMSProcedures.tklist{1} RealSigServ.qs{2} /\
  (forall (tk : Token) (pk : Pk) (cph : CTxt),
     ! (tk.`tk_trust \in RealTrustService.protectedTrusts{2}) =>
     tk.`tk_wdata.`tkw_ekeys.[pk] = Some cph => ! ((pk, encode_tag tk.`tk_trust, cph) \in MRPKE_lor.lorlist{2})) /\
  (forall (t : Trust), t \in RealTrustService.protectedTrusts{1} => all_genuine OpPolSrv.genuine{1} t) /\
  (forall (hid1 hid2 : HId),
     (hid1 \in KMSProcedures.hids{1}) /\ (hid2 \in KMSProcedures.hids{1}) /\ hid1 <> hid2 =>
     hid1.`1 <> hid2.`1 /\ hid1.`2 <> hid2.`2) /\
  inv_bounds MRPKE_lor.count_gen{2} KMSProcedures.count_hid{2} RealSigServ.count_sig{2} KMSProcedures.count_tkmng{2}
    MRPKE_lor.count_dec{2} KMSProcedures.count_unw{2} KMSProcedures.count_corr{2} MRPKE_lor.count_lor{2}
  ==>
  ={res} /\
  ={glob OA, glob RealSigServ, glob HstPolSrv, glob RealTrustService, glob OpPolSrv, glob KMSProcedures,
      KMSProcedures3.wrapped_keys, HSMs.who} /\
  inv_benc_keys HSMs.benc_keys{1} HSMs_MRPKEOr.benc_keys_stripped{2} MRPKE_lor.pklist{2} RealSigServ.pks_sks{1}
    KMSProcedures.hids{1} HSMs.who{1} /\
  (forall (pk : Pk), pk \in MRPKE_lor.pklist{2} => (oget MRPKE_lor.pklist{2}.[pk], pk) \in gen) /\
  inv_genuine OpPolSrv.genuine{1} RealSigServ.pks_sks{1} HSMs.benc_keys{1} KMSProcedures.hids{1} HSMs.who{1} /\
  inv_encode KMSProcedures.tklist{1} RealSigServ.qs{2} /\
  (forall (tk : Token) (pk : Pk) (cph : CTxt),
     ! (tk.`tk_trust \in RealTrustService.protectedTrusts{2}) =>
     tk.`tk_wdata.`tkw_ekeys.[pk] = Some cph => ! ((pk, encode_tag tk.`tk_trust, cph) \in MRPKE_lor.lorlist{2})) /\
  (forall (t : Trust), t \in RealTrustService.protectedTrusts{1} => all_genuine OpPolSrv.genuine{1} t) /\
  (forall (hid1 hid2 : HId),
     (hid1 \in KMSProcedures.hids{1}) /\ (hid2 \in KMSProcedures.hids{1}) /\ hid1 <> hid2 =>
     hid1.`1 <> hid2.`1 /\ hid1.`2 <> hid2.`2) /\
  inv_bounds MRPKE_lor.count_gen{2} KMSProcedures.count_hid{2} RealSigServ.count_sig{2} KMSProcedures.count_tkmng{2}
    MRPKE_lor.count_dec{2} KMSProcedures.count_unw{2} KMSProcedures.count_corr{2} MRPKE_lor.count_lor{2}.
proof.
  proc; conseq |>; inline *.
  seq 2 2 : (#pre /\ ={rsk, rkey}); 1: by auto.
  if; [done | | auto].
  sp 6 6; wp 1 1.
  if;[done | sim | auto].
  sp 5 5; if;[done | | auto].
  sp 12 12.
  if;1:smt(); sim.
qed.

lemma hop4 &m :
   Pr [ KMSRoR3(A,OA).main() @ &m : res ] - Pr[ KMSRoR4(A,OA).main() @ &m : res ] =
   Pr [ MRPKE_Sec(Bhop3(A,OA)).game(false) @ &m : res] - Pr[ MRPKE_Sec(Bhop3(A,OA)).game(true) @ &m : res].
proof.
cut -> : Pr [  KMSRoR3(A,OA).main() @ &m : res ] = 
         Pr [ MRPKE_Sec(Bhop3(A,OA)).game(false) @ &m : res].
+ byequiv => //.
  proc.
  inline  Bhop3(A, OA, MRPKE_lor).guess. 
  wp;call (_: ={glob OA,glob RealSigServ, 
              glob HstPolSrv, glob RealTrustService, 
              glob OpPolSrv, glob KMSProcedures, 
              KMSProcedures3.wrapped_keys, HSMs.who} /\ 
              MRPKE_lor.b{2} = false /\ 
              (* verification keys are identifiers. *)
              inv_benc_keys HSMs.benc_keys{1} HSMs_MRPKEOr.benc_keys_stripped{2} MRPKE_lor.pklist{2}
                 RealSigServ.pks_sks{1} KMSProcedures.hids{1} HSMs.who{1} /\
              (forall pk, pk \in MRPKE_lor.pklist => (oget MRPKE_lor.pklist.[pk],pk) \in gen){2} /\
              (inv_genuine OpPolSrv.genuine RealSigServ.pks_sks HSMs.benc_keys KMSProcedures.hids HSMs.who){1} /\
              inv_encode KMSProcedures.tklist{1} RealSigServ.qs{2} /\
              (forall (tk : Token) pk cph, (!tk.`tk_trust \in RealTrustService.protectedTrusts{2}) =>
                       tk.`tk_wdata.`tkw_ekeys.[pk] = Some cph =>
                       (!(pk,encode_tag tk.`tk_trust,cph) \in MRPKE_lor.lorlist{2})) /\
              (forall t, t \in RealTrustService.protectedTrusts{1} => all_genuine OpPolSrv.genuine{1} t) /\
              (forall hid1 hid2, (hid1 \in KMSProcedures.hids{1} /\ hid2 \in KMSProcedures.hids{1} /\ hid1 <> hid2) => 
                  (hid1.`1 <> hid2.`1 /\ hid1.`2 <> hid2.`2)) /\
              (inv_bounds MRPKE_lor.count_gen KMSProcedures.count_hid RealSigServ.count_sig KMSProcedures.count_tkmng MRPKE_lor.count_dec
                        KMSProcedures.count_unw KMSProcedures.count_corr MRPKE_lor.count_lor){2}); last first.
  + by inline *;auto; call(_:true); auto; smt (more_bounds in_fset0 mem_empty).
  + by conseq newOp_E.
  + by conseq requestAuthorization_E.
  + by conseq newHst_E.
  + by conseq installInitialTrust_E.
  + by conseq installUpdatedTrust_E.
  + by conseq newHSM_E.
  + by conseq newToken_E.
  (* updateTokenTrust *)
  + proc; sp;if;[1:done | 3: by auto].
    conseq />; if;[1:done | 3: auto => /> /#].
    seq 2 2 : (tdo{1} = None /\ 
           #pre /\ ={tdo,protected} /\ 
           (protected{1} = (all_genuine OpPolSrv.genuine{1} (tr_data tk{1}.`tk_trust) && 
            tk{1}.`tk_trust \in RealTrustService.protectedTrusts{1}))).
    + by inline *;auto.
    seq 1 1 : (#{/~tdo{1}}{/~tdo{2}}{~inv_bounds} pre /\ ={tdo} /\
                   (tdo{1} <> None => 
                      ((oget tdo{1}).`td_trust = tr_data tk{1}.`tk_trust /\
                       (tk{1}.`tk_trust \in RealTrustService.protectedTrusts{1} => 
                         ((hid_in hid HSMs.benc_keys){1} /\ 
                          (tk.`tk_wdata.`tkw_signer.`1 \in RealSigServ.pks_sks){1} /\
                          ((tk.`tk_wdata.`tkw_signer.`1,encode_msg (tk.`tk_trust , tk.`tk_wdata.`tkw_ekeys, tk.`tk_wdata.`tkw_signer,tk.`tk_updt),tk.`tk_wdata.`tkw_sig) \in
                           RealSigServ.qs){1})))) /\
                   inv_bounds MRPKE_lor.count_gen{2} KMSProcedures.count_hid{2} (RealSigServ.count_sig{2} + 1)  (KMSProcedures.count_tkmng{2} + 1)
                      MRPKE_lor.count_dec{2} KMSProcedures.count_unw{2} KMSProcedures.count_corr{2} (MRPKE_lor.count_lor{2} + 1)).
    + inline *;if;1:done.
      + by sp 12 12; if;1:smt();auto => /> /#.
      sp 15 15; wp 1 1.   
      if;[smt() | | skip => /#].
      rcondt{2} 9; 1: by auto; smt (mrenc_bounds).
      auto => /> *; split => *.
      + smt(corr_keys taginj).
      split; 2: smt().
      move /negP : H25 => /(_ _) //.
      have : (oget HSMs_MRPKEOr.benc_keys_stripped{2}.[hid{2}.`1] \in tk{2}.`tk_wdata.`tkw_ekeys); first by smt(mem_fdom imageE imageP). 
      have [#?? /(_ H23) [#] ?]:= H hid{2}.`1.
      rewrite mem_fdom => ? /= *.
      smt(corr_keys taginj).
    wp; if;[1:done | 3:skip => /> /#].
    seq 1 1 : (#pre /\ ={tdoo} /\ 
      (tdoo{2} <> None =>
        (oget tdoo{2}).`td_trust = new_trust{2} /\
        ((all_genuine OpPolSrv.genuine{2} new_trust{2} /\ tk{2}.`tk_trust \in RealTrustService.protectedTrusts{2}) => 
               (oget tdoo{2}).`td_trust \in RealTrustService.protectedTrusts{2}))).
    + by inline *; wp;skip => /> *; smt (in_fsetU in_fset1).
    if;[ 1:done | 3:skip => /> /#].
    wp;inline  IdealTrustService(OpPolSrv(OA)).isProtectedTrust.
    sp 3 3.
    seq 1 1 :(#{/~(protected{1} = _)}pre /\ ={protected} /\ 
               protected{2} = (all_genuine OpPolSrv.genuine{2} new_trust{2} && new_trust{2} \in RealTrustService.protectedTrusts{2})).
    + auto => /#. 
    if; 1: done; last first.
    + inline *. wp;sp.
      seq 1 1 : (#pre /\ ={ekeys} /\ ekeys{1} \in mencrypt (proj_pks mem{1}) tag{1} ptxt{1} /\ (proj_pks mem = fdom ekeys){1}).
      + by rnd;skip => />; smt (bpke_keys). 
      rcondt {1} 5; 1: by move=>*;wp;skip; smt (sig_bound).
      rcondt {2} 5; 1: by move=>*;wp;skip; smt (sig_bound).
      sp;if; 1:done;last by auto => />; smt(in_fsetU in_fset1).
      auto => />. smt (mem_fdom imageP encode_msg_inj in_fsetU in_fset1). 
    (* fake wrap *)
    wp;inline *.
    rcondt {2} 14; 1: by move => *;wp;skip;smt (mrenc_bounds).
    rcondt {2} 14.
    + move => *;wp;skip => /> *.
      have /> * := H21 H22.
      have ? :(forall hid, 
             hid \in tr_mems (oget tdoo{hr}).`td_trust => 
             hid.`2 \in fdom MRPKE_lor.pklist{hr}).
      + move=> *.
        have ? : hid1 \in OpPolSrv.genuine{hr};first by smt(encode_msg_inj in_fsetU1  imageE imageP oget_some).
        rewrite mem_fdom. 
        have [? /(_ H27) /> *]:= H1 hid1.
        have := H hid1.`1 =>  /#.
        progress.
          by smt(encode_msg_inj in_fsetU1  imageE imageP oget_some). 
          rewrite /proj_pks.   move : H8. rewrite -cardE -cardE /tr_data.  move : ( fcard_image_leq (fun (p : pkey * Pk) => p.`2) (tr_mems (oget tdoo{hr}).`td_trust)). smt(mrenc_bounds).
    sp.
    seq 1 1 : ( #pre /\ ekeys{1} = cph{2} /\ ekeys{1} \in mencrypt (proj_pks mem{1}) tag{1} ptxt{1} /\ (proj_pks mem = fdom ekeys){1}).
    + by rnd;skip => />; smt (bpke_keys). 
    rcondt {1} 5; 1: by move => *;wp;skip;smt (sig_bound).
    rcondt {2} 10; 1: by move => *;wp;skip;smt (sig_bound).
    wp;sp; if; 1: done; last by auto => />; smt(in_fsetU in_fset1).  
    auto => /> *;split.
    + rewrite /inv_encode; progress; smt (mem_fdom imageP encode_msg_inj in_fsetU in_fset1). 
    smt(mapP taginj mem_cat).
 
  (* addTokenKey *)
  + proc;conseq />.
    sp;seq 1 1 : (#pre /\ ={key}); first by auto.
    if;[1:done | 3: skip => /> /#].
    wp; if;[1:done | 3: skip => /> /#].
    seq 2 2 : (tdo{1} = None /\ 
           #pre /\ ={tdo,protected} /\ 
           (protected{1} = (all_genuine OpPolSrv.genuine{1} (tr_data tk{1}.`tk_trust) && 
            tk{1}.`tk_trust \in RealTrustService.protectedTrusts{1}))).
    + by inline *;auto.
    seq 1 1 : (#{/~tdo{1}}{/~tdo{2}}{~inv_bounds} pre /\ ={tdo} /\
                   (tdo{1} <> None => 
                      ((oget tdo{1}).`td_trust = tr_data tk{1}.`tk_trust /\
                       (tk{1}.`tk_trust \in RealTrustService.protectedTrusts{1} => 
                         ((hid_in hid HSMs.benc_keys){1} /\ 
                          (tk.`tk_wdata.`tkw_signer.`1 \in RealSigServ.pks_sks){1} /\
                          ((tk.`tk_wdata.`tkw_signer.`1,encode_msg (tk.`tk_trust , tk.`tk_wdata.`tkw_ekeys, tk.`tk_wdata.`tkw_signer,tk.`tk_updt),tk.`tk_wdata.`tkw_sig) \in
                           RealSigServ.qs){1})))) /\
                   inv_bounds MRPKE_lor.count_gen{2} KMSProcedures.count_hid{2} (RealSigServ.count_sig{2} + 1)  (KMSProcedures.count_tkmng{2} + 1)
                      MRPKE_lor.count_dec{2} KMSProcedures.count_unw{2} KMSProcedures.count_corr{2} (MRPKE_lor.count_lor{2} + 1)).
    + inline *;if;1:done.
      + by sp 12 12; if;1:smt();auto => /> /#.
      sp 15 15; wp 1 1.   
      if;[smt() | | skip => /#].
      rcondt{2} 9; 1: by auto; smt (mrenc_bounds).
      auto => /> *; split => *.
      + smt(corr_keys taginj).
      split; 2: smt().
      move /negP : H25 => /(_ _) //.
      have : (oget HSMs_MRPKEOr.benc_keys_stripped{2}.[hid{2}.`1] \in tk{2}.`tk_wdata.`tkw_ekeys); first by smt(mem_fdom imageE imageP). 
      have [#?? /(_ H23) [#] ?]:= H hid{2}.`1.
      rewrite mem_fdom => ? /= *.
      smt(corr_keys taginj).
    wp; if;[1:done | 3:skip => /> /#].
    sp;if; [1: done | 3: skip => /> /#].
    wp;inline IdealTrustService(OpPolSrv(OA)).isProtectedTrust.
    sp 3 3; seq 1 1 : (#pre).
    + by wp;skip=> /> * /#.
    if => //=; last first.
    (* normal wrap *)
    + inline *. wp;sp.
      seq 1 1 : (#pre /\ ={ekeys} /\ ekeys{1} \in mencrypt (proj_pks mem{1}) tag{1} ptxt{1} /\ (proj_pks mem = fdom ekeys){1}).
      + by rnd;skip => />; smt (bpke_keys). 
      rcondt {1} 5; 1: by move=>*;wp;skip; smt (sig_bound).
      rcondt {2} 5; 1: by move=>*;wp;skip; smt (sig_bound).
      sp;if; 1:done;last by auto => />; smt(in_fsetU in_fset1).
      auto => />; smt (mem_fdom imageP encode_msg_inj in_fsetU in_fset1). 
    (* fake wrap *)
    wp;inline *.
    rcondt {2} 14; 1: by move => *;wp;skip;smt (mrenc_bounds).
    rcondt {2} 14.
    + move => *;wp;skip => /> *.
      have /> * := H11 H19.
      have ? :(forall hid, 
             hid \in tr_mems (oget (add_key hdl{hr} key{hr} (oget tdo{hr}))).`td_trust => 
             hid.`2 \in fdom MRPKE_lor.pklist{hr}).
      + move => *.
        have ? : hid1 \in OpPolSrv.genuine{hr};first by smt(encode_msg_inj in_fsetU1  imageE imageP oget_some).
        rewrite mem_fdom. 
        have [? /(_ H26) /> *]:= H1 hid1.
        have := H hid1.`1 =>  /#.
        progress.
      by smt(encode_msg_inj in_fsetU1  imageE imageP oget_some).
          rewrite /proj_pks.   move : H8. rewrite -cardE -cardE /tr_data. 
          rewrite (_: tr_mems (oget (add_key hdl{hr} key{hr} (oget tdo{hr}))).`td_trust = tr_mems tk{hr}.`tk_trust); first by rewrite /add_key; smt().
          move : ( fcard_image_leq (fun (p : pkey * Pk) => p.`2) ((tr_mems tk{hr}.`tk_trust))). smt(mrenc_bounds).
    sp.
    seq 1 1 : ( #pre /\ ekeys{1} = cph{2} /\ ekeys{1} \in mencrypt (proj_pks mem{1}) tag{1} ptxt{1} /\ (proj_pks mem = fdom ekeys){1}).
    + by rnd;skip => />; smt (bpke_keys). 
    rcondt {1} 5; 1: by move => *;wp;skip;smt (sig_bound).
    rcondt {2} 10; 1: by move => *;wp;skip;smt (sig_bound).
    wp;sp; if; 1: done; last by auto => />; smt(in_fsetU in_fset1).  
    auto => /> *;split.
    + smt (mem_fdom imageP encode_msg_inj in_fsetU in_fset1). 
    move=> tk1 pk0 cph0; case ((pk0, encode_tag tk1.`tk_trust, cph0) \in lorlist_R); first by smt().
    move => *.
    have aux1 : ((oget (add_key hdl{2} key{2} (oget tdo{2}))).`td_trust = (oget tdo{2}).`td_trust) by smt().
    smt(mem_cat mapP taginj).

  + by conseq unwrap_E.
  + by conseq corrupt_E.
  + by conseq test_E.

cut -> // : Pr [ KMSRoR4(A,OA).main() @ &m : res ] = 
            Pr [ MRPKE_Sec(Bhop3(A,OA)).game(true) @ &m : res].
byequiv => //.
proc;inline *;wp.
call (_: ={glob OA,glob RealSigServ, 
              glob HstPolSrv, glob RealTrustService, 
              glob OpPolSrv, glob KMSProcedures, 
              KMSProcedures3.wrapped_keys, HSMs.who} /\ 
              MRPKE_lor.b{2} = true /\ 
              (* verification keys are identifiers. *)
              inv_benc_keys HSMs.benc_keys{1} HSMs_MRPKEOr.benc_keys_stripped{2} MRPKE_lor.pklist{2}
                 RealSigServ.pks_sks{1} KMSProcedures.hids{1} HSMs.who{1} /\
              (forall pk, pk \in MRPKE_lor.pklist => (oget MRPKE_lor.pklist.[pk],pk) \in gen){2} /\
              (inv_genuine OpPolSrv.genuine RealSigServ.pks_sks HSMs.benc_keys KMSProcedures.hids HSMs.who){1} /\
              inv_encode KMSProcedures.tklist{1} RealSigServ.qs{2} /\
              (* if a trust is not protected, then it was not used as a tag in lor *)
              (forall (tk : Token) pk cph, (!tk.`tk_trust \in RealTrustService.protectedTrusts{2}) =>
                       tk.`tk_wdata.`tkw_ekeys.[pk] = Some cph =>
                       (!(pk,encode_tag tk.`tk_trust,cph) \in MRPKE_lor.lorlist{2})) /\
              (* protected means all genuine *)
              (forall t, t \in RealTrustService.protectedTrusts{1} => all_genuine OpPolSrv.genuine{1} t) /\
              (forall hid1 hid2, (hid1 \in KMSProcedures.hids{1} /\ hid2 \in KMSProcedures.hids{1} /\ hid1 <> hid2) => 
                  (hid1.`1 <> hid2.`1 /\ hid1.`2 <> hid2.`2)) /\
              (inv_bounds MRPKE_lor.count_gen KMSProcedures.count_hid RealSigServ.count_sig KMSProcedures.count_tkmng MRPKE_lor.count_dec
                        KMSProcedures.count_unw KMSProcedures.count_corr MRPKE_lor.count_lor){2}); last first.
+ inline *;auto; call(_:true); auto; smt (more_bounds in_fset0 mem_empty).
+ by conseq newOp_E.
+ by conseq requestAuthorization_E.
+ by conseq newHst_E.
+ by conseq installInitialTrust_E.
+ by conseq installUpdatedTrust_E.
+ by conseq newHSM_E.
+ by conseq newToken_E.
(* updateTokenTrust *)
+ proc; sp;if;[1:done | 3: by auto].
  conseq />; if;[1:done | 3: auto => /> /#].
  seq 2 2 : (tdo{1} = None /\ 
         #pre /\ ={tdo,protected} /\ 
         (protected{1} = (all_genuine OpPolSrv.genuine{1} (tr_data tk{1}.`tk_trust) && 
          tk{1}.`tk_trust \in RealTrustService.protectedTrusts{1}))).
  + by inline *;auto.
  seq 1 1 : (#{/~tdo{1}}{/~tdo{2}}{~inv_bounds} pre /\ ={tdo} /\
                 (tdo{1} <> None => 
                    ((oget tdo{1}).`td_trust = tr_data tk{1}.`tk_trust /\
                     (tk{1}.`tk_trust \in RealTrustService.protectedTrusts{1} => 
                       ((hid_in hid HSMs.benc_keys){1} /\ 
                        (tk.`tk_wdata.`tkw_signer.`1 \in RealSigServ.pks_sks){1} /\
                        ((tk.`tk_wdata.`tkw_signer.`1,encode_msg (tk.`tk_trust , tk.`tk_wdata.`tkw_ekeys, tk.`tk_wdata.`tkw_signer,tk.`tk_updt),tk.`tk_wdata.`tkw_sig) \in
                         RealSigServ.qs){1})))) /\
                 inv_bounds MRPKE_lor.count_gen{2} KMSProcedures.count_hid{2} (RealSigServ.count_sig{2} + 1)  (KMSProcedures.count_tkmng{2} + 1)
                    MRPKE_lor.count_dec{2} KMSProcedures.count_unw{2} KMSProcedures.count_corr{2} (MRPKE_lor.count_lor{2} + 1)).
  + inline *;if;1:done.
    + by sp 12 12; if;1:smt();auto => /> /#.
    sp 15 15; wp 1 1.   
    if;[smt() | | skip => /#].
    rcondt{2} 9; 1: by auto; smt (mrenc_bounds).
    auto => /> *; split => *.
    + smt(corr_keys taginj).
    split; 2: smt().
    move /negP : H25 => /(_ _) //.
    have : (oget HSMs_MRPKEOr.benc_keys_stripped{2}.[hid{2}.`1] \in tk{2}.`tk_wdata.`tkw_ekeys); first by smt(mem_fdom imageE imageP). 
    have [#?? /(_ H23) [#] ?]:= H hid{2}.`1.
    rewrite mem_fdom => ? /= *.
    smt(corr_keys taginj).
  wp; if;[1:done | 3:skip => /> /#].
  seq 1 1 : (#pre /\ ={tdoo} /\ 
    (tdoo{2} <> None =>
      (oget tdoo{2}).`td_trust = new_trust{2} /\
      ((all_genuine OpPolSrv.genuine{2} new_trust{2} /\ tk{2}.`tk_trust \in RealTrustService.protectedTrusts{2}) => 
             (oget tdoo{2}).`td_trust \in RealTrustService.protectedTrusts{2}))).
  + by inline *; wp;skip => /> *; smt (in_fsetU in_fset1).
  if;[ 1:done | 3:skip => /> /#].
  wp;inline IdealTrustService(OpPolSrv(OA)).isProtectedTrust.
  sp 3 3.
  seq 1 1 :(#{/~(protected{1} = _)}pre /\ ={protected} /\ 
             protected{2} = (all_genuine OpPolSrv.genuine{2} new_trust{2} && new_trust{2} \in RealTrustService.protectedTrusts{2})).
  + auto => /#. 
  if; 1: done; last first.
  + inline *. wp;sp.
    seq 1 1 : (#pre /\ ekeys0{1} = ekeys{2} /\ ekeys0{1} \in mencrypt (proj_pks mem0{1}) tag0{1} ptxt0{1} /\ (proj_pks mem0 = fdom ekeys0){1}).
    + by rnd;skip => />; smt (bpke_keys). 
    rcondt {1} 5; 1: by move=>*;wp;skip; smt (sig_bound).
    rcondt {2} 5; 1: by move=>*;wp;skip; smt (sig_bound).
    sp;if; 1:done;last by auto => />; smt(in_fsetU in_fset1).
    by auto => />; smt (mem_fdom imageP encode_msg_inj in_fsetU in_fset1). 
  (* fake wrap *)
  wp;inline *.
  rcondt {2} 14; 1: by move => *;wp;skip;smt (mrenc_bounds).
  rcondt {2} 14.
  + move => *;wp;skip => /> *.
    have /> * := H21 H22.
    have ? :(forall hid, 
           hid \in tr_mems (oget tdoo{hr}).`td_trust => 
           hid.`2 \in fdom MRPKE_lor.pklist{hr}).
    + move=> *.
      have ? : hid1 \in OpPolSrv.genuine{hr};first by smt(encode_msg_inj in_fsetU1  imageE imageP oget_some).
      rewrite mem_fdom. 
      have [? /(_ H27) /> *]:= H1 hid1.
      have := H hid1.`1 =>  /#.
    progress.
    by smt(encode_msg_inj in_fsetU1  imageE imageP oget_some).
          rewrite /proj_pks.   move : H8. rewrite -cardE -cardE /tr_data. 
          move : ( fcard_image_leq (fun (p : pkey * Pk) => p.`2) (tr_mems (oget tdoo{hr}).`td_trust)). smt(mrenc_bounds).
  sp.
  seq 1 1 : ( #pre /\ ekeys{1} = cph{2} /\ ekeys{1} \in mencrypt (proj_pks mem{1}) tag{1} fake{1} /\ (proj_pks mem = fdom ekeys){1}).
  + by rnd;skip => />; smt (bpke_keys). 
  rcondt {1} 5; 1: by move => *;wp;skip;smt (sig_bound).
  rcondt {2} 10; 1: by move => *;wp;skip;smt (sig_bound).
  wp;sp; if; 1: done; last by auto => />; smt(in_fsetU in_fset1).  
  auto => /> *;split. rewrite /inv_encode => *.
  + smt (mem_fdom imageP encode_msg_inj in_fsetU in_fset1). 
  smt(mapP taginj mem_cat).
(* addTokenKey *)
+ proc;conseq />.
  sp;seq 1 1 : (#pre /\ ={key}); first by auto.
  if;[1:done | 3: skip => /> /#].
  wp; if;[1:done | 3: skip => /> /#].
  seq 2 2 : (tdo{1} = None /\ 
         #pre /\ ={tdo,protected} /\ 
         (protected{1} = (all_genuine OpPolSrv.genuine{1} (tr_data tk{1}.`tk_trust) && 
          tk{1}.`tk_trust \in RealTrustService.protectedTrusts{1}))).
  + by inline *;auto.
  seq 1 1 : (#{/~tdo{1}}{/~tdo{2}}{~inv_bounds} pre /\ ={tdo} /\
                 (tdo{1} <> None => 
                    ((oget tdo{1}).`td_trust = tr_data tk{1}.`tk_trust /\
                     (tk{1}.`tk_trust \in RealTrustService.protectedTrusts{1} => 
                       ((hid_in hid HSMs.benc_keys){1} /\ 
                        (tk.`tk_wdata.`tkw_signer.`1 \in RealSigServ.pks_sks){1} /\
                        ((tk.`tk_wdata.`tkw_signer.`1,encode_msg (tk.`tk_trust , tk.`tk_wdata.`tkw_ekeys, tk.`tk_wdata.`tkw_signer,tk.`tk_updt),tk.`tk_wdata.`tkw_sig) \in
                         RealSigServ.qs){1})))) /\
                 inv_bounds MRPKE_lor.count_gen{2} KMSProcedures.count_hid{2} (RealSigServ.count_sig{2} + 1)  (KMSProcedures.count_tkmng{2} + 1)
                    MRPKE_lor.count_dec{2} KMSProcedures.count_unw{2} KMSProcedures.count_corr{2} (MRPKE_lor.count_lor{2} + 1)).
  + inline *;if;1:done.
    + by sp 12 12; if;1:smt();auto => /> /#.
    sp 15 15; wp 1 1.   
    if;[smt() | | skip => /#].
    rcondt{2} 9; 1: by auto; smt (mrenc_bounds).
    auto => /> *; split => *.
    + smt(corr_keys taginj).
    split; 2: smt().
    move /negP : H25 => /(_ _) //.
    have : (oget HSMs_MRPKEOr.benc_keys_stripped{2}.[hid{2}.`1] \in tk{2}.`tk_wdata.`tkw_ekeys); first by smt(mem_fdom imageE imageP). 
    have [#?? /(_ H23) [#] ?]:= H hid{2}.`1.
    rewrite mem_fdom => ? /= *.
    smt(corr_keys taginj).
  wp; if;[1:done | 3:skip => /> /#].
  sp;if; [1: done | 3: skip => /> /#].
  wp;inline IdealTrustService(OpPolSrv(OA)).isProtectedTrust.
  sp 3 3; seq 1 1 : (#pre).
  + by wp;skip=> /> * /#.
  if => //=; last first.
  (* normal wrap *)
  + inline *. wp;sp.
    seq 1 1 : (#pre /\ ekeys0{1} = ekeys{2} /\ ekeys0{1} \in mencrypt (proj_pks mem0{1}) tag0{1} ptxt0{1} /\ (proj_pks mem0 = fdom ekeys0){1}).
    + by rnd;skip => />; smt (bpke_keys). 
    rcondt {1} 5; 1: by move=>*;wp;skip; smt (sig_bound).
    rcondt {2} 5; 1: by move=>*;wp;skip; smt (sig_bound).
    sp;if; 1:done;last by auto => />; smt(in_fsetU in_fset1).
    by auto => />; smt (mem_fdom imageP encode_msg_inj in_fsetU in_fset1). 
  (* fake wrap *)
  wp;inline *.
  rcondt {2} 14; 1: by move => *;wp;skip;smt (mrenc_bounds).
  rcondt {2} 14.
  + move => *;wp;skip => /> *.
    have /> * := H11 H19.
    have ? :(forall hid, 
           hid \in tr_mems (oget (add_key hdl{hr} key{hr} (oget tdo{hr}))).`td_trust => 
           hid.`2 \in fdom MRPKE_lor.pklist{hr}).
    + move => *.
      have ? : hid1 \in OpPolSrv.genuine{hr};first by smt(encode_msg_inj in_fsetU1  imageE imageP oget_some).
      rewrite mem_fdom. 
      have [? /(_ H26) /> *]:= H1 hid1.
      have := H hid1.`1 =>  /#.
   progress.
    by smt(encode_msg_inj in_fsetU1  imageE imageP oget_some).
          rewrite /proj_pks.   move : H8. rewrite -cardE -cardE /tr_data. 
          rewrite (_: tr_mems (oget (add_key hdl{hr} key{hr} (oget tdo{hr}))).`td_trust = tr_mems tk{hr}.`tk_trust); first by rewrite /add_key; smt().
          move : ( fcard_image_leq (fun (p : pkey * Pk) => p.`2) ((tr_mems tk{hr}.`tk_trust))). smt(mrenc_bounds).
  sp.
  seq 1 1 : ( #pre /\ ekeys{1} = cph{2} /\ ekeys{1} \in mencrypt (proj_pks mem{1}) tag{1} fake{1} /\ (proj_pks mem = fdom ekeys){1}).
  + by rnd;skip => />; smt (bpke_keys). 
  rcondt {1} 5; 1: by move => *;wp;skip;smt (sig_bound).
  rcondt {2} 10; 1: by move => *;wp;skip;smt (sig_bound).
  wp;sp; if; 1: done; last by auto => />; smt(in_fsetU in_fset1).  
  auto => /> *;split.
  + rewrite /inv_encode; smt (mem_fdom imageP encode_msg_inj in_fsetU in_fset1). 
  move=> tk1 pk0 cph0; case ((pk0, encode_tag tk1.`tk_trust, cph0) \in lorlist_R); first by smt().
  move => *.
  have aux1 : ((oget (add_key hdl{2} key{2} (oget tdo{2}))).`td_trust = (oget tdo{2}).`td_trust) by smt().
  smt(mem_cat mapP taginj).

+ by conseq unwrap_E.
+ by conseq corrupt_E.
+ by conseq test_E.
qed.

end section.

end Hop5.
