Contents of files:

* Policy.ec - syntax and definitions of abstract trust management policy

* KMSIND.ec - Syntax and IND security model for KMSDMP.

* KMSProto.ec - DMP protocol specification

* KMSProto_Hop*.ec - game hops

* KMSProto_final.ec - proof that in final game adversary has no advantage

* MainTheorem.ec - puts together the results of all hops for abstract policy

* GNPolicy - DMP trust management policy, specifies and proves honest trust 
             propagation invariant downto operator actions.
             Instantiates MainTheorem for this policy and contains full
             theorem statement for IND security.

* UCvsInd.ec - UC security model and IND model for abstract crypto API and their relation

* KMSUC.ec - Instantiation of the previous file with KMSDMP API

