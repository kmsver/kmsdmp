require import AllCore List FSet SmtMap Distr DBool Dexcepted Real RealExtra.
require KMSProto_Hop1_Sig.

abstract theory Hop2.

clone include KMSProto_Hop1_Sig.Hop1.
import KMSProto.
import KMSIND.
import PKSMK_HSM.
import Policy.
import MRPKE.

(******* IDEALIZED POLICY *************)

module HSMsP_TS(TS : TrustService) = {
   include HSMsP(IdealSigServ) [-checkTrustUpdate]
   proc checkTrustUpdate(old : TkData, new : Trust, 
                       auth : Authorizations) : TkData option = {
        var c;
        c <@ TS.checkTrustUpdate(old.`td_trust,new,auth);
        return if (c) 
               then Some (set_trust new old)
               else None;
   }
}.

module HstSP_TS(TS : TrustService) : HstService = {
  proc init () = {} 
  proc newHst = TS.newHst

  proc installInitialTrust(hstid : HstId, tk : Token) : bool = {
      var b,sid,cphs,msg,sig,check,tkw,trust,inst;
      b <- false;
      inst <- tk.`tk_updt;
      trust <- tk.`tk_trust;
      tkw <- tk.`tk_wdata;
      sid <- tkw.`tkw_signer;
      cphs <- tkw.`tkw_ekeys;
      msg <- encode_msg  (trust,cphs,sid,inst);
      sig <- tkw.`tkw_sig;
      check <@ IdealSigServ.verify(sid.`1, msg, sig);
      if (inst && check && (proj_pks (tr_mems trust)) = fdom cphs && (sid \in tr_mems trust)) {
         b <@ TS.isGoodInitialTrust(trust);
         if (b) {
            TS.installInitialTrust(hstid,trust);
         }
      }
      return b;
  }

  proc installUpdatedTrust(hstid : HstId, tk : Token) : bool = {
      var b,sid,cphs,msg,sig,check,tkw,trust,old,inst;
      b <- false;
      if (hstid \in HstPolSrv.hosts_tr) {
         inst <- tk.`tk_updt;
         trust <- tk.`tk_trust;
         tkw <- tk.`tk_wdata;
         sid <- tkw.`tkw_signer;
         cphs <- tkw.`tkw_ekeys;
         msg <- encode_msg  (trust,cphs,sid,inst);
         sig <- tkw.`tkw_sig;
         check <@ IdealSigServ.verify(sid.`1, msg, sig);
         old <@ TS.getInstalledTrust(hstid);
         (* signer must be in old trust *)
         if (inst && check && (proj_pks (tr_mems (oget old))) = fdom cphs && (sid \in tr_mems (oget old))) {
            b <@ TS.installUpdatedTrust(hstid,trust);
         }
      }
      return b;
  }

  proc isInstalledTrust(hstid : HstId, tk : Token) = {
      var b;
      b <@ TS.isInstalledTrust(hstid,tk.`tk_trust);
      return b;
  }

}.


module KMSProcedures2(HSMs : HSMService, HstS : HstService, 
                        TS : TrustService, OA : OpPolSrvT) : KMSOracles = {
  include KMSProcedures(HSMs,HstS,OA) [newHst,installInitialTrust,installUpdatedTrust,newHSM,updateTokenTrust,addTokenKey,unwrap,corrupt,test]

  proc init() = {
     HSMs.init();
     HstS.init();
     TS.init();
     KMSProcedures.hids <- fset0;
     KMSProcedures.tklist   <- fset0;
     KMSProcedures.corrupted <- fset0;
     KMSProcedures.tested <- empty;
     KMSProcedures.handles <- fset0;
     KMSProcedures.b         <$ {0,1};
     KMSProcedures.count_ops <- 0;
     KMSProcedures.count_auth <- 0;
     KMSProcedures.count_hst <- 0;
     KMSProcedures.count_updt <- 0;
     KMSProcedures.count_hid <- 0;
     KMSProcedures.count_tkmng <- 0;
     KMSProcedures.count_unw <- 0;
     KMSProcedures.count_corr <- 0;
     KMSProcedures.count_test <- 0;
     KMSProcedures.count_inst <- 0;

  }

   proc newOp (badOp : bool) = {
        var opid;
        opid <- witness;
        if (KMSProcedures.count_ops < q_ops) {
           opid <@ TS.newOp(badOp);
           KMSProcedures.count_ops <- KMSProcedures.count_ops + 1;
        }
        return opid;
   }

   proc requestAuthorization(request : Request, opid : OpId) : Authorization option = {
       var auth;
       auth <- witness;
       if (KMSProcedures.count_auth < q_auth) {        
           auth <@ TS.requestAuthorization(request,opid);
           KMSProcedures.count_auth <- KMSProcedures.count_auth + 1;
       }
       return auth;
  }

  proc newToken(hid : HId, new : Trust) : Token option = {
      var rtd,tk,td;
      rtd <- None;
      if (KMSProcedures.count_tkmng < q_tkmng /\ 
          size (elems (tr_mems (tr_data new))) < max_size) {
         if (hid \in KMSProcedures.hids && tr_initial new) {
            td <- {| td_updt = true;
                     td_trust = new;
                     td_skeys = empty; |};
            tk <@ HSMs.wrap(hid,td);
            rtd <- Some tk;
            KMSProcedures.tklist <- KMSProcedures.tklist `|` fset1 tk;
            (* Make policy aware of this trust in 
               the system. Side effect free in real policy.*)
            TS.isGoodInitialTrust(new);
         }
         KMSProcedures.count_tkmng <- KMSProcedures.count_tkmng + 1;
      }
      return rtd; 
  } 
}.

section.

declare module OA : OperatorActions {RealTrustService, RealSigServ, KMSProcedures, HSMs, HstPolSrv, OpPolSrv, HstS, HSMsI}.
declare module A  : KMSAdv     {OA, RealTrustService, RealSigServ, KMSProcedures, HSMs, HstPolSrv, OpPolSrv, HstS, HSMsI}.

axiom op_oa : exists (isGoodInitialTrust : (glob OA) -> Trust -> bool),
              forall (gOA : glob OA) (t : Trust),
                phoare [ OA.isGoodInitialTrust : t = trust /\ (glob OA) = gOA ==> res = isGoodInitialTrust gOA t /\ (glob OA) = gOA ] = 1%r.


local module KMSRoR2a(A : KMSAdv, OA : OpPolSrvT) = {
   module TS = RealTrustService(OA)
   module KMSProc = KMSProcedures2(HSMsP_TS(TS),HstSP_TS(TS),TS,OA)
   module A = A(KMSProc)

   proc main() : bool = {
        var b;
        KMSProc.init();
        b <@ A.guess();
        return (b = KMSProcedures.b) ;     
   }
}.

local equiv isGood_E : OpPolSrv(OA).isGoodInitialTrust ~ RealTrustService(OpPolSrv(OA)).isGoodInitialTrust : 
  ={trust, OpPolSrv.genuine, glob OA} ==> ={res, OpPolSrv.genuine, glob OA }.
proof. by proc => /=;inline *; wp; call (_: true);auto. qed.

local equiv wrap_E : HSMsP(IdealSigServ).wrap ~HSMsP_TS(RealTrustService(OpPolSrv(OA))).wrap : 
  ={hid, td, glob RealSigServ} ==> ={res, glob RealSigServ}.
proof. by proc; sim. qed.

local equiv unwrap_E : HSMsP(IdealSigServ).unwrap ~ HSMsP_TS(RealTrustService(OpPolSrv(OA))).unwrap : 
  ={hid, tk, glob RealSigServ, HSMs.benc_keys} ==> ={res, glob RealSigServ, HSMs.benc_keys}.
proof. by proc; sim. qed.

local lemma hop2a &m :
  Pr [  KMSRoR1(A,OA).main() @ &m : res ] =
  Pr [  KMSRoR2a(A,OpPolSrv(OA)).main() @ &m : res ]. 
proof.
byequiv; last 2 by [].
proc. inline*.
call (_: ={glob HSMPolSrv,glob RealSigServ,glob OA, glob HstPolSrv, glob KMSProcedures, glob OpPolSrv,glob HstS, glob HSMs} /\ 
         KMSProcedures.count_ops{1} = RealTrustService.count_ops{2} /\
         KMSProcedures.count_auth{1} = RealTrustService.count_auth{2} /\
         KMSProcedures.count_hst{1} = RealTrustService.count_hst{2} /\
         RealTrustService.count_updt{2} <= KMSProcedures.count_updt{1} /\
         RealTrustService.count_chk{2} <= KMSProcedures.count_tkmng{1}); 
  last by wp;rnd;wp;call(_:true);wp;skip. 
+ proc; inline *;sp;if => //=. 
  rcondt{2} 3; first by auto.
  by wp;call(_:true);wp;skip.
+ proc;inline *;sp;if => //=; rcondt{2} 4;first by auto.
  by wp; sim />.
+ proc; conseq />; inline HstSP_TS(RealTrustService(OpPolSrv(OA))).newHst; sp; if => //.
  rcondt{2} 2; first by auto.
  by wp; sim />.
+ by proc; sim /> (_: ={OpPolSrv.genuine, glob OA}); apply isGood_E.
+ by proc; inline *;wp;skip => /#.
+ by proc; sim />.
+ proc; sp; if => //; wp; if;[done| | skip => /#].
  inline{2} RealTrustService(OpPolSrv(OA)).isGoodInitialTrust OpPolSrv(OA).isGoodInitialTrust; wp.
  have [isg hc]:= op_oa. ecall{2} (hc (glob OA){2} (tr_data trust{2})); wp.
  by call wrap_E; auto => /#.
+ proc; conseq />; sp; if => //.
  wp; if;[done | | skip => /#].
  seq 1 1 : (#pre /\ ={tdo}).
  + by call unwrap_E.
  conseq />; inline OpPolSrv(OA).quorumAssumption; sp 2 2.
  if => //; last by skip => /#.
  seq 1 1: (#[/1:-8, -7:]pre /\ ={tdoo} /\ RealTrustService.count_chk{2} <= KMSProcedures.count_tkmng{1} + 1).
  + by conseq />; inline *; rcondt{2} 10; auto => /> /#.
  by sim />.
+ proc; conseq />; seq 2 2: (#pre /\ ={rtd, key}); 1: by sim />.
  if => //; wp. 
  seq 0 0 : (#pre /\ RealTrustService.count_chk{2} <= KMSProcedures.count_tkmng{2} + 1); 1: by skip => /#.
  by sim />.
+ by proc; sim />.
+ by proc; sim />.
by proc;sim />.
qed.

module KMSRoR2(A : KMSAdv, OA : OpPolSrvT) = {
   module TS = IdealTrustService(OA)
   module KMSProc = KMSProcedures2(HSMsP_TS(TS),HstSP_TS(TS),TS,OA)
   module A = A(KMSProc)

   proc main() : bool = {
        var b;
        KMSProc.init();
        b <@ A.guess();
        return (b = KMSProcedures.b) ;     
   }
}.

(**************** ADVERSARY *************)


module HSMsP_TS_Hop2(TS : TrustOraclesI) = {
   include HSMsP(IdealSigServ) [-checkTrustUpdate]
   proc checkTrustUpdate(old : TkData, new : Trust, 
                       auth : Authorizations) : TkData option = {
        var c;
        c <@ TS.checkTrustUpdate(old.`td_trust,new,auth);
        return if (c) 
               then Some (set_trust new old)
               else None;
   }
}.

module HstSP_TS_Hop2(TS : TrustOraclesI) : HstService = {
  proc init () = {} 
  proc newHst = TS.newHst


  proc isInstalledTrust(hstid : HstId, tk : Token) = {
      var b;
      b <@ TS.isInstalledTrust(hstid,tk.`tk_trust);
      return b;
  }

  proc installInitialTrust(hstid : HstId, tk : Token) : bool = {
      var b,sid,cphs,msg,sig,check,tkw,trust,inst;
      b <- false;
      inst <- tk.`tk_updt;
      trust <- tk.`tk_trust;
      tkw <- tk.`tk_wdata;
      sid <- tkw.`tkw_signer;
      cphs <- tkw.`tkw_ekeys;
      msg <- encode_msg  (trust,cphs,sid,inst);
      sig <- tkw.`tkw_sig;
      check <@ IdealSigServ.verify(sid.`1, msg, sig);
      if (inst && check && (proj_pks (tr_mems trust)) = fdom cphs && (sid \in tr_mems trust)) {
         b <@ TS.isGoodInitialTrust(trust);
         if (b) {
            TS.installInitialTrust(hstid,trust);
         }
      }
      return b;
  }

  proc installUpdatedTrust(hstid : HstId, tk : Token) : bool = {
      var b,sid,cphs,msg,sig,check,tkw,trust,old,inst;
      b <- false;
      old <@ TS.getInstalledTrust(hstid);
      if (old <> None) {
         inst <- tk.`tk_updt;
         trust <- tk.`tk_trust;
         tkw <- tk.`tk_wdata;
         sid <- tkw.`tkw_signer;
         cphs <- tkw.`tkw_ekeys;
         msg <- encode_msg  (trust,cphs,sid,inst);
         sig <- tkw.`tkw_sig;
         check <@ IdealSigServ.verify(sid.`1, msg, sig);
         (* signer must be in old trust *)
         if (inst && check && (proj_pks (tr_mems (oget old))) = fdom cphs && (sid \in tr_mems (oget old))) {
            b <@ TS.installUpdatedTrust(hstid,trust);
         }
      }
      return b;
  }
}.

module KMSProceduresHop2(TS : TrustOraclesI) : KMSOracles = {
  module HSMs = HSMsP_TS_Hop2(TS)
  module HstS = HstSP_TS_Hop2(TS)
  module OA = {
    proc init ()= {}
    include TS 
  } 
  module KP = KMSProcedures(HSMs,HstS,OA)
  include KP [newHst,installInitialTrust,installUpdatedTrust,newHSM,updateTokenTrust,addTokenKey,unwrap,corrupt,test]

  proc init() = {
     HSMs.init();
     HstS.init();
     KP.hids <- fset0;
     KP.tklist   <- fset0;
     KP.corrupted <- fset0;
     KP.tested <- empty;
     KP.handles <- fset0;
     KP.b         <$ {0,1};
     KP.count_ops <- 0;
     KP.count_auth <- 0;
     KP.count_hst <- 0;
     KP.count_updt <- 0;
     KP.count_hid <- 0;
     KP.count_tkmng <- 0;
     KP.count_unw <- 0;
     KP.count_corr <- 0;
     KP.count_test <- 0;
     KP.count_inst <- 0;

  }

   proc newOp (badOp : bool) = {
        var opid;
        opid <- witness;
        if (KMSProcedures.count_ops < q_ops) {
           opid <@ TS.newOp(badOp);
           KMSProcedures.count_ops <- KMSProcedures.count_ops + 1;
        }
        return opid;
   }

   proc requestAuthorization(request : Request, opid : OpId) : Authorization option = {
       var auth;
       auth <- witness;
       if (KMSProcedures.count_auth < q_auth) {        
           auth <@ TS.requestAuthorization(request,opid);
           KMSProcedures.count_auth <- KMSProcedures.count_auth + 1;
       }
       return auth;
  }

  proc newToken(hid : HId, new : Trust) : Token option = {
      var rtd,tk,td;
      rtd <- None;
      if (KMSProcedures.count_tkmng < q_tkmng /\ 
          size (elems (tr_mems (tr_data new))) < max_size) {
         if (hid \in KP.hids && tr_initial new) {
            td <- {| td_updt = true;
                     td_trust = new;
                     td_skeys = empty; |};
            tk <@ HSMs.wrap(hid,td);
            rtd <- Some tk;
            KP.tklist <- KP.tklist `|` fset1 tk;
            TS.isGoodInitialTrust(new);
         }
         KMSProcedures.count_tkmng <- KMSProcedures.count_tkmng + 1;
      }
      return rtd; 
  } 
}.

module Bhop2(A : KMSAdv,  O : TrustOraclesI) = {
  module A = A(KMSProceduresHop2(O)) 

  proc main() : bool = {
    var b;
    KMSProceduresHop2(O).init();
    b <@ A.guess();
    return (b=KMSProcedures.b);
  }
}.

local lemma hop2 &m:
   Pr[ KMSRoR1(A,OA).main() @ &m : res ] -   
   Pr[ KMSRoR2(A,OpPolSrv(OA)).main() @ &m : res ] =
   Pr[ TrustSecInd(Bhop2(A),RealTrustService(OpPolSrv(OA))).main() @ &m : res ] -
   Pr[ TrustSecInd(Bhop2(A),IdealTrustService(OpPolSrv(OA))).main() @ &m : res].
proof.
rewrite (hop2a &m).
cut ->: Pr [ KMSRoR2a(A,OpPolSrv(OA)).main() @ &m : res ] =
        Pr [ TrustSecInd(Bhop2(A),RealTrustService(OpPolSrv(OA))).main() @ &m : res].
+ byequiv; last 2 by [].
  proc; inline *. sp;wp.
  call (_: (={glob OA,glob RealTrustService, glob HstPolSrv, glob HSMs, 
             glob OpPolSrv, glob KMSProcedures, glob RealSigServ})).
  + by sim. + by sim. + by sim. + by sim. 
  + proc; sp; if => //.
    inline HstSP_TS(RealTrustService(OpPolSrv(OA))).installUpdatedTrust.
    inline HstSP_TS_Hop2(RealTrustService(OpPolSrv(OA))).installUpdatedTrust.
    inline RealTrustService(OpPolSrv(OA)).getInstalledTrust.
    sp; if; [done | | by auto].
    by swap{1} [9..10] -8; sim;auto.
  + by sim. + by sim. + by sim. + by sim. + by sim. + by sim. + by sim.
  by wp;rnd; wp; call (_:true);wp;skip.
cut -> //: Pr [  KMSRoR2(A,OpPolSrv(OA)).main() @ &m : res ] =
           Pr [ TrustSecInd(Bhop2(A),IdealTrustService(OpPolSrv(OA))).main() @ &m : res].
byequiv; last 2 by []. 
proc; inline*; sp; wp.
call (_: (={glob OA,glob RealTrustService, glob HstPolSrv, glob HSMs, 
             glob OpPolSrv, glob KMSProcedures, glob RealSigServ})).
  + by sim. + by sim. + by sim. + by sim. 
  + proc; sp; if => //; sim.
    inline HstSP_TS(IdealTrustService(OpPolSrv(OA))).installUpdatedTrust.
    inline HstSP_TS_Hop2(IdealTrustService(OpPolSrv(OA))).installUpdatedTrust.
    inline IdealTrustService(OpPolSrv(OA)).getInstalledTrust.
    sp; if; [done | | by auto].
    by swap{1} [9..10] -8; sim;auto.
  + by sim. + by sim. + by sim. + by sim. + by sim. + by sim. + by sim.
by wp;rnd; wp; call (_:true);wp;skip;progress.
qed.

lemma concl2 &m: 
  Pr[ KMSRoR(A,HSMs(RealSigServ),HstS(OpPolSrv(OA)),OpPolSrv(OA)).main() @ &m : res ] = 
  Pr[ KMSRoR2(A, OpPolSrv(OA)).main() @ &m : res ] + 
  Pr[ TrustSecInd(Bhop2(A),RealTrustService(OpPolSrv(OA))).main() @ &m : res ] - Pr[ TrustSecInd(Bhop2(A),IdealTrustService(OpPolSrv(OA))).main() @ &m : res] +
  Pr[ IndSig(RealSigServ, Bhop1(A, OA)).main() @ &m : res ] - Pr[ IndSig(IdealSigServ, Bhop1(A, OA)).main() @ &m : res ].
proof.
rewrite (concl1 OA A &m).
have /#:= hop2 &m.
qed.

end section.

end Hop2.