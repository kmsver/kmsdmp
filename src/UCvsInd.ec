require import AllCore Distr DBool FSet List SmtMap.
require AEAD.

clone import AEAD as AE.
type Key = K.
type Hdl.
type Token.
type Cmd.
type Rsp.

module type ICryptoAPI = {
   proc init() : unit
   proc tkNewKey(tk : Token, hdl : Hdl, cmd : Cmd) : Token option
   proc tkManage(tk : Token, cmd : Cmd) : Token option * Rsp option
   proc tkReveal(tk : Token, hdl : Hdl, att : Cmd) : Key option
}.

module type CryptoAPI = {
   include ICryptoAPI [-init]
}.

type Trace.
op new_tr : Trace.
op addNew_tr : Token -> Hdl -> Cmd -> Token option -> Trace -> Trace. 
op addManage_tr : Token -> Cmd -> Token option * Rsp option -> Trace -> Trace. 

op q_new : int.
op q_mng : int.

module CryptoAPIT(API : CryptoAPI) = {
   var t : Trace   
   var count_new : int
   var count_mng : int

   proc init() : unit = {
       t <- new_tr;
       count_new <- 0;
       count_mng <- 0;
   }

   proc tkNewKey(tk : Token, hdl : Hdl, cmd : Cmd) : Token option = {
       var tk';
       tk' <- None;
       if (count_new < q_new) {
          tk' <@ API.tkNewKey(tk,hdl,cmd);
          t <- addNew_tr tk hdl cmd tk' t;
          count_new <- count_new + 1;
       }
       return tk';
   }

   proc tkManage(tk : Token, cmd : Cmd) : Token option * Rsp option = {
       var tkres;
       tkres <- (None,None);
       if (count_mng < q_mng) {
           tkres <@ API.tkManage(tk,cmd);
           t <- addManage_tr tk cmd tkres t;
           count_mng <- count_mng + 1;
       }
       return tkres;
   }

   proc tkReveal(tk : Token, hdl : Hdl, cmd : Cmd) : Key option = {
        var ko;
        ko <@ API.tkReveal(tk,hdl,cmd);
        return ko;
   }
}.

op valid (t : Trace, tk : Token, hdl : Hdl, cmd : Cmd) : bool.
op honest (t : Trace, tk : Token, hdl : Hdl, cmd : Cmd) : bool.

(* IND SECURITY *)

module type IndO_T = {
   include CryptoAPI [-tkReveal]

   proc corruptR(tk : Token, hdl : Hdl, cmd : Cmd) : Key option
   proc insiderR(tk : Token, hdl : Hdl, cmd : Cmd) : Key option
   proc test(tk : Token, hdl : Hdl, cmd : Cmd)     : Key option
}.

module type IndA(O : IndO_T) = {
   proc guess() : bool
}.

op q_corr : int.
op q_insr : int.
op q_test : int.

module IndO(API : CryptoAPI) = {
  var b : bool
  var corrupted : Hdl fset
  var tested : (Hdl,Key) fmap
  var count_corr : int
  var count_insr : int
  var count_test : int

  proc init(b' : bool) = {
    corrupted <- fset0;
    tested <- empty;
    b <- b';
    count_corr <- 0;
    count_insr <- 0;
    count_test <- 0;
  }
   
  include API [-tkReveal]

  proc corruptR(tk : Token, hdl : Hdl, cmd : Cmd) : Key option = {
    var ko : Key option <- None;
    if (count_corr < q_corr) {    
       if ((!(hdl \in tested)) && valid CryptoAPIT.t tk hdl cmd) {
          ko <- API.tkReveal(tk,hdl,cmd);
          if (ko <> None) {
             corrupted <- corrupted `|` fset1 hdl;
          }
       }
       count_corr <- count_corr + 1;
    }
    return ko;
  }

  proc insiderR(tk : Token, hdl : Hdl, cmd : Cmd) : Key option = {
    var ko : Key option <- None;
    if (count_insr < q_insr) {
       if (!(honest CryptoAPIT.t tk hdl cmd)) {
          ko <- API.tkReveal(tk,hdl,cmd);
       }
       count_insr <- count_insr + 1;
    }
    return ko;
  }

  proc test(tk : Token, hdl : Hdl, cmd : Cmd)     : Key option = {
    var ko : Key option <- None;
    var rkey : Key;
    rkey <$ gen;
    if (count_test < q_test) {
       if ((!(hdl \in corrupted)) && valid CryptoAPIT.t tk hdl cmd) {
          ko <- API.tkReveal(tk,hdl,cmd);
          if (ko <> None) {
             if (hdl \in tested) {
                rkey <- oget tested.[hdl];
             }
             else {
                tested.[hdl] <- rkey;
             }
             ko <- if b then Some rkey else ko;
          }
       }
       count_test <- count_test + 1;
    }
    return ko;
  }
}.

module Ind(A : IndA, IAPI : ICryptoAPI) = {
  module API=CryptoAPIT(IAPI)
  module IO = IndO(API)
  module A = A(IO)

  proc main(b : bool) = {
    var b';
    IAPI.init();
    API.init();
    IO.init(b);
    b' <@ A.guess();
    return b' = b;
  }
}.

(* UC Security *)

type Pld = Msg.
type Cph = Cph.
op emptyD : AData = witness.

module type ICryptoService = {
   proc init() : unit
   proc new(hdl : Hdl) : unit
   proc enc(hdl : Hdl, pld : Pld) : Cph option
   proc dec(hdl : Hdl, cph : Cph) : Pld option
}.

module type CryptoService = {
   include ICryptoService [-init]
}.

module type AdvO = {
   include CryptoAPI [-tkReveal]

   proc corruptR(tk : Token, hdl : Hdl, cmd : Cmd) : Key option
   proc badEnc(hdl : Hdl, tk : Token, cmd : Cmd,pld : Pld) : Cph option
   proc badDec(hdl : Hdl, tk : Token, cmd : Cmd, cph : Cph) : Pld option
}.

type St.

module type Adv(O : AdvO) = {
   proc newParam(hdl : Hdl, st : St) : Token * Cmd * St
   proc encParam(hdl : Hdl, st : St) : Token * Cmd * St
   proc decParam(hdl : Hdl, st : St) : Token * Cmd * St
   proc takeCtl(st : St) : St
}.

module AOracles(API : CryptoAPI) = {
  include API
  var corrupted : Hdl fset
  var usedKeys : (Hdl,Key) fmap
  var count_corr : int
  var count_insr : int

  proc init() = {
     usedKeys <- empty;
     corrupted <- fset0;
     count_corr <- 0;
     count_insr <- 0;
  }

  proc corruptR(tk : Token, hdl : Hdl, cmd : Cmd) : Key option = {
    var ko : Key option <- None;
    if (count_corr < q_corr) {
       if ((!(hdl \in usedKeys)) && valid CryptoAPIT.t tk hdl cmd) {
          ko <- API.tkReveal(tk,hdl,cmd);
          if (ko <> None) {
             corrupted <- corrupted `|` fset1 hdl;
          }
       }
       count_corr <- count_corr + 1;
    }
    return ko;
  }

  proc badEnc(hdl : Hdl, tk : Token, cmd : Cmd,pld : Pld) : Cph option = {
     var key,ocph,cph;
     ocph <- None;
     if (count_insr < q_insr) {
        if (! honest CryptoAPIT.t tk hdl cmd) {
           key <@ API.tkReveal(tk,hdl,cmd);
           if (key <> None) {
              cph <$ enc (oget key) emptyD pld;
              ocph <- Some cph;
           }
        }
       count_insr <- count_insr + 1;
     }
     return ocph;
  }
 
  proc badDec(hdl : Hdl, tk : Token, cmd : Cmd, cph : Cph) : Pld option = {
     var key,opld;
     opld <- None;
     if (count_insr < q_insr) {
        if (! honest CryptoAPIT.t tk hdl cmd) {
           key <@ API.tkReveal(tk,hdl,cmd);
           if (key <> None) {
              opld <- dec (oget key) emptyD cph;
           }
        }
       count_insr <- count_insr + 1;
     }
     return opld;
  }    
}.

module type EnvO = {

   proc new(hdl : Hdl, st : St) : St
   proc enc(hdl : Hdl, pld : Pld, st : St) : Cph option * St
   proc dec(hdl : Hdl, cph : Cph, st : St) : Pld option * St

   proc takeCtl(st : St) : St

}.

module type Env(O : EnvO) = {
     proc guess() : bool
}.

op q_znew : int.

module RealCryptoService(API : CryptoAPI, A : Adv) = {
  var table : ((Hdl * Cph),Pld) fmap
  var count_znew : int
  var count_test : int

  module A = A(AOracles(API))

  include A [takeCtl]

  proc init() = {
      table <- empty;
      count_znew <- 0;
      count_test <- 0;
  }

  proc new(hdl : Hdl,st : St) : St = {
     var tk,cmd;
     if (count_znew < q_znew) {
        (tk,cmd,st) <@ A.newParam(hdl,st);
        count_znew <- count_znew + 1;
     }
     return st;
  }

  proc enc(hdl : Hdl, pld : Pld, st : St) : Cph option * St =  {
     var tk,cmd,key,ocph,cph;
     ocph <- None;
     if (count_test < q_test) {
        (tk,cmd,st) <@ A.encParam(hdl,st);
        if ((valid CryptoAPIT.t tk hdl cmd) 
                 && !(hdl \in AOracles.corrupted)) {
           key <@ API.tkReveal(tk,hdl,cmd);
           if (key <> None) {
              cph <$ enc (oget key) emptyD pld;
              AOracles.usedKeys.[hdl] <- oget key;
              table.[(hdl,cph)]<-pld;
              ocph <- Some cph;
           }
        }
        count_test <- count_test + 1;
     }
     return (ocph,st);
  }

  proc dec(hdl : Hdl, cph : Cph, st : St) : Pld option * St =  { 
     var tk,cmd,key,opld;
     opld <- None;
     if (count_test < q_test) {
        (tk,cmd,st) <@ A.decParam(hdl,st);
        if ((valid CryptoAPIT.t tk hdl cmd) 
              && !(hdl \in AOracles.corrupted)) {
           key <@ API.tkReveal(tk,hdl,cmd);
           if (key <> None) {
              opld <- dec (oget key) emptyD cph;
              AOracles.usedKeys.[hdl] <- oget key;
           }
        }
        count_test <- count_test + 1;
     }
     return (opld,st);
  }
  
}.

module RealGame(Z : Env, A : Adv, IAPI : ICryptoAPI)  = {
  module API = CryptoAPIT(IAPI)
  module RS = RealCryptoService(API,A) 

  proc main() = {
     var b;
     IAPI.init();
     API.init();
     AOracles(IAPI).init();
     RS.init();
     b <@ Z(RS).guess();
     return b;
  }   
  
}.

module IdealCryptoService(API : CryptoAPI, A : Adv) = {
  module RS = RealCryptoService(API,A) 
  include RS [-enc,dec]

  proc enc(hdl : Hdl, pld : Pld, st : St) : Cph option * St =  {
     var tk,cmd,key,ocph,cph;
     ocph <- None;
     if (RS.count_test < q_test) {
        (tk,cmd,st) <@ RS.A.encParam(hdl,st);
        if ((valid CryptoAPIT.t tk hdl cmd) 
              && !(hdl \in AOracles.corrupted)) {
           key <@ API.tkReveal(tk,hdl,cmd);
           if (key <> None) {
              cph <$ enc (oget key) emptyD witness;
              AOracles.usedKeys.[hdl] <- oget key;
              RS.table.[(hdl,cph)]<-pld;
              ocph <- Some cph;
           }
        }
        RS.count_test <- RS.count_test + 1;
     }
     return (ocph,st);
  }

  proc dec(hdl : Hdl, cph : Cph, st : St) : Pld option * St =  { 
     var tk,cmd,opld,key;
     opld <- None;
     if (RS.count_test < q_test) {
        (tk,cmd,st) <@ RS.A.decParam(hdl,st);
        if ((valid CryptoAPIT.t tk hdl cmd) 
              && !(hdl \in AOracles.corrupted)) {
           key <@ API.tkReveal(tk,hdl,cmd); (* not used *)
           if (key <> None) {
              AOracles.usedKeys.[hdl] <- oget key;
              opld <- RS.table.[(hdl,cph)];
           }
        }
        RS.count_test <- RS.count_test + 1;
     }
     return (opld,st);
  }
  
}.

module IdealGame(Z : Env, A : Adv, IAPI : ICryptoAPI)  = {
  module API = CryptoAPIT(IAPI)
  module IS = IdealCryptoService(API,A) 

  proc main() = {
     var b;
     IAPI.init();
     API.init();
     AOracles(IAPI).init();
     IS.init();
     b <@ Z(IS).guess();
     return b;
  }   
  
}.

(**************************************************)
(* Proof that Corr + Ind + AEAD => UC             *)
(**************************************************)


(** Hop 0: Add bad flags     *)

module Hop0CryptoService(API : CryptoAPI, A : Adv) = {
  module RS = RealCryptoService(API,A) 
  include RS [-init,enc,dec]
  var bad : bool

  proc init() = {
     RS.init();
     bad <- false;
  }

  proc enc(hdl : Hdl, pld : Pld, st : St) : Cph option * St =  {
     var tk,cmd,key,ocph,cph;
     ocph <- None;
     if (RS.count_test < q_test) {
        (tk,cmd,st) <@ RS.A.encParam(hdl,st);
        if ((valid CryptoAPIT.t tk hdl cmd) 
           && !(hdl \in AOracles.corrupted)) {
           key <@ API.tkReveal(tk,hdl,cmd);
           if (key <> None) {
              cph <$ enc (oget key) emptyD pld;
              AOracles.usedKeys.[hdl] <- oget key;
              RealCryptoService.table.[(hdl,cph)]<-pld;
              ocph <- Some cph;
           }
        }
        RS.count_test <- RS.count_test + 1;
     }
     return (ocph,st);
  }

  proc dec(hdl : Hdl, cph : Cph, st : St) : Pld option * St =  { 
     var tk,cmd,key,opld;
     opld <- None;
     if (RS.count_test < q_test) {
        (tk,cmd,st) <@ RS.A.decParam(hdl,st);
        if ((valid CryptoAPIT.t tk hdl cmd) 
              && !(hdl \in AOracles.corrupted)) {
           key <@ API.tkReveal(tk,hdl,cmd);
           if (key <> None) {
              opld <- dec (oget key) emptyD cph;
              AOracles.usedKeys.[hdl] <- oget key;
              bad <- bad || 
                      ((hdl,cph) \in RealCryptoService.table /\
                      !(opld = RealCryptoService.table.[(hdl,cph)]));
           }
        }
        RS.count_test <- RS.count_test + 1;
     }
     return (opld,st);
  }
  
}.

module Game0(Z : Env, A : Adv, IAPI : ICryptoAPI)  = {
  module API = CryptoAPIT(IAPI)
  module HS0 = Hop0CryptoService(API,A) 

  proc main() = {
     var b;
     IAPI.init();
     API.init();
     AOracles(IAPI).init();
     HS0.init();
     b <@ Z(HS0).guess();
     return b;
  }   
  
}.

section.

declare module A : Adv {CryptoAPIT,Hop0CryptoService}.
declare module Z : Env {A, CryptoAPIT,Hop0CryptoService}.
declare module IAPI : ICryptoAPI {Z, A,CryptoAPIT, Hop0CryptoService}.

lemma shifttohop0_pr &m :
    Pr [ RealGame(Z,A,IAPI).main() @ &m : res ]
      =  Pr[ Game0(Z,A,IAPI).main() @ &m : res ].
proof.
byequiv.
proc; inline *.
call (_: ={glob A, glob IAPI, glob CryptoAPIT, glob AOracles, glob RealCryptoService}).
proc;if => //=;wp;call(_: ={glob IAPI,  glob CryptoAPIT, glob AOracles, glob RealCryptoService}).
by sim.
by sim.
by sim.
by sim.
by sim.
by auto => />.
by sim.
by sim.
by sim.
by wp;call(_:true);skip;progress => /#.
by auto => />.
by auto => />.
qed.

end section.

(** Hop 2: ind => encrypt with a random key     **)

module Hop2CryptoService(API : CryptoAPI, A : Adv) = {
  module HS0 = Hop0CryptoService(API,A) 
  include HS0 [-enc,dec]

  proc enc(hdl : Hdl, pld : Pld, st : St) : Cph option * St =  {
     var tk,cmd,key,rkey,ocph,cph;
     ocph <- None;
     if (HS0.RS.count_test < q_test) {
        (tk,cmd,st) <@ HS0.RS.A.encParam(hdl,st);
        if ((valid CryptoAPIT.t tk hdl cmd) 
              && !(hdl \in AOracles.corrupted)) {
           key <@ API.tkReveal(tk,hdl,cmd);
           if (key <> None) {
              rkey <$ gen;
              key <- if (hdl \in AOracles.usedKeys) 
                     then AOracles.usedKeys.[hdl]
                     else Some rkey;
              cph <$ enc (oget key) emptyD pld;
              AOracles.usedKeys.[hdl] <- oget key;
              RealCryptoService.table.[(hdl,cph)]<-pld;
              ocph <- Some cph;
           }
        }
        HS0.RS.count_test <- HS0.RS.count_test + 1;
     }
     return (ocph,st);
  }

  proc dec(hdl : Hdl, cph : Cph, st : St) : Pld option * St =  { 
     var tk,cmd,key,rkey,opld;
     opld <- None;
     if (HS0.RS.count_test < q_test) {
        (tk,cmd,st) <@ HS0.RS.A.decParam(hdl,st);
        if ((valid CryptoAPIT.t tk hdl cmd) 
              && !(hdl \in AOracles.corrupted)) {
           key <@ API.tkReveal(tk,hdl,cmd);
           if (key <> None) {
              rkey <$ gen;
              key <- if (hdl \in AOracles.usedKeys) 
                     then AOracles.usedKeys.[hdl]
                     else Some rkey;
              opld <- dec (oget key) emptyD cph;
              Hop0CryptoService.bad <- 
                    Hop0CryptoService.bad || 
                       ((hdl,cph) \in RealCryptoService.table) /\
                       !(opld = RealCryptoService.table.[(hdl,cph)]);
              AOracles.usedKeys.[hdl] <- oget key;
           }
        }
        HS0.RS.count_test <- HS0.RS.count_test + 1;
     }
     return (opld,st);
  }

   
}.

module Game2(Z : Env, A : Adv, IAPI : ICryptoAPI)  = {
  module API = CryptoAPIT(IAPI)
  module HS2 = Hop2CryptoService(API,A) 

  proc main() = {
     var b;
     IAPI.init();
     API.init();
     AOracles(IAPI).init();
     HS2.init();
     b <@ Z(HS2).guess();
     return b;
  }   
  
}.

(* Ind adversary *)

module B_AOracles(API : IndO_T) = {
   include API [-corruptR,tkNewKey,tkManage]
   var corrupted : Hdl fset
   var usedKeys : Hdl fset
   var t : Trace
   var count_mng : int
   var count_new : int
   var count_corr : int
   var count_insr : int

   proc tkNewKey(tk : Token, hdl : Hdl, cmd : Cmd) : Token option = {
       var tk';
       tk' <- None;
       if (count_new < q_new) {
          tk' <@ API.tkNewKey(tk,hdl,cmd);
          t <- addNew_tr tk hdl cmd tk' t;
          count_new <- count_new + 1;
       }
       return tk';
   }

   proc tkManage(tk : Token, cmd : Cmd) : Token option * Rsp option = {
       var tkres;
       tkres <- (None,None);
       if (count_mng < q_mng) {
           tkres <@ API.tkManage(tk,cmd);
           t <- addManage_tr tk cmd tkres t;
           count_mng <- count_mng + 1;
       }
       return tkres;
   }


  proc corruptR(tk : Token, hdl : Hdl, cmd : Cmd) : Key option = {
    var ko : Key option <- None;
    if (count_corr < q_corr) {
       if ((valid t tk hdl cmd)&&(!(hdl \in usedKeys))) {
          ko <- API.corruptR(tk,hdl,cmd);
          if (ko <> None) {
             corrupted <- corrupted `|` fset1 hdl;
          }
       }
       count_corr <- count_corr + 1;
    }
    return ko;
  }

  proc badEnc(hdl : Hdl, tk : Token, cmd : Cmd,pld : Pld) : Cph option = {
     var key,ocph,cph;
     ocph <- None;
     if (count_insr < q_insr) {
        if (! honest t tk hdl cmd) {
           key <@ API.insiderR(tk,hdl,cmd);
           if (key <> None) {
              cph <$ enc (oget key) emptyD pld;
              ocph <- Some cph;
           }
        }
        count_insr <- count_insr + 1;
     }
     return ocph;
  }
 
  proc badDec(hdl : Hdl, tk : Token, cmd : Cmd, cph : Cph) : Pld option = {
     var key,opld;
     opld <- None;
     if (count_insr < q_insr) {
        if (! honest t tk hdl cmd) {
           key <@ API.insiderR(tk,hdl,cmd);
           if (key <> None) {
              opld <- dec (oget key) emptyD cph;
           }
        }
        count_insr <- count_insr + 1;
     }
     return opld;
  }    
}.

module B_ZOracles(API : IndO_T, A : Adv) = {
  var table : ((Hdl * Cph),Pld) fmap
  var count_znew : int
  var count_test : int

  module A = A(B_AOracles(API))

  include A [takeCtl]

  proc init() = {
      B_AOracles.t <- new_tr;
      B_AOracles.count_new <- 0;
      B_AOracles.count_mng <- 0;
      B_AOracles.count_corr <- 0;
      B_AOracles.count_insr <- 0;
      B_AOracles.corrupted <- fset0;
      B_AOracles.usedKeys <- fset0;
      table <- empty;
      count_znew <- 0;
      count_test <- 0;
  }

  proc new(hdl : Hdl,st : St) : St = {
     var tk,cmd;
     if (count_znew < q_znew) {
        (tk,cmd,st) <@ A.newParam(hdl,st);
        count_znew <- count_znew + 1;
     }
     return st;
  }

  proc enc(hdl : Hdl, pld : Pld, st : St) : Cph option * St =  {
     var tk,cmd,key,ocph,cph;
     ocph <- None;
     if (count_test < q_test) {
        (tk,cmd,st) <@ A.encParam(hdl,st);
        if ((valid B_AOracles.t tk hdl cmd)
               && !(hdl \in B_AOracles.corrupted)) {
           key <@ API.test(tk,hdl,cmd);
           if (key <> None) {
               cph <$ enc (oget key) emptyD pld;
               B_AOracles.usedKeys <-  B_AOracles.usedKeys `|` fset1 hdl;
               table.[(hdl,cph)]<-pld;
               ocph <- Some cph;
           }
        }
        count_test <- count_test + 1;
     }
     return (ocph,st);
  }

  proc dec(hdl : Hdl, cph : Cph, st : St) : Pld option * St =  { 
     var tk,cmd,key,opld;
     opld <- None;
     if (count_test < q_test) {
        (tk,cmd,st) <@ A.decParam(hdl,st);
        if ((valid B_AOracles.t tk hdl cmd)
               && !(hdl \in B_AOracles.corrupted)) {
           key <@ API.test(tk,hdl,cmd);
           if (key <> None) {
              B_AOracles.usedKeys <-  B_AOracles.usedKeys `|` fset1 hdl;
              opld <- dec (oget key) emptyD cph;
           }
        }
        count_test <- count_test + 1;
     }
     return (opld,st);
  }
  
}.

module B(Z : Env, A : Adv,O : IndO_T)  = {
   module ZO = B_ZOracles(O,A) 

   proc guess() = {
      var b;
      ZO.init();
      b <@ Z(ZO).guess();
      return b;
   }
}.

section.

declare module A : Adv {CryptoAPIT,Hop0CryptoService, IndO, B_ZOracles }.
declare module Z : Env {CryptoAPIT,Hop0CryptoService, A, IndO, B_ZOracles}.
declare module IAPI : ICryptoAPI {CryptoAPIT,Hop0CryptoService, Z, A, IndO, B_ZOracles}.


lemma leftHop &m : 
   Pr [Ind(B(Z,A),IAPI).main(false) @ &m : !res] =
     Pr [ Game0(Z,A,IAPI).main() @ &m : res].
proof.
byequiv.
proc; inline *; wp.
call(_: !IndO.b{1} /\  ={glob A, glob IAPI, glob CryptoAPIT} /\ 
            B_AOracles.corrupted{1} =  AOracles.corrupted{2} /\ 
            IndO.corrupted{1} = AOracles.corrupted{2} /\ 
            fdom IndO.tested{1} = fdom AOracles.usedKeys{2} /\ 
            B_AOracles.usedKeys{1} = fdom AOracles.usedKeys{2} /\  
            B_ZOracles.table{1} = RealCryptoService.table{2} /\
            B_AOracles.count_new{1} = CryptoAPIT.count_new{2} /\
            B_AOracles.count_mng{1} = CryptoAPIT.count_mng{2} /\
            B_AOracles.count_corr{1} = AOracles.count_corr{2} /\
            IndO.count_corr{1} <= AOracles.count_corr{2} /\
            B_AOracles.count_insr{1} = AOracles.count_insr{2} /\
            IndO.count_insr{1} <= AOracles.count_insr{2} /\
            B_ZOracles.count_znew{1} = RealCryptoService.count_znew{2} /\
            B_ZOracles.count_test{1} = RealCryptoService.count_test{2} /\
            IndO.count_test{1} <= RealCryptoService.count_test{2} /\
            B_AOracles.t{1} = CryptoAPIT.t{2});last first.
+ wp;call(_:true);skip;progress;smt(@SmtMap).
+ proc.
  if => //=.  
  wp.
  call(_: ={glob IAPI, glob CryptoAPIT} /\ 
            B_AOracles.corrupted{1} =  AOracles.corrupted{2} /\ 
            IndO.corrupted{1} = AOracles.corrupted{2} /\ 
            fdom IndO.tested{1} = fdom AOracles.usedKeys{2} /\ 
            B_AOracles.usedKeys{1} = fdom AOracles.usedKeys{2} /\  
            B_ZOracles.table{1} = RealCryptoService.table{2} /\
            B_AOracles.count_new{1} = CryptoAPIT.count_new{2} /\
            B_AOracles.count_mng{1} = CryptoAPIT.count_mng{2} /\
            B_AOracles.count_corr{1} = AOracles.count_corr{2} /\
            IndO.count_corr{1} <= AOracles.count_corr{2} /\
            B_AOracles.count_insr{1} = AOracles.count_insr{2} /\
            IndO.count_insr{1} <= AOracles.count_insr{2} /\
            B_ZOracles.count_znew{1} = RealCryptoService.count_znew{2} /\
            B_ZOracles.count_test{1} = RealCryptoService.count_test{2} /\
            IndO.count_test{1} <= RealCryptoService.count_test{2} /\
            B_AOracles.t{1} = CryptoAPIT.t{2}).
  proc; inline *.
  sp;if => //=.
  rcondt {1} 5; first by auto => />.
  by wp;call(_:true);auto => />.
  proc; inline *.
  sp;if => //=.
  rcondt {1} 4; first by auto => />.
  by wp;call(_:true);auto => />.
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. smt(@SmtMap).
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt(@SmtMap).
  by wp;call(_:true);wp;skip;progress;smt().
  by wp;skip;progress;smt().
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. 
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt().
  sp.
  seq 1 1 : (#pre /\ ko0{1} = ko{2}); first by call(_:true);skip;auto => />.
  sp;if => //=.
  by wp;rnd;wp;skip;progress;smt().
  by wp;skip;progress;smt().
  by wp;skip;progress;smt().
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. 
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt().
  sp.
  seq 1 1 : (#pre /\ ko0{1} = ko{2}); first by call(_:true);skip;auto => />.
  by wp;skip;progress;smt().
  by wp;skip;progress;smt().
  by wp;skip;progress;smt().
+ proc; inline *.
  seq 1 1 : (#pre /\ ={ocph}); first by auto => />.
  if => //=.
  seq 1 1 : (#pre /\ ={tk,cmd,st}).
  call(_: ={glob IAPI, glob CryptoAPIT} /\ 
            B_AOracles.corrupted{1} =  AOracles.corrupted{2} /\ 
            IndO.corrupted{1} = AOracles.corrupted{2} /\ 
            fdom IndO.tested{1} = fdom AOracles.usedKeys{2} /\ 
            B_AOracles.usedKeys{1} = fdom AOracles.usedKeys{2} /\  
            B_ZOracles.table{1} = RealCryptoService.table{2} /\
            B_AOracles.count_new{1} = CryptoAPIT.count_new{2} /\
            B_AOracles.count_mng{1} = CryptoAPIT.count_mng{2} /\
            B_AOracles.count_corr{1} = AOracles.count_corr{2} /\
            IndO.count_corr{1} <= AOracles.count_corr{2} /\
            B_AOracles.count_insr{1} = AOracles.count_insr{2} /\
            IndO.count_insr{1} <= AOracles.count_insr{2} /\
            B_ZOracles.count_znew{1} = RealCryptoService.count_znew{2} /\
            B_ZOracles.count_test{1} = RealCryptoService.count_test{2} /\
            IndO.count_test{1} <= RealCryptoService.count_test{2} /\
            B_AOracles.t{1} = CryptoAPIT.t{2}).
  proc; inline *.
  sp;if => //=.
  rcondt {1} 5; first by auto => />.
  by wp;call(_:true);auto => />.
  proc; inline *.
  sp;if => //=.
  rcondt {1} 4; first by auto => />.
  by wp;call(_:true);auto => />.
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. smt(@SmtMap).
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt(@SmtMap).
  by wp;call(_:true);wp;skip;progress;smt().
  by wp;skip;progress;smt().
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. 
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt().
  sp.
  seq 1 1 : (#pre /\ ko0{1} = ko{2}); first by call(_:true);skip;auto => />.
  case (ko0{1} = None).
  rcondf {1} 4; first by move => *;wp;skip;progress => /#.
  rcondf {2} 2; first by move => *;wp;skip;progress => /#.
  by wp;skip;progress;smt().
  rcondt {1} 4; first by move => *;wp;skip;progress => /#.
  rcondt {2} 2; first by move => *;wp;skip;progress => /#.
  wp;rnd;wp;skip;progress;smt().
  wp;skip;progress;smt().
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. 
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt().
  sp.
  seq 1 1 : (#pre /\ ko0{1} = ko{2}); first by call(_:true);skip;auto => />.
  case (ko0{1} = None).
  rcondf {1} 4; first by move => *;wp;skip;progress => /#.
  rcondf {2} 2; first by move => *;wp;skip;progress => /#.
  by wp;skip;progress;smt().
  rcondt {1} 4; first by move => *;wp;skip;progress => /#.
  rcondt {2} 2; first by move => *;wp;skip;progress => /#.
  wp;skip;progress;smt().
  wp;skip;progress;smt().
  wp;skip;progress;smt().
  sp; if => //=.
  rcondt {1} 6.
  move => *;wp;rnd;wp;skip;progress;smt().
  rcondt {1} 6; first by auto => />.
  sp.
  seq 1 0 : (#pre). by rnd{1}; skip; progress; apply gen_ll.
  sp.
  seq 1 1 : (#pre /\ ko0{1} = ko{2}); first by call(_:true);skip;auto => />.
  case (ko0{1} = None).
  rcondf {1} 5; first by move => *;wp;skip;progress => /#.
  rcondf {1} 2; first by move => *;wp;skip;progress => /#.
  rcondf {2} 2; first by move => *;wp;skip;progress => /#.
  by wp;skip;progress;smt().
  rcondt {1} 2; first by move => *;wp;skip;progress => /#.
  rcondt {2} 2; first by move => *;wp;skip;progress => /#.
  rcondt {1} 6; first by move => *;wp;skip;progress => /#.
  wp;rnd;wp;skip;progress;smt(@SmtMap @FSet).
  wp;skip;progress;smt().
+ proc; inline *.
  seq 1 1 : (#pre /\ ={opld}); first by auto => />.
  if => //=.
  seq 1 1 : (#pre /\ ={tk,cmd,st}).
  call(_: ={glob IAPI, glob CryptoAPIT} /\ 
            B_AOracles.corrupted{1} =  AOracles.corrupted{2} /\ 
            IndO.corrupted{1} = AOracles.corrupted{2} /\ 
            fdom IndO.tested{1} = fdom AOracles.usedKeys{2} /\ 
            B_AOracles.usedKeys{1} = fdom AOracles.usedKeys{2} /\  
            B_ZOracles.table{1} = RealCryptoService.table{2} /\
            B_AOracles.count_new{1} = CryptoAPIT.count_new{2} /\
            B_AOracles.count_mng{1} = CryptoAPIT.count_mng{2} /\
            B_AOracles.count_corr{1} = AOracles.count_corr{2} /\
            IndO.count_corr{1} <= AOracles.count_corr{2} /\
            B_AOracles.count_insr{1} = AOracles.count_insr{2} /\
            IndO.count_insr{1} <= AOracles.count_insr{2} /\
            B_ZOracles.count_znew{1} = RealCryptoService.count_znew{2} /\
            B_ZOracles.count_test{1} = RealCryptoService.count_test{2} /\
            IndO.count_test{1} <= RealCryptoService.count_test{2} /\
            B_AOracles.t{1} = CryptoAPIT.t{2}).
  proc; inline *.
  sp;if => //=.
  rcondt {1} 5; first by auto => />.
  by wp;call(_:true);auto => />.
  proc; inline *.
  sp;if => //=.
  rcondt {1} 4; first by auto => />.
  by wp;call(_:true);auto => />.
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. smt(@SmtMap).
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt(@SmtMap).
  by wp;call(_:true);wp;skip;progress;smt().
  by wp;skip;progress;smt().
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. 
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt().
  sp.
  seq 1 1 : (#pre /\ ko0{1} = ko{2}); first by call(_:true);skip;auto => />.
  case (ko0{1} = None).
  rcondf {1} 4; first by move => *;wp;skip;progress => /#.
  rcondf {2} 2; first by move => *;wp;skip;progress => /#.
  by wp;skip;progress;smt().
  rcondt {1} 4; first by move => *;wp;skip;progress => /#.
  rcondt {2} 2; first by move => *;wp;skip;progress => /#.
  wp;rnd;wp;skip;progress;smt().
  wp;skip;progress;smt().
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. 
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt().
  sp.
  seq 1 1 : (#pre /\ ko0{1} = ko{2}); first by call(_:true);skip;auto => />.
  case (ko0{1} = None).
  rcondf {1} 4; first by move => *;wp;skip;progress => /#.
  rcondf {2} 2; first by move => *;wp;skip;progress => /#.
  by wp;skip;progress;smt().
  rcondt {1} 4; first by move => *;wp;skip;progress => /#.
  rcondt {2} 2; first by move => *;wp;skip;progress => /#.
  wp;skip;progress;smt().
  wp;skip;progress;smt().
  wp;skip;progress;smt().
  sp; if => //=.
  rcondt {1} 6.
  move => *;wp;rnd;wp;skip;progress;smt().
  rcondt {1} 6; first by auto => />.
  sp.
  seq 1 0 : (#pre). by rnd{1}; skip; progress; apply gen_ll.
  sp.
  seq 1 1 : (#pre /\ ko0{1} = ko{2}); first by call(_:true);skip;auto => />.
  case (ko0{1} = None).
  rcondf {1} 5; first by move => *;wp;skip;progress => /#.
  rcondf {1} 2; first by move => *;wp;skip;progress => /#.
  rcondf {2} 2; first by move => *;wp;skip;progress => /#.
  by wp;skip;progress;smt().
  rcondt {1} 2; first by move => *;wp;skip;progress => /#.
  rcondt {2} 2; first by move => *;wp;skip;progress => /#.
  rcondt {1} 6; first by move => *;wp;skip;progress => /#.
  wp;skip;progress;smt(@SmtMap @FSet).
  wp;skip;progress;smt().
+ proc*.
  call(_: ={glob IAPI, glob CryptoAPIT} /\ 
            B_AOracles.corrupted{1} =  AOracles.corrupted{2} /\ 
            IndO.corrupted{1} = AOracles.corrupted{2} /\ 
            fdom IndO.tested{1} = fdom AOracles.usedKeys{2} /\ 
            B_AOracles.usedKeys{1} = fdom AOracles.usedKeys{2} /\  
            B_ZOracles.table{1} = RealCryptoService.table{2} /\
            B_AOracles.count_new{1} = CryptoAPIT.count_new{2} /\
            B_AOracles.count_mng{1} = CryptoAPIT.count_mng{2} /\
            B_AOracles.count_corr{1} = AOracles.count_corr{2} /\
            IndO.count_corr{1} <= AOracles.count_corr{2} /\
            B_AOracles.count_insr{1} = AOracles.count_insr{2} /\
            IndO.count_insr{1} <= AOracles.count_insr{2} /\
            B_ZOracles.count_znew{1} = RealCryptoService.count_znew{2} /\
            B_ZOracles.count_test{1} = RealCryptoService.count_test{2} /\
            IndO.count_test{1} <= RealCryptoService.count_test{2} /\
            B_AOracles.t{1} = CryptoAPIT.t{2}).
  proc; inline *.
  sp;if => //=.
  rcondt {1} 5; first by auto => />.
  by wp;call(_:true);auto => />.
  proc; inline *.
  sp;if => //=.
  rcondt {1} 4; first by auto => />.
  by wp;call(_:true);auto => />.
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. smt(@SmtMap).
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt(@SmtMap).
  by wp;call(_:true);wp;skip;progress;smt().
  by wp;skip;progress;smt().
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. 
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt().
  sp.
  seq 1 1 : (#pre /\ ko0{1} = ko{2}); first by call(_:true);skip;auto => />.
  case (ko0{1} = None).
  rcondf {1} 4; first by move => *;wp;skip;progress => /#.
  rcondf {2} 2; first by move => *;wp;skip;progress => /#.
  by wp;skip;progress;smt().
  rcondt {1} 4; first by move => *;wp;skip;progress => /#.
  rcondt {2} 2; first by move => *;wp;skip;progress => /#.
  wp;rnd;wp;skip;progress;smt().
  wp;skip;progress;smt().
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. 
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt().
  sp.
  seq 1 1 : (#pre /\ ko0{1} = ko{2}); first by call(_:true);skip;auto => />.
  case (ko0{1} = None).
  rcondf {1} 4; first by move => *;wp;skip;progress => /#.
  rcondf {2} 2; first by move => *;wp;skip;progress => /#.
  by wp;skip;progress;smt().
  rcondt {1} 4; first by move => *;wp;skip;progress => /#.
  rcondt {2} 2; first by move => *;wp;skip;progress => /#.
  wp;skip;progress;smt().
  wp;skip;progress;smt().
  wp;skip;progress;smt().
  by auto => />.
  by auto => />.
qed.

lemma rightHop &m : 
   Pr [Ind(B(Z,A),IAPI).main(true) @ &m : res] =
     Pr [ Game2(Z,A,IAPI).main() @ &m : res].
proof.
byequiv.
proc; inline *; wp.
call(_: IndO.b{1} /\  ={glob A, glob IAPI, glob CryptoAPIT} /\ 
            B_AOracles.corrupted{1} =  AOracles.corrupted{2} /\ 
            IndO.corrupted{1} = AOracles.corrupted{2} /\ 
            IndO.tested{1} = AOracles.usedKeys{2} /\ 
            B_AOracles.usedKeys{1} = fdom AOracles.usedKeys{2} /\  
            B_ZOracles.table{1} = RealCryptoService.table{2} /\
            B_AOracles.count_new{1} = CryptoAPIT.count_new{2} /\
            B_AOracles.count_mng{1} = CryptoAPIT.count_mng{2} /\
            B_AOracles.count_corr{1} = AOracles.count_corr{2} /\
            IndO.count_corr{1} <= AOracles.count_corr{2} /\
            B_AOracles.count_insr{1} = AOracles.count_insr{2} /\
            IndO.count_insr{1} <= AOracles.count_insr{2} /\
            B_ZOracles.count_znew{1} = RealCryptoService.count_znew{2} /\
            B_ZOracles.count_test{1} = RealCryptoService.count_test{2} /\
            IndO.count_test{1} <= RealCryptoService.count_test{2} /\
            B_AOracles.t{1} = CryptoAPIT.t{2});last first.
+ wp;call(_:true);skip;progress;smt(@SmtMap).
+ proc.
  if => //=.  
  wp.
  call(_: ={glob IAPI, glob CryptoAPIT} /\ 
            B_AOracles.corrupted{1} =  AOracles.corrupted{2} /\ 
            IndO.corrupted{1} = AOracles.corrupted{2} /\ 
            IndO.tested{1} = AOracles.usedKeys{2} /\ 
            B_AOracles.usedKeys{1} = fdom AOracles.usedKeys{2} /\  
            B_ZOracles.table{1} = RealCryptoService.table{2} /\
            B_AOracles.count_new{1} = CryptoAPIT.count_new{2} /\
            B_AOracles.count_mng{1} = CryptoAPIT.count_mng{2} /\
            B_AOracles.count_corr{1} = AOracles.count_corr{2} /\
            IndO.count_corr{1} <= AOracles.count_corr{2} /\
            B_AOracles.count_insr{1} = AOracles.count_insr{2} /\
            IndO.count_insr{1} <= AOracles.count_insr{2} /\
            B_ZOracles.count_znew{1} = RealCryptoService.count_znew{2} /\
            B_ZOracles.count_test{1} = RealCryptoService.count_test{2} /\
            IndO.count_test{1} <= RealCryptoService.count_test{2} /\
            B_AOracles.t{1} = CryptoAPIT.t{2}).
  proc; inline *.
  sp;if => //=.
  rcondt {1} 5; first by auto => />.
  by wp;call(_:true);auto => />.
  proc; inline *.
  sp;if => //=.
  rcondt {1} 4; first by auto => />.
  by wp;call(_:true);auto => />.
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. smt(@SmtMap).
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt(@SmtMap).
  by wp;call(_:true);wp;skip;progress;smt().
  by wp;skip;progress;smt().
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. 
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt().
  sp.
  seq 1 1 : (#pre /\ ko0{1} = ko{2}); first by call(_:true);skip;auto => />.
  sp;if => //=.
  by wp;rnd;wp;skip;progress;smt().
  by wp;skip;progress;smt().
  by wp;skip;progress;smt().
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. 
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt().
  sp.
  seq 1 1 : (#pre /\ ko0{1} = ko{2}); first by call(_:true);skip;auto => />.
  by wp;skip;progress;smt().
  by wp;skip;progress;smt().
  by wp;skip;progress;smt().
+ proc; inline *.
  seq 1 1 : (#pre /\ ={ocph}); first by auto => />.
  if => //=.
  seq 1 1 : (#pre /\ ={tk,cmd,st}).
  call(_: ={glob IAPI, glob CryptoAPIT} /\ 
            B_AOracles.corrupted{1} =  AOracles.corrupted{2} /\ 
            IndO.corrupted{1} = AOracles.corrupted{2} /\ 
            IndO.tested{1} = AOracles.usedKeys{2} /\ 
            B_AOracles.usedKeys{1} = fdom AOracles.usedKeys{2} /\  
            B_ZOracles.table{1} = RealCryptoService.table{2} /\
            B_AOracles.count_new{1} = CryptoAPIT.count_new{2} /\
            B_AOracles.count_mng{1} = CryptoAPIT.count_mng{2} /\
            B_AOracles.count_corr{1} = AOracles.count_corr{2} /\
            IndO.count_corr{1} <= AOracles.count_corr{2} /\
            B_AOracles.count_insr{1} = AOracles.count_insr{2} /\
            IndO.count_insr{1} <= AOracles.count_insr{2} /\
            B_ZOracles.count_znew{1} = RealCryptoService.count_znew{2} /\
            B_ZOracles.count_test{1} = RealCryptoService.count_test{2} /\
            IndO.count_test{1} <= RealCryptoService.count_test{2} /\
            B_AOracles.t{1} = CryptoAPIT.t{2}).
  proc; inline *.
  sp;if => //=.
  rcondt {1} 5; first by auto => />.
  by wp;call(_:true);auto => />.
  proc; inline *.
  sp;if => //=.
  rcondt {1} 4; first by auto => />.
  by wp;call(_:true);auto => />.
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. smt(@SmtMap).
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt(@SmtMap).
  by wp;call(_:true);wp;skip;progress;smt().
  by wp;skip;progress;smt().
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. 
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt().
  sp.
  seq 1 1 : (#pre /\ ko0{1} = ko{2}); first by call(_:true);skip;auto => />.
  case (ko0{1} = None).
  rcondf {1} 4; first by move => *;wp;skip;progress => /#.
  rcondf {2} 2; first by move => *;wp;skip;progress => /#.
  by wp;skip;progress;smt().
  rcondt {1} 4; first by move => *;wp;skip;progress => /#.
  rcondt {2} 2; first by move => *;wp;skip;progress => /#.
  wp;rnd;wp;skip;progress;smt().
  wp;skip;progress;smt().
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. 
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt().
  sp.
  seq 1 1 : (#pre /\ ko0{1} = ko{2}); first by call(_:true);skip;auto => />.
  case (ko0{1} = None).
  rcondf {1} 4; first by move => *;wp;skip;progress => /#.
  rcondf {2} 2; first by move => *;wp;skip;progress => /#.
  by wp;skip;progress;smt().
  rcondt {1} 4; first by move => *;wp;skip;progress => /#.
  rcondt {2} 2; first by move => *;wp;skip;progress => /#.
  wp;skip;progress;smt().
  wp;skip;progress;smt().
  wp;skip;progress;smt().
  sp; if => //=.
  rcondt {1} 6.
  move => *;wp;rnd;wp;skip;progress;smt().
  rcondt {1} 6; first by auto => />.
  swap {1} 5 4; sp.
  seq 1 1 : (#pre /\ ko0{1} = ko{2}); first by call(_:true);skip;auto => />.
  case (ko0{1} = None).
  rcondf {1} 3; first by move => *;wp;rnd;skip;progress => /#.
  rcondf {1} 5; first by move => *;wp;rnd;skip;progress => /#.
  rcondf {2} 2; first by move => *;wp;skip;progress => /#.
  wp;rnd{1};skip;progress; first by apply gen_ll.
  smt().
  rcondt {1} 3; first by move => *;wp;rnd;skip;progress => /#.
  rcondt {2} 2; first by move => *;wp;skip;progress => /#.
  rcondt {1} 7; first by move => *;wp;rnd;skip;progress => /#.

  wp;rnd;wp;rnd;wp;skip;progress;smt(@SmtMap).
  wp;skip;progress;smt().
+ proc; inline *.
  seq 1 1 : (#pre /\ ={opld}); first by auto => />.
  if => //=.
  seq 1 1 : (#pre /\ ={tk,cmd,st}).
  call(_: ={glob IAPI, glob CryptoAPIT} /\ 
            B_AOracles.corrupted{1} =  AOracles.corrupted{2} /\ 
            IndO.corrupted{1} = AOracles.corrupted{2} /\ 
            IndO.tested{1} = AOracles.usedKeys{2} /\ 
            B_AOracles.usedKeys{1} = fdom AOracles.usedKeys{2} /\  
            B_ZOracles.table{1} = RealCryptoService.table{2} /\
            B_AOracles.count_new{1} = CryptoAPIT.count_new{2} /\
            B_AOracles.count_mng{1} = CryptoAPIT.count_mng{2} /\
            B_AOracles.count_corr{1} = AOracles.count_corr{2} /\
            IndO.count_corr{1} <= AOracles.count_corr{2} /\
            B_AOracles.count_insr{1} = AOracles.count_insr{2} /\
            IndO.count_insr{1} <= AOracles.count_insr{2} /\
            B_ZOracles.count_znew{1} = RealCryptoService.count_znew{2} /\
            B_ZOracles.count_test{1} = RealCryptoService.count_test{2} /\
            IndO.count_test{1} <= RealCryptoService.count_test{2} /\
            B_AOracles.t{1} = CryptoAPIT.t{2}).
  proc; inline *.
  sp;if => //=.
  rcondt {1} 5; first by auto => />.
  by wp;call(_:true);auto => />.
  proc; inline *.
  sp;if => //=.
  rcondt {1} 4; first by auto => />.
  by wp;call(_:true);auto => />.
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. smt(@SmtMap).
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt(@SmtMap).
  by wp;call(_:true);wp;skip;progress;smt().
  by wp;skip;progress;smt().
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. 
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt().
  sp.
  seq 1 1 : (#pre /\ ko0{1} = ko{2}); first by call(_:true);skip;auto => />.
  case (ko0{1} = None).
  rcondf {1} 4; first by move => *;wp;skip;progress => /#.
  rcondf {2} 2; first by move => *;wp;skip;progress => /#.
  by wp;skip;progress;smt().
  rcondt {1} 4; first by move => *;wp;skip;progress => /#.
  rcondt {2} 2; first by move => *;wp;skip;progress => /#.
  wp;rnd;wp;skip;progress;smt().
  wp;skip;progress;smt().
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. 
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt().
  sp.
  seq 1 1 : (#pre /\ ko0{1} = ko{2}); first by call(_:true);skip;auto => />.
  case (ko0{1} = None).
  rcondf {1} 4; first by move => *;wp;skip;progress => /#.
  rcondf {2} 2; first by move => *;wp;skip;progress => /#.
  by wp;skip;progress;smt().
  rcondt {1} 4; first by move => *;wp;skip;progress => /#.
  rcondt {2} 2; first by move => *;wp;skip;progress => /#.
  wp;skip;progress;smt().
  wp;skip;progress;smt().
  wp;skip;progress;smt().
  sp; if => //=.
  rcondt {1} 6.
  move => *;wp;rnd;wp;skip;progress;smt().
  rcondt {1} 6; first by auto => />.
  swap {1} 5 4.
  sp.
  seq 1 1 : (#pre /\ ko0{1} = ko{2}); first by call(_:true);skip;auto => />.
  case (ko0{1} = None).
  rcondf {1} 6; first by move => *;wp;rnd;skip;progress => /#.
  rcondf {1} 3; first by move => *;wp;rnd;skip;progress => /#.
  rcondf {2} 2; first by move => *;wp;skip;progress => /#.
  wp;rnd{1};skip;progress; first by apply gen_ll.
  smt().
  rcondt {1} 6; first by move => *;wp;rnd;skip;progress => /#.
  rcondt {2} 2; first by move => *;wp;skip;progress => /#.
  rcondt {1} 3; first by move => *;wp;rnd;skip;progress => /#.
  wp;rnd;wp;skip;progress;smt(@SmtMap @FSet).
  wp;skip;progress;smt().
+ proc*.
  call(_: ={glob IAPI, glob CryptoAPIT} /\ 
            B_AOracles.corrupted{1} =  AOracles.corrupted{2} /\ 
            IndO.corrupted{1} = AOracles.corrupted{2} /\ 
            fdom IndO.tested{1} = fdom AOracles.usedKeys{2} /\ 
            B_AOracles.usedKeys{1} = fdom AOracles.usedKeys{2} /\  
            B_ZOracles.table{1} = RealCryptoService.table{2} /\
            B_AOracles.count_new{1} = CryptoAPIT.count_new{2} /\
            B_AOracles.count_mng{1} = CryptoAPIT.count_mng{2} /\
            B_AOracles.count_corr{1} = AOracles.count_corr{2} /\
            IndO.count_corr{1} <= AOracles.count_corr{2} /\
            B_AOracles.count_insr{1} = AOracles.count_insr{2} /\
            IndO.count_insr{1} <= AOracles.count_insr{2} /\
            B_ZOracles.count_znew{1} = RealCryptoService.count_znew{2} /\
            B_ZOracles.count_test{1} = RealCryptoService.count_test{2} /\
            IndO.count_test{1} <= RealCryptoService.count_test{2} /\
            B_AOracles.t{1} = CryptoAPIT.t{2}).
  proc; inline *.
  sp;if => //=.
  rcondt {1} 5; first by auto => />.
  by wp;call(_:true);auto => />.
  proc; inline *.
  sp;if => //=.
  rcondt {1} 4; first by auto => />.
  by wp;call(_:true);auto => />.
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. smt(@SmtMap).
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt(@SmtMap).
  by wp;call(_:true);wp;skip;progress;smt().
  by wp;skip;progress;smt().
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. 
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt().
  sp.
  seq 1 1 : (#pre /\ ko0{1} = ko{2}); first by call(_:true);skip;auto => />.
  case (ko0{1} = None).
  rcondf {1} 4; first by move => *;wp;skip;progress => /#.
  rcondf {2} 2; first by move => *;wp;skip;progress => /#.
  by wp;skip;progress;smt().
  rcondt {1} 4; first by move => *;wp;skip;progress => /#.
  rcondt {2} 2; first by move => *;wp;skip;progress => /#.
  wp;rnd;wp;skip;progress;smt().
  wp;skip;progress;smt().
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. 
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt().
  sp.
  seq 1 1 : (#pre /\ ko0{1} = ko{2}); first by call(_:true);skip;auto => />.
  case (ko0{1} = None).
  rcondf {1} 4; first by move => *;wp;skip;progress => /#.
  rcondf {2} 2; first by move => *;wp;skip;progress => /#.
  by wp;skip;progress;smt().
  rcondt {1} 4; first by move => *;wp;skip;progress => /#.
  rcondt {2} 2; first by move => *;wp;skip;progress => /#.
  wp;skip;progress;smt().
  wp;skip;progress;smt().
  wp;skip;progress;smt().
  by auto => />.
  by auto => />.
qed.


end section.

(** Hop 3: AEAD correctness => cph table     *)

module Hop3CryptoService(API : CryptoAPI, A : Adv) = {
  module HS0 = Hop0CryptoService(API,A) 
  include HS0 [-enc,dec]

  proc enc(hdl : Hdl, pld : Pld, st : St) : Cph option * St =  {
     var tk,cmd,key,ocph,cph,rkey;
     ocph <- None;
     if (HS0.RS.count_test < q_test) {
        (tk,cmd,st) <@ HS0.RS.A.encParam(hdl,st);
        if ((valid CryptoAPIT.t tk hdl cmd) 
              && !(hdl \in AOracles.corrupted)) {
           key <@ API.tkReveal(tk,hdl,cmd);
           if (key <> None) {
              rkey <$ gen;
              key <- if (hdl \in AOracles.usedKeys) 
                     then AOracles.usedKeys.[hdl]
                     else Some rkey;
              cph <$ enc (oget key) emptyD pld;
              AOracles.usedKeys.[hdl]<-oget key;
              RealCryptoService.table.[(hdl,cph)]<-pld;
              ocph <- Some cph;
          }
        }
        HS0.RS.count_test <- HS0.RS.count_test + 1;
     }
     return (ocph,st);
  }

  proc dec(hdl : Hdl, cph : Cph, st : St) : Pld option * St =  { 
     var tk,cmd,key,opld,rkey;
     opld <- None;
     if (HS0.RS.count_test < q_test) {
        (tk,cmd,st) <@ HS0.RS.A.decParam(hdl,st);
        if ((valid CryptoAPIT.t tk hdl cmd) 
              && !(hdl \in AOracles.corrupted)) {
           key <@ API.tkReveal(tk,hdl,cmd);
           if (key <> None) {
              rkey <$ gen;
              key <- if (hdl \in AOracles.usedKeys) 
                     then AOracles.usedKeys.[hdl]
                     else Some rkey;
              opld <- dec (oget key) emptyD cph;
              Hop0CryptoService.bad <- 
                   Hop0CryptoService.bad || 
                       ((hdl,cph) \in RealCryptoService.table) /\
                       !(opld = RealCryptoService.table.[(hdl,cph)]);
              opld <- if ((hdl,cph) \in RealCryptoService.table)
                      then RealCryptoService.table.[(hdl,cph)]
                      else opld;
              AOracles.usedKeys.[hdl]<-oget key;
           }
        }
        HS0.RS.count_test <- HS0.RS.count_test + 1;
     }
     return (opld,st);
  }
  
}.

module Game3(Z : Env, A : Adv, IAPI : ICryptoAPI)  = {
  module API = CryptoAPIT(IAPI)
  module HS3 = Hop3CryptoService(API,A) 

  proc main() = {
     var b;
     IAPI.init();
     API.init();
     AOracles(IAPI).init();
     HS3.init();
     b <@ Z(HS3).guess();
     return b;
  }   
  
}.

section.

declare module A : Adv {CryptoAPIT, Hop0CryptoService, Hop0CryptoService}.
declare module Z : Env {CryptoAPIT, A, Hop0CryptoService, Hop0CryptoService}.
declare module IAPI : ICryptoAPI {CryptoAPIT, Z, A, Hop0CryptoService}.

axiom z_ll :
forall (O <: EnvO{Z}),
  islossless O.new =>
  islossless O.enc =>
  islossless O.dec => islossless O.takeCtl => islossless Z(O).guess.

axiom newParam_ll :
forall (O <: AdvO{A}),
  islossless O.tkNewKey =>
  islossless O.tkManage =>
  islossless O.corruptR =>
  islossless O.badEnc => islossless O.badDec => islossless A(O).newParam.

axiom encParam_ll :
forall (O <: AdvO{A}),
  islossless O.tkNewKey =>
  islossless O.tkManage =>
  islossless O.corruptR =>
  islossless O.badEnc => islossless O.badDec => islossless A(O).encParam.

axiom decParam_ll :
forall (O <: AdvO{A}),
  islossless O.tkNewKey =>
  islossless O.tkManage =>
  islossless O.corruptR =>
  islossless O.badEnc => islossless O.badDec => islossless A(O).decParam.

axiom takeCtl_ll : forall (O <: AdvO{A}),
  islossless O.tkNewKey =>
  islossless O.tkManage =>
  islossless O.corruptR =>
  islossless O.badEnc => islossless O.badDec => islossless A(O).takeCtl.

axiom iapi_tkNewKey_ll : islossless IAPI.tkNewKey.
axiom iapi_tkManage_ll : islossless IAPI.tkManage.
axiom iapi_tkReveal_ll : islossless IAPI.tkReveal.

lemma tkNewKey_ll : islossless AOracles(CryptoAPIT(IAPI)).tkNewKey.
proof.
 by proc;inline*;
    sp; (if; last by done);
      wp; call iapi_tkNewKey_ll.
qed.

lemma tkManage_ll : islossless AOracles(CryptoAPIT(IAPI)).tkManage.
proof.
 by proc;inline*;
    sp; (if; last by done);
      wp; call iapi_tkManage_ll.
qed.

lemma corrupt_ll : islossless AOracles(CryptoAPIT(IAPI)).corruptR.
proof.
  by proc;inline*;
    sp; (if; last by done);
      (if; last by wp);
        wp; call iapi_tkReveal_ll; wp.
qed.

lemma badEnc_ll : islossless AOracles(CryptoAPIT(IAPI)).badEnc.
proof.
  proc;inline*;
  sp; if; last by done.
  if; last by wp.
  sp; seq 1 : (#pre); last by done.
    by call (_:true). 
    by call iapi_tkReveal_ll.
    sp; if; last by wp.
      wp; rnd; skip; progress. apply enc_ll.
    by hoare; call (_:true).
qed.

lemma badDec_ll : islossless AOracles(CryptoAPIT(IAPI)).badDec.
proof.
  proc;inline*;
  sp; if; last by done.
  if; last by wp.
  sp; seq 1 : (#pre); last by done.
    by call (_:true).
    by call iapi_tkReveal_ll.
    by wp.
    hoare; by call (_:true).
qed.

lemma hop3uptobad &m :
   `| Pr [ Game2(Z,A,IAPI).main()    @ &m : res] -
      Pr [ Game3(Z,A,IAPI).main()    @ &m : res] | <=
      Pr [ Game3(Z,A,IAPI).main()    @ &m : Hop0CryptoService.bad].
proof.
byequiv : (Hop0CryptoService.bad) => // [ | ?? [->] /= h /h -> //].
proc.
inline *.
call(_: Hop0CryptoService.bad, 
       ={glob IAPI, glob A, glob CryptoAPIT, glob AOracles, glob RealCryptoService, glob Hop0CryptoService},
       ={Hop0CryptoService.bad}); last first.
+ by wp;call(_: true);auto =>  /#.
+ by apply z_ll.
+ proc; inline *; if => //=; wp; call(_: ={glob IAPI, glob CryptoAPIT, glob AOracles, glob RealCryptoService, glob Hop0CryptoService}). 
  by proc; inline *;sp;if => //=; wp; call(_: true); auto => />. 
  by proc; inline *; sp; if => //=;wp; call(_: true); auto => />. 
  proc; inline *;sp;if => //=;sp; if => //=; auto => />.
  smt().
  by call(_: true); auto => />.
  proc; inline *;sp;if => //=; sp; if => //=; auto => />.
  sp;seq 1 1 : (#pre /\ ={ko}); first by call(_: true); auto => />.
  sp; if => //=.
  wp;rnd;auto => />.
  proc; inline *;sp;if => //=; if => //=;auto => />.
  smt().
  by call(_: true); auto => />.
  by auto => />.
+ move => *. 
  proc; inline*. 
  if; last by done. 
  wp; call (_:true); last by done.
    by apply newParam_ll.
    by apply tkNewKey_ll.
    by apply tkManage_ll.
    by apply corrupt_ll.
    by apply badEnc_ll.
    by apply badDec_ll.
+ move => *. 
  proc; inline*.
  if; last by done.
  wp; call (_:true); last by done.
    by apply newParam_ll.
    by apply tkNewKey_ll.
    by apply tkManage_ll.
    by apply corrupt_ll.
    by apply badEnc_ll.
    by apply badDec_ll.
+ proc; inline *. 
  seq 1 1 : (#pre /\ ={ocph}); first by auto => />.
  if => //=.
  seq 1 1 : (#pre /\ ={tk,cmd,st}).  
  wp; call(_: ={glob IAPI, glob CryptoAPIT, glob AOracles, glob RealCryptoService, glob Hop0CryptoService}). 
  by proc; inline *;sp; if => //=; wp; call(_: true); auto => />. 
  by proc; inline *;sp; if => //=; wp; call(_: true); auto => />. 
  proc; inline *;sp;if => //=; sp; if => //=;auto => />.
  smt().
  by call(_: true); auto => />.
  proc; inline *;sp;if => //=; sp; if => //=;auto => />.
  sp;seq 1 1 : (#pre /\ ={ko}); first by call(_: true); auto => />.
  sp; if => //=.
  wp;rnd;auto => />.
  proc; inline *;sp;if => //=;sp; if => //=; auto => />.
  smt().
  by call(_: true); auto => />.
  by auto => />.
  if => //=; sp.
  seq 2 2 : (#pre /\ ={key}); first by wp;call(_:true);auto => /#. 
  if => //=.
   by wp;rnd;wp;rnd;wp;skip;progress => /#.
   by wp;skip;progress => /#.
   by wp;skip;progress => /#.
+ move => *.
  proc; inline*.
  sp; if; last by done.
  seq 1 : (#pre); last by done.
    call (_: true); last by done.
      by proc; inline*.
      by proc; inline*.
      by proc; inline*.
      by proc; inline*.
      by proc; inline*.
    call (_: true); last by done.
      by apply encParam_ll.
      by apply tkNewKey_ll.
      by apply tkManage_ll.
      by apply corrupt_ll.
      by apply badEnc_ll.
      by apply badDec_ll.
    if; last by wp.
    sp; seq 1 : (#pre); last by done.
      by call (_:true).
      by call (_:true); first by apply iapi_tkReveal_ll.
      sp; (if; last by wp); wp; rnd; wp; rnd; skip; progress. 
        have ?: forall k a m, weight (enc k a m) = 1%r by apply enc_ll.
        case (hdl{hr} \in AOracles.usedKeys{hr}) => ?.
        by rewrite H4 /= gen_ll. 
        cut ->: (fun (x : K) => weight (enc (oget (Some x)) emptyD pld{hr}) = 1%r) = 
                (fun (x : K) => 1%r = 1%r).
          rewrite fun_ext /(==) => x.
          by rewrite oget_some H4 /=.
        by rewrite gen_ll.
      by hoare; call (_: true).
    by hoare; call (_:true).
+ move => *. 
  proc; inline*.
  sp; if; last by done.
  seq 1 : (#pre); last by done.
    call (_: true); last by done.
      by proc; inline*.
      by proc; inline*.
      by proc; inline*.
      by proc; inline*.
      by proc; inline*.
    call (_: true); last by done.
      by apply encParam_ll.
      by apply tkNewKey_ll.
      by apply tkManage_ll.
      by apply corrupt_ll.
      by apply badEnc_ll.
      by apply badDec_ll.
    if; last by wp.
    sp; seq 1 : (#pre); last by done.
      by call (_:true).
      by call (_:true); first by apply iapi_tkReveal_ll.
      sp; (if; last by wp); wp; rnd; wp; rnd; skip; progress.
        rewrite H /=.
        have ?: forall k a m, weight (enc k a m) = 1%r by apply enc_ll.
        case (hdl{hr} \in AOracles.usedKeys{hr}) => ?.
        by rewrite H4 /= gen_ll. 
        cut ->: (fun (x : K) => weight (enc (oget (Some x)) emptyD pld{hr}) = 1%r) = 
                (fun (x : K) => 1%r = 1%r).
          rewrite fun_ext /(==) => x.
          by rewrite oget_some H4 /=.
        by rewrite gen_ll.
      by hoare; call (_: true).
    by hoare; call (_:true).
+ proc; inline *. 
  seq 1 1 : (#pre /\ ={opld}); first by auto => />.
  if => //=.  
  seq 1 1 : (#pre /\ ={tk,cmd,st,opld}).  
  wp; call(_: ={glob IAPI, glob CryptoAPIT, glob AOracles, glob RealCryptoService}). 
  by proc; inline *; sp; if => //=; wp; call(_: true); auto => />. 
  by proc; inline *; sp; if => //=;wp; call(_: true); auto => />. 
  proc; inline *;sp;if => //=; sp; if => //=;auto => />.
  smt().
  by call(_: true); auto => />.
  proc; inline *;sp;if => //=; sp; if => //=;auto => />.
  sp;seq 1 1 : (#pre /\ ={ko}); first by call(_: true); auto => />.
  sp; if => //=.
  wp;rnd;auto => />.
  proc; inline *;sp;if => //=;sp; if => //=; auto => />.
  smt().
  by call(_: true); auto => />.
  by auto => />.
  if => //=; sp.
  sp;seq 1 1 : (#pre /\ ={ko}); first by call(_: true); auto => />.
  sp; if => //=.
  by wp;rnd;skip;progress => /#.
   by wp;skip;progress => /#.
   by wp;skip;progress => /#.
+ move => *.
  proc; inline*.
  sp; if; last by done.
  seq 1 : (#pre); last by done.
    call (_: true); last by done.
      by proc; inline*.
      by proc; inline*.
      by proc; inline*.
      by proc; inline*.
      by proc; inline*.
    call (_: true); last by done.
      by apply decParam_ll.
      by apply tkNewKey_ll.
      by apply tkManage_ll.
      by apply corrupt_ll.
      by apply badEnc_ll.
      by apply badDec_ll.
    if; last by wp.
    sp; seq 1 : (#pre); last by done.
      by call (_:true).
      by call (_:true); first by apply iapi_tkReveal_ll.
      sp; (if; last by wp); wp; rnd; skip; progress.
        rewrite H /=.
        by rewrite gen_ll. 
      by hoare; call (_: true).
    by hoare; call (_:true).
+ move => *.
  proc; inline*.
  sp; if; last by done.
  seq 1 : (#pre); last by done.
    call (_: true); last by done.
      by proc; inline*.
      by proc; inline*.
      by proc; inline*.
      by proc; inline*.
      by proc; inline*.
    call (_: true); last by done.
      by apply decParam_ll.
      by apply tkNewKey_ll.
      by apply tkManage_ll.
      by apply corrupt_ll.
      by apply badEnc_ll.
      by apply badDec_ll.
    if; last by wp.
    sp; seq 1 : (#pre); last by done.
      by call (_:true).
      by call (_:true); first by apply iapi_tkReveal_ll.
      sp; (if; last by wp); wp; rnd; skip; progress.
        rewrite H /=.
        by rewrite gen_ll. 
      by hoare; call (_: true).
    by hoare; call (_:true).
+ proc*.
  call(_: ={glob IAPI, glob CryptoAPIT, glob AOracles, glob RealCryptoService}). 
  by sim.
  by sim.
  by sim.
  by sim.
  by sim.
  auto => />.
+ move => *.
  proc*; call (_:true).
    by apply takeCtl_ll.
    by apply tkNewKey_ll.
    by apply tkManage_ll.
    by apply corrupt_ll.
    by apply badEnc_ll.
    by apply badDec_ll.
  by done.
+ move => *. 
  proc*; call (_:true).
    by apply takeCtl_ll.
    by apply tkNewKey_ll.
    by apply tkManage_ll.
    by apply corrupt_ll.
    by apply badEnc_ll.
    by apply badDec_ll.
  by done.
qed.

(* Show bad is correctness of AEAD *)

op corr_dec(kt : (Hdl,Key) fmap , pt : ((Hdl *Cph),Pld) fmap) = 
   forall hdl cph, (hdl,cph) \in pt =>
          hdl \in kt /\
          dec (oget kt.[hdl]) emptyD cph = pt.[(hdl,cph)].

lemma no_bad &m: 
  Pr [ Game3(Z,A,IAPI).main() @ &m : Hop0CryptoService.bad] = 0%r.
byphoare.
hoare.
proc.
call (_: !Hop0CryptoService.bad /\ corr_dec AOracles.usedKeys RealCryptoService.table).

+ proc;inline *.
  if => //=;wp.
  call (_: !Hop0CryptoService.bad /\ corr_dec AOracles.usedKeys RealCryptoService.table).
  by proc;inline*;sp;if => //=;wp;call(_:true);skip;progress.
  by proc;inline*;sp;if => //=;wp;call(_:true);skip;progress.
  proc;inline*;sp;if => //=;sp;if => //=;wp.
  by call(_:true);wp;skip;progress.
  by wp;skip;progress.
  proc;inline*;sp;if => //=;sp;if => //=.
  sp; seq 1 : (#pre); first by call(_:true); auto => />.
  sp;if => //=.
  by wp;rnd;wp;skip;progress.
  by wp;skip;progress.
  by wp;skip;progress.
  proc;inline*;sp;if => //=;sp;if => //=;wp.
  by call(_:true);wp;skip;progress.
  by wp;skip;progress.
  by wp;skip;progress.

+ proc;inline *.
  sp;if => //=.
  sp;seq 1 : #pre.
  call (_: !Hop0CryptoService.bad /\ corr_dec AOracles.usedKeys RealCryptoService.table).
  by proc;inline*;sp;if => //=;wp;call(_:true);skip;progress.
  by proc;inline*;sp;if => //=;wp;call(_:true);skip;progress.
  proc;inline*;sp;if => //=;sp;if => //=;wp.
  by call(_:true);wp;skip;progress.
  by wp;skip;progress.
  proc;inline*;sp;if => //=;sp;if => //=.
  sp; seq 1 : (#pre); first by call(_:true); auto => />.
  sp;if => //=.
  by wp;rnd;wp;skip;progress.
  by wp;skip;progress.
  by wp;skip;progress.
  proc;inline*;sp;if => //=;sp;if => //=;wp.
  by call(_:true);wp;skip;progress.
  by wp;skip;progress.
  by wp;skip;progress.
  if => //=.
  sp; seq 1 : (#pre); first by call(_:true); auto => />.
  sp;if => //=.
  by wp;rnd;wp;rnd;skip;progress;smt(get_setE set_get_eq aead_corr).
  by wp;skip;progress.
  by wp;skip;progress.

+ proc;inline *.
  sp;if => //=.
  sp;seq 1 : #pre.
  call (_: !Hop0CryptoService.bad /\ corr_dec AOracles.usedKeys RealCryptoService.table).
  by proc;inline*;sp;if => //=;wp;call(_:true);skip;progress.
  by proc;inline*;sp;if => //=;wp;call(_:true);skip;progress.
  proc;inline*;sp;if => //=;sp;if => //=;wp.
  by call(_:true);wp;skip;progress.
  by wp;skip;progress.
  proc;inline*;sp;if => //=;sp;if => //=.
  sp; seq 1 : (#pre); first by call(_:true); auto => />.
  sp;if => //=.
  by wp;rnd;wp;skip;progress.
  by wp;skip;progress.
  by wp;skip;progress.
  proc;inline*;sp;if => //=;sp;if => //=;wp.
  by call(_:true);wp;skip;progress.
  by wp;skip;progress.
  by wp;skip;progress.
  if => //=.
  sp; seq 1 : (#pre); first by call(_:true); auto => />.
  sp;if => //=.
  by wp;rnd;wp;skip;progress;smt(get_setE set_get_eq aead_corr).
  by wp;skip;progress.
  by wp;skip;progress.

+ proc*;call(_:true);auto => />.

+ inline *;wp; call(_:true);skip;progress;smt(@SmtMap).

done.
done.
qed.
     
end section.


(** Hop 4: AEAD security => encrypt witness **)

module Hop4CryptoService(API : CryptoAPI, A : Adv) = {
  module HS3 = Hop3CryptoService(API,A) 
  include HS3 [-enc, dec]

  proc enc(hdl : Hdl, pld : Pld, st : St) : Cph option * St =  {
     var tk,cmd,key,rkey,ocph,cph;
     ocph <- None;
     if (HS3.HS0.RS.count_test <q_test) {
        (tk,cmd,st) <@ HS3.HS0.RS.A.encParam(hdl,st);
        if ((valid CryptoAPIT.t tk hdl cmd) 
              && !(hdl \in AOracles.corrupted)) {
           key <@ API.tkReveal(tk,hdl,cmd);
           if (key <> None) {
              rkey <$ gen;
              key <- if (hdl \in AOracles.usedKeys) 
                     then AOracles.usedKeys.[hdl]
                     else Some rkey;
              cph <$ enc (oget key) emptyD witness;
              AOracles.usedKeys.[hdl]<- oget key;
              RealCryptoService.table.[(hdl,cph)]<-pld;
              ocph <- Some cph;
           }
       }
       HS3.HS0.RS.count_test <- HS3.HS0.RS.count_test + 1;
     }
     return (ocph,st);
  }

  proc dec(hdl : Hdl, cph : Cph, st : St) : Pld option * St =  { 
     var tk,cmd,key,opld,rkey;
     opld <- None;
     if (HS3.HS0.RS.count_test <q_test) {
        (tk,cmd,st) <@ HS3.HS0.RS.A.decParam(hdl,st);
        if ((valid CryptoAPIT.t tk hdl cmd) 
              && !(hdl \in AOracles.corrupted)) {
           key <@ API.tkReveal(tk,hdl,cmd);
           if (key <> None) {
              rkey <$ gen;
              key <- if (hdl \in AOracles.usedKeys) 
                     then AOracles.usedKeys.[hdl]
                     else Some rkey;
              opld <- dec (oget key) emptyD cph;
              Hop0CryptoService.bad <- 
                    Hop0CryptoService.bad || 
                      ((hdl,cph) \in RealCryptoService.table) /\
                     !(opld = RealCryptoService.table.[(hdl,cph)]);
              opld <- RealCryptoService.table.[(hdl,cph)];
              AOracles.usedKeys.[hdl]<-oget key;
          }
       }
       HS3.HS0.RS.count_test <- HS3.HS0.RS.count_test + 1;
     }
     return (opld,st);
  }

}.

module Game4(Z : Env, A : Adv, IAPI : ICryptoAPI)  = {
  module API = CryptoAPIT(IAPI)
  module HS4 = Hop4CryptoService(API,A) 

  proc main() = {
     var b;
     IAPI.init();
     API.init();
     AOracles(IAPI).init();
     HS4.init();
     b <@ Z(HS4).guess();
     return b;
  }   
  
}.

(* AEAD adversary *)


module C_ZOracles(IAPI : ICryptoAPI, O : AEAD_OraclesT, A : Adv) = {
  var keyids : (Hdl,int) fmap
  var keycount : int
  var count_znew : int
  var count_test : int

  module API = CryptoAPIT(IAPI)
  module A = A(AOracles(API))

  include A [takeCtl]

  proc init() = {
      keyids <- empty;
      keycount <- 0;
      count_znew <- 0;
      count_test <- 0;
      RealCryptoService.table <- empty;
      IAPI.init();
      API.init();
      AOracles(API).init();
  }

  proc new(hdl : Hdl,st : St) : St = {
     var tk,cmd;
     if (count_znew < q_znew) {
        (tk,cmd,st) <@ A.newParam(hdl,st);
        count_znew <- count_znew + 1;
     }
     return st;
  }

  proc enc(hdl : Hdl, pld : Pld, st : St) : Cph option * St =  {
     var tk,cmd,key,ocph,cph,kid;
     ocph <- None;
     if (count_test < q_test) {
        (tk,cmd,st) <@ A.encParam(hdl,st); 
        if ((valid CryptoAPIT.t tk hdl cmd) 
              && !(hdl \in AOracles.corrupted)) {
           key <@ API.tkReveal(tk,hdl,cmd);
           if (key <> None) {
              O.newkey();
              kid <- if (hdl \in AOracles.usedKeys) 
                     then oget keyids.[hdl]
                     else keycount;
              keycount <- keycount + 1;
              cph <@ O.lor(kid,pld,witness,emptyD);
              AOracles.usedKeys.[hdl]<-witness;
              keyids.[hdl] <- kid;
              RealCryptoService.table.[(hdl,oget cph)]<-pld;
              ocph <- cph;
           }
        }
        count_test <- count_test + 1;
     }
     return (ocph,st);
  }

  proc dec(hdl : Hdl, cph : Cph, st : St) : Pld option * St =  { 
     var tk,cmd,key,opld,kid;
     opld <- None;
     if (count_test < q_test) {
        (tk,cmd,st) <@ A.decParam(hdl,st);
        if ((valid CryptoAPIT.t tk hdl cmd) 
              && !(hdl \in AOracles.corrupted)) {
           key <@ API.tkReveal(tk,hdl,cmd);
           if (key <> None) {
              O.newkey();
              kid <- if (hdl \in AOracles.usedKeys) 
                      then oget keyids.[hdl]
                      else keycount;
              keycount <- keycount + 1;
              opld <- O.dec(kid,emptyD,cph);
              Hop0CryptoService.bad <- 
                    Hop0CryptoService.bad || 
                       ((hdl,cph) \in RealCryptoService.table) /\
                        !(opld = RealCryptoService.table.[(hdl,cph)]);
              opld <- if ((hdl,cph) \in RealCryptoService.table)
                      then RealCryptoService.table.[(hdl,cph)]
                      else opld;
              AOracles.usedKeys.[hdl]<-witness;
              keyids.[hdl] <- kid;
           }
        }
        count_test <- count_test + 1;
     }
     return (opld,st);
  }
  
}.

module C(IAPI : ICryptoAPI, Z : Env, A : Adv,O : AEAD_OraclesT)  = {
   module ZO = C_ZOracles(IAPI,O,A) 

   proc guess() = {
      var b;
      ZO.init();
      b <@ Z(ZO).guess();
      return b;
   }
}.

section.

declare module A : Adv { RealCryptoService,AEAD_OraclesCondProb, C_ZOracles }.
declare module Z : Env {RealCryptoService, AEAD_OraclesCondProb, A, C_ZOracles}.
declare module IAPI : ICryptoAPI { RealCryptoService,AEAD_OraclesCondProb, Z, A, C_ZOracles}.

lemma leftHopAEAD &m : 
   Pr[AEAD_LoRCondProb(C(IAPI,Z,A)).game(false) @ &m : !res]
    = Pr[ Game3(Z,A,IAPI).main() @ &m : res].
proof.
byequiv.
proc; inline *; wp.
call(_: !AEAD_Oracles.b{1} /\  ={glob A, glob IAPI, AOracles.count_corr, AOracles.count_insr, glob CryptoAPIT, RealCryptoService.table, AOracles.corrupted} /\ 
           fdom AOracles.usedKeys{1} = fdom AOracles.usedKeys{2} /\
           C_ZOracles.keycount{1} = AEAD_Oracles.keycount{1} /\ 
           fdom AOracles.usedKeys{1} = fdom C_ZOracles.keyids{1} /\
           (0 <= C_ZOracles.keycount{1}) /\
           (AOracles.usedKeys{1} <> empty => 0< C_ZOracles.keycount{1}) /\
           (forall hdl, hdl \in AOracles.usedKeys{1} => 
               (0 <= oget C_ZOracles.keyids{1}.[hdl] < AEAD_Oracles.keycount{1}) /\
                (AOracles.usedKeys{2}.[hdl] = 
                   AEAD_Oracles.keylist{1}.[oget C_ZOracles.keyids{1}.[hdl]])) /\
           (forall hdl hdl', hdl \in C_ZOracles.keyids{1} => 
                             hdl' \in C_ZOracles.keyids{1} => 
                     ((hdl = hdl') <=>
               (oget C_ZOracles.keyids{1}.[hdl] = oget C_ZOracles.keyids{1}.[hdl']))) /\
           (forall hdl c, (hdl \in C_ZOracles.keyids{1} => 
                ((oget C_ZOracles.keyids{1}.[hdl], emptyD, c) \in AEAD_Oracles.cphlist{1} => 
                   (hdl,c) \in RealCryptoService.table{1})) /\
                ((hdl,c) \in RealCryptoService.table{1} => (hdl \in C_ZOracles.keyids{1} /\
                 (oget C_ZOracles.keyids{1}.[hdl], emptyD, c) \in AEAD_Oracles.cphlist{1}))) /\
           (forall kid ad c, 
                     ((kid,ad, c) \in AEAD_Oracles.cphlist{1} => 0 <= kid < AEAD_Oracles.keycount{1})) /\
           C_ZOracles.count_znew{1} = RealCryptoService.count_znew{2} /\
           C_ZOracles.count_test{1} = RealCryptoService.count_test{2} ); last first.
+ inline *. 
  auto => />.
  smt().
  wp;call(_: true);auto => />.
  progress;smt(@FSet @SmtMap).

+ proc.
  if => //=. wp.
  call(_: ={glob IAPI, glob CryptoAPIT,AOracles.count_corr, AOracles.count_insr, RealCryptoService.table, AOracles.corrupted} /\ 
           fdom AOracles.usedKeys{1} = fdom AOracles.usedKeys{2} /\
           C_ZOracles.keycount{1} = AEAD_Oracles.keycount{1} /\ 
           fdom AOracles.usedKeys{1} = fdom C_ZOracles.keyids{1} /\
           (0 <= C_ZOracles.keycount{1}) /\
           (AOracles.usedKeys{1} <> empty => 0< C_ZOracles.keycount{1}) /\
           (forall hdl, hdl \in AOracles.usedKeys{1} => 
               (0 <= oget C_ZOracles.keyids{1}.[hdl] < AEAD_Oracles.keycount{1}) /\
                (AOracles.usedKeys{2}.[hdl] = 
                   AEAD_Oracles.keylist{1}.[oget C_ZOracles.keyids{1}.[hdl]])) /\
           (forall hdl hdl', hdl \in C_ZOracles.keyids{1} => 
                             hdl' \in C_ZOracles.keyids{1} => 
                     ((hdl = hdl') <=>
               (oget C_ZOracles.keyids{1}.[hdl] = oget C_ZOracles.keyids{1}.[hdl']))) /\
           (forall hdl c, (hdl \in C_ZOracles.keyids{1} => 
                ((oget C_ZOracles.keyids{1}.[hdl], emptyD, c) \in AEAD_Oracles.cphlist{1} => 
                   (hdl,c) \in RealCryptoService.table{1})) /\
                ((hdl,c) \in RealCryptoService.table{1} => (hdl \in C_ZOracles.keyids{1} /\
                 (oget C_ZOracles.keyids{1}.[hdl], emptyD, c) \in AEAD_Oracles.cphlist{1}))) /\
           (forall kid ad c, 
                     ((kid,ad, c) \in AEAD_Oracles.cphlist{1} => 0 <= kid < AEAD_Oracles.keycount{1}))/\
           C_ZOracles.count_znew{1} = RealCryptoService.count_znew{2} /\
           C_ZOracles.count_test{1} = RealCryptoService.count_test{2}).
  proc;inline *.
  sp; if => //=.
  by wp;call(_:true);auto => />.
  proc;inline *.
  sp; if => //=.
  by wp;call(_:true);auto => />.
  proc;inline *.
  sp; if => //=.
  sp; if => //=.
  smt(@SmtMap).
  sp; seq 1 1 : (#pre /\ ={ko0}); first by call(_:true);wp;skip;progress.
  by wp;skip;progress.
  by wp;skip;progress.
  proc;inline *.
  sp; if => //=.
  sp; if => //=.
  sp; seq 1 1 : (#pre /\ ={ko}); first by call(_:true);wp;skip;progress.
  sp;if => //=.  
  by wp;rnd;skip;progress.
  by wp;skip;progress.
  by wp;skip;progress.
  proc;inline *.
  sp; if => //=.
  sp; if => //=.
  sp; seq 1 1 : (#pre /\ ={ko}); first by call(_:true);wp;skip;progress.
  by wp;skip;progress.
  by wp;skip;progress;smt().
  by wp;skip;progress;smt().

+ proc;inline *.
  sp;if => //=. wp.
  seq 1 1 : (#pre /\ ={tk,cmd,st,ocph}).
  call(_: ={glob IAPI, glob CryptoAPIT, AOracles.count_corr, AOracles.count_insr, RealCryptoService.table, AOracles.corrupted} /\ 
           fdom AOracles.usedKeys{1} = fdom AOracles.usedKeys{2} /\
           C_ZOracles.keycount{1} = AEAD_Oracles.keycount{1} /\ 
           fdom AOracles.usedKeys{1} = fdom C_ZOracles.keyids{1} /\
           (0 <= C_ZOracles.keycount{1}) /\
           (AOracles.usedKeys{1} <> empty => 0< C_ZOracles.keycount{1}) /\
           (forall hdl, hdl \in AOracles.usedKeys{1} => 
               (0 <= oget C_ZOracles.keyids{1}.[hdl] < AEAD_Oracles.keycount{1}) /\
                (AOracles.usedKeys{2}.[hdl] = 
                   AEAD_Oracles.keylist{1}.[oget C_ZOracles.keyids{1}.[hdl]])) /\
           (forall hdl hdl', hdl \in C_ZOracles.keyids{1} => 
                             hdl' \in C_ZOracles.keyids{1} => 
                     ((hdl = hdl') <=>
               (oget C_ZOracles.keyids{1}.[hdl] = oget C_ZOracles.keyids{1}.[hdl']))) /\
           (forall hdl c, (hdl \in C_ZOracles.keyids{1} => 
                ((oget C_ZOracles.keyids{1}.[hdl], emptyD, c) \in AEAD_Oracles.cphlist{1} => 
                   (hdl,c) \in RealCryptoService.table{1})) /\
                ((hdl,c) \in RealCryptoService.table{1} => (hdl \in C_ZOracles.keyids{1} /\
                 (oget C_ZOracles.keyids{1}.[hdl], emptyD, c) \in AEAD_Oracles.cphlist{1}))) /\
           (forall kid ad c, 
                     ((kid,ad, c) \in AEAD_Oracles.cphlist{1} => 0 <= kid < AEAD_Oracles.keycount{1}))/\
           C_ZOracles.count_znew{1} = RealCryptoService.count_znew{2} /\
           C_ZOracles.count_test{1} = RealCryptoService.count_test{2}).
  proc;inline *.
  sp; if => //=.
  by wp;call(_:true);auto => />.
  proc;inline *.
  sp; if => //=.
  by wp;call(_:true);auto => />.
  proc;inline *.
  sp; if => //=.
  sp; if => //=.
  smt(@SmtMap).
  sp; seq 1 1 : (#pre /\ ={ko0}); first by call(_:true);wp;skip;progress.
  by wp;skip;progress.
  by wp;skip;progress.
  proc;inline *.
  sp; if => //=.
  sp; if => //=.
  sp; seq 1 1 : (#pre /\ ={ko}); first by call(_:true);wp;skip;progress.
  sp;if => //=.  
  by wp;rnd;skip;progress.
  by wp;skip;progress.
  by wp;skip;progress.
  proc;inline *.
  sp;if => //=.  
  sp; if => //=.
  sp; seq 1 1 : (#pre /\ ={ko}); first by call(_:true);wp;skip;progress.
  by wp;skip;progress.
  by wp;skip;progress;smt().
  by wp;skip;progress.
  if => //=.
  sp.
  seq 2 2 : (#pre /\ ={ko,key});first by wp;call(_:true);auto => />.
  if => //=.
  seq 1 1 : (#pre /\ key0{1} = rkey{2}); first by auto => />.
  seq 4 0 : (#[/:25,26:-10,-11:]pre /\ 
                     (forall (hdl : Hdl),
                       hdl \in AOracles.usedKeys{1} =>
                       0 <= oget C_ZOracles.keyids{1}.[hdl] < AEAD_Oracles.keycount{1} - 1 /\
                       AOracles.usedKeys{2}.[hdl] =
                       AEAD_Oracles.keylist{1}.[oget C_ZOracles.keyids{1}.[hdl]]) /\
                   (0 < C_ZOracles.keycount{1}) /\
                   (Some key0 = AEAD_Oracles.keylist.[AEAD_Oracles.keycount - 1]){1} /\
                   (kid = if hdl \in AOracles.usedKeys 
                           then oget C_ZOracles.keyids.[hdl] 
                           else (C_ZOracles.keycount - 1)){1} /\
                   (forall (kid1 : int) (ad : AData) (c0 : AE.Cph),
                        (kid1, ad, c0) \in AEAD_Oracles.cphlist{1} =>
                        0 <= kid1 < AEAD_Oracles.keycount{1}-1)).
  wp;skip;progress; smt(@FSet @SmtMap).

  rcondt {1} 6.
  move => *;wp;skip;progress;smt(@FSet  @SmtMap).
  wp;rnd;wp;skip;progress.
  smt(@SmtMap).
  smt(@SmtMap).
  smt(@SmtMap).
  smt(@SmtMap).
  smt(@SmtMap).
  case (hdl1 = hdl{2});smt(@SmtMap).
  case (hdl1 = hdl{2});smt(@SmtMap).

  case (hdl1 <> hdl{2}).
  smt(@SmtMap).
  move => *.
  case (hdl{2} \in  AOracles.usedKeys{1});last first.
  smt(@SmtMap).
  move => *.
  have ? : (hdl{2} \in  AOracles.usedKeys{2}).
  smt(@SmtMap).
  rewrite H22;simplify.
  have -> : (hdl1 = hdl{2}); first by smt(@SmtMap).
  have -> : (AOracles.usedKeys{2}.[hdl{2} <- oget AOracles.usedKeys{2}.[hdl{2}]].[hdl{2}] = AOracles.usedKeys{2}.[hdl{2}]).
  have inmap : (exists a, Some a =  AOracles.usedKeys{2}.[hdl{2}]); first by smt(set_valE).
  smt(@SmtMap).
  smt(@SmtMap).

  case (hdl{2} \in AOracles.usedKeys{1}).
  case(hdl1=hdl{2});smt(@SmtMap).
  move => *.
  move : H19;rewrite H22;simplify => H19.
  move : H20;rewrite H22;simplify => H20.
  move : H21;rewrite H22;simplify => H21.
  have ? : (hdl{2} \notin C_ZOracles.keyids{1}); first by smt(@SmtMap).
  case (hdl1 <> hdl{2}).
  case (hdl' <> hdl{2}).
  smt(@SmtMap).
  move => *.
  have ? : (hdl1 \in C_ZOracles.keyids{1}); first by  smt(@SmtMap).
  smt(@SmtMap).
  move => *.
  case (hdl' <> hdl{2}); last first.
  smt(@SmtMap).
  have ? : (oget
       C_ZOracles.keyids{1}.[hdl{2} <- AEAD_Oracles.keycount{1} - 1].[hdl1] = AEAD_Oracles.keycount{1} - 1); first by smt(@SmtMap).
  move  : H21;rewrite H25 => H21.
  move => *.
  have ? : (hdl' \in C_ZOracles.keyids{1}); first by  smt(@SmtMap).
  smt(@SmtMap).

  case (hdl{2} \in AOracles.usedKeys{1}).
  move => *.
  move : H19;rewrite H21;simplify => H19.
  move : H20;rewrite H21;simplify => H20.
  case (hdl1 = hdl{2}); last first.
  smt(@SmtMap @FSet).
  move => *.
  have ? : (oget
        C_ZOracles.keyids{1}.[hdl{2} <- oget C_ZOracles.keyids{1}.[hdl{2}]].[hdl1] = oget
        C_ZOracles.keyids{1}.[hdl1]); first by smt(@SmtMap).
  smt(@SmtMap @FSet).
  move => *.
  move : H19;rewrite H21;simplify => H19.
  move : H20;rewrite H21;simplify => H20.
  case (hdl1 = hdl{2}).
  smt(@SmtMap @FSet).
  move => *.
  have ? : (oget
        C_ZOracles.keyids{1}.[hdl{2} <- oget C_ZOracles.keyids{1}.[hdl{2}]].[hdl1] = oget
        C_ZOracles.keyids{1}.[hdl1]); first by smt(@SmtMap).
  smt(@SmtMap @FSet).  

  smt(@SmtMap).

  case (hdl{2} \in AOracles.usedKeys{1}).
  move => *.
  move : H17;rewrite H20;simplify => H17.
  move : H16;rewrite H20;simplify => H16.
  case (hdl1 = hdl{2}); last first.
  smt(@SmtMap @FSet).
  move => *.
  have ? : (oget
        C_ZOracles.keyids{1}.[hdl{2} <- oget C_ZOracles.keyids{1}.[hdl{2}]].[hdl1] = oget
        C_ZOracles.keyids{1}.[hdl1]); first by smt(@SmtMap).
  smt(@SmtMap @FSet).
  move => *.
  move : H17;rewrite H20;simplify => H17.
  case (hdl1 <> hdl{2}).
  smt(@SmtMap @FSet).
  move => *.
  have -> : (oget C_ZOracles.keyids{1}.[hdl{2} <- AEAD_Oracles.keycount{1} - 1].[hdl1] = AEAD_Oracles.keycount{1} - 1); first by smt(@SmtMap).
  case (c0 = cL); first by smt(@SmtMap @FSet).
  move => *.
  have ? : ((hdl1, c0) \in
     RealCryptoService.table{2}); first by smt(@SmtMap).
  have ? : ((oget C_ZOracles.keyids{1}.[hdl1], emptyD, c0) \in  AEAD_Oracles.cphlist{1}).
  smt(@SmtMap).
  smt(@SmtMap).

  smt(@SmtMap @FSet).
  smt(@SmtMap @FSet).


+ proc;inline *.
  sp; if => //=.
  seq 1 1 : (#pre /\ ={tk,cmd,st,opld}). wp.
  call(_: ={glob IAPI, glob CryptoAPIT, AOracles.count_corr, AOracles.count_insr, RealCryptoService.table, AOracles.corrupted} /\ 
           fdom AOracles.usedKeys{1} = fdom AOracles.usedKeys{2} /\
           C_ZOracles.keycount{1} = AEAD_Oracles.keycount{1} /\ 
           fdom AOracles.usedKeys{1} = fdom C_ZOracles.keyids{1} /\
           (0 <= C_ZOracles.keycount{1}) /\
           (AOracles.usedKeys{1} <> empty => 0< C_ZOracles.keycount{1}) /\
           (forall hdl, hdl \in AOracles.usedKeys{1} => 
               (0 <= oget C_ZOracles.keyids{1}.[hdl] < AEAD_Oracles.keycount{1}) /\
                (AOracles.usedKeys{2}.[hdl] = 
                   AEAD_Oracles.keylist{1}.[oget C_ZOracles.keyids{1}.[hdl]])) /\
           (forall hdl hdl', hdl \in C_ZOracles.keyids{1} => 
                             hdl' \in C_ZOracles.keyids{1} => 
                     ((hdl = hdl') <=>
               (oget C_ZOracles.keyids{1}.[hdl] = oget C_ZOracles.keyids{1}.[hdl']))) /\
           (forall hdl c, (hdl \in C_ZOracles.keyids{1} => 
                ((oget C_ZOracles.keyids{1}.[hdl], emptyD, c) \in AEAD_Oracles.cphlist{1} => 
                   (hdl,c) \in RealCryptoService.table{1})) /\
                ((hdl,c) \in RealCryptoService.table{1} => (hdl \in C_ZOracles.keyids{1} /\
                 (oget C_ZOracles.keyids{1}.[hdl], emptyD, c) \in AEAD_Oracles.cphlist{1}))) /\
           (forall kid ad c, 
                     ((kid,ad, c) \in AEAD_Oracles.cphlist{1} => 0 <= kid < AEAD_Oracles.keycount{1}))/\
           C_ZOracles.count_znew{1} = RealCryptoService.count_znew{2} /\
           C_ZOracles.count_test{1} = RealCryptoService.count_test{2}).
  proc;inline *.
  sp; if => //=.
  by wp;call(_:true);auto => />.
  proc;inline *.
  sp; if => //=.
  by wp;call(_:true);auto => />.
  proc;inline *.
  sp; if => //=.
  sp; if => //=.
  smt(@SmtMap).
  sp; seq 1 1 : (#pre /\ ={ko0}); first by call(_:true);wp;skip;progress.
  by wp;skip;progress.
  by wp;skip;progress.
  proc;inline *.
  sp; if => //=.
  sp; if => //=.
  sp; seq 1 1 : (#pre /\ ={ko}); first by call(_:true);wp;skip;progress.
  sp;if => //=.  
  by wp;rnd;skip;progress.
  by wp;skip;progress.
  by wp;skip;progress.
  proc;inline *.
  sp;if => //=.  
  sp; if => //=.
  sp; seq 1 1 : (#pre /\ ={ko}); first by call(_:true);wp;skip;progress.
  by wp;skip;progress.
  by wp;skip;progress;smt().
  by wp;skip;progress.
  if => //=.
  sp.
  seq 2 2 : (#pre /\ ={ko,key});first by wp;call(_:true);auto => />.
  if => //=.
  seq 1 1 : (#pre /\ key0{1} = rkey{2}); first by auto => />.
  seq 4 0 : (#[/:27,28:-14,-13:]pre /\ 
                     (forall (hdl : Hdl),
                       hdl \in AOracles.usedKeys{1} =>
                       0 <= oget C_ZOracles.keyids{1}.[hdl] < AEAD_Oracles.keycount{1} - 1 /\
                       AOracles.usedKeys{2}.[hdl] =
                       AEAD_Oracles.keylist{1}.[oget C_ZOracles.keyids{1}.[hdl]]) /\
                   (0 < C_ZOracles.keycount{1}) /\
                   (Some key0 = AEAD_Oracles.keylist.[AEAD_Oracles.keycount - 1]){1} /\
                   (kid = if hdl \in AOracles.usedKeys 
                           then oget C_ZOracles.keyids.[hdl] 
                           else (C_ZOracles.keycount - 1)){1} /\
                   (forall (kid1 : int) (ad : AData) (c0 : AE.Cph),
                        (kid1, ad, c0) \in AEAD_Oracles.cphlist{1} =>
                        0 <= kid1 < AEAD_Oracles.keycount{1}-1)).
  wp;skip;progress; smt(@SmtMap).

  case (!((hdl, cph) \in RealCryptoService.table){2}).
  move => *.
  rcondt {1} 5.
  move => *; wp;skip;progress;smt(@SmtMap).
  
  wp;skip;progress;smt(@SmtMap).
  wp;skip;progress;smt(@SmtMap).
by wp;skip;progress;smt().  
by wp;skip;progress;smt().  

+ proc*.
  call(_:  ={glob IAPI, glob CryptoAPIT,AOracles.count_corr, AOracles.count_insr,  RealCryptoService.table, AOracles.corrupted} /\ 
           fdom AOracles.usedKeys{1} = fdom AOracles.usedKeys{2} /\
           C_ZOracles.keycount{1} = AEAD_Oracles.keycount{1} /\ 
           fdom AOracles.usedKeys{1} = fdom C_ZOracles.keyids{1} /\
           (0 <= C_ZOracles.keycount{1}) /\
           (AOracles.usedKeys{1} <> empty => 0< C_ZOracles.keycount{1}) /\
           (forall hdl, hdl \in AOracles.usedKeys{1} => 
               (0 <= oget C_ZOracles.keyids{1}.[hdl] < AEAD_Oracles.keycount{1}) /\
                (AOracles.usedKeys{2}.[hdl] = 
                   AEAD_Oracles.keylist{1}.[oget C_ZOracles.keyids{1}.[hdl]])) /\
           (forall hdl hdl', hdl \in C_ZOracles.keyids{1} => 
                             hdl' \in C_ZOracles.keyids{1} => 
                     ((hdl = hdl') <=>
               (oget C_ZOracles.keyids{1}.[hdl] = oget C_ZOracles.keyids{1}.[hdl']))) /\
           (forall hdl c, (hdl \in C_ZOracles.keyids{1} => 
                ((oget C_ZOracles.keyids{1}.[hdl], emptyD, c) \in AEAD_Oracles.cphlist{1} => 
                   (hdl,c) \in RealCryptoService.table{1})) /\
                ((hdl,c) \in RealCryptoService.table{1} => (hdl \in C_ZOracles.keyids{1} /\
                 (oget C_ZOracles.keyids{1}.[hdl], emptyD, c) \in AEAD_Oracles.cphlist{1}))) /\
           (forall kid ad c, 
                     ((kid,ad, c) \in AEAD_Oracles.cphlist{1} => 0 <= kid < AEAD_Oracles.keycount{1}))/\
           C_ZOracles.count_znew{1} = RealCryptoService.count_znew{2} /\
           C_ZOracles.count_test{1} = RealCryptoService.count_test{2}).
  proc;inline *.
  sp; if => //=.
  by wp;call(_:true);auto => />.
  proc;inline *.
  sp; if => //=.
  by wp;call(_:true);auto => />.
  proc;inline *.
  sp; if => //=.
  sp; if => //=.
  smt(@SmtMap).
  sp; seq 1 1 : (#pre /\ ={ko0}); first by call(_:true);wp;skip;progress.
  by wp;skip;progress.
  by wp;skip;progress.
  proc;inline *.
  sp; if => //=.
  sp; if => //=.
  sp; seq 1 1 : (#pre /\ ={ko}); first by call(_:true);wp;skip;progress.
  sp;if => //=.  
  by wp;rnd;skip;progress.
  by wp;skip;progress.
  by wp;skip;progress.
  proc;inline *.
  sp;if => //=.  
  sp; if => //=.
  sp; seq 1 1 : (#pre /\ ={ko}); first by call(_:true);wp;skip;progress.
  by wp;skip;progress.
  by wp;skip;progress;smt().
  by wp;skip;progress.
  by auto => />.
  by auto => />.
qed.

lemma rightHopAEAD &m : 
   Pr[AEAD_LoRCondProb(C(IAPI,Z,A)).game(true) @ &m : res]
    = Pr[ Game4(Z,A,IAPI).main() @ &m : res].
proof.
byequiv.
proc; inline *; wp.
call(_: AEAD_Oracles.b{1} /\  ={glob A, glob IAPI, AOracles.count_corr, AOracles.count_insr, glob CryptoAPIT,  RealCryptoService.table, AOracles.corrupted} /\ 
           fdom AOracles.usedKeys{1} = fdom AOracles.usedKeys{2} /\
           C_ZOracles.keycount{1} = AEAD_Oracles.keycount{1} /\ 
           fdom AOracles.usedKeys{1} = fdom C_ZOracles.keyids{1} /\
           (0 <= C_ZOracles.keycount{1}) /\
           (AOracles.usedKeys{1} <> empty => 0< C_ZOracles.keycount{1}) /\
           (forall hdl, hdl \in AOracles.usedKeys{1} => 
               (0 <= oget C_ZOracles.keyids{1}.[hdl] < AEAD_Oracles.keycount{1}) /\
                (AOracles.usedKeys{2}.[hdl] = 
                   AEAD_Oracles.keylist{1}.[oget C_ZOracles.keyids{1}.[hdl]])) /\
           (forall hdl hdl', hdl \in C_ZOracles.keyids{1} => 
                             hdl' \in C_ZOracles.keyids{1} => 
                     ((hdl = hdl') <=>
               (oget C_ZOracles.keyids{1}.[hdl] = oget C_ZOracles.keyids{1}.[hdl']))) /\
           (forall hdl c, (hdl \in C_ZOracles.keyids{1} => 
                ((oget C_ZOracles.keyids{1}.[hdl], emptyD, c) \in AEAD_Oracles.cphlist{1} => 
                   (hdl,c) \in RealCryptoService.table{1})) /\
                ((hdl,c) \in RealCryptoService.table{1} => (hdl \in C_ZOracles.keyids{1} /\
                 (oget C_ZOracles.keyids{1}.[hdl], emptyD, c) \in AEAD_Oracles.cphlist{1}))) /\
           (forall kid ad c, 
                     ((kid,ad, c) \in AEAD_Oracles.cphlist{1} => 0 <= kid < AEAD_Oracles.keycount{1}))/\
           C_ZOracles.count_znew{1} = RealCryptoService.count_znew{2} /\
           C_ZOracles.count_test{1} = RealCryptoService.count_test{2}); last first.
+ inline *. 
  auto => />.
  smt().
  wp;call(_: true);auto => />.
  progress;smt(@SmtMap @FSet).

+ proc.
  sp; if => //=. wp.
  call(_: ={glob IAPI, glob CryptoAPIT, AOracles.count_corr, AOracles.count_insr,RealCryptoService.table, AOracles.corrupted} /\ 
           fdom AOracles.usedKeys{1} = fdom AOracles.usedKeys{2} /\
           C_ZOracles.keycount{1} = AEAD_Oracles.keycount{1} /\ 
           fdom AOracles.usedKeys{1} = fdom C_ZOracles.keyids{1} /\
           (0 <= C_ZOracles.keycount{1}) /\
           (AOracles.usedKeys{1} <> empty => 0< C_ZOracles.keycount{1}) /\
           (forall hdl, hdl \in AOracles.usedKeys{1} => 
               (0 <= oget C_ZOracles.keyids{1}.[hdl] < AEAD_Oracles.keycount{1}) /\
                (AOracles.usedKeys{2}.[hdl] = 
                   AEAD_Oracles.keylist{1}.[oget C_ZOracles.keyids{1}.[hdl]])) /\
           (forall hdl hdl', hdl \in C_ZOracles.keyids{1} => 
                             hdl' \in C_ZOracles.keyids{1} => 
                     ((hdl = hdl') <=>
               (oget C_ZOracles.keyids{1}.[hdl] = oget C_ZOracles.keyids{1}.[hdl']))) /\
           (forall hdl c, (hdl \in C_ZOracles.keyids{1} => 
                ((oget C_ZOracles.keyids{1}.[hdl], emptyD, c) \in AEAD_Oracles.cphlist{1} => 
                   (hdl,c) \in RealCryptoService.table{1})) /\
                ((hdl,c) \in RealCryptoService.table{1} => (hdl \in C_ZOracles.keyids{1} /\
                 (oget C_ZOracles.keyids{1}.[hdl], emptyD, c) \in AEAD_Oracles.cphlist{1}))) /\
           (forall kid ad c, 
                     ((kid,ad, c) \in AEAD_Oracles.cphlist{1} => 0 <= kid < AEAD_Oracles.keycount{1}))/\
           C_ZOracles.count_znew{1} = RealCryptoService.count_znew{2} /\
           C_ZOracles.count_test{1} = RealCryptoService.count_test{2}).
  proc;inline *.
  sp; if => //=.
  by wp;call(_:true);auto => />.
  proc;inline *.
  sp; if => //=.
  by wp;call(_:true);auto => />.
  proc;inline *.
  sp; if => //=.
  progress.
  sp; if => //=.
  smt(@SmtMap).
  sp; seq 1 1 : (#pre /\ ={ko0}); first by call(_:true);wp;skip;progress.
  by wp;skip;progress.
  by wp;skip;progress.
  proc;inline *.
  sp; if => //=.
  sp; if => //=.
  sp; seq 1 1 : (#pre /\ ={ko}); first by call(_:true);wp;skip;progress.
  sp;if => //=.  
  by wp;rnd;skip;progress.
  by wp;skip;progress.
  by wp;skip;progress.
  proc;inline *.
  sp;if => //=.  
  sp; if => //=.
  sp; seq 1 1 : (#pre /\ ={ko}); first by call(_:true);wp;skip;progress.
  by wp;skip;progress.
  by wp;skip;progress.
  by wp;skip;progress.
+ proc;inline *.
  sp;if => //=.
  seq 1 1 : (#pre /\ ={tk,cmd,st,ocph}).
  call(_: ={glob IAPI, glob CryptoAPIT, AOracles.count_corr, AOracles.count_insr, RealCryptoService.table, AOracles.corrupted} /\ 
           fdom AOracles.usedKeys{1} = fdom AOracles.usedKeys{2} /\
           C_ZOracles.keycount{1} = AEAD_Oracles.keycount{1} /\ 
           fdom AOracles.usedKeys{1} = fdom C_ZOracles.keyids{1} /\
           (0 <= C_ZOracles.keycount{1}) /\
           (AOracles.usedKeys{1} <> empty => 0< C_ZOracles.keycount{1}) /\
           (forall hdl, hdl \in AOracles.usedKeys{1} => 
               (0 <= oget C_ZOracles.keyids{1}.[hdl] < AEAD_Oracles.keycount{1}) /\
                (AOracles.usedKeys{2}.[hdl] = 
                   AEAD_Oracles.keylist{1}.[oget C_ZOracles.keyids{1}.[hdl]])) /\
           (forall hdl hdl', hdl \in C_ZOracles.keyids{1} => 
                             hdl' \in C_ZOracles.keyids{1} => 
                     ((hdl = hdl') <=>
               (oget C_ZOracles.keyids{1}.[hdl] = oget C_ZOracles.keyids{1}.[hdl']))) /\
           (forall hdl c, (hdl \in C_ZOracles.keyids{1} => 
                ((oget C_ZOracles.keyids{1}.[hdl], emptyD, c) \in AEAD_Oracles.cphlist{1} => 
                   (hdl,c) \in RealCryptoService.table{1})) /\
                ((hdl,c) \in RealCryptoService.table{1} => (hdl \in C_ZOracles.keyids{1} /\
                 (oget C_ZOracles.keyids{1}.[hdl], emptyD, c) \in AEAD_Oracles.cphlist{1}))) /\
           (forall kid ad c, 
                     ((kid,ad, c) \in AEAD_Oracles.cphlist{1} => 0 <= kid < AEAD_Oracles.keycount{1}))/\
           C_ZOracles.count_znew{1} = RealCryptoService.count_znew{2} /\
           C_ZOracles.count_test{1} = RealCryptoService.count_test{2}).
  proc;inline *.
  sp; if => //=.
  by wp;call(_:true);auto => />.
  proc;inline *.
  sp; if => //=.
  by wp;call(_:true);auto => />.
  proc;inline *.
  sp; if => //=.
  progress.
  sp; if => //=.
  smt(@SmtMap).
  sp; seq 1 1 : (#pre /\ ={ko0}); first by call(_:true);wp;skip;progress.
  by wp;skip;progress.
  by wp;skip;progress.
  proc;inline *.
  sp; if => //=.
  sp; if => //=.
  sp; seq 1 1 : (#pre /\ ={ko}); first by call(_:true);wp;skip;progress.
  sp;if => //=.  
  by wp;rnd;skip;progress.
  by wp;skip;progress.
  by wp;skip;progress.
  proc;inline *.
  sp;if => //=.  
  sp; if => //=.
  sp; seq 1 1 : (#pre /\ ={ko}); first by call(_:true);wp;skip;progress.
  by wp;skip;progress.
  by wp;skip;progress;smt().
  by wp;skip;progress.
  if => //=.
  sp.
  seq 2 2 : (#pre /\ ={ko,key});first by wp;call(_:true);auto => />.
  if => //=.
  seq 1 1 : (#pre /\ key0{1} = rkey{2}); first by auto => />.
  seq 4 0 : (#[/:25,26:-10,-11:]pre /\ 
                     (forall (hdl : Hdl),
                       hdl \in AOracles.usedKeys{1} =>
                       0 <= oget C_ZOracles.keyids{1}.[hdl] < AEAD_Oracles.keycount{1} - 1 /\
                       AOracles.usedKeys{2}.[hdl] =
                       AEAD_Oracles.keylist{1}.[oget C_ZOracles.keyids{1}.[hdl]]) /\
                   (0 < C_ZOracles.keycount{1}) /\
                   (Some key0 = AEAD_Oracles.keylist.[AEAD_Oracles.keycount - 1]){1} /\
                   (kid = if hdl \in AOracles.usedKeys 
                           then oget C_ZOracles.keyids.[hdl] 
                           else (C_ZOracles.keycount - 1)){1} /\
                   (forall (kid1 : int) (ad : AData) (c0 : AE.Cph),
                        (kid1, ad, c0) \in AEAD_Oracles.cphlist{1} =>
                        0 <= kid1 < AEAD_Oracles.keycount{1}-1)).

  wp;skip;progress; smt(@SmtMap).

  rcondt {1} 6.
  move => *;wp;skip;progress;smt(@SmtMap).
  wp;rnd;wp;skip;progress.
  smt(@SmtMap).
  smt(@SmtMap).
  smt(@SmtMap).
  smt(@SmtMap).
  smt(@SmtMap).
  case (hdl1 = hdl{2});smt(@SmtMap).
  case (hdl1 = hdl{2});smt(@SmtMap).

  case (hdl1 <> hdl{2}).
  smt(@SmtMap).
  move => *.
  case (hdl{2} \in  AOracles.usedKeys{1});last first.
  smt(@SmtMap).
  move => *.
  have ? : (hdl{2} \in  AOracles.usedKeys{2}).
  smt(@SmtMap).
  rewrite H22;simplify.
  have -> : (hdl1 = hdl{2}); first by smt(@SmtMap).
  have -> : (AOracles.usedKeys{2}.[hdl{2} <- oget AOracles.usedKeys{2}.[hdl{2}]].[hdl{2}] = AOracles.usedKeys{2}.[hdl{2}]).
  have inmap : (exists a, Some a =  AOracles.usedKeys{2}.[hdl{2}]); first by smt(set_valE).
  smt(@SmtMap).
  smt(@SmtMap).

  case (hdl{2} \in AOracles.usedKeys{1}).
  case(hdl1=hdl{2});smt(@SmtMap).
  move => *.
  move : H19;rewrite H22;simplify => H19.
  move : H20;rewrite H22;simplify => H20.
  move : H21;rewrite H22;simplify => H21.
  have ? : (hdl{2} \notin C_ZOracles.keyids{1}); first by smt(@SmtMap).
  case (hdl1 <> hdl{2}).
  case (hdl' <> hdl{2}).
  smt(@SmtMap).
  move => *.
  have ? : (hdl1 \in C_ZOracles.keyids{1}); first by  smt(@SmtMap).
  smt(@SmtMap).
  move => *.
  case (hdl' <> hdl{2}); last first.
  smt(@SmtMap).
  have ? : (oget
       C_ZOracles.keyids{1}.[hdl{2} <- AEAD_Oracles.keycount{1} - 1].[hdl1] = AEAD_Oracles.keycount{1} - 1); first by smt(@SmtMap).
  move  : H21;rewrite H25 => H21.
  move => *.
  have ? : (hdl' \in C_ZOracles.keyids{1}); first by  smt(@SmtMap).
  smt(@SmtMap).

  case (hdl{2} \in AOracles.usedKeys{1}).
  move => *.
  move : H19;rewrite H21;simplify => H19.
  move : H20;rewrite H21;simplify => H20.
  case (hdl1 = hdl{2}); last first.
  smt(@SmtMap @FSet).
  move => *.
  have ? : (oget
        C_ZOracles.keyids{1}.[hdl{2} <- oget C_ZOracles.keyids{1}.[hdl{2}]].[hdl1] = oget
        C_ZOracles.keyids{1}.[hdl1]); first by smt(@SmtMap).
  smt(@SmtMap @FSet).
  move => *.
  move : H19;rewrite H21;simplify => H19.
  move : H20;rewrite H21;simplify => H20.
  case (hdl1 = hdl{2}).
  smt(@SmtMap @FSet).
  move => *.
  have ? : (oget
        C_ZOracles.keyids{1}.[hdl{2} <- oget C_ZOracles.keyids{1}.[hdl{2}]].[hdl1] = oget
        C_ZOracles.keyids{1}.[hdl1]); first by smt(@SmtMap).
  smt(@SmtMap @FSet).  

  smt(@SmtMap).

  case (hdl{2} \in AOracles.usedKeys{1}).
  move => *.
  move : H17;rewrite H20;simplify => H17.
  move : H16;rewrite H20;simplify => H16.
  case (hdl1 = hdl{2}); last first.
  smt(@SmtMap @FSet).
  move => *.
  have ? : (oget
        C_ZOracles.keyids{1}.[hdl{2} <- oget C_ZOracles.keyids{1}.[hdl{2}]].[hdl1] = oget
        C_ZOracles.keyids{1}.[hdl1]); first by smt(@SmtMap).
  smt(@SmtMap @FSet).
  move => *.
  move : H17;rewrite H20;simplify => H17.
  case (hdl1 <> hdl{2}).
  smt(@SmtMap @FSet).
  move => *.
  have -> : (oget C_ZOracles.keyids{1}.[hdl{2} <- AEAD_Oracles.keycount{1} - 1].[hdl1] = AEAD_Oracles.keycount{1} - 1); first by smt(@SmtMap).
  case (c0 = cL); first by smt(@SmtMap @FSet).
  move => *.
  have ? : ((hdl1, c0) \in
     RealCryptoService.table{2}); first by smt(@SmtMap).
  have ? : ((oget C_ZOracles.keyids{1}.[hdl1], emptyD, c0) \in  AEAD_Oracles.cphlist{1}).
  smt(@SmtMap).
  smt(@SmtMap).

  smt(@SmtMap @FSet).
  smt(@SmtMap @FSet).

by wp;skip;progress.
by wp;skip;progress.

+ proc;inline *.
  sp; if => //=.
  seq 1 1 : (#pre /\ ={tk,cmd,st,opld}).
  call(_: ={glob IAPI, glob CryptoAPIT, AOracles.count_corr, AOracles.count_insr,  RealCryptoService.table, AOracles.corrupted} /\ 
           fdom AOracles.usedKeys{1} = fdom AOracles.usedKeys{2} /\
           C_ZOracles.keycount{1} = AEAD_Oracles.keycount{1} /\ 
           fdom AOracles.usedKeys{1} = fdom C_ZOracles.keyids{1} /\
           (0 <= C_ZOracles.keycount{1}) /\
           (AOracles.usedKeys{1} <> empty => 0< C_ZOracles.keycount{1}) /\
           (forall hdl, hdl \in AOracles.usedKeys{1} => 
               (0 <= oget C_ZOracles.keyids{1}.[hdl] < AEAD_Oracles.keycount{1}) /\
                (AOracles.usedKeys{2}.[hdl] = 
                   AEAD_Oracles.keylist{1}.[oget C_ZOracles.keyids{1}.[hdl]])) /\
           (forall hdl hdl', hdl \in C_ZOracles.keyids{1} => 
                             hdl' \in C_ZOracles.keyids{1} => 
                     ((hdl = hdl') <=>
               (oget C_ZOracles.keyids{1}.[hdl] = oget C_ZOracles.keyids{1}.[hdl']))) /\
           (forall hdl c, (hdl \in C_ZOracles.keyids{1} => 
                ((oget C_ZOracles.keyids{1}.[hdl], emptyD, c) \in AEAD_Oracles.cphlist{1} => 
                   (hdl,c) \in RealCryptoService.table{1})) /\
                ((hdl,c) \in RealCryptoService.table{1} => (hdl \in C_ZOracles.keyids{1} /\
                 (oget C_ZOracles.keyids{1}.[hdl], emptyD, c) \in AEAD_Oracles.cphlist{1}))) /\
           (forall kid ad c, 
                     ((kid,ad, c) \in AEAD_Oracles.cphlist{1} => 0 <= kid < AEAD_Oracles.keycount{1}))/\
           C_ZOracles.count_znew{1} = RealCryptoService.count_znew{2} /\
           C_ZOracles.count_test{1} = RealCryptoService.count_test{2}).
  proc;inline *.
  sp; if => //=.
  by wp;call(_:true);auto => />.
  proc;inline *.
  sp; if => //=.
  by wp;call(_:true);auto => />.
  proc;inline *.
  sp; if => //=.
  progress.
  sp; if => //=.
  smt(@SmtMap).
  sp; seq 1 1 : (#pre /\ ={ko0}); first by call(_:true);wp;skip;progress.
  by wp;skip;progress.
  by wp;skip;progress.
  proc;inline *.
  sp; if => //=.
  sp; if => //=.
  sp; seq 1 1 : (#pre /\ ={ko}); first by call(_:true);wp;skip;progress.
  sp;if => //=.  
  by wp;rnd;skip;progress.
  by wp;skip;progress.
  by wp;skip;progress.
  proc;inline *.
  sp;if => //=.  
  sp; if => //=.
  sp; seq 1 1 : (#pre /\ ={ko}); first by call(_:true);wp;skip;progress.
  by wp;skip;progress.
  by wp;skip;progress;smt(@SmtMap).
  by wp;skip;progress.
  if => //=.
  sp.
  seq 2 2 : (#pre /\ ={ko,key});first by wp;call(_:true);auto => />.
  if => //=.
  seq 1 1 : (#pre /\ key0{1} = rkey{2}); first by auto => />.

  seq 4 0 : (#[/:25,26:-11,-10:]pre /\ 
                     (forall (hdl : Hdl),
                       hdl \in AOracles.usedKeys{1} =>
                       0 <= oget C_ZOracles.keyids{1}.[hdl] < AEAD_Oracles.keycount{1} - 1 /\
                       AOracles.usedKeys{2}.[hdl] =
                       AEAD_Oracles.keylist{1}.[oget C_ZOracles.keyids{1}.[hdl]]) /\
                   (0 < C_ZOracles.keycount{1}) /\
                   (Some key0 = AEAD_Oracles.keylist.[AEAD_Oracles.keycount - 1]){1} /\
                   (kid = if hdl \in AOracles.usedKeys 
                           then oget C_ZOracles.keyids.[hdl] 
                           else (C_ZOracles.keycount - 1)){1} /\
                   (forall (kid1 : int) (ad : AData) (c0 : AE.Cph),
                        (kid1, ad, c0) \in AEAD_Oracles.cphlist{1} =>
                        0 <= kid1 < AEAD_Oracles.keycount{1}-1)).
  wp;skip;progress; smt(@SmtMap).

  case (!((hdl, cph) \in RealCryptoService.table){2}).
  move => *.
  rcondt {1} 5.
  move => *; wp;skip;progress;smt(@SmtMap).
  
  wp;skip;progress;smt(@SmtMap).
  wp;skip;progress; smt(@SmtMap).
by wp;skip;progress.
by wp;skip;progress.
  
+ proc*.
  call(_:  ={glob IAPI, glob CryptoAPIT, AOracles.count_corr, AOracles.count_insr,  RealCryptoService.table, AOracles.corrupted} /\ 
           fdom AOracles.usedKeys{1} = fdom AOracles.usedKeys{2} /\
           C_ZOracles.keycount{1} = AEAD_Oracles.keycount{1} /\ 
           fdom AOracles.usedKeys{1} = fdom C_ZOracles.keyids{1} /\
           (0 <= C_ZOracles.keycount{1}) /\
           (AOracles.usedKeys{1} <> empty => 0< C_ZOracles.keycount{1}) /\
           (forall hdl, hdl \in AOracles.usedKeys{1} => 
               (0 <= oget C_ZOracles.keyids{1}.[hdl] < AEAD_Oracles.keycount{1}) /\
                (AOracles.usedKeys{2}.[hdl] = 
                   AEAD_Oracles.keylist{1}.[oget C_ZOracles.keyids{1}.[hdl]])) /\
           (forall hdl hdl', hdl \in C_ZOracles.keyids{1} => 
                             hdl' \in C_ZOracles.keyids{1} => 
                     ((hdl = hdl') <=>
               (oget C_ZOracles.keyids{1}.[hdl] = oget C_ZOracles.keyids{1}.[hdl']))) /\
           (forall hdl c, (hdl \in C_ZOracles.keyids{1} => 
                ((oget C_ZOracles.keyids{1}.[hdl], emptyD, c) \in AEAD_Oracles.cphlist{1} => 
                   (hdl,c) \in RealCryptoService.table{1})) /\
                ((hdl,c) \in RealCryptoService.table{1} => (hdl \in C_ZOracles.keyids{1} /\
                 (oget C_ZOracles.keyids{1}.[hdl], emptyD, c) \in AEAD_Oracles.cphlist{1}))) /\
           (forall kid ad c, 
                     ((kid,ad, c) \in AEAD_Oracles.cphlist{1} => 0 <= kid < AEAD_Oracles.keycount{1}))/\
           C_ZOracles.count_znew{1} = RealCryptoService.count_znew{2} /\
           C_ZOracles.count_test{1} = RealCryptoService.count_test{2}).
  proc;inline *.
  sp; if => //=.
  by wp;call(_:true);auto => />.
  proc;inline *.
  sp; if => //=.
  by wp;call(_:true);auto => />.
  proc;inline *.
  sp; if => //=.
  progress.
  sp; if => //=.
  smt(@SmtMap).
  sp; seq 1 1 : (#pre /\ ={ko0}); first by call(_:true);wp;skip;progress.
  by wp;skip;progress.
  by wp;skip;progress.
  proc;inline *.
  sp; if => //=.
  sp; if => //=.
  sp; seq 1 1 : (#pre /\ ={ko}); first by call(_:true);wp;skip;progress.
  sp;if => //=.  
  by wp;rnd;skip;progress.
  by wp;skip;progress.
  by wp;skip;progress.
  proc;inline *.
  sp;if => //=.  
  sp; if => //=.
  sp; seq 1 1 : (#pre /\ ={ko}); first by call(_:true);wp;skip;progress.
  by wp;skip;progress.
  by wp;skip;progress;smt().
  by wp;skip;progress.
  by auto => />.
  by auto => />.
qed.

end section.


(** Hop 6: Ind => back to real keys **)

module Hop6CryptoService(API : CryptoAPI, A : Adv) = {
  module HS0 = Hop0CryptoService(API,A) 
  include HS0 [-enc,dec]

  proc enc(hdl : Hdl, pld : Pld, st : St) : Cph option * St =  {
     var tk,cmd,key,ocph,cph;
     ocph <- None;
     if (HS0.RS.count_test < q_test) {
        (tk,cmd,st) <@ HS0.RS.A.encParam(hdl,st);
        if ((valid CryptoAPIT.t tk hdl cmd)
               && !(hdl \in AOracles.corrupted)) {
           key <@ API.tkReveal(tk,hdl,cmd);
           if (key <> None) {
              cph <$ enc (oget key) emptyD witness;
              AOracles.usedKeys.[hdl]<- oget key;
              RealCryptoService.table.[(hdl,cph)]<-pld;
              ocph <- Some cph;
           }
        }
        HS0.RS.count_test <- HS0.RS.count_test + 1;
     }
     return (ocph,st);
  }

  proc dec(hdl : Hdl, cph : Cph, st : St) : Pld option * St =  { 
     var tk,cmd,key,opld;
     opld <- None;
     if (HS0.RS.count_test < q_test) {
        (tk,cmd,st) <@ HS0.RS.A.decParam(hdl,st);
        if ((valid CryptoAPIT.t tk hdl cmd)
               && !(hdl \in AOracles.corrupted)) {
           key <@ API.tkReveal(tk,hdl,cmd);
           if (key <> None) {
              opld <- dec (oget key) emptyD cph;
              AOracles.usedKeys.[hdl]<- oget key;
              Hop0CryptoService.bad <- 
                    Hop0CryptoService.bad || 
                       (!(opld = RealCryptoService.table.[(hdl,cph)]));
              opld <- RealCryptoService.table.[(hdl,cph)];
           }
        }
        HS0.RS.count_test <- HS0.RS.count_test + 1;
     }
     return (opld,st);
  }

}.

module Game6(Z : Env, A : Adv, IAPI : ICryptoAPI)  = {
  module API = CryptoAPIT(IAPI)
  module HS6 = Hop6CryptoService(API,A) 

  proc main() = {
     var b;
     IAPI.init();
     API.init();
     AOracles(IAPI).init();
     HS6.init();
     b <@ Z(HS6).guess();
     return b;
  }   
  
}.

(* Ind adv *)

module B_ZOracles1(API : IndO_T, A : Adv) = {
  module BZO = B_ZOracles(API,A)
  include BZO [-enc,dec]

  proc enc(hdl : Hdl, pld : Pld, st : St) : Cph option * St =  {
     var tk,cmd,key,ocph,cph;
     ocph <- None;
     if (B_ZOracles.count_test < q_test) {
        (tk,cmd,st) <@ BZO.A.encParam(hdl,st);
        if ((valid B_AOracles.t tk hdl cmd)
               && !(hdl \in B_AOracles.corrupted)) {
           key <@ API.test(tk,hdl,cmd);
           if (key <> None) {
              cph <$ enc (oget key) emptyD witness;
              B_AOracles.usedKeys <- B_AOracles.usedKeys `|` fset1 hdl;          
              B_ZOracles.table.[(hdl,cph)]<-pld;
              ocph <- Some cph;
          }
        }
        B_ZOracles.count_test <- B_ZOracles.count_test + 1;
     }
     return (ocph,st);
  }

  proc dec(hdl : Hdl, cph : Cph, st : St) : Pld option * St =  { 
     var tk,cmd,key,opld;
     opld <- None;
     if (B_ZOracles.count_test < q_test) {
        (tk,cmd,st) <@ BZO.A.decParam(hdl,st);
        if ((valid B_AOracles.t tk hdl cmd)
               && !(hdl \in B_AOracles.corrupted)) {
           key <@ API.test(tk,hdl,cmd);
           if (key <> None) {
              opld <- B_ZOracles.table.[(hdl,cph)];
              B_AOracles.usedKeys <- B_AOracles.usedKeys `|` fset1 hdl;  
           }     
        }
        B_ZOracles.count_test <- B_ZOracles.count_test + 1;
     }
     return (opld,st);
  }
}.

module B1(Z : Env, A : Adv,O : IndO_T)  = {
   module ZO = B_ZOracles1(O,A) 

   proc guess() = {
      var b;
      ZO.init();
      b <@ Z(ZO).guess();
      return b;
   }
}.

section.

declare module A : Adv {CryptoAPIT,Hop0CryptoService, IndO, B_ZOracles }.
declare module Z : Env {CryptoAPIT,Hop0CryptoService, A, IndO, B_ZOracles}.
declare module IAPI : ICryptoAPI {CryptoAPIT,Hop0CryptoService, Z, A, IndO, B_ZOracles}.

lemma LeftHop2 &m : 
   Pr [ Ind(B1(Z,A),IAPI).main(true) @ &m : res] =
      Pr[ Game4(Z,A,IAPI).main() @ &m : res].
proof.
byequiv.
proc; inline *; wp.
call(_: IndO.b{1} /\  ={glob A, glob IAPI, glob CryptoAPIT} /\ 
            B_AOracles.corrupted{1} =  AOracles.corrupted{2} /\ 
            IndO.corrupted{1} = AOracles.corrupted{2} /\ 
            IndO.tested{1} = AOracles.usedKeys{2} /\ 
            B_AOracles.usedKeys{1} = fdom AOracles.usedKeys{2} /\  
            B_ZOracles.table{1} = RealCryptoService.table{2} /\
            B_AOracles.count_new{1} = CryptoAPIT.count_new{2} /\
            B_AOracles.count_mng{1} = CryptoAPIT.count_mng{2} /\
            B_AOracles.count_corr{1} = AOracles.count_corr{2} /\
            IndO.count_corr{1} <= AOracles.count_corr{2} /\
            B_AOracles.count_insr{1} = AOracles.count_insr{2} /\
            IndO.count_insr{1} <= AOracles.count_insr{2} /\
            B_ZOracles.count_znew{1} = RealCryptoService.count_znew{2} /\
            B_ZOracles.count_test{1} = RealCryptoService.count_test{2} /\
            IndO.count_test{1} <= RealCryptoService.count_test{2} /\
            B_AOracles.t{1} = CryptoAPIT.t{2});last first.
+ wp;call(_:true);skip;progress;smt(@SmtMap).
+ proc.
  if => //=.  
  wp.
  call(_: ={glob IAPI, glob CryptoAPIT} /\ 
            B_AOracles.corrupted{1} =  AOracles.corrupted{2} /\ 
            IndO.corrupted{1} = AOracles.corrupted{2} /\ 
            IndO.tested{1} = AOracles.usedKeys{2} /\ 
            B_AOracles.usedKeys{1} = fdom AOracles.usedKeys{2} /\  
            B_ZOracles.table{1} = RealCryptoService.table{2} /\
            B_AOracles.count_new{1} = CryptoAPIT.count_new{2} /\
            B_AOracles.count_mng{1} = CryptoAPIT.count_mng{2} /\
            B_AOracles.count_corr{1} = AOracles.count_corr{2} /\
            IndO.count_corr{1} <= AOracles.count_corr{2} /\
            B_AOracles.count_insr{1} = AOracles.count_insr{2} /\
            IndO.count_insr{1} <= AOracles.count_insr{2} /\
            B_ZOracles.count_znew{1} = RealCryptoService.count_znew{2} /\
            B_ZOracles.count_test{1} = RealCryptoService.count_test{2} /\
            IndO.count_test{1} <= RealCryptoService.count_test{2} /\
            B_AOracles.t{1} = CryptoAPIT.t{2}).
  proc; inline *.
  sp;if => //=.
  rcondt {1} 5; first by auto => />.
  by wp;call(_:true);auto => />.
  proc; inline *.
  sp;if => //=.
  rcondt {1} 4; first by auto => />.
  by wp;call(_:true);auto => />.
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. smt(@SmtMap).
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt(@SmtMap).
  by wp;call(_:true);wp;skip;progress;smt().
  by wp;skip;progress;smt().
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. 
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt().
  sp.
  seq 1 1 : (#pre /\ ko0{1} = ko{2}); first by call(_:true);skip;auto => />.
  sp;if => //=.
  by wp;rnd;wp;skip;progress;smt().
  by wp;skip;progress;smt().
  by wp;skip;progress;smt().
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. 
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt().
  sp.
  seq 1 1 : (#pre /\ ko0{1} = ko{2}); first by call(_:true);skip;auto => />.
  by wp;skip;progress;smt().
  by wp;skip;progress;smt().
  by wp;skip;progress;smt().
+ proc; inline *.
  seq 1 1 : (#pre /\ ={ocph}); first by auto => />.
  if => //=.
  seq 1 1 : (#pre /\ ={tk,cmd,st}).
  call(_: ={glob IAPI, glob CryptoAPIT} /\ 
            B_AOracles.corrupted{1} =  AOracles.corrupted{2} /\ 
            IndO.corrupted{1} = AOracles.corrupted{2} /\ 
            IndO.tested{1} = AOracles.usedKeys{2} /\ 
            B_AOracles.usedKeys{1} = fdom AOracles.usedKeys{2} /\  
            B_ZOracles.table{1} = RealCryptoService.table{2} /\
            B_AOracles.count_new{1} = CryptoAPIT.count_new{2} /\
            B_AOracles.count_mng{1} = CryptoAPIT.count_mng{2} /\
            B_AOracles.count_corr{1} = AOracles.count_corr{2} /\
            IndO.count_corr{1} <= AOracles.count_corr{2} /\
            B_AOracles.count_insr{1} = AOracles.count_insr{2} /\
            IndO.count_insr{1} <= AOracles.count_insr{2} /\
            B_ZOracles.count_znew{1} = RealCryptoService.count_znew{2} /\
            B_ZOracles.count_test{1} = RealCryptoService.count_test{2} /\
            IndO.count_test{1} <= RealCryptoService.count_test{2} /\
            B_AOracles.t{1} = CryptoAPIT.t{2}).
  proc; inline *.
  sp;if => //=.
  rcondt {1} 5; first by auto => />.
  by wp;call(_:true);auto => />.
  proc; inline *.
  sp;if => //=.
  rcondt {1} 4; first by auto => />.
  by wp;call(_:true);auto => />.
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. smt(@SmtMap).
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt(@SmtMap).
  by wp;call(_:true);wp;skip;progress;smt().
  by wp;skip;progress;smt().
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. 
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt().
  sp.
  seq 1 1 : (#pre /\ ko0{1} = ko{2}); first by call(_:true);skip;auto => />.
  case (ko0{1} = None).
  rcondf {1} 4; first by move => *;wp;skip;progress => /#.
  rcondf {2} 2; first by move => *;wp;skip;progress => /#.
  by wp;skip;progress;smt().
  rcondt {1} 4; first by move => *;wp;skip;progress => /#.
  rcondt {2} 2; first by move => *;wp;skip;progress => /#.
  wp;rnd;wp;skip;progress;smt().
  wp;skip;progress;smt().
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. 
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt().
  sp.
  seq 1 1 : (#pre /\ ko0{1} = ko{2}); first by call(_:true);skip;auto => />.
  case (ko0{1} = None).
  rcondf {1} 4; first by move => *;wp;skip;progress => /#.
  rcondf {2} 2; first by move => *;wp;skip;progress => /#.
  by wp;skip;progress;smt().
  rcondt {1} 4; first by move => *;wp;skip;progress => /#.
  rcondt {2} 2; first by move => *;wp;skip;progress => /#.
  wp;skip;progress;smt().
  wp;skip;progress;smt().
  wp;skip;progress;smt().
  sp; if => //=.
  rcondt {1} 6.
  move => *;wp;rnd;wp;skip;progress;smt().
  rcondt {1} 6; first by auto => />.
  swap {1} 5 4; sp.
  seq 1 1 : (#pre /\ ko0{1} = ko{2}); first by call(_:true);skip;auto => />.
  case (ko0{1} = None).
  rcondf {1} 3; first by move => *;wp;rnd;skip;progress => /#.
  rcondf {1} 5; first by move => *;wp;rnd;skip;progress => /#.
  rcondf {2} 2; first by move => *;wp;skip;progress => /#.
  wp;rnd{1};skip;progress.
  by apply gen_ll.
  by smt().
  rcondt {1} 3; first by move => *;wp;rnd;skip;progress => /#.
  rcondt {2} 2; first by move => *;wp;skip;progress => /#.
  rcondt {1} 7; first by move => *;wp;rnd;skip;progress => /#.

  wp;rnd;wp;rnd;wp;skip;progress; smt(@SmtMap).
  wp;skip;progress;smt(@SmtMap).
+ proc; inline *.
  seq 1 1 : (#pre /\ ={opld}); first by auto => />.
  if => //=.
  seq 1 1 : (#pre /\ ={tk,cmd,st}).
  call(_: ={glob IAPI, glob CryptoAPIT} /\ 
            B_AOracles.corrupted{1} =  AOracles.corrupted{2} /\ 
            IndO.corrupted{1} = AOracles.corrupted{2} /\ 
            IndO.tested{1} = AOracles.usedKeys{2} /\ 
            B_AOracles.usedKeys{1} = fdom AOracles.usedKeys{2} /\  
            B_ZOracles.table{1} = RealCryptoService.table{2} /\
            B_AOracles.count_new{1} = CryptoAPIT.count_new{2} /\
            B_AOracles.count_mng{1} = CryptoAPIT.count_mng{2} /\
            B_AOracles.count_corr{1} = AOracles.count_corr{2} /\
            IndO.count_corr{1} <= AOracles.count_corr{2} /\
            B_AOracles.count_insr{1} = AOracles.count_insr{2} /\
            IndO.count_insr{1} <= AOracles.count_insr{2} /\
            B_ZOracles.count_znew{1} = RealCryptoService.count_znew{2} /\
            B_ZOracles.count_test{1} = RealCryptoService.count_test{2} /\
            IndO.count_test{1} <= RealCryptoService.count_test{2} /\
            B_AOracles.t{1} = CryptoAPIT.t{2}).
  proc; inline *.
  sp;if => //=.
  rcondt {1} 5; first by auto => />.
  by wp;call(_:true);auto => />.
  proc; inline *.
  sp;if => //=.
  rcondt {1} 4; first by auto => />.
  by wp;call(_:true);auto => />.
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. smt(@SmtMap).
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt(@SmtMap).
  by wp;call(_:true);wp;skip;progress;smt().
  by wp;skip;progress;smt().
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. 
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt().
  sp.
  seq 1 1 : (#pre /\ ko0{1} = ko{2}); first by call(_:true);skip;auto => />.
  case (ko0{1} = None).
  rcondf {1} 4; first by move => *;wp;skip;progress => /#.
  rcondf {2} 2; first by move => *;wp;skip;progress => /#.
  by wp;skip;progress;smt().
  rcondt {1} 4; first by move => *;wp;skip;progress => /#.
  rcondt {2} 2; first by move => *;wp;skip;progress => /#.
  wp;rnd;wp;skip;progress;smt().
  wp;skip;progress;smt().
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. 
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt().
  sp.
  seq 1 1 : (#pre /\ ko0{1} = ko{2}); first by call(_:true);skip;auto => />.
  case (ko0{1} = None).
  rcondf {1} 4; first by move => *;wp;skip;progress => /#.
  rcondf {2} 2; first by move => *;wp;skip;progress => /#.
  by wp;skip;progress;smt().
  rcondt {1} 4; first by move => *;wp;skip;progress => /#.
  rcondt {2} 2; first by move => *;wp;skip;progress => /#.
  wp;skip;progress;smt().
  wp;skip;progress;smt().
  wp;skip;progress;smt().
  sp; if => //=.
  rcondt {1} 6.
  move => *;wp;rnd;wp;skip;progress;smt().
  rcondt {1} 6; first by auto => />.
  swap {1} 5 4.
  sp.
  seq 1 1 : (#pre /\ ko0{1} = ko{2}); first by call(_:true);skip;auto => />.
  case (ko0{1} = None).
  rcondf {1} 6; first by move => *;wp;rnd;skip;progress => /#.
  rcondf {1} 3; first by move => *;wp;rnd;skip;progress => /#.
  rcondf {2} 2; first by move => *;wp;skip;progress => /#.
  wp;rnd{1};skip;progress.
  by apply gen_ll.
  by smt().
  rcondt {1} 6; first by move => *;wp;rnd;skip;progress => /#.
  rcondt {2} 2; first by move => *;wp;skip;progress => /#.
  rcondt {1} 3; first by move => *;wp;rnd;skip;progress => /#.
  wp;rnd;wp;skip;progress;smt(@SmtMap).
  wp;skip;progress;smt().
+ proc*.
  call(_: ={glob IAPI, glob CryptoAPIT} /\ 
            B_AOracles.corrupted{1} =  AOracles.corrupted{2} /\ 
            IndO.corrupted{1} = AOracles.corrupted{2} /\ 
            fdom IndO.tested{1} = fdom AOracles.usedKeys{2} /\ 
            B_AOracles.usedKeys{1} = fdom AOracles.usedKeys{2} /\  
            B_ZOracles.table{1} = RealCryptoService.table{2} /\
            B_AOracles.count_new{1} = CryptoAPIT.count_new{2} /\
            B_AOracles.count_mng{1} = CryptoAPIT.count_mng{2} /\
            B_AOracles.count_corr{1} = AOracles.count_corr{2} /\
            IndO.count_corr{1} <= AOracles.count_corr{2} /\
            B_AOracles.count_insr{1} = AOracles.count_insr{2} /\
            IndO.count_insr{1} <= AOracles.count_insr{2} /\
            B_ZOracles.count_znew{1} = RealCryptoService.count_znew{2} /\
            B_ZOracles.count_test{1} = RealCryptoService.count_test{2} /\
            IndO.count_test{1} <= RealCryptoService.count_test{2} /\
            B_AOracles.t{1} = CryptoAPIT.t{2}).
  proc; inline *.
  sp;if => //=.
  rcondt {1} 5; first by auto => />.
  by wp;call(_:true);auto => />.
  proc; inline *.
  sp;if => //=.
  rcondt {1} 4; first by auto => />.
  by wp;call(_:true);auto => />.
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. smt(@SmtMap).
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt(@SmtMap).
  by wp;call(_:true);wp;skip;progress;smt().
  by wp;skip;progress;smt().
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. 
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt().
  sp.
  seq 1 1 : (#pre /\ ko0{1} = ko{2}); first by call(_:true);skip;auto => />.
  case (ko0{1} = None).
  rcondf {1} 4; first by move => *;wp;skip;progress => /#.
  rcondf {2} 2; first by move => *;wp;skip;progress => /#.
  by wp;skip;progress;smt().
  rcondt {1} 4; first by move => *;wp;skip;progress => /#.
  rcondt {2} 2; first by move => *;wp;skip;progress => /#.
  wp;rnd;wp;skip;progress;smt().
  wp;skip;progress;smt().
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. 
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt().
  sp.
  seq 1 1 : (#pre /\ ko0{1} = ko{2}); first by call(_:true);skip;auto => />.
  case (ko0{1} = None).
  rcondf {1} 4; first by move => *;wp;skip;progress => /#.
  rcondf {2} 2; first by move => *;wp;skip;progress => /#.
  by wp;skip;progress;smt().
  rcondt {1} 4; first by move => *;wp;skip;progress => /#.
  rcondt {2} 2; first by move => *;wp;skip;progress => /#.
  wp;skip;progress;smt().
  wp;skip;progress;smt().
  wp;skip;progress;smt().
  by auto => />.
  by auto => />.
qed.


lemma RighttHop2 &m : 
   Pr [ Ind(B1(Z,A),IAPI).main(false) @ &m : !res] =
      Pr[ Game6(Z,A,IAPI).main() @ &m : res].
proof.
byequiv.
proc; inline *; wp.
call(_: !IndO.b{1} /\  ={glob A, glob IAPI, glob CryptoAPIT} /\ 
            B_AOracles.corrupted{1} =  AOracles.corrupted{2} /\ 
            IndO.corrupted{1} = AOracles.corrupted{2} /\ 
            fdom IndO.tested{1} = fdom AOracles.usedKeys{2} /\ 
            B_AOracles.usedKeys{1} = fdom AOracles.usedKeys{2} /\  
            B_ZOracles.table{1} = RealCryptoService.table{2} /\
            B_AOracles.count_new{1} = CryptoAPIT.count_new{2} /\
            B_AOracles.count_mng{1} = CryptoAPIT.count_mng{2} /\
            B_AOracles.count_corr{1} = AOracles.count_corr{2} /\
            IndO.count_corr{1} <= AOracles.count_corr{2} /\
            B_AOracles.count_insr{1} = AOracles.count_insr{2} /\
            IndO.count_insr{1} <= AOracles.count_insr{2} /\
            B_ZOracles.count_znew{1} = RealCryptoService.count_znew{2} /\
            B_ZOracles.count_test{1} = RealCryptoService.count_test{2} /\
            IndO.count_test{1} <= RealCryptoService.count_test{2} /\
            B_AOracles.t{1} = CryptoAPIT.t{2});last first.
+ wp;call(_:true);skip;progress;smt(@SmtMap).
+ proc.
  if => //=.  
  wp.
  call(_: ={glob IAPI, glob CryptoAPIT} /\ 
            B_AOracles.corrupted{1} =  AOracles.corrupted{2} /\ 
            IndO.corrupted{1} = AOracles.corrupted{2} /\ 
            fdom IndO.tested{1} = fdom AOracles.usedKeys{2} /\ 
            B_AOracles.usedKeys{1} = fdom AOracles.usedKeys{2} /\  
            B_ZOracles.table{1} = RealCryptoService.table{2} /\
            B_AOracles.count_new{1} = CryptoAPIT.count_new{2} /\
            B_AOracles.count_mng{1} = CryptoAPIT.count_mng{2} /\
            B_AOracles.count_corr{1} = AOracles.count_corr{2} /\
            IndO.count_corr{1} <= AOracles.count_corr{2} /\
            B_AOracles.count_insr{1} = AOracles.count_insr{2} /\
            IndO.count_insr{1} <= AOracles.count_insr{2} /\
            B_ZOracles.count_znew{1} = RealCryptoService.count_znew{2} /\
            B_ZOracles.count_test{1} = RealCryptoService.count_test{2} /\
            IndO.count_test{1} <= RealCryptoService.count_test{2} /\
            B_AOracles.t{1} = CryptoAPIT.t{2}).
  proc; inline *.
  sp;if => //=.
  rcondt {1} 5; first by auto => />.
  by wp;call(_:true);auto => />.
  proc; inline *.
  sp;if => //=.
  rcondt {1} 4; first by auto => />.
  by wp;call(_:true);auto => />.
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. smt(@SmtMap).
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt(@SmtMap).
  by wp;call(_:true);wp;skip;progress;smt().
  by wp;skip;progress;smt().
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. 
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt().
  sp.
  seq 1 1 : (#pre /\ ko0{1} = ko{2}); first by call(_:true);skip;auto => />.
  sp;if => //=.
  by wp;rnd;wp;skip;progress;smt().
  by wp;skip;progress;smt().
  by wp;skip;progress;smt().
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. 
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt().
  sp.
  seq 1 1 : (#pre /\ ko0{1} = ko{2}); first by call(_:true);skip;auto => />.
  by wp;skip;progress;smt().
  by wp;skip;progress;smt().
  by wp;skip;progress;smt().
+ proc; inline *.
  seq 1 1 : (#pre /\ ={ocph}); first by auto => />.
  if => //=.
  seq 1 1 : (#pre /\ ={tk,cmd,st}).
  call(_: ={glob IAPI, glob CryptoAPIT} /\ 
            B_AOracles.corrupted{1} =  AOracles.corrupted{2} /\ 
            IndO.corrupted{1} = AOracles.corrupted{2} /\ 
            fdom IndO.tested{1} = fdom AOracles.usedKeys{2} /\ 
            B_AOracles.usedKeys{1} = fdom AOracles.usedKeys{2} /\  
            B_ZOracles.table{1} = RealCryptoService.table{2} /\
            B_AOracles.count_new{1} = CryptoAPIT.count_new{2} /\
            B_AOracles.count_mng{1} = CryptoAPIT.count_mng{2} /\
            B_AOracles.count_corr{1} = AOracles.count_corr{2} /\
            IndO.count_corr{1} <= AOracles.count_corr{2} /\
            B_AOracles.count_insr{1} = AOracles.count_insr{2} /\
            IndO.count_insr{1} <= AOracles.count_insr{2} /\
            B_ZOracles.count_znew{1} = RealCryptoService.count_znew{2} /\
            B_ZOracles.count_test{1} = RealCryptoService.count_test{2} /\
            IndO.count_test{1} <= RealCryptoService.count_test{2} /\
            B_AOracles.t{1} = CryptoAPIT.t{2}).
  proc; inline *.
  sp;if => //=.
  rcondt {1} 5; first by auto => />.
  by wp;call(_:true);auto => />.
  proc; inline *.
  sp;if => //=.
  rcondt {1} 4; first by auto => />.
  by wp;call(_:true);auto => />.
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. smt(@SmtMap).
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt(@SmtMap).
  by wp;call(_:true);wp;skip;progress;smt().
  by wp;skip;progress;smt().
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. 
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt().
  sp.
  seq 1 1 : (#pre /\ ko0{1} = ko{2}); first by call(_:true);skip;auto => />.
  case (ko0{1} = None).
  rcondf {1} 4; first by move => *;wp;skip;progress => /#.
  rcondf {2} 2; first by move => *;wp;skip;progress => /#.
  by wp;skip;progress;smt().
  rcondt {1} 4; first by move => *;wp;skip;progress => /#.
  rcondt {2} 2; first by move => *;wp;skip;progress => /#.
  wp;rnd;wp;skip;progress;smt().
  wp;skip;progress;smt().
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. 
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt().
  sp.
  seq 1 1 : (#pre /\ ko0{1} = ko{2}); first by call(_:true);skip;auto => />.
  case (ko0{1} = None).
  rcondf {1} 4; first by move => *;wp;skip;progress => /#.
  rcondf {2} 2; first by move => *;wp;skip;progress => /#.
  by wp;skip;progress;smt().
  rcondt {1} 4; first by move => *;wp;skip;progress => /#.
  rcondt {2} 2; first by move => *;wp;skip;progress => /#.
  wp;skip;progress;smt().
  wp;skip;progress;smt().
  wp;skip;progress;smt().
  sp; if => //=.
  rcondt {1} 6.
  move => *;wp;rnd;wp;skip;progress;smt().
  rcondt {1} 6; first by auto => />.
  sp.
  seq 1 0 : (#pre). by rnd{1}; skip; progress; apply gen_ll.
  sp.
  seq 1 1 : (#pre /\ ko0{1} = ko{2}); first by call(_:true);skip;auto => />.
  case (ko0{1} = None).
  rcondf {1} 5; first by move => *;wp;skip;progress => /#.
  rcondf {1} 2; first by move => *;wp;skip;progress => /#.
  rcondf {2} 2; first by move => *;wp;skip;progress => /#.
  by wp;skip;progress;smt().
  rcondt {1} 2; first by move => *;wp;skip;progress => /#.
  rcondt {2} 2; first by move => *;wp;skip;progress => /#.
  rcondt {1} 6; first by move => *;wp;skip;progress => /#.
  wp;rnd;wp;skip;progress;smt(@SmtMap @FSet).
  wp;skip;progress;smt().
+ proc; inline *.
  seq 1 1 : (#pre /\ ={opld}); first by auto => />.
  if => //=.
  seq 1 1 : (#pre /\ ={tk,cmd,st}).
  call(_: ={glob IAPI, glob CryptoAPIT} /\ 
            B_AOracles.corrupted{1} =  AOracles.corrupted{2} /\ 
            IndO.corrupted{1} = AOracles.corrupted{2} /\ 
            fdom IndO.tested{1} = fdom AOracles.usedKeys{2} /\ 
            B_AOracles.usedKeys{1} = fdom AOracles.usedKeys{2} /\  
            B_ZOracles.table{1} = RealCryptoService.table{2} /\
            B_AOracles.count_new{1} = CryptoAPIT.count_new{2} /\
            B_AOracles.count_mng{1} = CryptoAPIT.count_mng{2} /\
            B_AOracles.count_corr{1} = AOracles.count_corr{2} /\
            IndO.count_corr{1} <= AOracles.count_corr{2} /\
            B_AOracles.count_insr{1} = AOracles.count_insr{2} /\
            IndO.count_insr{1} <= AOracles.count_insr{2} /\
            B_ZOracles.count_znew{1} = RealCryptoService.count_znew{2} /\
            B_ZOracles.count_test{1} = RealCryptoService.count_test{2} /\
            IndO.count_test{1} <= RealCryptoService.count_test{2} /\
            B_AOracles.t{1} = CryptoAPIT.t{2}).
  proc; inline *.
  sp;if => //=.
  rcondt {1} 5; first by auto => />.
  by wp;call(_:true);auto => />.
  proc; inline *.
  sp;if => //=.
  rcondt {1} 4; first by auto => />.
  by wp;call(_:true);auto => />.
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. smt(@SmtMap).
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt(@SmtMap).
  by wp;call(_:true);wp;skip;progress;smt().
  by wp;skip;progress;smt().
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. 
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt().
  sp.
  seq 1 1 : (#pre /\ ko0{1} = ko{2}); first by call(_:true);skip;auto => />.
  case (ko0{1} = None).
  rcondf {1} 4; first by move => *;wp;skip;progress => /#.
  rcondf {2} 2; first by move => *;wp;skip;progress => /#.
  by wp;skip;progress;smt().
  rcondt {1} 4; first by move => *;wp;skip;progress => /#.
  rcondt {2} 2; first by move => *;wp;skip;progress => /#.
  wp;rnd;wp;skip;progress;smt().
  wp;skip;progress;smt().
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. 
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt().
  sp.
  seq 1 1 : (#pre /\ ko0{1} = ko{2}); first by call(_:true);skip;auto => />.
  case (ko0{1} = None).
  rcondf {1} 4; first by move => *;wp;skip;progress => /#.
  rcondf {2} 2; first by move => *;wp;skip;progress => /#.
  by wp;skip;progress;smt().
  rcondt {1} 4; first by move => *;wp;skip;progress => /#.
  rcondt {2} 2; first by move => *;wp;skip;progress => /#.
  wp;skip;progress;smt().
  wp;skip;progress;smt().
  wp;skip;progress;smt().
  sp; if => //=.
  rcondt {1} 6.
  move => *;wp;rnd;wp;skip;progress;smt().
  rcondt {1} 6; first by auto => />.
  sp.
  seq 1 0 : (#pre). by rnd{1}; skip; progress; apply gen_ll.
  sp.
  seq 1 1 : (#pre /\ ko0{1} = ko{2}); first by call(_:true);skip;auto => />.
  case (ko0{1} = None).
  rcondf {1} 5; first by move => *;wp;skip;progress => /#.
  rcondf {1} 2; first by move => *;wp;skip;progress => /#.
  rcondf {2} 2; first by move => *;wp;skip;progress => /#.
  by wp;skip;progress;smt().
  rcondt {1} 2; first by move => *;wp;skip;progress => /#.
  rcondt {2} 2; first by move => *;wp;skip;progress => /#.
  rcondt {1} 6; first by move => *;wp;skip;progress => /#.
  wp;skip;progress;smt(@SmtMap @FSet).
  wp;skip;progress;smt().
+ proc*.
  call(_: ={glob IAPI, glob CryptoAPIT} /\ 
            B_AOracles.corrupted{1} =  AOracles.corrupted{2} /\ 
            IndO.corrupted{1} = AOracles.corrupted{2} /\ 
            fdom IndO.tested{1} = fdom AOracles.usedKeys{2} /\ 
            B_AOracles.usedKeys{1} = fdom AOracles.usedKeys{2} /\  
            B_ZOracles.table{1} = RealCryptoService.table{2} /\
            B_AOracles.count_new{1} = CryptoAPIT.count_new{2} /\
            B_AOracles.count_mng{1} = CryptoAPIT.count_mng{2} /\
            B_AOracles.count_corr{1} = AOracles.count_corr{2} /\
            IndO.count_corr{1} <= AOracles.count_corr{2} /\
            B_AOracles.count_insr{1} = AOracles.count_insr{2} /\
            IndO.count_insr{1} <= AOracles.count_insr{2} /\
            B_ZOracles.count_znew{1} = RealCryptoService.count_znew{2} /\
            B_ZOracles.count_test{1} = RealCryptoService.count_test{2} /\
            IndO.count_test{1} <= RealCryptoService.count_test{2} /\
            B_AOracles.t{1} = CryptoAPIT.t{2}).
  proc; inline *.
  sp;if => //=.
  rcondt {1} 5; first by auto => />.
  by wp;call(_:true);auto => />.
  proc; inline *.
  sp;if => //=.
  rcondt {1} 4; first by auto => />.
  by wp;call(_:true);auto => />.
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. smt(@SmtMap).
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt(@SmtMap).
  by wp;call(_:true);wp;skip;progress;smt().
  by wp;skip;progress;smt().
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. 
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt().
  sp.
  seq 1 1 : (#pre /\ ko0{1} = ko{2}); first by call(_:true);skip;auto => />.
  case (ko0{1} = None).
  rcondf {1} 4; first by move => *;wp;skip;progress => /#.
  rcondf {2} 2; first by move => *;wp;skip;progress => /#.
  by wp;skip;progress;smt().
  rcondt {1} 4; first by move => *;wp;skip;progress => /#.
  rcondt {2} 2; first by move => *;wp;skip;progress => /#.
  wp;rnd;wp;skip;progress;smt().
  wp;skip;progress;smt().
  proc;inline *.
  sp; if => //=. 
  sp; if => //=. 
  rcondt {1} 5; first by auto => />;smt().
  rcondt {1} 5; first by auto => />;smt().
  sp.
  seq 1 1 : (#pre /\ ko0{1} = ko{2}); first by call(_:true);skip;auto => />.
  case (ko0{1} = None).
  rcondf {1} 4; first by move => *;wp;skip;progress => /#.
  rcondf {2} 2; first by move => *;wp;skip;progress => /#.
  by wp;skip;progress;smt().
  rcondt {1} 4; first by move => *;wp;skip;progress => /#.
  rcondt {2} 2; first by move => *;wp;skip;progress => /#.
  wp;skip;progress;smt().
  wp;skip;progress;smt().
  wp;skip;progress;smt().
  by auto => />.
  by auto => />.
qed.

end section. 

(* In Ideal world *)

section.

declare module A : Adv {CryptoAPIT,Hop0CryptoService}.
declare module Z : Env {A,CryptoAPIT, Hop0CryptoService}.
declare module IAPI : ICryptoAPI {Z, A,CryptoAPIT, Hop0CryptoService}.

lemma shifttoideal0 &m:
   Pr[ IdealGame(Z,A,IAPI).main() @ &m : res] =
     Pr[ Game6(Z,A,IAPI).main() @ &m : res].
proof.
byequiv.
proc; inline *.
call (_: ={glob A, glob IAPI, glob CryptoAPIT, glob RealCryptoService}).
proc;if => //=;wp;call(_: ={glob IAPI,  glob CryptoAPIT, glob RealCryptoService}).
by sim.
by sim.
by sim.
by sim.
by sim.
by auto => />.
by sim.
by sim.
by sim.
by wp;call(_: true);wp;skip;progress.
by auto => />.
by auto => />.
qed.

end section.

section.

declare module A : Adv {CryptoAPIT,AEAD_OraclesCondProb, C_ZOracles,  AEAD_Oracles, Hop0CryptoService,  IndO, AOracles, B_AOracles, B_ZOracles}.
declare module Z : Env {CryptoAPIT,A,  AEAD_OraclesCondProb, C_ZOracles, AEAD_Oracles, Hop0CryptoService,  IndO, AOracles, B_AOracles, B_ZOracles}.
declare module IAPI : ICryptoAPI {CryptoAPIT,Z, A, AEAD_OraclesCondProb,  C_ZOracles, AEAD_Oracles, Hop0CryptoService,  IndO, AOracles, B_AOracles, B_ZOracles}.

axiom z_ll :
forall (O <: EnvO{Z}),
  islossless O.new =>
  islossless O.enc =>
  islossless O.dec => islossless O.takeCtl => islossless Z(O).guess.

axiom newParam_ll :
forall (O <: AdvO{A}),
  islossless O.tkNewKey =>
  islossless O.tkManage =>
  islossless O.corruptR =>
  islossless O.badEnc => islossless O.badDec => islossless A(O).newParam.

axiom encParam_ll :
forall (O <: AdvO{A}),
  islossless O.tkNewKey =>
  islossless O.tkManage =>
  islossless O.corruptR =>
  islossless O.badEnc => islossless O.badDec => islossless A(O).encParam.

axiom decParam_ll :
forall (O <: AdvO{A}),
  islossless O.tkNewKey =>
  islossless O.tkManage =>
  islossless O.corruptR =>
  islossless O.badEnc => islossless O.badDec => islossless A(O).decParam.

axiom takeCtl_ll : forall (O <: AdvO{A}),
  islossless O.tkNewKey =>
  islossless O.tkManage =>
  islossless O.corruptR =>
  islossless O.badEnc => islossless O.badDec => islossless A(O).takeCtl.

axiom iapi_tkNewKey_ll : islossless IAPI.tkNewKey.
axiom iapi_tkManage_ll : islossless IAPI.tkManage.
axiom iapi_tkReveal_ll : islossless IAPI.tkReveal.

lemma tkNewKey_lll : islossless AOracles(CryptoAPIT(IAPI)).tkNewKey.
proof.
 by proc;inline*;
    sp; (if; last by done);
      wp; call iapi_tkNewKey_ll.
qed.

lemma tkManage_lll : islossless AOracles(CryptoAPIT(IAPI)).tkManage.
proof.
 by proc;inline*;
    sp; (if; last by done);
      wp; call iapi_tkManage_ll.
qed.

lemma corrupt_lll : islossless AOracles(CryptoAPIT(IAPI)).corruptR.
proof.
  by proc;inline*;
    sp; (if; last by done);
      (if; last by wp);
        wp; call iapi_tkReveal_ll; wp.
qed.

lemma badEnc_lll : islossless AOracles(CryptoAPIT(IAPI)).badEnc.
proof.
  proc;inline*;
  sp; if; last by done.
  if; last by wp.
  sp; seq 1 : (#pre); last by done.
    by call (_:true). 
    by call iapi_tkReveal_ll.
    sp; if; last by wp.
      wp; rnd; skip; progress. apply enc_ll.
    by hoare; call (_:true).
qed.

lemma badDec_lll : islossless AOracles(CryptoAPIT(IAPI)).badDec.
proof.
  proc;inline*;
  sp; if; last by done.
  if; last by wp.
  sp; seq 1 : (#pre); last by done.
    by call (_:true).
    by call iapi_tkReveal_ll.
    by wp.
    hoare; by call (_:true).
qed.

require import Real.

lemma MainTheorem &m :
  `|Pr [ RealGame(Z,A,IAPI).main() @ &m : res ] - 
     Pr [ IdealGame(Z,A,IAPI).main() @ &m : res ] | <=
      `| Pr [ Ind(B(Z,A),IAPI).main(false) @ &m : !res ] -
            Pr [ Ind(B(Z,A),IAPI).main(true) @ &m : res ] | +
      `| Pr [AEAD_LoRCondProb(C(IAPI,Z,A)).game(false) @ &m : !res] -
           Pr [AEAD_LoRCondProb(C(IAPI,Z,A)).game(true) @ &m : res] | +
      `| Pr [ Ind(B1(Z,A),IAPI).main(false) @ &m : !res ] -
            Pr [ Ind(B1(Z,A),IAPI).main(true) @ &m : res ] |.
proof.
rewrite (shifttohop0_pr A Z IAPI &m).
rewrite (shifttoideal0 A Z IAPI &m).

have hop2 : (
  `| Pr [ Game0(Z,A,IAPI).main()    @ &m : res] -
      Pr [ Game2(Z,A,IAPI).main()    @ &m : res] | <=
      `| Pr [ Ind(B(Z,A),IAPI).main(false) @ &m : !res ] -
            Pr [ Ind(B(Z,A),IAPI).main(true) @ &m : res ] |
).
  move : (leftHop A Z IAPI &m) => lefthop.
  move : (rightHop A Z IAPI &m) => righthop.
  by smt().

have hop3 : (
  `| Pr [ Game2(Z,A,IAPI).main()    @ &m : res] -
      Pr [ Game3(Z,A,IAPI).main()    @ &m : res] | = 0%r
).
  move : (hop3uptobad A Z IAPI z_ll newParam_ll encParam_ll decParam_ll takeCtl_ll iapi_tkNewKey_ll iapi_tkManage_ll iapi_tkReveal_ll &m) => hop3uptobad.
  move : (no_bad A Z IAPI z_ll newParam_ll encParam_ll decParam_ll takeCtl_ll iapi_tkNewKey_ll iapi_tkManage_ll iapi_tkReveal_ll &m) => bad.
  by smt().

have hop4 : (
  `| Pr [ Game3(Z,A,IAPI).main()    @ &m : res] -
      Pr [ Game4(Z,A,IAPI).main()    @ &m : res] | <=
      `| Pr [ AEAD_LoRCondProb(C(IAPI,Z,A)).game(false) @ &m : !res ] -
            Pr [AEAD_LoRCondProb(C(IAPI,Z,A)).game(true) @ &m : res ] |
).
  move : (leftHopAEAD A Z IAPI &m) => lefthopaead.
  move : (rightHopAEAD A Z IAPI &m) => righthopaead.
  by smt().

have hop5 : (
  `| Pr [ Game4(Z,A,IAPI).main()    @ &m : res] -
      Pr [ Game6(Z,A,IAPI).main()    @ &m : res] | <=
      `| Pr [ Ind(B1(Z,A),IAPI).main(false) @ &m : !res ] -
            Pr [ Ind(B1(Z,A),IAPI).main(true) @ &m : res ] |
).
  move : (LeftHop2 A Z IAPI &m) => lefthop2.
  move : (RighttHop2 A Z IAPI &m) => righthop2.
  by smt().

by smt().
qed.

end section.
