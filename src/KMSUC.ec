require import AllCore SmtMap FSet List.
require UCvsInd.
require KMSProto.

clone import KMSProto.KMSProto as OneSig.
import KMSIND.
import PKSMK_HSM.
import Policy.
import MRPKE.


type Cmd = [
  | CnewOp of bool
  | CrequestAuthorization of Request & OpId
  | CnewHst
  | CinstallInitialTrust of HstId
  | CinstallUpdatedTrust of HstId
  | CnewHSM
  | CnewToken of HId & Trust
  | CupdateTokenTrust of HId & Trust & Authorizations
  | CaddTokenKey of HId 
  | Creveal of HstId & HId
].

type Rsp = [
  | RnewOp of (OpId option * OpSk option)
  | RrequestAuthorization of Authorization option
  | RnewHst of HstId
  | RinstallInitialTrust of bool
  | RinstallUpdatedTrust of bool
  | RnewHSM of HId option
].

op getRnewOp r = 
   with r = RnewOp oid => oid
   with r = RrequestAuthorization auth => witness
   with r = RnewHst hstid => witness
   with r = RinstallInitialTrust b => witness
   with r = RinstallUpdatedTrust b => witness
   with r = RnewHSM hid => witness.

op getRrequestAuthorization r = 
   with r = RnewOp oid => witness
   with r = RrequestAuthorization auth => auth
   with r = RnewHst hstid => witness
   with r = RinstallInitialTrust b => witness
   with r = RinstallUpdatedTrust b => witness
   with r = RnewHSM hid => witness.

op getRnewHst r = 
   with r = RnewOp oid => witness
   with r = RrequestAuthorization auth => witness
   with r = RnewHst hstid => hstid
   with r = RinstallInitialTrust b => witness
   with r = RinstallUpdatedTrust b => witness
   with r = RnewHSM hid => witness.

op getRinstallInitialTrust r = 
   with r = RnewOp oid => witness
   with r = RrequestAuthorization auth => witness
   with r = RnewHst hstid => witness
   with r = RinstallInitialTrust b => b
   with r = RinstallUpdatedTrust b => witness
   with r = RnewHSM hid => witness.

op getRinstallUpdatedTrust r = 
   with r = RnewOp oid => witness
   with r = RrequestAuthorization auth => witness
   with r = RnewHst hstid => witness
   with r = RinstallInitialTrust b => witness
   with r = RinstallUpdatedTrust b => b
   with r = RnewHSM hid => witness.

op getRnewHSM r = 
   with r = RnewOp oid => witness
   with r = RrequestAuthorization auth => witness
   with r = RnewHst hstid => witness
   with r = RinstallInitialTrust b => witness
   with r = RinstallUpdatedTrust b => witness
   with r = RnewHSM hid => hid
.

type Hdl = Handle.

module KMSCryptoAPI(OA : OpPolSrvT) = {
   module KP = KMSProceduresCondProb(HSMs(RealSigServ),HstS(OA),OA) 
   proc init() : unit = {
     HSMs(RealSigServ).init();
     OA.init();
     HstS(OA).init();
     KMSProcedures.hids <- fset0;
     KMSProcedures.tklist   <- fset0;
     KMSProcedures.corrupted <- fset0;
     KMSProcedures.tested <- empty;
     KMSProcedures.handles <- fset0;
     KMSProcedures.count_ops <- 0;
     KMSProcedures.count_auth <- 0;
     KMSProcedures.count_hst <- 0;
     KMSProcedures.count_inst <- 0;
     KMSProcedures.count_updt <- 0;
     KMSProcedures.count_hid <- 0;
     KMSProcedures.count_tkmng <- 0;
     KMSProcedures.count_unw <- 0;
     KMSProcedures.count_corr <- 0;
     KMSProcedures.count_test <- 0;
   }
   proc tkNewKey(tk : Token, hdl : Handle, cmd : Cmd) : Token option = {
      var to;
      var hid : HId;
      to <- None;
      if (cmd is CaddTokenKey hid) {
             to <@ KP.addTokenKey(hid,hdl,tk);
      }
      return to;
   }
   proc tkManage(tk : Token, cmd : Cmd) : Token option * Rsp option = {
      var to,ro;
      var boid, b : bool;
      var ooid : OpId option * OpSk option;
      var oid : OpId;
      var auth : Authorization option;
      var req : Request;
      var hstid : HstId;
      var hid : HId option;
      var trust : Trust;
      var auths : Authorizations;
      match (cmd) with
      | CnewOp boid  => 
           {to <- None;
            ooid <@ KP.newOp(boid);
            ro <- Some (RnewOp ooid);}
      | CrequestAuthorization req oid  => 
           {to <- None;
            auth <@ KP.requestAuthorization(req,oid);
            ro <- Some (RrequestAuthorization auth);}
      | CnewHst  => 
           {to <- None;
            hstid <@ KP.newHst();
            ro <- Some (RnewHst hstid);}
      | CinstallInitialTrust hstid  => 
           {to <- None;
            b <@ KP.installInitialTrust(hstid,tk);
            ro <- Some (RinstallInitialTrust b);}
      | CinstallUpdatedTrust hstid  => 
           {to <- None;
            b <@ KP.installUpdatedTrust(hstid,tk);
            ro <- Some (RinstallUpdatedTrust b);}
      | CnewHSM  => 
           {to <- None;
            hid <@ KP.newHSM();
            ro <- Some (RnewHSM hid);}
      | CnewToken hid trust  => 
           {ro <- None;
            to <@ KP.newToken(hid,trust);}
      | CupdateTokenTrust hid trust auths  => 
           {ro <- None;
            to <@ KP.updateTokenTrust(hid,trust,auths,tk);}
      | CaddTokenKey hid =>
           {to <- None;
            ro <- None;}
      | Creveal _ _  => 
           {to <- None;
            ro <- None;}
      end;
      return (to,ro);
   }
   proc tkReveal(tk : Token, hdl : Handle, cmd : Cmd) : Key option  ={
      var tdo,ko;
      var hid : HId;
      ko <- None;
      if (cmd is Creveal _ hid) {
        ko <- None;
         tdo <@ HSMs(RealSigServ).unwrap(hid,tk);
         if (tdo <> None && hdl \in (oget tdo).`td_skeys) {
            ko <- (oget tdo).`td_skeys.[hdl];
         }
      }
      return ko;
   }
}.

type Trace = (HstId,Trust) fmap * HId fset * Token fset.
op new_tr : Trace = (empty,fset0,fset0).

op addNew_tr(tk : Token,hdl : Hdl,c : Cmd,tko : Token option,tr : Trace) =
   with c = CnewOp _ => tr
   with c = CrequestAuthorization _ _ => tr
   with c = CnewHst => tr
   with c = CinstallInitialTrust _ => tr 
   with c = CinstallUpdatedTrust _ => tr
   with c = CnewHSM => tr
   with c = CnewToken _ _ => tr 
   with c = CupdateTokenTrust _ _ _ => tr
   with c = CaddTokenKey _ => 
          if tko <> None
          then let (hstmap,genuine,tklist) = tr
               in (hstmap,genuine,tklist `|` fset1 (oget tko))
          else tr
   with c = Creveal _ _ => tr.

op addManage_tr(tk : Token,c : Cmd, out : Token option * Rsp option,tr : Trace) = 
   with c = CnewOp _ => tr
   with c = CrequestAuthorization _ _ => tr
   with c = CnewHst => tr
   with c = CinstallInitialTrust hstid => 
         let b = getRinstallInitialTrust (oget out.`2) in
         let (hstmap,genuine,tklist) = tr
         in if b 
            then (hstmap.[hstid <- tk.`tk_trust],genuine,tklist)
            else tr
   with c = CinstallUpdatedTrust hstid => 
         let b = getRinstallUpdatedTrust (oget out.`2) in
         let (hstmap,genuine,tklist) = tr
         in if b 
            then (hstmap.[hstid <- tk.`tk_trust],genuine,tklist)
            else tr
   with c = CnewHSM => 
         let hid = getRnewHSM (oget out.`2) in
         let (hstmap,genuine,tklist) = tr
         in (hstmap, if hid = None then genuine else genuine `|` fset1 (oget hid),tklist)
   with c = CnewToken _ _ => 
         let (hstmap,genuine,tklist) = tr
         in if out.`1 <> None 
            then (hstmap,genuine,tklist `|` fset1 (oget out.`1))
            else tr
   with c = CupdateTokenTrust _ _ _ =>
         let (hstmap,genuine,tklist) = tr
         in if out.`1 <> None 
            then (hstmap,genuine,tklist `|` fset1 (oget out.`1))
            else tr
   with c = CaddTokenKey _ => tr
   with c = Creveal _ _ => tr.
  
op valid (t : Trace, tk : Token, hdl : Hdl, c : Cmd) : bool =
   with c = CnewOp _ => false
   with c = CrequestAuthorization _ _ => false
   with c = CnewHst => false
   with c = CinstallInitialTrust _ => false 
   with c = CinstallUpdatedTrust _ => false
   with c = CnewHSM => false
   with c = CnewToken _ _ => false 
   with c = CupdateTokenTrust _ _ _ => false
   with c = CaddTokenKey _ => false 
   with c = Creveal hstid hid => 
      let (hstmap,hids,tklist) = t 
      in ((hid \in hids) && (hstmap.[hstid] = Some tk.`tk_trust)).

op honest (t : Trace, tk : Token, hdl : Hdl, c : Cmd) : bool =
   with c = CnewOp _ => true
   with c = CrequestAuthorization _ _ => true
   with c = CnewHst => true
   with c = CinstallInitialTrust _ => true 
   with c = CinstallUpdatedTrust _ => true
   with c = CnewHSM => true
   with c = CnewToken _ _ => true 
   with c = CupdateTokenTrust _ _ _ => true
   with c = CaddTokenKey _ => true
   with c = Creveal hstid hid => 
     (let (hstmap,hids,tklist) = t
      in (!((hid \in hids) &&
           (!(tk \in tklist) || 
            !(all_genuine hids (tr_data (tk_trust tk))))))).

type St.
type AData.
type Msg.

op q_mng : int.

axiom bounds : q_mng < q_inst /\ q_mng < q_updt /\ q_mng < q_hid  /\ q_mng < q_hst /\ q_mng < q_ops /\ q_mng < q_auth.

clone import UCvsInd as UC with
   type AE.K <- Key,
   type AE.AData <- AData,
   type AE.Msg <- Msg,
   op AE.gen = genKey,
   type Hdl <- Hdl,
   type St <- St,
   type Token <- Token,
   type Cmd <- Cmd,
   type Rsp <- Rsp,
   type Trace <- Trace,
   op new_tr <- new_tr,
   op addNew_tr <- addNew_tr,
   op addManage_tr <- addManage_tr,
   op valid <- valid,
   op honest <- honest,
   op q_corr <- q_corr,
   op q_test <- q_test,
   op q_insr <- q_unw,
   op q_mng <- q_mng.

(* We construct an attacker that breaks KMSif A breaks Ind *)

module Dispatch(O:KMSOracles) : IndO_T = {
  var hosts_tr : (HstId,Trust) fmap
  var count_new : int
  var count_mng : int
  var count_corr : int
  var count_insr : int
  var count_test : int

  proc init() = {
     hosts_tr <- empty;
     count_new <- 0;
     count_mng <- 0;
     count_corr <- 0;
     count_insr <- 0;
     count_test <- 0;
  }

  proc tkNewKey(tk : Token, hdl : Hdl, cmd : Cmd) : Token option = {
      var to;
      var hid : HId;
      to <- None;
      if (count_new < UC.q_new) {
         if (cmd is CaddTokenKey hid) {
             to <@ O.addTokenKey(hid,hdl,tk);
         }
         count_new <- count_new + 1;
      }
      return to;
  }
  
  proc tkManage(tk : Token, cmd : Cmd) : Token option * Rsp option = {
     var to,ro;
      var boid, b : bool;
      var ooid : OpId option * OpSk option;
      var oid : OpId;
      var auth : Authorization option;
      var req : Request;
      var hstid : HstId;
      var hid : HId option;
      var trust : Trust;
      var auths : Authorizations;
      to <- None; ro <- None;
      if (count_mng < q_mng) {
        match (cmd) with
        | CnewOp boid  => 
             {to <- None;
              ooid <@ O.newOp(boid);
              ro <- Some (RnewOp ooid);}
        | CrequestAuthorization req oid  => 
             {to <- None;
              auth <@ O.requestAuthorization(req,oid);
              ro <- Some (RrequestAuthorization auth);}
        | CnewHst  => 
             {to <- None;
              hstid <@ O.newHst();
              ro <- Some (RnewHst hstid);}
        | CinstallInitialTrust hstid  => 
             {to <- None;
              b <@ O.installInitialTrust(hstid,tk);
              if (b) { hosts_tr.[hstid] <- tk.`tk_trust; }
              ro <- Some (RinstallInitialTrust b);}
        | CinstallUpdatedTrust hstid  => 
             {to <- None;
              b <@ O.installUpdatedTrust(hstid,tk);
              if (b) { hosts_tr.[hstid] <- tk.`tk_trust; }
              ro <- Some (RinstallUpdatedTrust b);}
        | CnewHSM  => 
             {to <- None;
              hid <@ O.newHSM();
              ro <- Some (RnewHSM hid);}
        | CnewToken hid trust  => 
             {ro <- None;
              to <@ O.newToken(hid,trust);}
        | CupdateTokenTrust hid trust auths  => 
             {ro <- None;
              to <@ O.updateTokenTrust(hid,trust,auths,tk);}
         | CaddTokenKey hid =>
             {to <- None;
              ro <- None;}
        | Creveal _ _  => 
             {to <- None;
              ro <- None;}
        end;
        count_mng <- count_mng + 1;
      }
      return (to,ro);
  }
  
  proc corruptR(tk : Token, hdl : Hdl, cmd : Cmd) : Key option = {
      var ko;
      var hid : HId;
      ko <- None;
      if (count_corr < q_corr) {
         if (cmd is Creveal hstid hid) {
            ko <- None;
            if (hosts_tr.[hstid] = Some tk.`tk_trust) {
               ko <@ O.corrupt(hid,tk,hdl);
            }
         }
         count_corr <- count_corr + 1;
      }
      return ko;
  }
  
  proc insiderR(tk : Token, hdl : Hdl, cmd : Cmd) : Key option = {
      var ko,tdo;
      var hid : HId;
      ko <- None;
      if (count_insr < q_unw) {
         if (cmd is Creveal hstid hid) {
           ko <- None;
            tdo <@ O.unwrap(hid,tk);
            if (tdo <> None && hdl \in (oget tdo).`td_skeys) {
               ko <- (oget tdo).`td_skeys.[hdl];
            }
         }
         count_insr <- count_insr + 1;
      }
      return ko;
  }
  
  proc test(tk : Token, hdl : Hdl, cmd : Cmd) : Key option = {
      var ko;
      var hid : HId;
      ko <- None;
      if (count_test < q_test) {
         if (cmd is Creveal hstid hid) {
            ko <@ O.test(hid,hstid,tk,hdl);
         }
         count_test <- count_test + 1;
      }
      return ko;
  }
}.

module BB(A : IndA, O:KMSOracles) = {
       proc guess(): bool = {
            var b;
            Dispatch(O).init();
            b <@ A(Dispatch(O)).guess();
            return(b);
       }
}.

section.

declare module A : IndA { CryptoAPIT, OpPolSrv,Dispatch, HSMs, KMSProcedures, RealSigServ, HstPolSrv, IndO}.
declare module OA : OperatorActions {CryptoAPIT, OpPolSrv,AE.AEAD_OraclesCondProb,C_ZOracles, B_ZOracles, B_AOracles, Hop0CryptoService, RealCryptoService, AOracles, AE.AEAD_Oracles, Dispatch, A,HSMs, KMSProcedures, RealSigServ, HstPolSrv, IndO}.

lemma rorimpliesind &m b:
   Pr [ Ind(A,KMSCryptoAPI(OpPolSrv(OA))).main(b) @ &m: res ] =
   Pr [ KMSRoRCondProb(BB(A),HSMs(RealSigServ),HstS(OpPolSrv(OA)),OpPolSrv(OA)).game(b) @ &m: res].
proof.
byequiv.
proc; inline *. wp.
call (_: ={glob OA, glob RealSigServ, glob HSMs, glob OpPolSrv, glob HstPolSrv, KMSProcedures.hids, KMSProcedures.tklist, KMSProcedures.handles, KMSProcedures.count_ops, KMSProcedures.count_auth, KMSProcedures.count_hst, KMSProcedures.count_inst, KMSProcedures.count_updt, KMSProcedures.count_hid, KMSProcedures.count_tkmng} /\ IndO.b{1} = KMSProcedures.b{2} /\
Dispatch.hosts_tr{2} = HstPolSrv.hosts_tr{2} /\
CryptoAPIT.t.`1{1} = HstPolSrv.hosts_tr{2} /\
CryptoAPIT.t.`2{1} = KMSProcedures.hids{2} /\
CryptoAPIT.t.`3{1} = KMSProcedures.tklist{2} /\
IndO.corrupted{1} = KMSProcedures.corrupted{2} /\
IndO.tested{1} = KMSProcedures.tested{2} /\
CryptoAPIT.t.`2{1} = OpPolSrv.genuine{2} /\
CryptoAPIT.count_new{1} = Dispatch.count_new{2}/\
IndO.count_corr{1} = Dispatch.count_corr{2} /\
KMSProcedures.count_corr{2} <= IndO.count_corr{1} /\
IndO.count_test{1} = Dispatch.count_test{2} /\
KMSProcedures.count_test{2} <= IndO.count_test{1} /\
IndO.count_insr{1} = Dispatch.count_insr{2} /\
KMSProcedures.count_unw{2} <= IndO.count_insr{1} /\
CryptoAPIT.count_mng{1} = Dispatch.count_mng{2} /\
KMSProcedures.count_hid{2} <= CryptoAPIT.count_mng{1}
); last first.
by wp;call(_:true);wp;skip;progress => /#.
proc.
sp; if => //=.
inline KMSCryptoAPI(OpPolSrv(OA)).tkNewKey.
inline KMSProceduresCondProb(HSMs(RealSigServ), HstS(OpPolSrv(OA)), OpPolSrv(OA)).addTokenKey.
sp;wp.
match = => //=.
+ move => *.
  inline KMSCryptoAPI(OpPolSrv(OA)).KP.addTokenKey.
  sp.
  seq 1 1 : (#pre /\ ={key}); first by auto => />.
  if => //=; last by auto => />.
  if => //=; last by auto => />.
  seq 1 1 : (#pre /\ ={tdo}); first by call (_: ={glob HSMs}); wp; skip;progress. 
  sp;if => //=; last by auto => />.
  sp;if => //=; last by wp;skip;progress.
  wp; call(_: ={glob RealSigServ}). 
     wp; call(_: ={glob RealSigServ}). 
     by sim.
  by wp;rnd;wp;skip;progress => />.
  by auto => /#.

proc.
sp; if => //=. progress.
inline KMSCryptoAPI(OpPolSrv(OA)).tkManage.
sp.
match = => //=.
+ move => *.
  inline KMSProceduresCondProb(HSMs(RealSigServ), HstS(OpPolSrv(OA)), OpPolSrv(OA)).newOp.
  sp; if => //=; last  by wp;skip;progress => /#.
  wp;call(_: ={glob OpPolSrv, glob OA}).
  by sim.
  by wp;skip;progress => /#.

+ move => *.
  inline KMSProceduresCondProb(HSMs(RealSigServ), HstS(OpPolSrv(OA)), OpPolSrv(OA)).requestAuthorization.
  sp; if => //=; last  by wp;skip;progress => /#.
  wp;call(_: ={glob OpPolSrv, glob OA}).
  by sim.
   by wp;skip;progress => /#.

+ move => *.
  inline KMSProceduresCondProb(HSMs(RealSigServ), HstS(OpPolSrv(OA)), OpPolSrv(OA)).newHst.
  sp; if => //=; last  by wp;skip;progress => /#.
  wp;call(_: ={glob OpPolSrv, glob OA}).
  by sim.
   by wp;skip;progress => /#.

+ move => *.
  inline KMSProceduresCondProb(HSMs(RealSigServ), HstS(OpPolSrv(OA)), OpPolSrv(OA)).installInitialTrust.
  sp; if => //=; last first.
     wp;skip;progress => /#. 
  inline *. 
  sp; if => //=.
  wp;call(_:true);wp;skip;progress => /#.
  wp;skip;progress => /#.
+ move => *.
  inline KMSProceduresCondProb(HSMs(RealSigServ), HstS(OpPolSrv(OA)), OpPolSrv(OA)).installUpdatedTrust.
  sp; if => //=; last first.
     wp;skip;progress => /#.
  inline *. 
  wp;skip;progress => /#.

+ inline KMSCryptoAPI(OpPolSrv(OA)).KP.newHSM.
  sp; if => //=.
  inline OpPolSrv(OA).addHId.
  wp; call(_: ={glob HSMs, glob RealSigServ, glob OpPolSrv}). by sim.
  skip;progress; by try rewrite oget_some; smt(@FSet). 
  wp;skip;progress; by smt(bounds). 


+ move => *.
  inline KMSProceduresCondProb(HSMs(RealSigServ), HstS(OpPolSrv(OA)), OpPolSrv(OA)).newToken.
  sp; if => //=; last first.
     wp;skip;progress => /#.  
  sp; if => //=; last first.
     wp;skip;progress => /#.  
  wp;call(_: ={glob HSMs, glob RealSigServ}).
  by sim.
  wp;skip;progress => /#. 
+ move => *.
  inline KMSProceduresCondProb(HSMs(RealSigServ), HstS(OpPolSrv(OA)), OpPolSrv(OA)).updateTokenTrust.
  sp; if => //=; last first.
     wp;skip;progress => /#.  
  sp; if => //=; last first.
     wp;skip;progress => /#. 
  seq 2 2 : (#pre /\ ={tdo,qa}). 
      call(_: ={ glob OA, glob OpPolSrv}). by sim.
  wp;call(_: ={glob HSMs, glob RealSigServ}). by sim.
  wp;skip;progress => /#.
  sp; if => //=; last first.
     wp;skip;progress => /#. 
  seq 1 1 : (#pre /\ ={tdoo}). 
      call(_: ={ glob HSMs, glob RealSigServ}). by sim.
     wp;skip;progress => /#.
  sp; if => //=; last first.
     wp;skip;progress => /#. 
  wp; call(_: ={ glob HSMs, glob RealSigServ}). by sim.
     wp;skip;progress => /#. 

+ move => *.
  by wp;skip;progress=> /#.

+ move => *.
  by wp;skip;progress => /#.

+ proc.
  sp;if => //=.
  if {1}.
  inline *;sp;match =; first by smt().
  by move => *; auto => />.
  by move => *; auto => />.
  by move => *; auto => />.
  by move => *; auto => />.
  by move => *; auto => />.
  by move => *; auto => />.
  by move => *; auto => />.
  by move => *; auto => />.
  by move => *; auto => />.

  move => *. inline KMSProceduresCondProb(HSMs(RealSigServ), HstS(OpPolSrv(OA)), OpPolSrv(OA)).corrupt.
  rcondt {2} 2. move => *;wp;skip;progress.
     move : H5.
     pose xxx := Some tk{hr}.`tk_trust. smt().
  rcondt {2} 6; first by move => *;wp;skip;progress => /#.
  rcondt {2} 6.  move => *;wp;skip;progress.
     move : H5.
     pose xxx := Some tk{hr}.`tk_trust. smt().

  by wp;skip;progress => /#.
  
  match{2}.
     + move => *. wp;skip;progress=> /#.
     + move => *. wp;skip;progress=> /#.
     + move => *. wp;skip;progress=> /#.
     + move => *. wp;skip;progress=> /#.
     + move => *. wp;skip;progress=> /#.
     + move => *. wp;skip;progress=> /#.
     + move => *. wp;skip;progress=> /#.
     + move => *. wp;skip;progress=> /#.
     + move => *. wp;skip;progress=> /#.
     + move => *. 
        inline *. 
        sp 0 1; if{2}. 
        rcondt {2} 5. auto => />. smt().
        rcondf {2} 5. auto => />. smt().
        wp;skip;progress. smt().
        wp;skip;progress. smt().

+ proc.
  sp;if => //=.
  if {1}.
  inline *;sp;match =; first by smt().
  by move => *; auto => />.
  by move => *; auto => />.
  by move => *; auto => />.
  by move => *; auto => />.
  by move => *; auto => />.
  by move => *; auto => />.
  by move => *; auto => />.
  by move => *; auto => />.
  by move => *; auto => />.

  move => *. 
  inline *. 
  rcondt {2} 5; first by move => *; wp;skip;progress => /#.
  rcondt {2} 5. move => *; wp;skip;progress. move : H4. smt().
  rcondt {2} 7. 
  by move => *; wp;skip;progress; right; smt().
  by wp;skip;progress;smt().

  match{2}.
     + move => *. wp;skip;progress=> /#.
     + move => *. wp;skip;progress=> /#.
     + move => *. wp;skip;progress=> /#.
     + move => *. wp;skip;progress=> /#.
     + move => *. wp;skip;progress=> /#.
     + move => *. wp;skip;progress=> /#.
     + move => *. wp;skip;progress=> /#.
     + move => *. wp;skip;progress=> /#.
     + move => *. wp;skip;progress=> /#.
     + move => *. inline KMSProceduresCondProb(HSMs(RealSigServ), HstS(OpPolSrv(OA)), OpPolSrv(OA)).unwrap.
            rcondt{2} 5; first by move => *;wp;skip;progress => /#.
            case ((hid{2} \in KMSProcedures.hids{2})).
            rcondt{2} 5; first by auto => />.
            rcondf{2} 6. move => *. inline *; wp;skip;progress.  move : H4. smt().  
            by inline *; wp;skip;progress => /#. 
            rcondf{2} 5; first by auto => />.
            by inline *; wp;skip;progress => /#. 

+ proc.
  case ((IndO.count_test < q_test){1}); last first.
  rcondf {1} 3; first by auto => />.
  rcondf {2} 2; first by auto => />.
  by rnd{1};wp;skip;progress; apply genKey_ll.
  rcondt {1} 3; first by auto => />.
  rcondt {2} 2; first by auto => />.
  case ((! (hdl \in IndO.corrupted) && valid CryptoAPIT.t tk hdl cmd){1}); last first.
  rcondf {1} 3; first by auto => />.
  sp;match {2}.
  move => *. wp;rnd{1};wp;skip;progress. smt().
  move => *. wp;rnd{1};wp;skip;progress. smt().
  move => *. wp;rnd{1};wp;skip;progress. smt().
  move => *. wp;rnd{1};wp;skip;progress. smt().
  move => *. wp;rnd{1};wp;skip;progress. smt().
  move => *. wp;rnd{1};wp;skip;progress. smt().
  move => *. wp;rnd{1};wp;skip;progress. smt().
  move => *. wp;rnd{1};wp;skip;progress. smt().
  move => *. wp;rnd{1};wp;skip;progress. smt().

  move => *. 
  inline *. 
  rcondt {2} 7; first by move => *; rnd;wp;skip;progress => /#.
  rcondf {2} 13.
     move => *; wp;rnd;wp;skip;progress. 
       move : H4.
     pose xxx := Some tk{hr}.`tk_trust. 
       smt().
  by wp;rnd;wp;skip;progress;smt().

  rcondt {1} 3; first by auto => />.
  sp; match {2}. 
  move => *. 
     inline *. 
     seq 1 1 : #pre. rnd{1};wp;skip;progress.
        sp; match {1}. 
           move => *. by auto => />.
           move => *. by auto => />.
           move => *. by auto => />.
           move => *. by auto => />.
           move => *. by auto => />.
           move => *. by auto => />.
           move => *. by auto => />. 
           move => *. by auto => />.
           move => *. by auto => />.

  move => *. exfalso. smt().
  move => *. exfalso. smt().
  move => *. exfalso. smt().
  move => *. exfalso. smt().
  move => *. exfalso. smt().
  move => *. exfalso. smt().
  move => *. exfalso. smt().
  move => *. exfalso. smt().
  move => *. exfalso. smt().

  move => *. 
     inline KMSProceduresCondProb(HSMs(RealSigServ), HstS(OpPolSrv(OA)), OpPolSrv(OA)).test.     
    sp;seq  1 1 : (#pre /\ = {rkey}); first by auto=> />.
     inline  CryptoAPIT(KMSCryptoAPI(OpPolSrv(OA))).tkReveal.
     inline KMSCryptoAPI(OpPolSrv(OA)).tkReveal.
     sp 7 0. match{1}.
           move => *. inline *. by auto => />.
           move => *. inline *. by auto => />.
           move => *. inline *. by auto => />.
           move => *. inline *. by auto => />.
           move => *. inline *. by auto => />.
           move => *. inline *. by auto => />.
           move => *. inline *. by auto => />.
           move => *. inline *. by auto => />.
           move => *. inline *. by auto => />.
           move => *. 
    rcondt {2} 1; first by move => *; wp;skip;progress;smt().
    inline HstS(OpPolSrv(OA)).isInstalledTrust.
    inline HstPolSrv.isInstalledTrust. 
    rcondt {2} 7.  move => *. wp;skip;progress.
          move : H5.
           pose xxx :=   Some tk{hr}.`tk_trust.
           smt().
          move : H5.
           pose xxx :=   Some tk{hr}.`tk_trust.
           smt().
    wp;call(_: ={glob HSMs, glob RealSigServ}). by sim.
    wp;skip;progress;smt().
    by auto.  
    by auto.
qed.
 
lemma rorimpliesindn &m b:
   Pr [ Ind(A,KMSCryptoAPI(OpPolSrv(OA))).main(b) @ &m: !res ] =
   Pr [ KMSRoRCondProb(BB(A),HSMs(RealSigServ),HstS(OpPolSrv(OA)),OpPolSrv(OA)).game(b) @ &m: !res].
byequiv.
proc; inline *. wp.
call (_: ={glob OA, glob RealSigServ, glob HSMs, glob OpPolSrv, glob HstPolSrv, KMSProcedures.hids, KMSProcedures.tklist, KMSProcedures.handles, KMSProcedures.count_ops, KMSProcedures.count_auth, KMSProcedures.count_hst, KMSProcedures.count_inst, KMSProcedures.count_updt, KMSProcedures.count_hid, KMSProcedures.count_tkmng} /\ IndO.b{1} = KMSProcedures.b{2} /\
Dispatch.hosts_tr{2} = HstPolSrv.hosts_tr{2} /\
CryptoAPIT.t.`1{1} = HstPolSrv.hosts_tr{2} /\
CryptoAPIT.t.`2{1} = KMSProcedures.hids{2} /\
CryptoAPIT.t.`3{1} = KMSProcedures.tklist{2} /\
IndO.corrupted{1} = KMSProcedures.corrupted{2} /\
IndO.tested{1} = KMSProcedures.tested{2} /\
CryptoAPIT.t.`2{1} = OpPolSrv.genuine{2} /\
CryptoAPIT.count_new{1} = Dispatch.count_new{2}/\
IndO.count_corr{1} = Dispatch.count_corr{2} /\
KMSProcedures.count_corr{2} <= IndO.count_corr{1} /\
IndO.count_test{1} = Dispatch.count_test{2} /\
KMSProcedures.count_test{2} <= IndO.count_test{1} /\
IndO.count_insr{1} = Dispatch.count_insr{2} /\
KMSProcedures.count_unw{2} <= IndO.count_insr{1} /\
CryptoAPIT.count_mng{1} = Dispatch.count_mng{2} /\
KMSProcedures.count_hid{2} <= CryptoAPIT.count_mng{1}
); last first.
by wp;call(_:true);wp;skip;progress => /#.
proc.
sp; if => //=.
inline KMSCryptoAPI(OpPolSrv(OA)).tkNewKey.
inline KMSProceduresCondProb(HSMs(RealSigServ), HstS(OpPolSrv(OA)), OpPolSrv(OA)).addTokenKey.
sp;wp.
match = => //=.
+ move => *.
  inline KMSCryptoAPI(OpPolSrv(OA)).KP.addTokenKey.
  sp.
  seq 1 1 : (#pre /\ ={key}); first by auto => />.
  if => //=; last by auto => />.
  if => //=; last by auto => />.
  seq 1 1 : (#pre /\ ={tdo}); first by call (_: ={glob HSMs}); wp; skip;progress. 
  sp;if => //=; last by auto => />.
  sp;if => //=; last by wp;skip;progress.
  wp; call(_: ={glob RealSigServ}). 
     wp; call(_: ={glob RealSigServ}). 
     by sim.
  by wp;rnd;wp;skip;progress => />.
  by auto => /#.

proc.
sp; if => //=. progress.
inline KMSCryptoAPI(OpPolSrv(OA)).tkManage.
sp.
match = => //=.
+ move => *.
  inline KMSProceduresCondProb(HSMs(RealSigServ), HstS(OpPolSrv(OA)), OpPolSrv(OA)).newOp.
  sp; if => //=; last  by wp;skip;progress => /#.
  wp;call(_: ={glob OpPolSrv, glob OA}).
  by sim.
  by wp;skip;progress => /#.

+ move => *.
  inline KMSProceduresCondProb(HSMs(RealSigServ), HstS(OpPolSrv(OA)), OpPolSrv(OA)).requestAuthorization.
  sp; if => //=; last  by wp;skip;progress => /#.
  wp;call(_: ={glob OpPolSrv, glob OA}).
  by sim.
   by wp;skip;progress => /#.

+ move => *.
  inline KMSProceduresCondProb(HSMs(RealSigServ), HstS(OpPolSrv(OA)), OpPolSrv(OA)).newHst.
  sp; if => //=; last  by wp;skip;progress => /#.
  wp;call(_: ={glob OpPolSrv, glob OA}).
  by sim.
   by wp;skip;progress => /#.

+ move => *.
  inline KMSProceduresCondProb(HSMs(RealSigServ), HstS(OpPolSrv(OA)), OpPolSrv(OA)).installInitialTrust.
  sp; if => //=; last first.
     wp;skip;progress => /#.
  inline *. 
  sp; if => //=.
  wp;call(_:true);wp;skip;progress => /#.
  wp;skip;progress => /#.

+ move => *.
  inline KMSProceduresCondProb(HSMs(RealSigServ), HstS(OpPolSrv(OA)), OpPolSrv(OA)).installUpdatedTrust.
  sp; if => //=; last first.
     wp;skip;progress => /#.
  inline *. 
  wp;skip;progress => /#.

+ inline KMSCryptoAPI(OpPolSrv(OA)).KP.newHSM.
  sp; if => //=.
  inline OpPolSrv(OA).addHId.
  wp; call(_: ={glob HSMs, glob RealSigServ, glob OpPolSrv}). by sim.
  skip;progress; try rewrite oget_some; smt(@FSet).
  wp;skip; progress; smt(@FSet).   


+ move => *.
  inline KMSProceduresCondProb(HSMs(RealSigServ), HstS(OpPolSrv(OA)), OpPolSrv(OA)).newToken.
  sp; if => //=; last first.
     wp;skip;progress => /#. 
  sp; if => //=; last first.
     wp;skip;progress => /#. 
  wp;call(_: ={glob HSMs, glob RealSigServ}).
  by sim.
  wp;skip;progress => /#. 

+ move => *.
  inline KMSProceduresCondProb(HSMs(RealSigServ), HstS(OpPolSrv(OA)), OpPolSrv(OA)).updateTokenTrust.
  sp; if => //=; last first.
     wp;skip;progress => /#. 
  sp; if => //=; last first.
     wp;skip;progress => /#. 
  seq 2 2 : (#pre /\ ={tdo,qa}). 
      call(_: ={ glob OA, glob OpPolSrv}). by sim.
  wp;call(_: ={glob HSMs, glob RealSigServ}). by sim.
     wp;skip;progress => /#. 
  sp; if => //=; last first.
     wp;skip;progress => /#. 
  seq 1 1 : (#pre /\ ={tdoo}). 
      call(_: ={ glob HSMs, glob RealSigServ}). by sim.
     wp;skip;progress => /#. 
  sp; if => //=; last first.
     wp;skip;progress => /#. 
  wp; call(_: ={ glob HSMs, glob RealSigServ}). by sim.
     wp;skip;progress => /#. 

+ move => *.
  by wp;skip;progress=> /#.

+ move => *.
  by wp;skip;progress => /#.

+ proc.
  sp;if => //=.
  if {1}.
  inline *;sp;match =; first by smt().
  by move => *; auto => />.
  by move => *; auto => />.
  by move => *; auto => />.
  by move => *; auto => />.
  by move => *; auto => />.
  by move => *; auto => />.
  by move => *; auto => />.
  by move => *; auto => />.
  by move => *; auto => />.

  move => *. inline KMSProceduresCondProb(HSMs(RealSigServ), HstS(OpPolSrv(OA)), OpPolSrv(OA)).corrupt.
  rcondt {2} 2. move => *;wp;skip;progress.
     move : H5.
     pose xxx := Some tk{hr}.`tk_trust. smt().
  rcondt {2} 6; first by move => *;wp;skip;progress => /#.
  rcondt {2} 6.  move => *;wp;skip;progress.
     move : H5.
     pose xxx := Some tk{hr}.`tk_trust. smt().
  by wp;skip;progress => /#.

  match{2}.
     + move => *. wp;skip;progress=> /#.
     + move => *. wp;skip;progress=> /#.
     + move => *. wp;skip;progress=> /#.
     + move => *. wp;skip;progress=> /#.
     + move => *. wp;skip;progress=> /#.
     + move => *. wp;skip;progress=> /#.
     + move => *. wp;skip;progress=> /#.
     + move => *. wp;skip;progress=> /#.
     + move => *. wp;skip;progress=> /#.
     + move => *. 
         inline *. sp 0 1. if {2}. 
         rcondt {2} 5. auto => />.  smt().
         rcondf {2} 5. auto => />.  smt().
     wp;skip;progress => /#.
     wp;skip;progress => /#.

+ proc.
  sp;if => //=.
  if {1}.
  inline *;sp;match =; first by smt().
  by move => *; auto => />.
  by move => *; auto => />.
  by move => *; auto => />.
  by move => *; auto => />.
  by move => *; auto => />.
  by move => *; auto => />.
  by move => *; auto => />.
  by move => *; auto => />.
  by move => *; auto => />.

  move => *. 
  inline *. 
  rcondt {2} 5; first by move => *; wp;skip;progress => /#.
  rcondt {2} 5.  move => *; wp;skip;progress. move : H4. smt(). 
  rcondt {2} 7. move => *; wp;skip;progress. move : H4. smt(). 
  by wp;skip;progress;smt().

  match{2}.
     + move => *. wp;skip;progress=> /#.
     + move => *. wp;skip;progress=> /#.
     + move => *. wp;skip;progress=> /#.
     + move => *. wp;skip;progress=> /#.
     + move => *. wp;skip;progress=> /#.
     + move => *. wp;skip;progress=> /#.
     + move => *. wp;skip;progress=> /#.
     + move => *. wp;skip;progress=> /#.
     + move => *. wp;skip;progress=> /#.
     + move => *. inline KMSProceduresCondProb(HSMs(RealSigServ), HstS(OpPolSrv(OA)), OpPolSrv(OA)).unwrap.
            rcondt{2} 5; first by move => *;wp;skip;progress => /#.
            case ((hid{2} \in KMSProcedures.hids{2})).
            rcondt{2} 5; first by auto => />.
            rcondf{2} 6. move => *; inline *; wp;skip;progress. move : H4. smt().  
            by inline *; wp;skip;progress => /#. 
            rcondf{2} 5; first by auto => />.
            by inline *; wp;skip;progress => /#. 

+ proc.
  case ((IndO.count_test < q_test){1}); last first.
  rcondf {1} 3; first by auto => />.
  rcondf {2} 2; first by auto => />.
  by rnd{1};wp;skip;progress; apply genKey_ll.
  rcondt {1} 3; first by auto => />.
  rcondt {2} 2; first by auto => />.
  case ((! (hdl \in IndO.corrupted) && valid CryptoAPIT.t tk hdl cmd){1}); last first.
  rcondf {1} 3; first by auto => />.
  sp;match {2}.
  move => *. wp;rnd{1};wp;skip;progress. smt().
  move => *. wp;rnd{1};wp;skip;progress. smt().
  move => *. wp;rnd{1};wp;skip;progress. smt().
  move => *. wp;rnd{1};wp;skip;progress. smt().
  move => *. wp;rnd{1};wp;skip;progress. smt().
  move => *. wp;rnd{1};wp;skip;progress. smt().
  move => *. wp;rnd{1};wp;skip;progress. smt().
  move => *. wp;rnd{1};wp;skip;progress. smt().
  move => *. wp;rnd{1};wp;skip;progress. smt().

  move => *. 
  inline *. 
  rcondt {2} 7; first by move => *; rnd;wp;skip;progress => /#.
  rcondf {2} 13.
     move => *; wp;rnd;wp;skip;progress. 
       move : H4.
     pose xxx := Some tk{hr}.`tk_trust. 
       smt().
  by wp;rnd;wp;skip;progress;smt().

  rcondt {1} 3; first by auto => />.
  sp; match {2}. 
  move => *. 
     inline *. 
     seq 1 1 : #pre. rnd{1};wp;skip;progress.
        sp; match {1}. 
           move => *. by auto => />.
           move => *. by auto => />.
           move => *. by auto => />.
           move => *. by auto => />.
           move => *. by auto => />.
           move => *. by auto => />.
           move => *. by auto => />. 
           move => *. by auto => />.
           move => *. by auto => />.

  move => *. exfalso. smt().
  move => *. exfalso. smt().
  move => *. exfalso. smt().
  move => *. exfalso. smt().
  move => *. exfalso. smt().
  move => *. exfalso. smt().
  move => *. exfalso. smt().
  move => *. exfalso. smt().
  move => *. exfalso. smt().

  move => *. 
     inline KMSProceduresCondProb(HSMs(RealSigServ), HstS(OpPolSrv(OA)), OpPolSrv(OA)).test.     
    sp;seq  1 1 : (#pre /\ = {rkey}); first by auto=> />.
     inline  CryptoAPIT(KMSCryptoAPI(OpPolSrv(OA))).tkReveal.
     inline KMSCryptoAPI(OpPolSrv(OA)).tkReveal.
     sp 7 0. match{1}.
           move => *. inline *. by auto => />.
           move => *. inline *. by auto => />.
           move => *. inline *. by auto => />.
           move => *. inline *. by auto => />.
           move => *. inline *. by auto => />.
           move => *. inline *. by auto => />.
           move => *. inline *. by auto => />.
           move => *. inline *. by auto => />.
           move => *. inline *. by auto => />.
           move => *. 
    rcondt {2} 1; first by move => *; wp;skip;progress;smt().
    inline HstS(OpPolSrv(OA)).isInstalledTrust.
    inline HstPolSrv.isInstalledTrust. 
    rcondt {2} 7.  move => *. wp;skip;progress.
          move : H5.
           pose xxx :=   Some tk{hr}.`tk_trust.
           smt().
          move : H5.
           pose xxx :=   Some tk{hr}.`tk_trust.
           smt().
    wp;call(_: ={glob HSMs, glob RealSigServ}). by sim.
    wp;skip;progress;smt().
    by auto.  
    by auto.
qed.

end section.

section.

declare module OA : OperatorActions {OpPolSrv,AE.AEAD_OraclesCondProb,C_ZOracles, B_ZOracles, B_AOracles, Hop0CryptoService, RealCryptoService, AOracles, AE.AEAD_Oracles, Dispatch, HSMs, KMSProcedures, RealSigServ, HstPolSrv, IndO, CryptoAPIT}.

local module IAPI = KMSCryptoAPI(OpPolSrv(OA)).

declare module AZ : Adv {Dispatch,CryptoAPIT, OA,IAPI, AE.AEAD_Oracles,C_ZOracles,  AE.AEAD_OraclesCondProb, Hop0CryptoService,  IndO, AOracles, B_AOracles, B_ZOracles}.
declare module Z : Env {Dispatch,CryptoAPIT,OA, IAPI, AZ,AE.AEAD_Oracles, AE.AEAD_OraclesCondProb,  C_ZOracles,Hop0CryptoService,  IndO, AOracles, B_AOracles, B_ZOracles}.


axiom z_ll : forall (O <: EnvO{Z}),
  islossless O.new =>
  islossless O.enc =>
  islossless O.dec => islossless O.takeCtl => islossless Z(O).guess.

axiom a_newParam_ll : forall (O <: AdvO{AZ}),
  islossless O.tkNewKey =>
  islossless O.tkManage =>
  islossless O.corruptR =>
  islossless O.badEnc => islossless O.badDec => islossless AZ(O).newParam.

axiom a_encParam_ll : forall (O <: AdvO{AZ}),
  islossless O.tkNewKey =>
  islossless O.tkManage =>
  islossless O.corruptR =>
  islossless O.badEnc => islossless O.badDec => islossless AZ(O).encParam.

axiom a_decParam_ll : forall (O <: AdvO{AZ}),
  islossless O.tkNewKey =>
  islossless O.tkManage =>
  islossless O.corruptR =>
  islossless O.badEnc => islossless O.badDec => islossless AZ(O).decParam.

axiom a_takeCtl_ll : forall (O <: AdvO{AZ}),
  islossless O.tkNewKey =>
  islossless O.tkManage =>
  islossless O.corruptR =>
  islossless O.badEnc => islossless O.badDec => islossless AZ(O).takeCtl.

(* Fixme: ll hypothesis over API can be proved *)

local lemma MainTheoremKMS&m : 
  islossless IAPI.tkNewKey =>
  islossless IAPI.tkManage =>
  islossless IAPI.tkReveal =>
 `|Pr[RealGame(Z, AZ, IAPI).main() @ &m : res] -
      Pr[IdealGame(Z, AZ, IAPI).main() @ &m : res]| <=
    `|Pr[Ind(B(Z, AZ), IAPI).main(false) @ &m : !res] -
      Pr[Ind(B(Z, AZ), IAPI).main(true) @ &m : res]| +
    `|Pr[AE.AEAD_LoRCondProb(C(IAPI, Z, AZ)).game(false) @ &m : !res] -
      Pr[AE.AEAD_LoRCondProb(C(IAPI, Z, AZ)).game(true) @ &m : res]| +
    `|Pr[Ind(B1(Z, AZ), IAPI).main(false) @ &m : !res] -
      Pr[Ind(B1(Z, AZ), IAPI).main(true) @ &m : res]|.
 move => iapi_tkNewKey_ll iapi_tkManage_ll iapi_tkReveal_ll.
 apply (MainTheorem AZ Z IAPI _ _ _ _ _ _ _ _ &m). 
      apply z_ll. apply a_newParam_ll. apply a_encParam_ll. apply a_decParam_ll.
      apply a_takeCtl_ll. apply iapi_tkNewKey_ll. apply iapi_tkManage_ll.
      apply iapi_tkReveal_ll.
qed.

local lemma MainTheoremKMSC &m : 
  islossless IAPI.tkNewKey =>
  islossless IAPI.tkManage =>
  islossless IAPI.tkReveal =>
 `|Pr[RealGame(Z, AZ, IAPI).main() @ &m : res] -
      Pr[IdealGame(Z, AZ, IAPI).main() @ &m : res]| <=
    `|Pr [KMSRoRCondProb(BB(B(Z, AZ)),HSMs(RealSigServ),HstS(OpPolSrv(OA)),OpPolSrv(OA)).game(false) @ &m: !res] -
      Pr [KMSRoRCondProb(BB(B(Z, AZ)),HSMs(RealSigServ),HstS(OpPolSrv(OA)),OpPolSrv(OA)).game(true) @ &m: res]| +
    `|Pr[AE.AEAD_LoRCondProb(C(IAPI, Z, AZ)).game(false) @ &m : !res] -
      Pr[AE.AEAD_LoRCondProb(C(IAPI, Z, AZ)).game(true) @ &m : res]| +
    `|Pr [KMSRoRCondProb(BB(B1(Z, AZ)),HSMs(RealSigServ),HstS(OpPolSrv(OA)),OpPolSrv(OA)).game(false) @ &m: !res] -
      Pr [KMSRoRCondProb(BB(B1(Z, AZ)),HSMs(RealSigServ),HstS(OpPolSrv(OA)),OpPolSrv(OA)).game(true) @ &m: res]|.
proof.
 move => iapi_tkNewKey_ll iapi_tkManage_ll iapi_tkReveal_ll.
move : (rorimpliesind (B(Z,AZ)) OA &m true) => *.
move : (rorimpliesindn (B(Z,AZ)) OA &m false) => *.
move : (rorimpliesindn (B1(Z,AZ)) OA &m false) => *.
move : (rorimpliesind (B1(Z,AZ)) OA &m true) => *.
rewrite -H.
rewrite -H0.
rewrite -H1.
rewrite -H2.
by smt(MainTheoremKMS).
qed.

end section.
