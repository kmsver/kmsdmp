require import AllCore List FSet SmtMap Distr DBool Dexcepted Real RealExtra.
require import Finite.
require (*--*) FinType.
require (*--*) RndExcept.
require KMSProto_Hop2_Pol.

abstract theory Hop3.

clone include KMSProto_Hop2_Pol.Hop2.
import KMSProto.
import KMSIND.
import PKSMK_HSM.
import Policy.
import MRPKE.

(******************************************************)
(* HANDLING KEY COLLISIONS                      *)
(******************************************************)

op signedTk (tk:Token) (qs : (pkey * message * signature) fset) =
     (tk.`tk_wdata.`tkw_signer.`1, 
      encode_msg 
         (tk.`tk_trust, tk.`tk_wdata.`tkw_ekeys,tk.`tk_wdata.`tkw_signer, tk.`tk_updt), 
            tk.`tk_wdata.`tkw_sig) \in qs. 

op signedTr (tr:Trust) (qs : (pkey * message * signature) fset) = 
          exists (tk : Token), (tr = tk.`tk_trust /\ tk.`tk_updt /\ signedTk tk qs). 

op mkToken i tr ekeys hid s =
              {| tk_updt = i;
                 tk_trust = tr;
                 tk_wdata = {| tkw_ekeys = ekeys; 
                               tkw_signer = hid; 
                               tkw_sig = s |}; |}.

op nosigpks_elems (qs : (pkey * message * signature) FSet.fset) (genuine : HId FSet.fset) (parent : (Trust, Trust) SmtMap.fmap) = 
   map fst (flatten 
    (map (fun (q:pkey * message * signature) => 
      let tk = recover_token q.`2 q.`3 in
      if tk <> None /\ q.`1 = (oget tk).`tk_wdata.`tkw_signer.`1 then elems (tr_mems (oget tk).`tk_trust `\` genuine) else []) (elems qs) ++
     map (fun t => elems (tr_mems t `\` genuine)) (elems (SmtMap.frng parent)))).

op nosigpks (qs : (pkey * message * signature) fset) (genuine : HId fset) (parent : (Trust,Trust) fmap) (pk : pkey) : bool =
    pk \in nosigpks_elems qs genuine parent
axiomatized by nosigpksE.  

lemma nosigpks_prop qs genuine parent verk :
   nosigpks qs genuine parent verk <=>
   exists (hid : HId),
      ((exists (tk : Token), signedTk tk qs /\ hid \in tr_mems tk.`tk_trust `\` genuine) \/
       exists (t : Trust), t \in SmtMap.frng parent /\ hid \in tr_mems t `\` genuine) /\
      verk = hid.`1.
proof.
  rewrite nosigpksE;split.
  + move => /mapP [hid []] /= /flattenP [mems []] /mem_cat h hidin ->; exists hid => /=.
    case: h => /mapP.
    + move => [[q1 q2 q3] []];rewrite -memE /=.
      case: (recover_token q2 q3 <> None /\ q1 = (oget (recover_token q2 q3)).`tk_wdata.`tkw_signer.`1).
      + move=> [] /recover_encode /= [] hq2 hq3 hq1 hin hmems. 
        by left;exists (oget (recover_token q2 q3)); rewrite /signedTk -hq1 -hq2 -hq3 hin memE -hmems hidin.
      by move=> ?? ->>; move: hidin.
    by move=> [t []] /= ht ->>;right; exists t; rewrite !memE.
  rewrite /nosigpks_elems=> -[[hid1 hid2]] [] [[tk [hs hin]] | [t [ht hin]]] ->>; apply mem_map_fst;exists hid2 => /=; apply flattenP.
  + exists (elems (tr_mems tk.`tk_trust `\` genuine)).
    rewrite -memE hin mem_cat;left.
    pose f := (fun (q : pkey * message * signature) =>
     if recover_token q.`2 q.`3 <> None /\ q.`1 = (oget (recover_token q.`2 q.`3)).`tk_wdata.`tkw_signer.`1 then
       elems (tr_mems (oget (recover_token q.`2 q.`3)).`tk_trust `\` genuine)
     else []).
    have -> : elems (tr_mems tk.`tk_trust `\` genuine) = 
     f (tk.`tk_wdata.`tkw_signer.`1, encode_msg (tk.`tk_trust, tk.`tk_wdata.`tkw_ekeys, tk.`tk_wdata.`tkw_signer, tk.`tk_updt), tk.`tk_wdata.`tkw_sig).
    + by rewrite /f /= encode_recover /=.
    by apply map_f; rewrite -memE.
  exists (elems (tr_mems t `\` genuine)); rewrite -memE hin /= mem_cat;right.
  by apply: (map_f _ _ t);rewrite -memE.
qed.


op n_keygen : int.
axiom keygen_uni : forall k, k \in keygen => mu1 keygen k = 1%r/n_keygen%r.
axiom ge0_max_size : 0 <= max_size.
axiom ge0_q_hid : 0 <= q_hid.
axiom q_hid_q_gen : q_hid <= PKSMK_HSM.q_gen.
axiom keygen_restr_ll (X : keys list) : size X <= (q_hid + q_tkmng + q_tkmng) * max_size => is_lossless (keygen \ mem X).

axiom more_bounds: 0 <= q_unw /\ 0 <= q_corr /\ 0 <= q_tkmng.

axiom keygen_uniq pk sk1 sk2: (pk,sk1) \in keygen => (pk,sk2) \in keygen => sk1 = sk2.

lemma gt0_n_keygen: 0 < n_keygen.
proof.
  case (exists k, 0%r < mu1 keygen k).
  + move=> [k hk];apply lt_fromint. 
    by have /(_ hk) heq := keygen_uni k;move: hk;rewrite heq /= StdOrder.RealOrder.invr_gt0.
  move=> /Logic.negb_exists /= hnin.
  have : weight keygen = 0%r.
  + by rewrite support_eq0 // weight_dnull.
  by rewrite keygen_ll.
qed.

op nosigpks_elems' qs genuine parent = 
  pmap (fun pk => omap (fun sk => (pk,sk)) (assoc (to_seq (support keygen)) pk))
    (nosigpks_elems qs genuine parent).
import StdBigop.Bigint.

lemma size_nosigpks_elems qs genuine parent:
  card (fdom parent) <= q_tkmng =>
  (forall (t k: Trust), parent.[k] = Some t => card (tr_mems t) <= max_size) =>
  card qs <= q_hid + q_tkmng =>
  (forall (q : pkey * message * signature),
     q \in qs =>
     recover_token q.`2 q.`3 <> None => card (tr_mems (oget (recover_token q.`2 q.`3)).`tk_trust) <= max_size) =>
  size (nosigpks_elems' qs genuine parent) <= (q_hid + q_tkmng + q_tkmng) * max_size.
proof.
  move=> hcp hpmax hcq hqmax.
  rewrite /nosigpks_elems' pmap_map.
  rewrite size_map size_filter.
  apply: (lez_trans _ _ _ (count_size (predC1 None) _)).
  rewrite size_map /nosigpks_elems size_map size_flatten.
  rewrite sumzE BIA.big_mapT BIA.big_cat mulzDl StdOrder.IntOrder.ler_add BIA.big_mapT /(\o) /=.
  + apply (lez_trans (BIA.big predT (fun _ => max_size) (elems qs))).
    + apply ler_sum_seq => q; rewrite -memE => /hqmax /=.
      case _ : (_ /\ _) => /= [ [] h? | ???];last by apply ge0_max_size.
      rewrite -cardE fcardD => /(_ h) h1 _; smt (fcard_ge0).
    by rewrite big_constz mulzC count_predT -cardE; smt (fcard_ge0 ge0_max_size).
  apply (lez_trans (BIA.big predT (fun _ => max_size) (elems (frng parent)))).
  + apply ler_sum_seq => q; rewrite -memE mem_frng rngE /= => -[t /hpmax ht] _.
    by rewrite -cardE fcardD; smt (fcard_ge0).  
  rewrite big_constz mulzC count_predT -cardE; smt (le_card_frng_fdom fcard_ge0 ge0_max_size).
qed.

lemma dexcepted_ext (d:'a distr) (p1 p2:'a -> bool) :  (forall x, x \in d => p1 x = p2 x) => d \ p1 = d \ p2.
proof.
  move=> h;apply eq_distr => x.
  rewrite !dexcepted1E.
  case: (0%r < mu1 d x) => hx. 
  + by rewrite h // (mu_eq_support _ _ _ h).
  by have -> :  mu1 d x = 0%r by smt (ge0_mu).    
qed.

lemma nosigpks_nosigpks_elems' qs genuine parentTrust:   
        keygen \ mem (nosigpks_elems' qs genuine parentTrust) =
        keygen \ fun (pksk : pkey * skey) => nosigpks qs genuine parentTrust pksk.`1.
proof.
  apply dexcepted_ext => k hin /=.
  rewrite /nosigpks_elems'.
  rewrite pmap_map nosigpksE mapP.
  rewrite eq_iff; split.
  + move=> -[ []]; 1: by rewrite mem_filter /predC1. 
    move=> k';rewrite mem_filter oget_some => /> _.
    by rewrite mapP => -[pk] /=; case _ : (assoc (to_seq (support keygen)) pk).
  move=> hk1; exists (Some k); rewrite oget_some /= mem_filter /predC1 /= mapP.
  exists k.`1; rewrite hk1 /=.
  have fkg : is_finite (support keygen).
  + by apply uniform_finite => ?? /keygen_uni -> /keygen_uni ->.
  have := mem_assoc_uniq (to_seq (support keygen)) k.`1 k.`2 _.
  + apply map_inj_in_uniq => /=; last by apply uniq_to_seq.
    by move=> [pk1 sk1] [pk2 sk2]; rewrite !mem_to_seq // => />; apply keygen_uniq.
  by rewrite mem_to_seq //; case: k hin hk1 => k1 k2 /= -> /= hk1 ->.
qed.

module KMSProcedures3pre(OA : OperatorActions) : KMSOracles = {
  module TS = IdealTrustService(OpPolSrv(OA))
  module KMSProc = KMSProcedures2(HSMsP_TS(TS),HstSP_TS(TS),TS,OpPolSrv(OA))

  include  KMSProc [-newHSM]

  proc newHSM() : HId option = {
    var hid, rhid;
    var verk,pk, sk;
    var pksig,sksig;
    rhid <- None;
    if (KMSProcedures.count_hid < q_hid) {
      if (size (nosigpks_elems' PKSMK_HSM.RealSigServ.qs OpPolSrv.genuine RealTrustService.parentTrust) <=
            (q_hid + q_tkmng + q_tkmng) * max_size) {
        (sk,pk) <$ gen;
        if (pk \in HSMs.who) {
          verk <- oget HSMs.who.[pk];
        } else {
          (pksig,sksig) <$ keygen \ (fun (pksk : pkey*skey) => 
                            nosigpks RealSigServ.qs OpPolSrv.genuine RealTrustService.parentTrust pksk.`1);
          if (pksig \notin RealSigServ.pks_sks) { RealSigServ.pks_sks.[pksig] <- sksig; }
          verk  <-pksig;
          if (verk \in HSMs.benc_keys) {
            (pk, sk) <- oget HSMs.benc_keys.[verk];
          } else {
            HSMs.who.[pk] <- verk;
            HSMs.benc_keys.[verk] <- (pk,sk);
          }
        }
        hid <- (verk,pk);
        if (!(hid \in KMSProcedures.hids)) {
           KMSProcedures.hids <- KMSProcedures.hids `|` fset1 hid;
        }
        OpPolSrv(OA).addHId(hid);
        KMSProcedures.count_hid <- KMSProcedures.count_hid + 1;
        rhid <- Some hid;
      }
    }
    return rhid;
  }
}.

module KMSRoR3pre(A : KMSAdv, OA : OperatorActions) = {
   module A = A(KMSProcedures3pre(OA))

   proc main() : bool = {
        var b;
        KMSProcedures3pre(OA).init();
        b <@ A.guess();
        return (b = KMSProcedures.b) ;     
   }
}.

section PR2_PR3.

declare module OA : OperatorActions {RealTrustService, RealSigServ, KMSProcedures, HSMs, HstPolSrv, OpPolSrv, HstS, HSMsI}.
declare module A : KMSAdv      {OA, RealTrustService, RealSigServ, KMSProcedures, HSMs, HstPolSrv, OpPolSrv, HstS, HSMsI}.

local clone RndExcept as RE with
  type input <- unit,
  type t <- keys,
  op d <- (fun (_:unit) => keygen),
  type out <- bool
  proof d_ll by apply keygen_ll. 

local clone RE.AdversaryN as REA with
  op p <- (q_hid + q_tkmng + q_tkmng) * max_size, 
  op q <- q_hid,
  op n <- n_keygen
  proof *.

realize p_pos. by smt (more_bounds ge0_max_size ge0_q_hid). qed.
realize q_pos. by apply ge0_q_hid. qed.
realize n_pos. by apply gt0_n_keygen. qed.
realize d_uni. 
  move=> ? x.
  case: (x \in keygen); 1: by move=> /keygen_uni ->.
  move=> ?;have -> : mu1 keygen x = 0%r by smt (ge0_mu). 
  smt (ge0_mu le_fromint gt0_n_keygen).
qed.
realize p_d_ll. move=> _;apply keygen_restr_ll. qed.

local clone REA.Count as RC.

local module AS (S : RE.SAMPLE_ADV) = {
  module TS = IdealTrustService(OpPolSrv(OA))
  
  module KMSProc = {
    include KMSProcedures2(HSMsP_TS(TS), HstSP_TS(TS), TS, OpPolSrv(OA)) [-newHSM]

    proc newHSM() : HId option = {
      var hid, rhid;
      var verk,pk, sk;
      var pksig,sksig;
      rhid <- None;
      if (KMSProcedures.count_hid < q_hid) {
         (sk,pk) <$ gen;
         if (pk \in HSMs.who) {
              verk <- oget HSMs.who.[pk];
         } else {
              (pksig,sksig) <@ S.sample((), nosigpks_elems' RealSigServ.qs OpPolSrv.genuine RealTrustService.parentTrust );
              if (pksig \notin RealSigServ.pks_sks) { RealSigServ.pks_sks.[pksig] <- sksig; }
              verk  <-pksig;
              if (verk \in HSMs.benc_keys) {
                 (pk, sk) <- oget HSMs.benc_keys.[verk];
               } else {
                 HSMs.who.[pk] <- verk;
                 HSMs.benc_keys.[verk] <- (pk,sk);
               }
         }
         hid <- (verk,pk);
         if (!(hid \in KMSProcedures.hids)) {
             KMSProcedures.hids <- KMSProcedures.hids `|` fset1 hid;
         }
         OpPolSrv(OA).addHId(hid);
         KMSProcedures.count_hid <- KMSProcedures.count_hid + 1;
         rhid <- Some hid;
      }
      return rhid;
    }
  }
  
  module A = A(KMSProc)
  
  proc main() : bool = {
    var b : bool;
    
    KMSProc.init();
    b <@ A.guess();
    
    return b = KMSProcedures.b;
  }
}.

local equiv wrap_qs c:
  HSMsP_TS(IdealTrustService(OpPolSrv(OA))).wrap ~ HSMsP_TS(IdealTrustService(OpPolSrv(OA))).wrap :
    ={RealSigServ.count_sig, RealSigServ.qs, RealSigServ.pks_sks, hid, td} /\
    (hid.`1 \in RealSigServ.pks_sks => card (tr_mems td.`td_trust) <= max_size){2} /\
    (forall (q : pkey * message * signature),
       q \in RealSigServ.qs{2} =>
       recover_token q.`2 q.`3 <> None => 
        card (tr_mems (oget (recover_token q.`2 q.`3)).`tk_trust) <= max_size) /\
    card RealSigServ.qs{2} <= c 
    ==>
    ={RealSigServ.count_sig, RealSigServ.qs, RealSigServ.pks_sks, res} /\
    (forall (q : pkey * message * signature),
       q \in RealSigServ.qs{2} =>
       recover_token q.`2 q.`3 <> None => 
        card (tr_mems (oget (recover_token q.`2 q.`3)).`tk_trust) <= max_size) /\
    card RealSigServ.qs{2} <= c + 1.
proof.
  proc;inline *;sp;wp.
  seq 1 1 : (#pre /\ ={ekeys}); 1: by auto.
  sp; if => //; last by auto => /> /#.
  if => //; last by auto => /> /#.
  auto => /> &2 hct hin hc ?? sL _;split; 2: smt (fcardUI fcard_ge0 fcard1).
  move=> q;rewrite in_fsetU in_fset1 => -[/hin // | -> /=].
  have /= -> /= /# := 
    encode_recover {| tk_updt = td{2}.`td_updt; tk_trust = td{2}.`td_trust; 
      tk_wdata = {| tkw_ekeys = ekeys{2}; tkw_signer = hid{2}; tkw_sig = sL; |}; |}.
qed.

local equiv unwrap_qs tk0 :
  HSMsP_TS(IdealTrustService(OpPolSrv(OA))).unwrap ~ HSMsP_TS(IdealTrustService(OpPolSrv(OA))).unwrap :
  ={RealSigServ.count_sig, RealSigServ.qs, RealSigServ.pks_sks,HSMs.benc_keys, hid, tk} /\
  tk{2} = tk0 
  ==>
  ={res} /\ (res <> None => (oget res).`td_trust = tk0.`tk_trust){2}.
proof. by proc;inline *; auto => /> *; rewrite oget_some. qed.

local equiv newToken_E : 
  KMSProcedures2(HSMsP_TS(IdealTrustService(OpPolSrv(OA))), HstSP_TS(IdealTrustService(OpPolSrv(OA))), 
    IdealTrustService(OpPolSrv(OA)), OpPolSrv(OA)).newToken ~ 
  KMSProcedures2(HSMsP_TS(IdealTrustService(OpPolSrv(OA))), HstSP_TS(IdealTrustService(OpPolSrv(OA))), 
  IdealTrustService(OpPolSrv(OA)), OpPolSrv(OA)).newToken :
  ={hid, new} /\
  (={KMSProcedures.count_test, KMSProcedures.count_corr,
             KMSProcedures.count_unw, KMSProcedures.count_tkmng,
             KMSProcedures.count_hid, 
             KMSProcedures.count_updt, KMSProcedures.count_hst,
             KMSProcedures.count_auth, KMSProcedures.count_ops,
             KMSProcedures.b, KMSProcedures.handles, KMSProcedures.tested,
             KMSProcedures.corrupted, KMSProcedures.tklist,
             KMSProcedures.hids, OpPolSrv.badOps, OpPolSrv.goodOps,
             OpPolSrv.genuine} /\
         ={HSMs.benc_keys, HSMs.who} /\
         ={glob OA} /\
         ={RealTrustService.parentTrust, RealTrustService.protectedTrusts,
             RealTrustService.count_chk, RealTrustService.count_updt,
             RealTrustService.count_hst, RealTrustService.count_auth,
             RealTrustService.count_ops, HstPolSrv.hosts_tr, OpPolSrv.badOps,
             OpPolSrv.goodOps, OpPolSrv.genuine} /\
         ={OpPolSrv.badOps, OpPolSrv.goodOps, OpPolSrv.genuine} /\
         ={HstPolSrv.hosts_tr, RealSigServ.count_sig, RealSigServ.qs,
             RealSigServ.pks_sks}) /\
  RealSigServ.count_pk{1} <= KMSProcedures.count_hid{1} /\
  card RealSigServ.qs{2} <= KMSProcedures.count_tkmng{2} + KMSProcedures.count_hid{2} /\
  KMSProcedures.count_hid{2} <= q_hid /\
  RC.Count.c{2} <= KMSProcedures.count_hid{2} /\
  card (fdom RealTrustService.parentTrust{2}) <= KMSProcedures.count_tkmng{2} <= q_tkmng /\
  (forall (t k : Trust), RealTrustService.parentTrust{2}.[k] = Some t => card (tr_mems t) <= max_size) /\
  forall (q : pkey * message * signature),
    q \in RealSigServ.qs{2} =>
    recover_token q.`2 q.`3 <> None => card (tr_mems (oget (recover_token q.`2 q.`3)).`tk_trust) <= max_size

  ==> 
  ={res} /\
 (={KMSProcedures.count_test, KMSProcedures.count_corr,
             KMSProcedures.count_unw, KMSProcedures.count_tkmng,
             KMSProcedures.count_hid, 
             KMSProcedures.count_updt, KMSProcedures.count_hst,
             KMSProcedures.count_auth, KMSProcedures.count_ops,
             KMSProcedures.b, KMSProcedures.handles, KMSProcedures.tested,
             KMSProcedures.corrupted, KMSProcedures.tklist,
             KMSProcedures.hids, OpPolSrv.badOps, OpPolSrv.goodOps,
             OpPolSrv.genuine} /\
         ={HSMs.benc_keys, HSMs.who} /\
         ={glob OA} /\
         ={RealTrustService.parentTrust, RealTrustService.protectedTrusts,
             RealTrustService.count_chk, RealTrustService.count_updt,
             RealTrustService.count_hst, RealTrustService.count_auth,
             RealTrustService.count_ops, HstPolSrv.hosts_tr, OpPolSrv.badOps,
             OpPolSrv.goodOps, OpPolSrv.genuine} /\
         ={OpPolSrv.badOps, OpPolSrv.goodOps, OpPolSrv.genuine} /\
         ={HstPolSrv.hosts_tr, RealSigServ.count_sig, RealSigServ.qs,
             RealSigServ.pks_sks}) /\
  RealSigServ.count_pk{1} <= KMSProcedures.count_hid{1} /\
  card RealSigServ.qs{2} <= KMSProcedures.count_tkmng{2} + KMSProcedures.count_hid{2} /\
  KMSProcedures.count_hid{2} <= q_hid /\
  RC.Count.c{2} <= KMSProcedures.count_hid{2} /\
  card (fdom RealTrustService.parentTrust{2}) <= KMSProcedures.count_tkmng{2} <= q_tkmng /\
  (forall (t k : Trust), RealTrustService.parentTrust{2}.[k] = Some t => card (tr_mems t) <= max_size) /\
  forall (q : pkey * message * signature),
    q \in RealSigServ.qs{2} =>
    recover_token q.`2 q.`3 <> None => card (tr_mems (oget (recover_token q.`2 q.`3)).`tk_trust) <= max_size.
proof.
  proc; conseq />.
  sp 1 1; if => //.
  if => //; last by auto => /> /#.
  wp; call (_: ={glob OA, glob RealTrustService, glob OpPolSrv}); 1: by sim.
  sp;wp. ecall (wrap_qs (KMSProcedures.count_tkmng{2} + KMSProcedures.count_hid{2})).
  skip; smt (cardE). 
qed.

local equiv updateTokenTrust_E : 
 KMSProcedures(HSMsP_TS(IdealTrustService(OpPolSrv(OA))), HstSP_TS(IdealTrustService(OpPolSrv(OA))), OpPolSrv(OA)).updateTokenTrust ~ 
 KMSProcedures(HSMsP_TS(IdealTrustService(OpPolSrv(OA))), HstSP_TS(IdealTrustService(OpPolSrv(OA))), OpPolSrv(OA)).updateTokenTrust :
  ={hid, new_trust, auth, tk} /\
  (={KMSProcedures.count_test, KMSProcedures.count_corr,
             KMSProcedures.count_unw, KMSProcedures.count_tkmng,
             KMSProcedures.count_hid, 
             KMSProcedures.count_updt, KMSProcedures.count_hst,
             KMSProcedures.count_auth, KMSProcedures.count_ops,
             KMSProcedures.b, KMSProcedures.handles, KMSProcedures.tested,
             KMSProcedures.corrupted, KMSProcedures.tklist,
             KMSProcedures.hids, OpPolSrv.badOps, OpPolSrv.goodOps,
             OpPolSrv.genuine} /\
         ={HSMs.benc_keys, HSMs.who} /\
         ={glob OA} /\
         ={RealTrustService.parentTrust, RealTrustService.protectedTrusts,
             RealTrustService.count_chk, RealTrustService.count_updt,
             RealTrustService.count_hst, RealTrustService.count_auth,
             RealTrustService.count_ops, HstPolSrv.hosts_tr, OpPolSrv.badOps,
             OpPolSrv.goodOps, OpPolSrv.genuine} /\
         ={OpPolSrv.badOps, OpPolSrv.goodOps, OpPolSrv.genuine} /\
         ={HstPolSrv.hosts_tr, RealSigServ.count_sig, RealSigServ.qs,
             RealSigServ.pks_sks}) /\
  RealSigServ.count_pk{1} <= KMSProcedures.count_hid{1} /\
  card RealSigServ.qs{2} <= KMSProcedures.count_tkmng{2} + KMSProcedures.count_hid{2} /\
  KMSProcedures.count_hid{2} <= q_hid /\
  RC.Count.c{2} <= KMSProcedures.count_hid{2} /\
  card (fdom RealTrustService.parentTrust{2}) <= KMSProcedures.count_tkmng{2} <= q_tkmng /\
  (forall (t k : Trust), RealTrustService.parentTrust{2}.[k] = Some t => card (tr_mems t) <= max_size) /\
  forall (q : pkey * message * signature),
    q \in RealSigServ.qs{2} =>
    recover_token q.`2 q.`3 <> None => card (tr_mems (oget (recover_token q.`2 q.`3)).`tk_trust) <= max_size

  ==> 
  ={res} /\
  (={KMSProcedures.count_test, KMSProcedures.count_corr,
             KMSProcedures.count_unw, KMSProcedures.count_tkmng,
             KMSProcedures.count_hid, 
             KMSProcedures.count_updt, KMSProcedures.count_hst,
             KMSProcedures.count_auth, KMSProcedures.count_ops,
             KMSProcedures.b, KMSProcedures.handles, KMSProcedures.tested,
             KMSProcedures.corrupted, KMSProcedures.tklist,
             KMSProcedures.hids, OpPolSrv.badOps, OpPolSrv.goodOps,
             OpPolSrv.genuine} /\
         ={HSMs.benc_keys, HSMs.who} /\
         ={glob OA} /\
         ={RealTrustService.parentTrust, RealTrustService.protectedTrusts,
             RealTrustService.count_chk, RealTrustService.count_updt,
             RealTrustService.count_hst, RealTrustService.count_auth,
             RealTrustService.count_ops, HstPolSrv.hosts_tr, OpPolSrv.badOps,
             OpPolSrv.goodOps, OpPolSrv.genuine} /\
         ={OpPolSrv.badOps, OpPolSrv.goodOps, OpPolSrv.genuine} /\
         ={HstPolSrv.hosts_tr, RealSigServ.count_sig, RealSigServ.qs,
             RealSigServ.pks_sks}) /\
  RealSigServ.count_pk{1} <= KMSProcedures.count_hid{1} /\
  card RealSigServ.qs{2} <= KMSProcedures.count_tkmng{2} + KMSProcedures.count_hid{2} /\
  KMSProcedures.count_hid{2} <= q_hid /\
  RC.Count.c{2} <= KMSProcedures.count_hid{2} /\
  card (fdom RealTrustService.parentTrust{2}) <= KMSProcedures.count_tkmng{2} <= q_tkmng /\
  (forall (t k : Trust), RealTrustService.parentTrust{2}.[k] = Some t => card (tr_mems t) <= max_size) /\
  forall (q : pkey * message * signature),
    q \in RealSigServ.qs{2} =>
    recover_token q.`2 q.`3 <> None => card (tr_mems (oget (recover_token q.`2 q.`3)).`tk_trust) <= max_size.
proof.
  proc; conseq />.
  sp 1 1; if => //.
  if => //; wp; last by auto => /> /#.
  seq 1 1: (#pre /\ ={tdo} /\ ((tdo<> None) => (oget tdo).`td_trust = tk.`tk_trust){1}).
  + by conseq />; ecall (unwrap_qs (tk{2})).
  inline OpPolSrv(OA).quorumAssumption;sp 2 2.
  if => //; last by auto => /> /#.
  seq 1 1: (#[/:-12,-11:]pre /\ 
   card (fdom RealTrustService.parentTrust{2}) <= KMSProcedures.count_tkmng{2} + 1 /\
   ={tdoo} /\ (tdoo <> None => (oget tdoo).`td_trust = new_trust){2}). conseq />.
  + inline *. wp;skip => /> *; smt ( fdom_set fcardUI fcard_ge0 fcard1 cardE get_setE).
  if => //; last by auto=> /#.
  wp; ecall (wrap_qs (KMSProcedures.count_tkmng{2} + KMSProcedures.count_hid{2})).
  skip; smt (cardE).
qed.

local equiv addTokenKey_E : 
   KMSProcedures2(HSMsP_TS(IdealTrustService(OpPolSrv(OA))), HstSP_TS(IdealTrustService(OpPolSrv(OA))), 
     IdealTrustService(OpPolSrv(OA)), OpPolSrv(OA)).addTokenKey ~ 
   KMSProcedures2(HSMsP_TS(IdealTrustService(OpPolSrv(OA))), HstSP_TS(IdealTrustService(OpPolSrv(OA))), 
     IdealTrustService(OpPolSrv(OA)), OpPolSrv(OA)).addTokenKey :
 ={hid, hdl, tk} /\
  (={KMSProcedures.count_test, KMSProcedures.count_corr,
             KMSProcedures.count_unw, KMSProcedures.count_tkmng,
             KMSProcedures.count_hid, 
             KMSProcedures.count_updt, KMSProcedures.count_hst,
             KMSProcedures.count_auth, KMSProcedures.count_ops,
             KMSProcedures.b, KMSProcedures.handles, KMSProcedures.tested,
             KMSProcedures.corrupted, KMSProcedures.tklist,
             KMSProcedures.hids, OpPolSrv.badOps, OpPolSrv.goodOps,
             OpPolSrv.genuine} /\
         ={HSMs.benc_keys, HSMs.who} /\
         ={glob OA} /\
         ={RealTrustService.parentTrust, RealTrustService.protectedTrusts,
             RealTrustService.count_chk, RealTrustService.count_updt,
             RealTrustService.count_hst, RealTrustService.count_auth,
             RealTrustService.count_ops, HstPolSrv.hosts_tr, OpPolSrv.badOps,
             OpPolSrv.goodOps, OpPolSrv.genuine} /\
         ={OpPolSrv.badOps, OpPolSrv.goodOps, OpPolSrv.genuine} /\
         ={HstPolSrv.hosts_tr, RealSigServ.count_sig, RealSigServ.qs,
             RealSigServ.pks_sks}) /\
  RealSigServ.count_pk{1} <= KMSProcedures.count_hid{1} /\
  card RealSigServ.qs{2} <= KMSProcedures.count_tkmng{2} + KMSProcedures.count_hid{2} /\
  KMSProcedures.count_hid{2} <= q_hid /\
  RC.Count.c{2} <= KMSProcedures.count_hid{2} /\
  card (fdom RealTrustService.parentTrust{2}) <= KMSProcedures.count_tkmng{2} <= q_tkmng /\
  (forall (t k : Trust), RealTrustService.parentTrust{2}.[k] = Some t => card (tr_mems t) <= max_size) /\
  forall (q : pkey * message * signature),
    q \in RealSigServ.qs{2} =>
    recover_token q.`2 q.`3 <> None => card (tr_mems (oget (recover_token q.`2 q.`3)).`tk_trust) <= max_size

  ==>

  ={res} /\
  (={KMSProcedures.count_test, KMSProcedures.count_corr,
             KMSProcedures.count_unw, KMSProcedures.count_tkmng,
             KMSProcedures.count_hid, 
             KMSProcedures.count_updt, KMSProcedures.count_hst,
             KMSProcedures.count_auth, KMSProcedures.count_ops,
             KMSProcedures.b, KMSProcedures.handles, KMSProcedures.tested,
             KMSProcedures.corrupted, KMSProcedures.tklist,
             KMSProcedures.hids, OpPolSrv.badOps, OpPolSrv.goodOps,
             OpPolSrv.genuine} /\
         ={HSMs.benc_keys, HSMs.who} /\
         ={glob OA} /\
         ={RealTrustService.parentTrust, RealTrustService.protectedTrusts,
             RealTrustService.count_chk, RealTrustService.count_updt,
             RealTrustService.count_hst, RealTrustService.count_auth,
             RealTrustService.count_ops, HstPolSrv.hosts_tr, OpPolSrv.badOps,
             OpPolSrv.goodOps, OpPolSrv.genuine} /\
         ={OpPolSrv.badOps, OpPolSrv.goodOps, OpPolSrv.genuine} /\
         ={HstPolSrv.hosts_tr, RealSigServ.count_sig, RealSigServ.qs,
             RealSigServ.pks_sks}) /\
  RealSigServ.count_pk{1} <= KMSProcedures.count_hid{1} /\
  card RealSigServ.qs{2} <= KMSProcedures.count_tkmng{2} + KMSProcedures.count_hid{2} /\
  KMSProcedures.count_hid{2} <= q_hid /\
  RC.Count.c{2} <= KMSProcedures.count_hid{2} /\
  card (fdom RealTrustService.parentTrust{2}) <= KMSProcedures.count_tkmng{2} <= q_tkmng /\
  (forall (t k : Trust), RealTrustService.parentTrust{2}.[k] = Some t => card (tr_mems t) <= max_size) /\
  forall (q : pkey * message * signature),
    q \in RealSigServ.qs{2} =>
    recover_token q.`2 q.`3 <> None => card (tr_mems (oget (recover_token q.`2 q.`3)).`tk_trust) <= max_size.
proof.
  proc; sp 1 1; conseq />.
  seq 1 1 : (#pre /\ ={key}); 1: by sim />.
  if => //;wp; if=> //; last by auto => /#.
  inline *; sp 15 15.
  if; 1: done; last by rcondf{1} 2; 2: rcondf{2} 2; auto => /> /#.
  sp 5 5.
  if; 1: done; last by rcondf{1} 2; 2: rcondf{2} 2; auto => /> /#.
  sp 2 2. if => //. sp 2 2. if => [//||];last by auto => /> /#.
  wp; sp. seq 1 1 : (#pre /\ ={ekeys}); 1: by sim />.
  sp; if => [//||]; last by auto => /> /#.
  sp; if => [//||]; last by auto => /> /#.
  auto => /> &1 &2; rewrite /add_key /=.
  case : (hdl{2} \in decode_ptxt _) => //=.
  rewrite /= => *; split; 1: smt (fcardUI fcard_ge0 fcard1).
  split; 1: smt().
  move=> q;rewrite in_fsetU in_fset1 => -[/H6 // | -> /=].
  have /= -> /= := 
    encode_recover {| tk_updt = false; tk_trust = tk{2}.`tk_trust; 
       tk_wdata = {| tkw_ekeys = ekeys{2}; tkw_signer = hid{2}; tkw_sig = s0L; |}; |}.
  smt (cardE). 
qed.

axiom A_ll (O<:KMSOracles) :
  islossless O.newOp =>
  islossless O.requestAuthorization =>
  islossless O.newHst =>
  islossless O.installInitialTrust =>
  islossless O.installUpdatedTrust =>
  islossless O.newHSM =>
  islossless O.newToken =>
  islossless O.updateTokenTrust =>
  islossless O.addTokenKey =>
  islossless O.unwrap =>
  islossless O.corrupt =>
  islossless O.test =>
  islossless A(O).guess.

axiom init_ll : islossless OA.init.
axiom newOp_ll : islossless OA.newOp.
axiom request_ll : islossless OA.requestAuthorization.
axiom isGood_ll : islossless OA.isGoodInitialTrust.

hint solve 1 random : init_ll newOp_ll request_ll newHst_ll isGood_ll dbool_ll. 

lemma uptobad &m : 
   `| Pr [  KMSRoR2(A,OpPolSrv(OA)).main() @ &m : res ] - Pr [  KMSRoR3pre(A,OA).main() @ &m : res ] | <=
      q_hid%r * ((q_hid + q_tkmng + q_tkmng) * max_size)%r / n_keygen%r.
proof.
  have /= := RC.pr_abs AS _ &m (fun b _ _ => b).
  + move=> O hO; proc; islossless.
    apply (A_ll (<: AS(O).KMSProc));islossless. 
  have -> : Pr[KMSRoR2(A, OpPolSrv(OA)).main() @ &m : res] = Pr[REA.Main(REA.R(RE.Sample), RC.CountA(AS)).main() @ &m : res]. 
  + byequiv => //.
    proc;inline *;wp.
    call (_ : ={glob KMSProcedures, glob HSMs, glob OA, 
                glob RealTrustService, glob OpPolSrv, glob  HstPolSrv, RealSigServ.count_sig, RealSigServ.qs, RealSigServ.pks_sks} /\
         (RealSigServ.count_pk <= KMSProcedures.count_hid){1} /\
         (card RealSigServ.qs <= KMSProcedures.count_tkmng + KMSProcedures.count_hid /\
          KMSProcedures.count_hid <= q_hid /\
          RC.Count.c <= KMSProcedures.count_hid /\
          card (fdom  RealTrustService.parentTrust) <= KMSProcedures.count_tkmng <= q_tkmng /\
          (forall t k, RealTrustService.parentTrust.[k] = Some t => card (tr_mems t) <= max_size) /\
          (forall q, q \in RealSigServ.qs => 
             let tk = recover_token q.`2 q.`3 in tk <> None => 
             card (tr_mems (oget tk).`tk_trust) <= max_size)){2});
      try (by sim />); last first.
    + auto; call (_: true);auto => /> *.
      rewrite fdom0 !fcards0; smt (in_fset0 ge0_q_hid emptyE more_bounds).

    + conseq />; proc.
      sp 1 1; if => //.
      inline{1} HSMsP_TS(IdealTrustService(OpPolSrv(OA))).gen.
      seq 1 1: (#pre /\ ={sk,pk}); 1: by auto.
      wp; call(_: ={OpPolSrv.genuine}); 1: by auto.
      if => //; 1: by auto => /> /#.
      inline{2} RC.Count(REA.R(RE.Sample)).sample REA.R(RE.Sample).sample RE.Sample.sample.
      rcondt{2} 4; 1: by move=> &m1; auto => /#.
      rcondt{2} 7. move=> &m1; auto => /> *. apply size_nosigpks_elems => // /#.
      inline{1} IdealSigServ.keygen.
      rcondt{1} 2; 1: by auto; smt (q_hid_q_gen).
      rewrite /=.
      swap{1} 4 4. swap{2} 12 6.
      seq 2 12 : (#pre /\ (pk0,sk0){1} = (pksig,sksig){2}); 1: by auto.
      seq 4 4 : (#[/3:-2]pre /\ ={hid}); 1: by sim />. 
      wp;skip => /> /#.
    + by conseq newToken_E.
    + by conseq updateTokenTrust_E.
    by conseq addTokenKey_E.

  have -> /# : Pr[KMSRoR3pre(A, OA).main() @ &m : res] = 
     Pr[REA.Main(REA.R(RE.SampleE), RC.CountA(AS)).main() @ &m : res].
  byequiv => //.
  proc;inline *;wp.
  call (_ : ={glob KMSProcedures, glob HSMs, glob OA, 
                glob RealTrustService, glob OpPolSrv, glob  HstPolSrv, RealSigServ.count_sig, RealSigServ.qs, RealSigServ.pks_sks } /\
         (RealSigServ.count_pk <= KMSProcedures.count_hid){1} /\
         (card RealSigServ.qs <= KMSProcedures.count_tkmng + KMSProcedures.count_hid /\
          KMSProcedures.count_hid <= q_hid /\
          RC.Count.c <= KMSProcedures.count_hid /\
          card (fdom  RealTrustService.parentTrust) <= KMSProcedures.count_tkmng <= q_tkmng /\
          (forall t k, RealTrustService.parentTrust.[k] = Some t => card (tr_mems t)<= max_size) /\
          (forall q, q \in RealSigServ.qs => 
             let tk = recover_token q.`2 q.`3 in tk <> None => 
             card (tr_mems (oget tk).`tk_trust) <= max_size)){2});
    (try by sim />); last first.
  + auto; call (_: true);auto => /> *.
    rewrite fdom0 !fcards0; smt (in_fset0 ge0_q_hid emptyE more_bounds).
  + proc. conseq />.
    sp 1 1; if => //.
    rcondt{1} 1.
    + by move=> *;skip;smt (size_nosigpks_elems).
    seq 1 1 : (#pre /\ ={sk,pk}); 1: by auto.
    wp; call(_: ={OpPolSrv.genuine}); 1: by auto.
    if => //; 1: by auto => /> /#.
    inline{2} RC.Count(REA.R(RE.SampleE)).sample REA.R(RE.SampleE).sample RE.SampleE.sample.
    rcondt{2} 4; 1: by move=> &m1; auto => /#.
    rcondt{2} 7. move=> &m1; auto => /> *;apply size_nosigpks_elems => // /#.
    swap{2} 11 1; seq 1 11 : (#pre /\ ={pksig, sksig}).
    + conseq />; sp; wp; rnd; skip.
      by move=> /> &m1 &m2 *; rewrite nosigpks_nosigpks_elems'.
    swap{2} 1 5.
    seq 5 5 : (#[/3:-4]pre /\ ={hid}); 1: by sim />. 
    by wp;skip => /> /#.
  + by conseq newToken_E.
  + by conseq updateTokenTrust_E.
  by conseq addTokenKey_E.
qed.

axiom op_oa : exists (isGoodInitialTrust : (glob OA) -> Trust -> bool),
              forall (gOA : glob OA) (t : Trust),
                phoare [ OA.isGoodInitialTrust : t = trust /\ (glob OA) = gOA ==> res = isGoodInitialTrust gOA t /\ (glob OA) = gOA ] = 1%r.

lemma concl3 &m: 
  Pr[ KMSRoR(A,HSMs(RealSigServ),HstS(OpPolSrv(OA)),OpPolSrv(OA)).main() @ &m : res ] <= 
  Pr[ KMSRoR3pre(A, OA).main() @ &m : res ] + 
  q_hid%r * ((q_hid + q_tkmng + q_tkmng) * max_size)%r / n_keygen%r + 
  Pr[ TrustSecInd(Bhop2(A),RealTrustService(OpPolSrv(OA))).main() @ &m : res ] - Pr[ TrustSecInd(Bhop2(A),IdealTrustService(OpPolSrv(OA))).main() @ &m : res] +
  Pr[ IndSig(RealSigServ, Bhop1(A, OA)).main() @ &m : res ] - Pr[ IndSig(IdealSigServ, Bhop1(A, OA)).main() @ &m : res ].
proof.
rewrite (concl2 OA A op_oa &m).
have /#:=  uptobad &m.
qed.
      
end section PR2_PR3.

end Hop3.
